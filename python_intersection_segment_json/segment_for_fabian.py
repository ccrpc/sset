from posixpath import split
from telnetlib import WILL
import openpyxl
import json

workbook = openpyxl.load_workbook(filename="bctool_all (3).xlsx")
worksheet = workbook["Module02a"]


output_dictionary = {
    "Countermeasure code and Name": None,
    "HSM Applicability Flag": None,  # -- 41
    "AADT Major Min": None,  # -- 44
    "AADT Major Max": None,  # -- 45
}

final_output_array = []


def string_for_code(row_number, column_number):
    cell_value = worksheet.cell(row=row_number, column=column_number).value
    split_string = cell_value.split("-")
    return split_string[0].strip()


def string_for_countermeasure(row_number, column_number):
    cell_value = worksheet.cell(row=row_number, column=column_number).value
    split_string = cell_value.split("-")

    if len(split_string) == 3:
        return split_string[2].strip()
    elif len(split_string) == 2:
        # Countermeasure on row 618 is missing "Pavement" category so split_string[1] is the name.
        return split_string[1].strip()
    else:
        # Len > 3
        return "-".join(split_string[2:]).strip()


for i in range(823, 1019):
    if i == 873 or i == 892 or i == 908 or i == 924 or i == 939 or i == 955 or i == 981:
        continue
    row_number = i
    counter_measure_string = string_for_countermeasure(row_number, column_number=2)
    code_string = string_for_code(row_number, column_number=2)

    output_dictionary["Countermeasure code and Name"] = code_string + " " + counter_measure_string
    output_dictionary["HSM Applicability Flag"] = worksheet.cell(row=row_number, column=41).value
    output_dictionary["AADT Major Min"] = worksheet.cell(row=row_number, column=44).value
    output_dictionary["AADT Major Max"] = worksheet.cell(row=row_number, column=45).value

    final_output_array.append(output_dictionary.copy())


json_string = json.dumps(final_output_array, indent=2)

with open("output/segment_fabian.json", "w") as output_file:
    output_file.write(json_string)

print("Yes")
