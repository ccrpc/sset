from posixpath import split
from telnetlib import WILL
import openpyxl
import json

workbook = openpyxl.load_workbook(filename="bctool_all (3).xlsx")
worksheet = workbook["Module02a"]

output_dictionary = {
    "Unit": None,  # row-- 4
    "Service Life": None,  # row-- 5
    "CMF": None,  # row-- 6
    "All": None,  # row-- 8 if string --> 6
    "Angle": None,  # row-- 9 if string --> 6
    "Animal": None,  # row--10 if string -->6
    "Fixed Object": None,  # row-- 11 if string -->6
    "Front to Front": None,  # row-- 12 if string -->6
    "Left Turn": None,  # row-- 13 if string -->6
    "Other Non-Collision": None,  # row--14 if string -->6
    "Other Object": None,  # row-- 15 if string -->6
    "Overturned": None,  # row-- 16 if string -->6
    "Pedestrian": None,  # row-- 17 if string -->6
    "Pedalcyclist": None,  # row-- 18 if string -->6
    "Parked Motor Vehicle": None,  # row-- 19 if string -->6
    "Read End": None,  # row-- 20 if string -->6
    "Right Turn": None,  # row-- 21 if string -->6
    "Sideswipe Same Direction": None,  # row-- 22 if string -->6
    "Sideswipe Opposite Direction": None,  # row-- 23 if string -->6
    "Turning": None,  # row-- 24 if string -->6
    "Train": None,  # row--  25 if string -->6
    "NGT": None,  # row-- 26 if empty --> 1 if string--> "put manually_____________________________"
    "Run off Road": None,  # row-- 27
    "WP": None,  # row-- 28 if string -->6 if empty -->1
    "Setting Type": None,  # row-- 30
    "Facility Type": None,  # row-- 31
    "Code": None,  # row-- trim row ___2___
    "Category": None,  # row-- row-- trim row ___2___
    "Countermeasure": None,  # row-- trim row-- trim row ___2___
    "Affected Crash Types": [],  # put everyuthing in from row 7 into Affected_Crash_Types=[] then assign
}


count = 0
final_output_array = []
list_of_name_of_the_countermeasure_of_the_intersections = []  # this is to send to Tyler.


def helper_function_row_6(row_number, column_number):
    cell_value = worksheet.cell(row=row_number, column=column_number).value
    if type(cell_value) == float or type(cell_value) == int:
        return cell_value
    if cell_value == None:
        return 1.0
    if type(cell_value) == str:
        return worksheet.cell(row=row_number, column=6).value


def helper_for_night_time_run_off_road(row_number, column_number):
    cell_value = worksheet.cell(row=row_number, column=column_number).value
    if type(cell_value) == float or type(cell_value) == int:
        return cell_value
    if cell_value == None:
        return 1.0
    if type(cell_value) == str:
        return "_____________________________put manually_____________________________"


def string_for_code(row_number, column_number):
    cell_value = worksheet.cell(row=row_number, column=column_number).value
    split_string = cell_value.split("-")
    return split_string[0].strip()


def string_for_catregory(row_number, column_number):
    # Countermeasure on row 618 is missing "Pavement" category so return that manually.
    if row_number == 618:
        return "Pavement"
    cell_value = worksheet.cell(row=row_number, column=column_number).value
    split_string = cell_value.split("-")
    return split_string[1].strip()


def string_for_countermeasure(row_number, column_number):
    cell_value = worksheet.cell(row=row_number, column=column_number).value
    split_string = cell_value.split("-")

    if len(split_string) == 3:
        return split_string[2].strip()
    elif len(split_string) == 2:
        # Countermeasure on row 618 is missing "Pavement" category so split_string[1] is the name.
        return split_string[1].strip()
    else:
        # Len > 3
        return "-".join(split_string[2:]).strip()


for i in range(535, 813):
    if i == 542 or i == 665 or i == 727 or i == 805:
        continue
    row_number = i
    output_dictionary["Unit"] = worksheet.cell(row=row_number, column=4).value
    output_dictionary["Service Life"] = worksheet.cell(row=row_number, column=5).value

    wkl = worksheet.cell(row=row_number, column=6).value

    if type(wkl) == str:
        output_dictionary["CMF"] = "Calculated"
        output_dictionary["All"] = "Calculated"
        output_dictionary["Angle"] = "Calculated"
        output_dictionary["Animal"] = "Calculated"
        output_dictionary["Fixed Object"] = "Calculated"
        output_dictionary["Front to Front"] = "Calculated"
        output_dictionary["Left Turn"] = "Calculated"
        output_dictionary["Other Non-Collision"] = "Calculated"
        output_dictionary["Other Object"] = "Calculated"
        output_dictionary["Overturned"] = "Calculated"
        output_dictionary["Pedestrian"] = "Calculated"
        output_dictionary["Pedalcyclist"] = "Calculated"
        output_dictionary["Parked Motor Vehicle"] = "Calculated"
        output_dictionary["Read End"] = "Calculated"
        output_dictionary["Right Turn"] = "Calculated"
        output_dictionary["Sideswipe Same Direction"] = "Calculated"
        output_dictionary["Sideswipe Opposite Direction"] = "Calculated"
        output_dictionary["Turning"] = "Calculated"
        output_dictionary["Train"] = "Calculated"
        output_dictionary["NGT"] = "Calculated"
        output_dictionary["Run off Road"] = "Calculated"
        output_dictionary["WP"] = "Calculated"  # Wet pavement is always empty for intersections.

    else:
        output_dictionary["CMF"] = wkl
        output_dictionary["All"] = row_6 = helper_function_row_6(row_number, column_number=8)
        output_dictionary["Angle"] = row_6 = helper_function_row_6(row_number, column_number=9)
        output_dictionary["Animal"] = row_6 = helper_function_row_6(row_number, column_number=10)
        output_dictionary["Fixed Object"] = row_6 = helper_function_row_6(row_number, column_number=11)
        output_dictionary["Front to Front"] = row_6 = helper_function_row_6(row_number, column_number=12)
        output_dictionary["Left Turn"] = row_6 = helper_function_row_6(row_number, column_number=13)
        output_dictionary["Other Non-Collision"] = row_6 = helper_function_row_6(row_number, column_number=14)
        output_dictionary["Other Object"] = row_6 = helper_function_row_6(row_number, column_number=15)
        output_dictionary["Overturned"] = row_6 = helper_function_row_6(row_number, column_number=16)
        output_dictionary["Pedestrian"] = row_6 = helper_function_row_6(row_number, column_number=17)
        output_dictionary["Pedalcyclist"] = row_6 = helper_function_row_6(row_number, column_number=18)
        output_dictionary["Parked Motor Vehicle"] = row_6 = helper_function_row_6(row_number, column_number=19)
        output_dictionary["Read End"] = row_6 = helper_function_row_6(row_number, column_number=20)
        output_dictionary["Right Turn"] = row_6 = helper_function_row_6(row_number, column_number=21)
        output_dictionary["Sideswipe Same Direction"] = row_6 = helper_function_row_6(row_number, column_number=22)
        output_dictionary["Sideswipe Opposite Direction"] = row_6 = helper_function_row_6(row_number, column_number=23)
        output_dictionary["Turning"] = row_6 = helper_function_row_6(row_number, column_number=24)
        output_dictionary["Train"] = row_6 = helper_function_row_6(row_number, column_number=25)
        output_dictionary["NGT"] = night_time = helper_for_night_time_run_off_road(row_number, column_number=26)
        output_dictionary["Run off Road"] = runoff_road = helper_for_night_time_run_off_road(
            row_number, column_number=27
        )
        output_dictionary["WP"] = 1  # Wet pavement is always empty for intersections.

    # output_dictionary["All"]= row_6=helper_function_row_6(row_number,column_number=8)
    # output_dictionary["Angle"]=row_6=helper_function_row_6(row_number,column_number=9)
    # output_dictionary["Animal"]=row_6=helper_function_row_6(row_number,column_number=10)
    # output_dictionary["Fixed Object"]=row_6=helper_function_row_6(row_number,column_number=11)
    # output_dictionary["Front to Front"]=row_6=helper_function_row_6(row_number,column_number=12)
    # output_dictionary["Left Turn"]=row_6=helper_function_row_6(row_number,column_number=13)
    # output_dictionary["Other Non-Collision"]=row_6=helper_function_row_6(row_number,column_number=14)
    # output_dictionary["Other Object"]=row_6=helper_function_row_6(row_number,column_number=15)
    # output_dictionary["Overturned"]=row_6=helper_function_row_6(row_number,column_number=16)
    # output_dictionary["Pedestrian"]=row_6=helper_function_row_6(row_number,column_number=17)
    # output_dictionary["Pedalcyclist"]=row_6=helper_function_row_6(row_number,column_number=18)
    # output_dictionary["Parked Motor Vehicle"]=row_6=helper_function_row_6(row_number,column_number=19)
    # output_dictionary["Read End"]=row_6=helper_function_row_6(row_number,column_number=20)
    # output_dictionary["Right Turn"]=row_6=helper_function_row_6(row_number,column_number=21)
    # output_dictionary["Sideswipe Same Direction"]=row_6=helper_function_row_6(row_number,column_number=22)
    # output_dictionary["Sideswipe Opposite Direction"]=row_6=helper_function_row_6(row_number,column_number=23)
    # output_dictionary["Turning"]=row_6=helper_function_row_6(row_number,column_number=24)
    # output_dictionary["Train"]=row_6=helper_function_row_6(row_number,column_number=25)
    # output_dictionary["NGT"]=night_time=helper_for_night_time_run_off_road(row_number,column_number=26)
    # output_dictionary["Run off Road"]=runoff_road=helper_for_night_time_run_off_road(row_number,column_number=27)
    # output_dictionary["WP"]=1 # Wet pavement is always empty for intersections.

    output_dictionary["Setting Type"] = worksheet.cell(row=row_number, column=30).value
    output_dictionary["Facility Type"] = worksheet.cell(row=row_number, column=31).value
    output_dictionary["Code"] = code_string = string_for_code(row_number, column_number=2)
    output_dictionary["Category"] = category_string = string_for_catregory(row_number, column_number=2)
    output_dictionary["Countermeasure"] = counter_measure_string = string_for_countermeasure(
        row_number, column_number=2
    )
    output_dictionary["Affected Crash Types"] = worksheet.cell(row=row_number, column=7).value.split(",")

    final_output_array.append(output_dictionary.copy())

    # if type(wkl)==str:
    #     list_of_name_of_the_countermeasure_of_the_intersections.append(code_string+"-"+counter_measure_string)


json_string = json.dumps(final_output_array, indent=2)

with open("output/output.json", "w") as output_file:
    output_file.write(json_string)


# json_string_1=json.dumps(list_of_name_of_the_countermeasure_of_the_intersections, indent=1)
# with open("output_1.json", "w") as output_file:
#     output_file.write(json_string_1)
