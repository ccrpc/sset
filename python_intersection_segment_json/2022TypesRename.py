import json

renameRules = [["Train", "Railway Train"], ["Rear End", "Front to Rear"], ["Head On", "Front to Front"]]
affectedRenameRules = [["HO", "FTF"], ["RE", "FTR"]]

with open("/opt/code/sset/frontend/src/BCLIb/assets/cmf_data/countermeasure_data.json", "r") as file:
    countermeasures = json.load(file)

for countermeasure in countermeasures:
    for renameFrom, renameTo in renameRules:
        if renameFrom in countermeasure:
            countermeasure[renameTo] = countermeasure.pop(renameFrom)
    for renameFrom, renameTo in affectedRenameRules:
        if renameFrom in countermeasure["Affected Crash Types"]:
            countermeasure["Affected Crash Types"].remove(renameFrom)
            countermeasure["Affected Crash Types"].append(renameTo)

with open("output/renamedCountermeasures.json", "w") as file:
    json.dump(countermeasures, file, indent=2)
