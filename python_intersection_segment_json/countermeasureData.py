from typing import List
import openpyxl
import json

output_dictionary = {
    "Unit": None,  # row 4
    "Service Life": None,  # row 5
    "Affected Crash Types": [],  # row 7
    "CMF": None,  # row 6
    "Setting Type": None,  # row 30
    "Facility Type": None,  # row 31
    "Code": None,  # row trim row ___2___
    "Category": None,  # row row trim row ___2___
    "Countermeasure": None,  # row trim row trim row ___2___
}

cmfLocationDictionary = {
    "CMF": 6,
    "All": 8,
    "Angle": 9,
    "Animal": 10,
    "Fixed Object": 11,
    "Front to Front": 12,
    "Left Turn": 13,
    "Other Non-Collision": 14,
    "Other Object": 15,
    "Overturned": 16,
    "Pedestrian": 17,
    "Pedalcyclist": 18,
    "Parked Motor Vehicle": 19,
    "Front to Rear": 20,
    "Right Turn": 21,
    "Sideswipe Same Direction": 22,
    "Sideswipe Opposite Direction": 23,
    "Turning": 24,
    "Railway Train": 25,
    "Night Time": 26,
    "Run off Road": 27,
    "Wet Pavement": 28,
}


def cmfValue(row: int, column: int) -> float:
    cmf = worksheet.cell(row, column).value
    if type(cmf) == float or type(cmf) == int:
        return cmf
    elif cmf == None:
        return 1.0
    elif type(cmf) == str and cmf.startswith("="):
        return "Calculated"
    else:
        return cmf


def splitString(row: int, column: int, part: str) -> str:
    fullString = worksheet.cell(row, column).value
    stringParts = fullString.split("-")

    if part == "code":
        return stringParts[0].strip()
    elif part == "category":
        if row == 618:
            return "Pavement"
        else:
            return stringParts[1].strip()
    elif part == "cmf":
        length = len(stringParts)
        if length == 3:
            return stringParts[2].strip()
        elif length == 2:
            return stringParts[1].strip()
        elif length > 3:
            return "-".join(stringParts[2:]).strip()

    return ""


def affectedTypes(row: int, column: int) -> List[str]:
    affected: List[str] = worksheet.cell(row, column).value.split(",")
    result: List[str] = []
    for type in affected:
        result.append(type.strip())

    return result


def cmfInformation(row: int) -> dict:
    result = {}
    result["Unit"] = worksheet.cell(row, 4).value
    result["Service Life"] = worksheet.cell(row, 5).value

    for (
        name,
        column,
    ) in cmfLocationDictionary.items():
        result[name] = cmfValue(row, column)

    result["Affected Crash Types"] = affectedTypes(row, 7)

    result["Code"] = splitString(row, 2, "code")
    result["Category"] = splitString(row, 2, "category")
    result["Countermeasure"] = splitString(row, 2, "cmf")

    result["Setting Type"] = worksheet.cell(row, 30).value
    result["Facility Type"] = worksheet.cell(row, 31).value
    result["Project Type"] = worksheet.cell(row, 50).value
    result["Project System"] = worksheet.cell(row, 51).value

    return result


if __name__ == "__main__":
    print("Loading Workbook...")
    workbook = openpyxl.load_workbook(filename="bctool_all (3).xlsx")
    worksheet = workbook["Module02a"]
    print("Workbook Loaded")

    output = []

    print("Reading Intersection countermeasure data...")
    for row in range(535, 813):
        if row == 542 or row == 665 or row == 727 or row == 805:
            continue
        output.append(cmfInformation(row))

    print("Reading Segment countermeasure data...")
    for row in range(823, 1019):
        if row == 873 or row == 892 or row == 908 or row == 924 or row == 939 or row == 955 or row == 981:
            continue
        output.append(cmfInformation(row))

    print("Countermeasure data read.")
    print("Creating file...")

    with open(
        "output/test_countermeasure_data.json",
        "w",
    ) as file:
        json.dump(output, file, indent=2)
    print("File created.")
