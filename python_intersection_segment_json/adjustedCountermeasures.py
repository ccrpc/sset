import openpyxl
import json

print("Loading Workbook...")
workbook = openpyxl.load_workbook(filename="bctool_all (3).xlsx")
worksheet = workbook["Module02d"]
print("Workbook Loaded")

cmfTagColumn = 3  # Column C
distributionTagColumn = 4  # D

cmfRows = range(18, 78)
distributionRows = range(83, 124)
cmfColumns = range(5, 26)
zeroValues = [None, "=0", "", "-"]


def iterate(rows, columns, tagColumn, zeroValues):
    output = {}
    for row in rows:
        tag = worksheet.cell(row=row, column=tagColumn).value
        for column in columns:
            cmfName = worksheet.cell(row=17, column=column).value
            cmfValue = worksheet.cell(row=row, column=column).value
            if cmfValue in zeroValues:
                cmfValue = 0

            if not tag in output:
                output[tag] = {}

            output[tag][cmfName] = cmfValue
    return output


print("Creating JSON...")
outputJson = {
    "inputCMF": iterate(cmfRows, cmfColumns, cmfTagColumn, zeroValues),
    "crashDistributions": iterate(distributionRows, cmfColumns, distributionTagColumn, zeroValues),
}
print("JSON Created")

print("Writing JSON to File...")
with open("output/adjustedCMF.json", "w") as file:
    json.dump(outputJson, file, indent=2)
print("JSON Written")
print("Done")
