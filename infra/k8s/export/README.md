## Creating the secret

The exporting of the map tiles requires a connection to the postgis database and therefore authentication.

Create the file postgis-secret.yaml in this repository.

NOTE: This file should not be checked into the git repository. The project's .gitignore should cause it to be ignored, but double check that is the case before making a commit.

Create a postgis-secret.yaml file and a registry-ccrpc-secret.yaml file, copy the contents of the example files to them, and then edit the files to replace 'replaceme' with the password / config.
Copy the contents of the postgis-secret.example.yaml into the new postgis-secret.yaml.

```yaml
POSTGRES_PASSWORD: replaceme
```

and

```yaml
data:
  .dockerconfigjson: replaceme
```

### Creating the files

To create the files and copy the examples to the newly created files, one can run
`cp /opt/code/sset/infra/k8s/export/postgis-secret.example.yaml /opt/code/sset/infra/k8s/export/postgis-secret.yaml && cp /opt/code/sset/infra/k8s/export/registry-ccrpc-secret.example.yaml /opt/code/sset/infra/k8s/export/registry-ccrpc-secret.yaml`

### base64 encode

Kubernetes secret data fields are base64 encoded. To encode something you can pipe it to the `base64` command which should be available by base on linux.

For example to encode the string 'hello', one would use `echo -n "hello" | base64`. This should give the output `aGVsbG8=`.

Note: Without the `-n` flag the `echo` command appends a newline to its output, this messes up the base64 encoding. For example the, very similar to above, command `echo -n "hello" | base64` returns `aGVsbG8K`.

While "aGVsbG8=" and "aGVsbG8K" are very similar, it will mess up a kubernetes secret and be incorrect.

### pcd_view password

The text you will want to replace "replaceme" with the is the password for pcd_view. This can be found in the password database.

If you do not wish to put it in the terminal via the `echo -n` method above one can also use xclip, which is a clipboard utility.

Copy the password from the database as you normally would, then run `xclip -o | base64 | xclip -sel clip`. Now when you control+paste you will have the base64 encoded version of the password (or whatever else you had ctrl+copied).

### registry-ccrpc

The text you will want to replace 'replaceme' with can be found in secrets in our kubernetes configurations. This is often located at /opt/kubernetes/rpc on the linux machines.

If they are not on your machine, refer to your onboarding document.
