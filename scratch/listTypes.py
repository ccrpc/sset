import json

with open("/opt/code/sset/frontend/src/BCLIb/assets/cmf_data/countermeasure_data.json", "r") as file:
    countermeasures = json.load(file)

print(list(set(list(map(lambda x: x["Setting Type"], countermeasures)))))

print(list(set(list(map(lambda x: x["Facility Type"], countermeasures)))))

print(list(set(list(map(lambda x: x["Unit"], countermeasures)))))

print(list(set(list(map(lambda x: x["Project Type"], countermeasures)))))

print(list(set(list(map(lambda x: x["Project System"], countermeasures)))))
