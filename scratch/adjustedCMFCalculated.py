countermeasures = [
    [
        "4.1.19.S8.1",
        "4C",
        "Install TWLTL (two-way left turn lane) on two lane road",
    ],
    [
        "4.1.19.S1.1",
        "4D",
        "Install TWLTL (two-way left turn lane) on two lane road",
    ],
    [
        "4.1.21.S8.1",
        "4E",
        "Install bicycle lanes",
    ],
    [
        "4.1.21.S12.1",
        "4E",
        "Install bicycle lanes",
    ],
    [
        "4.2.7.S12.1",
        "4F",
        "Convert an open median to a directional median",
    ],
    [
        "4.2.7.S12.2",
        "4G",
        "Convert an open median to a left-in only median",
    ],
]

output = []

for i in range(len(countermeasures)):
    template = (
        "{\n"
        "    // Macro case: None\n"
        f"    // Adjusted CMF Tag: {countermeasures[i][1]}\n"
        f"    name: '{countermeasures[i][2]}',"
        f'    code: "{countermeasures[i][0]}",'
        '    prompt: "Adjusted cmf determined by locality and peer group",'
        "    numberOfInputs: 2,"
        "    aadtMajorRequired: false,"
        "    aadtMinorRequired: false,"
        '    inputPrompts: ["Peer Group", "Locality"],'
        '    inputTypes: ["number", "string"],'
        "    calculate: (inputs: (string | number)[]): CmfVector => {"
        "      const distributionLabel = createDistributionTag("
        "        inputs[0] as number,"
        '        inputs[1] as "state" | "local",'
        "        RoadwayType.Intersection"
        "      );"
        f'      return adjustedCMFVector("{countermeasures[i][1]}", distributionLabel);'
        "    },"
        "    validate: ("
        "      inputs: (string | number)[],"
        "      aadtMajor: number,"
        "      aadtMinor: number"
        "    ) => {"
        "      validate({"
        f'        code: "{countermeasures[i][0]}",'
        "        correctNumberOfInputs: 2,"
        '        correctTypeArray: ["number", "string"],'
        "        aadtMajorRequired: false,"
        "        aadtMinorRequired: false,"
        "        providedInputs: inputs,"
        "        aadtMajor: aadtMajor,"
        "        aadtMinor: aadtMinor,"
        "      });"
        ""
        "      if ((inputs[0] as number) <= 0) {"
        '        throw new InvalidInputError("Peer group must be a positive integer");'
        "      }"
        '      if (inputs[1] !== "local" && inputs[1] !== "state") {'
        "        throw new InvalidInputError("
        "          \"Locality must be either 'local' or 'state'\""
        "        );"
        "      }"
        "    },"
        "  },"
    )
    output.append(template)

print("\n".join(output))
