'Filter and copy data to Module02c
Sheets("Module02a").Visible = True
Sheets("Module02a").Activate
       
'check if aggregate or crash type data is input. Only use commented out block for aggregated crash data.
        
'    Range("A4:AU400").Select
'    Selection.AutoFilter
'    ActiveSheet.Range("$A$4:$AU$400").AutoFilter Field:=30, Criteria1:="=All", _
'        Operator:=xlOr, Criteria2:=setting
'    ActiveSheet.Range("$A$4:$AU$400").AutoFilter Field:=31, Criteria1:="=All", _
'        Operator:=xlOr, Criteria2:=facility
'    ActiveSheet.Range("$A$4:$AU$400").AutoFilter Field:=7, Criteria1:="=All", _
'        Operator:=xlOr

   
    Range("$A$4:$BV$2000").Select                       'Range updated: TJS (12/20/2016)
    Selection.AutoFilter
    ActiveSheet.Range("$A$4:$BV$2000").AutoFilter Field:=30, Criteria1:="=All", _
            Operator:=xlOr, Criteria2:=setting          'Range updated: TJS (11/17/2016)
    ActiveSheet.Range("$A$4:$BV$2000").AutoFilter Field:=31, Criteria1:="=All", _
            Operator:=xlOr, Criteria2:=facility         'Range updated: TJS (11/17/2016)
   
'Added by TJS (12/20/2016)______________________________________________
'This added set of filters removes the necessity to update selection ranges when CMFs are added or removed

    ActiveSheet.Range("$A$4:$BV$2000").AutoFilter Field:=50, Criteria1:=projtype            'Range updated: TJS (11/17/2016)
    ActiveSheet.Range("$A$4:$BV$2000").AutoFilter Field:=51, Criteria1:=projsystem          'Range updated: TJS (11/17/2016)

'Added by TJS (12/20/2016)______________________________________________
'This added filter selects only CMFs applicable to the crash types which have a crash history for the analysis
'Only used when crash data is disaggregated by crash type

    ActiveSheet.Range("$A$4:$BV$2000").AutoFilter Field:=74, Criteria1:="=x"                'Range updated: TJS (11/17/2016)


    Range("A5:A2000").Select
    Selection.Copy
    Sheets("Module02c").Select
    Range("B5").Select
    ActiveSheet.Paste

'=====================================
'Previously used CMF selection ranges
'SEE "FilterCMF.A.2" FOR ARCHIVED CODE
'=====================================
    
'Count # of records to fix pulldown menus

    Range("B5").Select
    myrange = Range(Selection, Selection.End(xlDown)).Rows.Count
    Range("B2") = myrange
    
'x = 5
'ctr = 0
    'Do Until Cells(x, 3).Value = ""
     '   If Cells(x, 3).Value <> "" Then
      '  ctr = ctr + 1
       ' x = x + 1
        'End If
    'Loop
    
    'Range("D80").Value = ctr