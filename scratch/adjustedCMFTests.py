crashTypes = [
    "AG",
    "AN",
    "FO",
    "FTF",
    "LT",
    "OtherNC",
    "OtherO",
    "OVT",
    "PD",
    "PDC",
    "PKV",
    "FTR",
    "RT",
    "SSD",
    "SOD",
    "T",
    "TR",
    "NGT",
    "ROR",
    "WP",
]

countermeasures = [
    [
        "4.1.19.S8.1",
        "4C",
        [
            0.745047051442911,
            0.745047051442911,
            0.745047051442911,
            0.739,
            0.745047051442911,
            0.745047051442911,
            0.739,
            0.745047051442911,
            0.739,
            0.739,
            0.739,
            0.613,
            0.745047051442911,
            0.739,
            0.739,
            0.745047051442911,
            0.739,
        ],
    ],
    [
        "4.1.19.S1.1",
        "4D",
        [
            0.745047051442911,
            0.745047051442911,
            0.745047051442911,
            0.739,
            0.745047051442911,
            0.745047051442911,
            0.739,
            0.745047051442911,
            0.739,
            0.739,
            0.739,
            0.613,
            0.745047051442911,
            0.739,
            0.739,
            0.745047051442911,
            0.739,
        ],
    ],
    [
        "4.1.21.S8.1",
        "4E",
        [
            0.940785714285714,
            0.940785714285714,
            0.940785714285714,
            0.946,
            0.940785714285714,
            0.940785714285714,
            0.946,
            0.940785714285714,
            0.855,
            1.509,
            0.946,
            0.940785714285714,
            0.940785714285714,
            0.946,
            0.946,
            0.940785714285715,
            0.946,
        ],
    ],
    [
        "4.1.21.S12.1",
        "4E",
        [
            0.940785714285714,
            0.940785714285714,
            0.940785714285714,
            0.946,
            0.940785714285714,
            0.940785714285714,
            0.946,
            0.940785714285714,
            0.855,
            1.509,
            0.946,
            0.940785714285714,
            0.940785714285714,
            0.946,
            0.946,
            0.940785714285715,
            0.946,
        ],
    ],
    [
        "4.2.7.S12.1",
        "4F",
        [
            0.955976070528967,
            0.955976070528967,
            0.955976070528967,
            0.93,
            0.43,
            0.955976070528967,
            0.93,
            0.955976070528967,
            0.93,
            0.93,
            0.93,
            0.955976070528967,
            0.955976070528967,
            0.93,
            0.93,
            0.640390428211587,
            0.93,
        ],
    ],
    [
        "4.2.7.S12.2",
        "4G",
        [
            0.970780856423174,
            0.970780856423174,
            0.970780856423174,
            0.95,
            0.55,
            0.970780856423174,
            0.95,
            0.970780856423174,
            0.95,
            0.95,
            0.95,
            0.970780856423174,
            0.970780856423174,
            0.95,
            0.95,
            0.71831234256927,
            0.95,
        ],
    ],
]

output = []

for i in range(len(countermeasures)):
    cmfValues = []
    for k in range(len(crashTypes)):
        key = crashTypes[k]
        value = countermeasures[i][2][k] if k < len(countermeasures[i][2]) else 1
        if len(str(value)) >= 10:
            cmfValues.append(["CrashType.{}".format(key), "roundNumber({}, 9)".format(value)])
        else:
            cmfValues.append(["CrashType.{}".format(key), value])

    cmfString = str(cmfValues).replace("'", "")

    template = (
        "\n"
        "// _____________________________________________________________________________________________\n"
        "\n"
        f"// {countermeasures[i][0]}\n"
        "// Macro Case None\n"
        f"// Adjusted CMF Tag: {countermeasures[i][1]}\n"
        "\n"
        "testCounterMeasure({\n"
        f'  code: "{countermeasures[i][0]}",\n'
        f"  expectedCMF: new Map({cmfString}),\n"
        '  correctInputs: [1, "local"],'
        '  incorrectInputsDueToType: [["string", 2]],'
        "  incorrectInputsDueToLength: [3],"
        "  aadtMajorRequired: false,"
        "  aadtMinorRequired: false,"
        "  aadtMajor: undefined,"
        "  aadtMinor: undefined,"
        "  correctNumberOfInputs: 2,"
        '  correctTypeArray: ["number", "string"],'
        "  inputValidation: true,"
        "  incorrectInputsDueToValue: ["
        '    [0, "local"],'
        '    [5, "ducks"],'
        "  ],"
        "  expectedValueErrors: ["
        '    "Peer group must be a positive integer",'
        "    \"Locality must be either 'local' or 'state'\","
        "  ],"
        "});"
    )
    output.append(template)

print("\n".join(output))
