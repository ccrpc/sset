import json


def isCalculated(countermeasure):
    if (
        countermeasure["CMF"] == "Calculated"
        or countermeasure["All"] == "Calculated"
        or countermeasure["Angle"] == "Calculated"
        or countermeasure["Animal"] == "Calculated"
        or countermeasure["Fixed Object"] == "Calculated"
        or countermeasure["Front to Front"] == "Calculated"
        or countermeasure["Left Turn"] == "Calculated"
        or countermeasure["Other Non-Collision"] == "Calculated"
        or countermeasure["Other Object"] == "Calculated"
        or countermeasure["Overturned"] == "Calculated"
        or countermeasure["Pedestrian"] == "Calculated"
        or countermeasure["Pedalcyclist"] == "Calculated"
        or countermeasure["Parked Motor Vehicle"] == "Calculated"
        or countermeasure["Front to Rear"] == "Calculated"
        or countermeasure["Right Turn"] == "Calculated"
        or countermeasure["Sideswipe Same Direction"] == "Calculated"
        or countermeasure["Sideswipe Opposite Direction"] == "Calculated"
        or countermeasure["Turning"] == "Calculated"
        or countermeasure["Train"] == "Calculated"
        or countermeasure["Night Time"] == "Calculated"
        or countermeasure["Run off Road"] == "Calculated"
        or countermeasure["Wet Pavement"] == "Calculated"
    ):
        return True
    return False


alreadyCalculated = []

with open("/opt/code/sset/scratch/alreadyCalculated.json", "r") as file:
    alreadyCalculated = json.load(file)


def notCalculated(countermeasure):
    return countermeasure["Code"] not in alreadyCalculated


with open("/opt/code/sset/frontend/src/BCLIb/assets/cmf_data/countermeasure_data.json", "r") as file:
    countermeasures = json.load(file)

print("Number of countermeasures: {}".format(len(countermeasures)))

calculatedCountermeasures = list(filter(isCalculated, countermeasures))
print("Number of Calculated Countermeasures: {}".format(len(calculatedCountermeasures)))

print("Number of Calculated Countermeasures implemented: {}".format(len(alreadyCalculated)))

unimplementedCountermeasures = list(filter(notCalculated, calculatedCountermeasures))
print(
    "Number of Calculated Countermeasures that still need to be implemented: {}".format(
        len(unimplementedCountermeasures)
    )
)

print("Next 7 to implement")
print("\n".join(list(map(lambda x: x["Code"], unimplementedCountermeasures[:7]))))
