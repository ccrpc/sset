Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
'DPB 03/13/2013 added flags for user defined countermeasures
'TJS 11/28/2016 added selection for State vs Local systems
'***********************************************
'* Check for flags on CMF selection change     *
'***********************************************
Public IDCM1 As Integer
Public rowctr As Integer
Public CMFVal As Single
Public IDCM2 As Integer
Public IDCM3 As Integer
Public IDCM4 As Integer
Public answer As Integer
Public CMFlag As String
Public warning1 As String
Public warning2 As String
'fSpecialCase is a flag indicating the results of the special case function call (1=complete, 0=cancel)
Public fSpecialCase As Integer     'Added by TJS (12/19/2016)


'***********************************************
'* Intersections                               *
'***********************************************

Sub IntDropDown1_Change()
 'Updated by TJS (11/28/2016)


    If Sheets("BCTool").Range("A2").Value = 0 Then      'Added by TJS (11/28/2016)
    'State Intersections (1)
    
        unhide
        Sheets("Module05A").Select
        IDCM1 = Range("C15").Value
        IDCM2 = 0
        IDCM3 = 0
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments) 
        Sheets("Module02A").Select
        rowctr = 5
        Do
            If Cells(rowctr, 1).Value = IDCM1 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM1
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05A").Range("B15").Value = 1
            End If
        'add warning message to summary worksheet
            If answer = 1 Then

                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    'Sheets("SumIntx").Visible = True
                    Sheets("SumIntx").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then    'Added by TJS (12/13/2016)
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx").Visible = False
            End If
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05A").Range("B15").Value = 1
            End If
        'add warning message to summary worksheet
            If answer = 1 Then
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value

                    'Sheets("SumIntx").Visible = True
                    Sheets("SumIntx").Select
                    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then      'Added by TJS(12/13/2016)
                            Exit Do
                        End If
                        rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx").Visible = False
            End If
       End If
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05A").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMIntx").Range("P7").Value = "X" Or _
            Worksheets("CMIntx").Range("P9").Value = "X" Or _
            Worksheets("CMIntx").Range("P11").Value = "X" Or _
            Worksheets("CMIntx").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
                
                'Sheets("SumIntx").Visible = True
                Sheets("SumIntx").Select

                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumIntx").Visible = False
        End If
    '============================================================================================
       
       
        'Check Additional information Flag
        Sheets("Module02A").Select
        If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05A").Range("B15") = 1
            End If
        End If
       
        hide
        Sheets("CMIntx").Visible = True
        Sheets("CMIntx").Select



    Else        'Added by TJS (11/28/2016)
    'Local Intersections (1)
        unhide
        Sheets("Module05C").Select
        IDCM1 = Range("C15").Value
        IDCM2 = 0
        IDCM3 = 0
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 5
        Do
            If Cells(rowctr, 1).Value = IDCM1 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM1
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05C").Range("B15").Value = 1
            End If
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value

                    Sheets("SumIntx_Local").Visible = True
                    Sheets("SumIntx_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx_Local").Visible = False
            End If
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05C").Range("B15").Value = 1
            End If
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumIntx_Local").Visible = True
                    Sheets("SumIntx_Local").Select
                    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx_Local").Visible = False
            End If
       End If
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05C").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMIntx_Local").Range("P7").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P9").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P11").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P13").Value = "X" Then
            
                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"

                Sheets("SumIntx_Local").Visible = True
                Sheets("SumIntx_Local").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumIntx_Local").Visible = False
            End If
    '============================================================================================
       
       
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05C").Range("B15") = 1
            End If
       End If
       
       hide
       Sheets("CMIntx_Local").Visible = True
       Sheets("CMIntx_Local").Select

    End If

End Sub

Sub IntDropDown2_Change()
 'Updated by TJS (11/28/2016)


    If Sheets("BCTool").Range("A2").Value = 0 Then      'Added by TJS (11/28/2016)
    'State Intersections (2)
        unhide
        Sheets("Module05A").Select
        IDCM1 = 0
        IDCM2 = Range("C17").Value
        IDCM3 = 0
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 5
        Do
            If Cells(rowctr, 1).Value = IDCM2 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM2
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05A").Range("B17").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    'Sheets("SumIntx").Visible = True
                    Sheets("SumIntx").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx").Visible = False
            End If
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05A").Range("B17").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    'Sheets("SumIntx").Visible = True
                    Sheets("SumIntx").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05A").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMIntx").Range("P7").Value = "X" Or _
            Worksheets("CMIntx").Range("P9").Value = "X" Or _
            Worksheets("CMIntx").Range("P11").Value = "X" Or _
            Worksheets("CMIntx").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                'Sheets("SumIntx").Visible = True
                Sheets("SumIntx").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumIntx").Visible = False
            End If
    '============================================================================================
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05A").Range("B17") = 1
            End If
       End If
       hide
       Sheets("CMIntx").Visible = True
       Sheets("CMIntx").Select

    Else        'Added by TJS (11/28/2016)
    'Local Intersections (2)
    
        unhide
        Sheets("Module05C").Select
        IDCM1 = 0
        IDCM2 = Range("C17").Value
        IDCM3 = 0
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 5
        Do
            If Cells(rowctr, 1).Value = IDCM2 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM2
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05C").Range("B17").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumIntx_Local").Visible = True
                    Sheets("SumIntx_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx_Local").Visible = False
            End If
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05C").Range("B17").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumIntx_Local").Visible = True
                    Sheets("SumIntx_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx_Local").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05C").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMIntx_Local").Range("P7").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P9").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P11").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                Sheets("SumIntx_Local").Visible = True
                Sheets("SumIntx_Local").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumIntx_Local").Visible = False
            End If
    '============================================================================================
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05C").Range("B17") = 1
            End If
       End If
       hide
       Sheets("CMIntx_Local").Visible = True
       Sheets("CMIntx_Local").Select
    End If

End Sub

Sub IntDropDown3_Change()
 'Updated by TJS (11/28/2016)


    If Sheets("BCTool").Range("A2").Value = 0 Then      'Added by TJS (11/28/2016)
    'State Intersections (3)
    
        unhide
        Sheets("Module05A").Select
        IDCM1 = 0
        IDCM2 = 0
        IDCM3 = Range("C19").Value
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 5
        Do
            If Cells(rowctr, 1).Value = IDCM3 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM3
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05A").Range("B19").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    'Sheets("SumIntx").Visible = True
                    Sheets("SumIntx").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx").Visible = False
            End If
       End If
       
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05A").Range("B19").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    'Sheets("SumIntx").Visible = True
                    Sheets("SumIntx").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05A").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMIntx").Range("P7").Value = "X" Or _
            Worksheets("CMIntx").Range("P9").Value = "X" Or _
            Worksheets("CMIntx").Range("P11").Value = "X" Or _
            Worksheets("CMIntx").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"

                'Sheets("SumIntx").Visible = True
                Sheets("SumIntx").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumIntx").Visible = False
            End If
    '============================================================================================
       
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05A").Range("B19") = 1
            End If
       End If
       hide
       Sheets("CMIntx").Visible = True
       Sheets("CMIntx").Select


    Else        'Added by TJS (11/28/2016)
    'Local Intersection (3)
    
        unhide
        Sheets("Module05C").Select
        IDCM1 = 0
        IDCM2 = 0
        IDCM3 = Range("C19").Value
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 5
        Do
            If Cells(rowctr, 1).Value = IDCM3 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM3
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05C").Range("B19").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumIntx_Local").Visible = True
                    Sheets("SumIntx_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                        rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx_Local").Visible = False
            End If
       End If
       
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05C").Range("B19").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumIntx_Local").Visible = True
                    Sheets("SumIntx_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx_Local").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05C").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMIntx_Local").Range("P7").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P9").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P11").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"

                Sheets("SumIntx_Local").Visible = True
                Sheets("SumIntx_Local").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumIntx_Local").Visible = False
            End If
    '============================================================================================
       
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05C").Range("B19") = 1
            End If
       End If
       hide
       Sheets("CMIntx_Local").Visible = True
       Sheets("CMIntx_Local").Select
    End If



End Sub

Sub IntDropDown4_Change()
 'Updated by TJS (11/28/2016)


    If Sheets("BCTool").Range("A2").Value = 0 Then      'Added by TJS (11/28/2016)
    'State Intersections (4)
        
        unhide
        Sheets("Module05A").Select
        IDCM1 = 0
        IDCM2 = 0
        IDCM3 = 0
        IDCM4 = Range("C21").Value
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 6
        Do
            If Cells(rowctr, 1).Value = IDCM4 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM4
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05A").Range("B21").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    'Sheets("SumIntx").Visible = True
                    Sheets("SumIntx").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx").Visible = False
            End If
       End If
       
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05A").Range("B21").Value = 1
            End If
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    'Sheets("SumIntx").Visible = True
                    Sheets("SumIntx").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05A").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMIntx").Range("P7").Value = "X" Or _
            Worksheets("CMIntx").Range("P9").Value = "X" Or _
            Worksheets("CMIntx").Range("P11").Value = "X" Or _
            Worksheets("CMIntx").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                'Sheets("SumIntx").Visible = True
                Sheets("SumIntx").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumIntx").Visible = False
            End If
    '============================================================================================
    
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05A").Range("B21") = 1
            End If
       End If
       
       hide
       Sheets("CMIntx").Visible = True
       Sheets("CMIntx").Select
    
    
    Else        'Added by TJS (11/28/2016)
    'Local Intersections (4)
        
        unhide
        Sheets("Module05C").Select
        IDCM1 = 0
        IDCM2 = 0
        IDCM3 = 0
        IDCM4 = Range("C21").Value
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 6
        Do
            If Cells(rowctr, 1).Value = IDCM4 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM4
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05C").Range("B21").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumIntx_Local").Visible = True
                    Sheets("SumIntx_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx_Local").Visible = False
            End If
       End If
       
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05C").Range("B21").Value = 1
            End If
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumIntx_Local").Visible = True
                    Sheets("SumIntx_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumIntx_Local").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05C").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMIntx_Local").Range("P7").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P9").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P11").Value = "X" Or _
            Worksheets("CMIntx_Local").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                Sheets("SumIntx_Local").Visible = True
                Sheets("SumIntx_Local").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumIntx_Local").Visible = False
            End If
    '============================================================================================
    
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05C").Range("B21") = 1
            End If
       End If
       
       hide
       Sheets("CMIntx_Local").Visible = True
       Sheets("CMIntx_Local").Select
    End If


End Sub


'***********************************************
'* Segments                                    *
'***********************************************
Sub DropDown1_Change()
 'Updated by TJS (11/28/2016)

    
    If Sheets("BCTool").Range("A2").Value = 0 Then      'Added by TJS (11/28/2016)
    'State Segments (1)
    
        unhide
        Sheets("Module05B").Select
        IDCM1 = Range("C15").Value
        IDCM2 = 0
        IDCM3 = 0
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 131
        
        Do
            If Cells(rowctr, 1).Value = IDCM1 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM1
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05B").Range("B15").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm").Visible = True
                    Sheets("SumSegm").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
                    
                    'Sheets("SumSegm").Visible = False
            End If
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05B").Range("B15").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumSegm").Visible = True
                    Sheets("SumSegm").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05B").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMSegm").Range("P7").Value = "X" Or _
            Worksheets("CMSegm").Range("P9").Value = "X" Or _
            Worksheets("CMSegm").Range("P11").Value = "X" Or _
            Worksheets("CMSegm").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                Sheets("SumSegm").Visible = True
                Sheets("SumSegm").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumSegm").Visible = False
            End If
    '============================================================================================
    
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05B").Range("B15") = 1
            End If
       End If
       hide
       Sheets("CMSegm").Visible = True
       Sheets("CMSegm").Select


    Else        'Added by TJS (11/28/2016)
    'Local Segments (1)
    
        unhide
        Sheets("Module05D").Select
        IDCM1 = Range("C15").Value
        IDCM2 = 0
        IDCM3 = 0
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 131
        
        Do
            If Cells(rowctr, 1).Value = IDCM1 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM1
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05D").Range("B15").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm_Local").Visible = True
                    Sheets("SumSegm_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
                    
                    'Sheets("SumSegm_Local").Visible = False
            End If
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05D").Range("B15").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumSegm_Local").Visible = True
                    Sheets("SumSegm_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm_Local").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05D").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMSegm_Local").Range("P7").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P9").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P11").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                Sheets("SumSegm_Local").Visible = True
                Sheets("SumSegm_Local").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumSegm_Local").Visible = False
            End If
    '============================================================================================
    
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05D").Range("B15") = 1
            End If
       End If
       hide
       Sheets("CMSegm_Local").Visible = True
       Sheets("CMSegm_Local").Select
    End If

 End Sub
 
 Sub DropDown2_Change()
 'Updated by TJS (11/28/2016)
    
    
    If Sheets("BCTool").Range("A2").Value = 0 Then      'Added by TJS (11/28/2016)
    'State Segments (2)
    
        unhide
        Sheets("Module05B").Select
        IDCM1 = 0
        IDCM2 = Range("C17").Value
        IDCM3 = 0
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 131
        Do
            If Cells(rowctr, 1).Value = IDCM2 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM2
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
       
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05B").Range("B17").Value = 1
            End If
            If answer = 1 Then
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm").Visible = True
                    Sheets("SumSegm").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm").Visible = False
            End If
       
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05B").Range("B17").Value = 1
            End If
    
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm").Visible = True
                    Sheets("SumSegm").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05B").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMSegm").Range("P7").Value = "X" Or _
            Worksheets("CMSegm").Range("P9").Value = "X" Or _
            Worksheets("CMSegm").Range("P11").Value = "X" Or _
            Worksheets("CMSegm").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                Sheets("SumSegm").Visible = True
                Sheets("SumSegm").Select
    
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumSegm").Visible = False
            End If
    '============================================================================================
    
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05B").Range("B17") = 1
            End If
       End If
       hide
       Sheets("CMSegm").Visible = True
       Sheets("CMSegm").Select


    Else        'Added by TJS (11/28/2016)
    'Local Segments (2)

        unhide
        Sheets("Module05D").Select
        IDCM1 = 0
        IDCM2 = Range("C17").Value
        IDCM3 = 0
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 131
        Do
            If Cells(rowctr, 1).Value = IDCM2 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM2
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
       
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05D").Range("B17").Value = 1
            End If
            If answer = 1 Then
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm_Local").Visible = True
                    Sheets("SumSegm_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm_Local").Visible = False
            End If
       
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05D").Range("B17").Value = 1
            End If
    
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm_Local").Visible = True
                    Sheets("SumSegm_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm_Local").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05D").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMSegm_Local").Range("P7").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P9").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P11").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                Sheets("SumSegm_Local").Visible = True
                Sheets("SumSegm_Local").Select
    
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumSegm_Local").Visible = False
            End If
    '============================================================================================
    
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05D").Range("B17") = 1
            End If
       End If
       hide
       Sheets("CMSegm_Local").Visible = True
       Sheets("CMSegm_Local").Select
    End If
End Sub

 Sub DropDown3_Change()
 'Updated by TJS (11/28/2016)



    If Sheets("BCTool").Range("A2").Value = 0 Then      'Added by TJS (11/28/2016)
    'State Segments (3)

        unhide
        Sheets("Module05B").Select
        IDCM2 = 0
        IDCM1 = 0
        IDCM3 = Range("C19").Value
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 131
        Do
            If Cells(rowctr, 1).Value = IDCM3 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM3
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
       
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05B").Range("B19").Value = 1
            End If
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumSegm").Visible = True
                    Sheets("SumSegm").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm").Visible = False
            End If
       
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05B").Range("B19").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm").Visible = True
                    Sheets("SumSegm").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05B").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMSegm").Range("P7").Value = "X" Or _
            Worksheets("CMSegm").Range("P9").Value = "X" Or _
            Worksheets("CMSegm").Range("P11").Value = "X" Or _
            Worksheets("CMSegm").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                Sheets("SumSegm").Visible = True
                Sheets("SumSegm").Select
    
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumSegm").Visible = False
            End If
    '============================================================================================
    
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05B").Range("B19") = 1
            End If
       End If
       hide
       Sheets("CMSegm").Visible = True
       Sheets("CMSegm").Select


    Else        'Added by TJS (11/28/2016)
    'Local Segments (3)

        unhide
        Sheets("Module05D").Select
        IDCM2 = 0
        IDCM1 = 0
        IDCM3 = Range("C19").Value
        IDCM4 = 0
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 131
        Do
            If Cells(rowctr, 1).Value = IDCM3 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM3
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
       
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05D").Range("B19").Value = 1
            End If
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
                    
                    Sheets("SumSegm_Local").Visible = True
                    Sheets("SumSegm_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm_Local").Visible = False
            End If
       
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05D").Range("B19").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm_Local").Visible = True
                    Sheets("SumSegm_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm_Local").Visible = False
            End If
       End If
    
    '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05D").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMSegm_Local").Range("P7").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P9").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P11").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                Sheets("SumSegm_Local").Visible = True
                Sheets("SumSegm_Local").Select
    
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumSegm_Local").Visible = False
            End If
    '============================================================================================
    
       'Check Additional information Flag
        Sheets("Module02A").Select
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05D").Range("B19") = 1
            End If
       End If
       hide
       Sheets("CMSegm_Local").Visible = True
       Sheets("CMSegm_Local").Select
    End If
End Sub

 Sub DropDown4_Change()
 'Updated by TJS (11/28/2016)


    If Sheets("BCTool").Range("A2").Value = 0 Then      'Added by TJS (11/28/2016)
    'State Segments (4)

        unhide
        Sheets("Module05B").Select
        IDCM2 = 0
        IDCM1 = 0
        IDCM3 = 0
        IDCM4 = Range("C21").Value
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 131
        Do
            If Cells(rowctr, 1).Value = IDCM4 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM4
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
       
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05B").Range("B21").Value = 1
            End If
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm").Visible = True
                    Sheets("SumSegm").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm").Visible = False
            End If
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05B").Range("B21").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm").Visible = True
                    Sheets("SumSegm").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm").Visible = False
            End If
       End If
       
       '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05B").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMSegm").Range("P7").Value = "X" Or _
            Worksheets("CMSegm").Range("P9").Value = "X" Or _
            Worksheets("CMSegm").Range("P11").Value = "X" Or _
            Worksheets("CMSegm").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                Sheets("SumSegm").Visible = True
                Sheets("SumSegm").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumSegm").Visible = False
            End If
    '============================================================================================
       
       'Check Additional information Flag
        Sheets("Module02A").Select
       
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05B").Range("B21") = 1
            End If
       End If
       hide
       Sheets("CMSegm").Visible = True
       Sheets("CMSegm").Select
        
        
    Else        'Added by TJS (11/28/2016)
    'Local Segments (4)
    
        unhide
        Sheets("Module05D").Select
        IDCM2 = 0
        IDCM1 = 0
        IDCM3 = 0
        IDCM4 = Range("C21").Value
    'Check HSM Applicability Flag
       
        'ID correct row for CMF (start on correct row (rowctr) for segments)
        Sheets("Module02A").Select
        rowctr = 131
        Do
            If Cells(rowctr, 1).Value = IDCM4 Then
            Exit Do
            Else
            rowctr = rowctr + 1
            End If
        Loop Until Cells(rowctr, 1).Value = IDCM4
       
       'check HSM Flag
       If Cells(rowctr, 41).Value = "x" Then
       answer = MsgBox("Warning - Selected countermeasure does not fully match HSM Setting/Facility Type Criteria." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
       
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05D").Range("B21").Value = 1
            End If
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm_Local").Visible = True
                    Sheets("SumSegm_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " does not fully match HSM Setting/Facility Type Criteria" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm_Local").Visible = False
            End If
       End If
       
        Sheets("Module02A").Select
        'Check AADT Flag
       
       If Cells(rowctr, 42).Value = "x" Then
       answer = MsgBox("Warning - AADT is not within HSM limits." & vbCrLf & vbCrLf & "Please refer to the " & Cells(rowctr, 39) & " for more information." & vbCrLf & vbCrLf & "Would you like to continue?", vbExclamation + vbOKCancel, "HSM Compliance Warning")
        'update to blank out CMF selection
            If answer = 2 Then
            Sheets("Module05D").Range("B21").Value = 1
            End If
            
        'add warning message to summary worksheet
            If answer = 1 Then
            
            
                    Sheets("Module02A").Select
                    warning1 = Cells(rowctr, 2).Value
    
                    Sheets("SumSegm_Local").Visible = True
                    Sheets("SumSegm_Local").Select
    
                    rowctr2 = 5
                    Do
                        If Cells(rowctr2, 14).Value = "" Then
                            Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits"
                            Exit Do
                        ElseIf Cells(rowctr2, 14).Value = warning1 & " AADT is not within HSM limits" Then
                            Exit Do
                        End If
                    rowctr2 = rowctr2 + 1
                    Loop Until rowctr2 = 14
            
                    'Sheets("SumSegm_Local").Visible = False
            End If
       End If
       
       '==========================================================================================
    ' Add a message to flag the use of User Defined CMFs
    'added DPB 03/13/2013
        Sheets("Module05D").Select
        'Check for user defined CMFs

        'Added by TJS (12/06/2016)
        If Worksheets("CMSegm_Local").Range("P7").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P9").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P11").Value = "X" Or _
            Worksheets("CMSegm_Local").Range("P13").Value = "X" Then

                MsgBox "Warning - You have selected a User Defined Countermeasure." & vbCrLf & vbCrLf & _
                        "Please utilize the verified countermeasure database included in the tool first and ensure that there is no valid " & _
                        "substitute available for your selection before electing to use the User Defined Countermeasure. If you elect to use " & _
                        "a User Defined Countermeasure in your analysis, please provide appropriate supporting documentation along with results."
                
                warning1 = "The analysis contains a User Defined Countermeasure (please provide supporting documentation)"
    
                Sheets("SumSegm_Local").Visible = True
                Sheets("SumSegm_Local").Select
                
                rowctr2 = 5
                Do
                    If Cells(rowctr2, 14).Value = "" Then
                        Cells(rowctr2, 14).Value = warning1
                        Exit Do
                    ElseIf Cells(rowctr2, 14).Value = warning1 Then
                        Exit Do
                    End If
                rowctr2 = rowctr2 + 1
                Loop Until rowctr2 = 14
        
                'Sheets("SumSegm_Local").Visible = False
            End If
    '============================================================================================
       
       'Check Additional information Flag
        Sheets("Module02A").Select
       
       If Cells(rowctr, 43).Value <> "" Then
            CMFlag = Cells(rowctr, 43).Value
            fSpecialCase = SpecialCase
            If fSpecialCase = 0 Then        'Added by TJS (12/19/2016)
                Worksheets("Module05D").Range("B21") = 1
            End If
       End If
       hide
       Sheets("CMSegm_Local").Visible = True
       Sheets("CMSegm_Local").Select
    End If
End Sub
    
