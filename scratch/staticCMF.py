crashTypes = [
    "AG",
    "AN",
    "FO",
    "FTF",
    "LT",
    "OtherNC",
    "OtherO",
    "OVT",
    "PD",
    "PDC",
    "PKV",
    "FTR",
    "RT",
    "SSD",
    "SOD",
    "T",
    "TR",
    "NGT",
    "ROR",
    "WP",
]
cmfValues = [
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
    0.866,
]
output = []


for i in range(len(crashTypes)):
    key = crashTypes[i]
    value = cmfValues[i] if i < len(cmfValues) else 1
    if len(str(value)) >= 10:
        output.append(["CrashType.{}".format(key), "roundNumber({}, 9)".format(value)])
    else:
        output.append(["CrashType.{}".format(key), value])

print("expectedCMF: new Map({}),".format(str(output).replace("'", "")))
