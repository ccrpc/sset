Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
 Public Function SpecialCase()
unhide
Sheets("Module02a").Select
Dim var1 As Variant
Dim var2 As Variant
Dim var3 As Variant
Dim var4 As Variant


Select Case CMFlag
'***********************************************
'* Intersections                               *
'***********************************************

'Skew Angle
Case "1A"
Case1A:

    var1 = InputBox("Input Existing Skew Angle (Degrees)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Skew Angle Variables")
        
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case1A
        End If
        
    var2 = InputBox("Input Future Skew Angle (Degrees)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Skew Angle Variables")
        
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case1A
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    

    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "1B"
Case1B:

    var1 = InputBox("Input Existing Skew Angle (Degrees)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Skew Angle Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case1B
        End If
        
    var2 = InputBox("Input Future Skew Angle (Degrees)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Skew Angle Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case1B
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "1C"
Case1C:

    var1 = InputBox("Input Existing Skew Angle (Degrees)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Skew Angle Variables")
        
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case1C
        End If
    
    var2 = InputBox("Input Future Skew Angle (Degrees)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Skew Angle Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case1C
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "1D"
Case1D:

    var1 = InputBox("Input Existing Skew Angle (Degrees)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Skew Angle Variables")
        
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case1D
        End If
    
    var2 = InputBox("Input Future Skew Angle (Degrees)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Skew Angle Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case1D
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


'***********************************************
'* Segments                                    *
'***********************************************

'Lane Width
Case "2A"
Case2A:

    ' Input Boxes
    
    var1 = InputBox("Input Existing Lane Width (in feet, round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Lane Width Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2A
        End If
    
    var2 = InputBox("Input Future Lane Width (in feet, round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Lane Width Variables")
        
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2A
        End If
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2B"
Case2B:

    ' Input Boxes
    
    var1 = InputBox("Input Existing Lane Width (in feet, round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Lane Width Variables")
        
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2B
        End If
    
    var2 = InputBox("Input Future Lane Width (in feet, round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Lane Width Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2B
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2C"
Case2C:

    ' Input Boxes

    var1 = InputBox("Input Existing Lane Width (round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Lane Width Variables")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2C
        End If

    var2 = InputBox("Input Future Lane Width (round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Lane Width Variables")

        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2C
        End If

    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2

    CMFVal = Round(Cells(rowctr, 6), 2)

    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

'Paved Shoulder
Case "2D"
Case2D:

    ' Input Boxes
    
    var1 = InputBox("Input Existing Shoulder Width (in feet, round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Shoulder Width Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2D
        End If
        
    var2 = InputBox("Input Future Shoulder Width (in feet, round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Shoulder Width Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2D
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2E"
Case2E:

    ' Input Boxes
    
    var1 = InputBox("Input Existing Shoulder Width (in feet, round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Shoulder Width Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2E
        End If
        
    var2 = InputBox("Input Future Shoulder Width (in feet, round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Shoulder Width Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2E
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2F"
Case2F:

    ' Input Boxes
    
    var1 = InputBox("Input Existing Shoulder Width (round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Shoulder Width Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2F
        End If
        
    var2 = InputBox("Input Future Shoulder Width (round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Shoulder Width Variables")
   
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2F
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


'Shoulder Type

Case "2G"
Case2G:
'Updated by TJS (12/27/2016)

    ' Input Boxes
3
    var1 = InputBox("Input Existing Shoulder Width (Whole Numbers between 1 to 10)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Change Shoulder Type Variables")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2G
        End If
        
4   'LCase function added by TJS (12/27/2016)
    var2 = LCase(InputBox("Existing Shoulder Type (Paved, Gravel, Composite, or Turf)" _
            & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Change Shoulder Type Variables Variables"))

        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = CleanStr(var2, 0, 0, 0, 0, 0)        'Added by TJS (12/27/2016)
        
5   'LCase function added by TJS (12/27/2016)
    var3 = LCase(InputBox("Future Shoulder Type (Paved, Gravel, Composite, or Turf)" _
            & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Change Shoulder Type Variables Variables"))
    
        'Input validation
        If var3 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var3 = CleanStr(var3, 0, 0, 0, 0, 0)        'Added by TJS (12/27/2016)
    
    If var1 < 1 Or var1 > 10 Then
    MsgBox "Shoulder width must be between 1 and 10. If shoulder width is greater than 10, please input 10."
    GoTo 3
    End If
    
    If var2 <> "paved" And var2 <> "composite" And var2 <> "turf" And var2 <> "gravel" Then
    MsgBox "Invalid shoulder type selected. Shoulder Type Options are Paved, Composite, Turf, and Gravel."
    GoTo 4
    End If
    
    If var3 <> "paved" And var3 <> "composite" And var3 <> "turf" And var3 <> "gravel" Then
    MsgBox "Invalid shoulder type selected. Shoulder Type Options are Paved, Composite, Turf, and Gravel."
    GoTo 5
    End If
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    Cells(rowctr, 36).Value = var3
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

'TWLTL
Case "2H"
Case2H:

' Input Boxes
    
    var1 = InputBox("Input Driveway Density (driveways/mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "TWLTL Variables")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2H
        End If
        
6
    var2 = InputBox("Input Proportion of Crashes that are Driveway Related (in decimal)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "TWLTL Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2H
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    If var2 > 1 Then
    MsgBox "Crash Proportion is in decimal form"
    GoTo 6
    End If
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2I"
Case2I:

' Input Boxes
    
    var1 = InputBox("Input Driveway Density (driveways/mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "TWLTL Variables")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2I
        End If
        
7
    var2 = InputBox("Input Proportion of Crashes that are Driveway Related (in decimal)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "TWLTL Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2I
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    If var2 > 1 Then
    MsgBox "Crash Proportion is in decimal form"
    GoTo 7
    End If
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2J"
Case2J:

    ' Input Boxes
    
    var1 = InputBox("Input Driveway Density (driveways/mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "TWLTL Variables")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2J
        End If
        
8
    var2 = InputBox("Input Proportion of Crashes that are Driveway Related (in decimal)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "TWLTL Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2J
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    If var2 > 1 Then
    MsgBox "Crash Proportion is in decimal form"
    GoTo 8
    End If
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2K"
Case2K:

    ' Input Boxes
    
    var1 = InputBox("Input Driveway Density (driveways/mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "TWLTL Variables")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2K
        End If
        
9
    var2 = InputBox("Input Proportion of Crashes that are Driveway Related (in decimal)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "TWLTL Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2K
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    If var2 > 1 Then
    MsgBox "Crash Proportion is in decimal form"
    GoTo 9
    End If
    
    CMFVal = Round(Cells(rowctr, 6), 2)

    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

'Change Width of Existing Median
Case "2L"
Case2L:

' Input Boxes
    
    var1 = InputBox("Input Existing Median Width (Round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Median Width Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2L
        End If
        
    var2 = InputBox("Input Future Median Width (Round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Median Width Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2L
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2M"
Case2M:

' Input Boxes
    
    var1 = InputBox("Input Existing Median Width (Round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Median Width Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2M
        End If
        
    var2 = InputBox("Input Future Median Width (Round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Median Width Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2M
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2N"
Case2N:

' Input Boxes
    
    var1 = InputBox("Input Existing Median Width (Round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Median Width Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2N
        End If
        
    var2 = InputBox("Input Future Median Width (Round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Median Width Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2N
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2O"
Case2O:

' Input Boxes
    
    var1 = InputBox("Input Existing Median Width (Round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Median Width Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2O
        End If
        
    var2 = InputBox("Input Future Median Width (Round to nearest 1')" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Median Width Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2O
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

'Superelevation
Case "2P"
Case2P:

    var1 = InputBox("Input Existing Superelevation Variance (Decimal)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Superelevation Variables")
        
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2P
        ElseIf var1 > 0.4 Or var1 < 0 Then
            MsgBox "Input invalid. Please enter a decimal number between 0 and 0.4."
            GoTo Case2P
        End If
    
    var2 = InputBox("Input Future Superelevation Variance (Decimal)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Superelevation Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2P
        ElseIf var2 > 0.4 Or var2 < 0 Then
            MsgBox "Input invalid. Please enter a decimal number between 0 and 0.4."
            GoTo Case2P
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

'Horizontal Curve Radius

Case "2Q"
Case2Q:

    var1 = InputBox("Input Existing Curve Length (mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Horizontal Curve Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2Q
        End If
        
    var2 = InputBox("Input Existing Radius of Curvature (feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Horizontal Curve Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2Q
        End If
    
    var3 = InputBox("Input Future Curve Length (mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Horizontal Curve Variables")
    
        'Input validation
        If var3 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var3) = vbNullString Then
            GoTo Case2Q
        End If
        
    var4 = InputBox("Input Future Radius of Curvature (feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Horizontal Curve Variables")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2Q
        End If
        
    Cells(rowctr, 34).Value = var1 & " / " & var2
    Cells(rowctr, 35).Value = var3 & " / " & var4
    Cells(rowctr, 36).Formula = "=((1.55*" & var1 & ")+(80.2/" & var2 & ")-(0.012*1))/(1.55*" & var1 & ")"
    Cells(rowctr, 37).Formula = "=((1.55*" & var3 & ")+(80.2/" & var4 & ")-(0.012*1))/(1.55*" & var3 & ")"
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2R"
Case2R:

    var1 = InputBox("Input Existing Curve Length (mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Horizontal Curve Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2R
        End If
        
    var2 = InputBox("Input Existing Radius of Curvature (feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Horizontal Curve Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2R
        End If
    
    var3 = InputBox("Input Future Curve Length (mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Horizontal Curve Variables")
    
        'Input validation
        If var3 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var3) = vbNullString Then
            GoTo Case2R
        End If
        
    var4 = InputBox("Input Future Radius of Curvature (feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Horizontal Curve Variables")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2R
        End If
        
    Cells(rowctr, 34).Value = var1 & " / " & var2
    Cells(rowctr, 35).Value = var3 & " / " & var4
    Cells(rowctr, 36).Formula = "=((1.55*" & var1 & ")+(80.2/" & var2 & ")-(0.012*0))/(1.55*" & var1 & ")"
    Cells(rowctr, 37).Formula = "=((1.55*" & var3 & ")+(80.2/" & var4 & ")-(0.012*0))/(1.55*" & var3 & ")"
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

'Flatten Sideslopes

Case "2S"
Case2S:

1
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
2
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
        
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
        
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 1
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 2
         End If
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2T"
Case2T:

10
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
11
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
         
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 10
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 11
         End If
    
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AS"
Case2AS:

30
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
31
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
         
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 30
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 31
         End If
    
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2AT"
Case2AT:

40
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
41
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
         
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 40
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 41
         End If
    
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AU"
Case2AU:

50
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
51
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
         
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 50
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 51
         End If
    
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AV"
Case2AV:

61
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
62
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
        
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
        
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 61
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 62
         End If
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AW"
Case2AW:

70
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
71
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
         
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 70
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 71
         End If
    
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
   
Case "2AX"
Case2AX:

80
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
81
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
         
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 80
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 81
         End If
    
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    

Case "2AY"
Case2AY:

90
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
91
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
         
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 90
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 91
         End If
    
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AZ"
Case2AZ:

100
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
101
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
         
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 100
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 101
         End If
    
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2BA"
Case2BA:

110
    var1 = InputBox("Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:4H")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var1 = UCase(var1)
        
111
    var2 = InputBox("Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only)." & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Sideslope Variables", "1V:5H")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        End If
        var2 = UCase(var2)
         
         If (var1 <> "1V:2H" And var1 <> "1V:3H" And var1 <> "1V:4H" And var1 <> "1V:5H" And var1 <> "1V:6H") Then
         MsgBox "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H,1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
         GoTo 110
         End If
    
         If var2 <> "1V:4H" And var2 <> "1V:5H" And var2 <> "1V:6H" And var2 <> "1V:7H" Then
         MsgBox "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
         GoTo 111
         End If
    
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
'Increase Distance to Roadside Features

Case "2U"
Case2U:

    var1 = InputBox("Input Existing Distance to Roadside Features (in feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Roadside Feature Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2U
        End If
        
    var2 = InputBox("Input Future Distance to Roadside Features (in feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Roadside Feature Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2U
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
'Reduce Roadside Hazard Rating

Case "2V"
Case2V:

12
    var1 = InputBox("Existing Roadside Hazard Rating (1-7)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Roadside Hazard Variables")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2V
        End If
        
13
    var2 = InputBox("Future Roadside Hazard Rating (1-7)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Roadside Hazard Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2V
        End If
    
    If var1 < 1 Or var1 > 7 Then
    MsgBox "Roadside Hazard Rating must be Between 1-7"
    GoTo 12
    End If
    
    If var2 < 1 Or var2 > 7 Then
    MsgBox "Roadside Hazard Rating must be Between 1-7"
    GoTo 13
    End If
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

'Convert Angle to Parallel Parking

Case "2W"
Case2W:

    var1 = InputBox("Curb Length With On-street Parking (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2W
        End If
        
    var2 = InputBox("Total Curb Length  (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2W
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2X"
Case2X:

    var1 = InputBox("Curb Length With On-street Parking (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2X
        End If
        
    var2 = InputBox("Total Curb Length  (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2X
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2Y"
Case2Y:

    var1 = InputBox("Curb Length With On-street Parking (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2Y
        End If
        
    var2 = InputBox("Total Curb Length  (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2Y
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2Z"
Case2Z:

    var1 = InputBox("Curb Length With On-street Parking (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2Z
        End If
        
    var2 = InputBox("Total Curb Length  (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2Z
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2AA"
Case2AA:

    var1 = InputBox("Curb Length With On-street Parking (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AA
        End If
        
    var2 = InputBox("Total Curb Length  (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AA
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AB"
Case2AB:

    var1 = InputBox("Curb Length With On-street Parking (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AB
        End If
        
    var2 = InputBox("Total Curb Length  (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AB
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AC"
Case2AC:

    var1 = InputBox("Curb Length With On-street Parking (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AC
        End If
        
    var2 = InputBox("Total Curb Length  (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AC
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AD"
Case2AD:

    var1 = InputBox("Curb Length With On-street Parking (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AD
        End If
        
    var2 = InputBox("Total Curb Length  (Feet)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Parking Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AD
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

'Modify Access Point Density

Case "2AE"
Case2AE:

    var1 = InputBox("Existing Driveway Density (Dwy/Mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Access Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AE
        End If
        
    var2 = InputBox("Future Driveway Density  (Dwy/Mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Access Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AE
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AE
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AF"
Case2AF:

    var1 = InputBox("Existing Driveway Density (Dwy/Mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Access Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AF
        End If
        
    var2 = InputBox("Future Driveway Density  (Dwy/Mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Access Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AF
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AF
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    

Case "2AG"
Case2AG:

    var1 = InputBox("Existing Driveway Density (Dwy/Mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Access Variables")

        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AG
        End If

    var2 = InputBox("Future Driveway Density  (Dwy/Mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Access Variables")

        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AG
        End If
        
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AG
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________

    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2

    CMFVal = Round(Cells(rowctr, 6), 2)

    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AH"
Case2AH:

    var1 = InputBox("Existing Driveway Density (Dwy/Mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Access Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AH
        End If
        
    var2 = InputBox("Future Driveway Density  (Dwy/Mi)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Access Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AH
        End If
        
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AH
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

'Modify Work Zone Duration

Case "2AI"
Case2AI:

    var1 = InputBox("Existing Work Zone Duration (Days)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Duration Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AI
        End If
        
    var2 = InputBox("Proposed Work Zone Duration (Days)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Duration Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AI
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AI
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AJ"
Case2AJ:

    var1 = InputBox("Existing Work Zone Duration (Days)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Duration Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AJ
        End If
        
    var2 = InputBox("Proposed Work Zone Duration (Days)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Duration Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AJ
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AJ
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2AK"
Case2AK:

    var1 = InputBox("Existing Work Zone Duration (Days)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Duration Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AK
        End If
        
    var2 = InputBox("Proposed Work Zone Duration (Days)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Duration Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AK
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AK
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AL"
Case2AL:

    var1 = InputBox("Existing Work Zone Duration (Days)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Duration Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AL
        End If
        
    var2 = InputBox("Proposed Work Zone Duration (Days)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Duration Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AL
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AL
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AM"
Case2AM:

    var1 = InputBox("Existing Work Zone Duration (Days)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Duration Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AM
        End If
        
    var2 = InputBox("Proposed Work Zone Duration (Days)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Duration Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AM
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AM
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

'Modify Work Zone Length

Case "2AN"
Case2AN:

    var1 = InputBox("Existing Work Zone Length (Miles)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Length Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AN
        End If
        
    var2 = InputBox("Proposed Work Zone Length (Miles)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Length Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AN
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AN
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
    
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2AO"
Case2AO:

    var1 = InputBox("Existing Work Zone Length (Miles)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Length Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AO
        End If
        
    var2 = InputBox("Proposed Work Zone Length (Miles)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Length Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AO
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AO
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AP"
Case2AP:

    var1 = InputBox("Existing Work Zone Length (Miles)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Length Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AP
        End If
        
    var2 = InputBox("Proposed Work Zone Length (Miles)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Length Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AP
        End If
        
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AP
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AQ"
Case2AQ:

    var1 = InputBox("Existing Work Zone Length (Miles)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Length Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AQ
        End If
        
    var2 = InputBox("Proposed Work Zone Length (Miles)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Length Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AQ
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AQ
        End If
        
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
        
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal
    
Case "2AR"
Case2AR:

    var1 = InputBox("Existing Work Zone Length (Miles)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Length Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case2AR
        End If
        
    var2 = InputBox("Proposed Work Zone Length (Miles)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Work Zone Length Variables")
    
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case2AR
        End If
    
    'Request service life from user__________________________
    'Added by TJS (01/06/2017)
    var4 = InputBox("Effective Service Life (Whole number years, 1-50)" & vbCrLf & vbCrLf & "Refer to " & Cells(rowctr, 39), "Service Life")
    
        'Input validation
        If var4 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var4) = vbNullString Then
            GoTo Case2AR
        End If
    
        If var4 < 1 Then
            var4 = 1
        ElseIf var4 > 50 Then
            var4 = 50
        End If
    
    Cells(rowctr, 5).Value = CLng(var4)
    '_________________________________________________________
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal

Case "2BB"
Case2BB:

    MsgBox ("Please reference the IDOT Bureau of Design & Environment Manual, Chapter 38-7 to determine the applicablity of this countermeasure.")
   

'=======================================
'========== Local CMF cases ============
'=======================================


Case "XX"
'Catchall case for countermeasures which have no function but may appear ambiguous to users
CaseXX:

'Case added by TJS (12/02/2016)
'Catchall case for CMFs which are not functional but represent a variable countermeasure

    MsgBox ("Please reference the CMF source listed below to determine the applicablity of this countermeasure." & vbCrLf & vbCrLf _
            & "Source: " & Cells(rowctr, 39))


Case "4A"
Case4A:

'Case added by TJS (12/02/2016)

    var1 = InputBox("Existing median opening density (Median Openings/Mile)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Median Opening Density Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case4A
        End If
        
    var2 = InputBox("Proposed median opening density (Median Openings/Mile)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Median Opening Density Variables")
            
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case4A
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


Case "4B"
Case4B:

'Case added by TJS (12/02/2016)

    var1 = InputBox("Existing bridge width minus roadway width (Feet)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Bridge Width Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case4B
        End If
        
    var2 = InputBox("Proposed bridge width minus roadway width (Feet)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Bridge Width Variables")
            
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case4B
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


Case "4C"
Case4C:

'Case added by TJS (12/02/2016)

    var1 = InputBox("Existing driveway density (Driveways/Mile)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Driveway Density Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case4C
        End If
        
    var2 = InputBox("Proposed driveway density (Driveways/Mile)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Driveway Density Variables")
            
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case4C
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


Case "4DA"
Case4DA:

'Case added by TJS (12/02/2016)

    var1 = InputBox("Existing traffic signal spacing (x*1000ft/Traffic Signal)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Traffic Signal Spacing Variables (1000ft per Signal)")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case4DA
        End If
        
    var2 = InputBox("Proposed traffic signal spacing (x*1000ft/Traffic Signal)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Traffic Signal Spacing Variables (1000ft per Signal)")
            
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case4DA
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Cells(rowctr, 6)   'CMF varies
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


Case "4E"
Case4E:

'Case added by TJS (12/02/2016)

    var1 = InputBox("Existing unsignalized cross roads density (# Cross Roads/Mile)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Unsignalized Cross Roads Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case4E
        End If
        
    var2 = InputBox("Proposed unsignalized cross roads density (# Cross Roads/Mile)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Unsignalized Cross Roads Variables")
            
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case4E
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Cells(rowctr, 6)   'CMF varies
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


Case "4FA"
Case4FA:

'Case added by TJS (12/02/2016)

    var1 = InputBox("Existing number of 3-leg intersections in study area" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "3-Leg Intersection Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case4FA
        End If
        
    var2 = InputBox("Proposed number of 3-leg intersections in study area" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "3-Leg Intersection Variables")
            
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case4FA
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Cells(rowctr, 6)   'CMF varies
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


Case "4FB"
Case4FB:

'Case added by TJS (12/02/2016)

    var1 = InputBox("Existing number of 4-leg intersections in study area" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "4-Leg Intersection Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case4FB
        End If
        
    var2 = InputBox("Proposed number of 4-leg intersections in study area" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "4-Leg Intersection Variables")
            
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case4FB
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Cells(rowctr, 6)   'CMF varies
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


Case "4G"
Case4G:

'Case added by TJS (12/02/2016)

    var1 = InputBox("Existing traffic volume of study area (AADT)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "4-Leg Intersection Variables")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case4G
        End If
                
    Cells(rowctr, 34).Value = var1
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


Case "4DB"
Case4DB:

'Case added by TJS (12/02/2016)

    var1 = InputBox("Existing traffic signal spacing (Signals/Mile)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Traffic Signal Spacing Variables (Signals per Mile)")
    
        'Input validation
        If var1 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var1) = vbNullString Then
            GoTo Case4DB
        End If
        
    var2 = InputBox("Proposed traffic signal spacing (Signals/Mile)" & vbCrLf & vbCrLf & "Refer to " _
            & Cells(rowctr, 39), "Traffic Signal Spacing Variables (Signals per Mile)")
            
        'Input validation
        If var2 = "" Then
            SpecialCase = 0
            Exit Function
        ElseIf OnlyNumbers(var2) = vbNullString Then
            GoTo Case4DB
        End If
        
    Cells(rowctr, 34).Value = var1
    Cells(rowctr, 35).Value = var2
    
    CMFVal = Round(Cells(rowctr, 6), 2)
    
    'Report Calculated CMF
    MsgBox "Calculated CMF= " & CMFVal


Case Else
'Case added by TJS(12/19/2016)
'Case indicates failure of program to select a case, indicating that an error has occurred in the workbook
    MsgBox "Countermeasure special case selection has failed. Please alert the BC Tool developer to address the issue." & vbCrLf & vbCrLf & _
            "Please select a different countermeasure."
    SpecialCase = 0
    Exit Function
    
End Select
    
    'Added by TJS (12/19/2016)
    SpecialCase = 1     'Indicates that a CMF case was successfully selected and completed

End Function
