import { Config } from "@stencil/core";
import { sass } from "@stencil/sass";
import dotenv from "rollup-plugin-dotenv";

export const config: Config = {
  namespace: "sset",
  srcDir: "src",
  globalScript: "src/global/js/index.ts",
  globalStyle: "src/global/scss/style.scss",
  sourceMap: true,
  outputTargets: [
    {
      type: "www",
      serviceWorker: null,
      copy: [
        {
          src: "index.html",
        },
        {
          src: "docx-export/assets/idot_logo.png",
          dest: "build/assets/idot_logo.png",
        },
      ],
    },
  ],
  plugins: [sass(), dotenv()],
};
