# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [1.2.5](https://gitlab.com/ccrpc/sset/compare/v1.2.4...v1.2.5) (2024-10-01)


### Features

* **frontend:** Adding icon ([e106717](https://gitlab.com/ccrpc/sset/commit/e10671729159b7f605889fd82439e674b92380b4))
* **frontend:** The category 'Freezing Rain or Freezing Drizzle' includes Freezing Rain ([cfa8005](https://gitlab.com/ccrpc/sset/commit/cfa8005b24c5d8f458f9c438c073ef8ee2101ebb))
* **tiles:** Updating tile creation scripts ([5bd87f5](https://gitlab.com/ccrpc/sset/commit/5bd87f58ee13f3330dbe227c5de5bcb04d2319ef))
* Updating hard coded 2018 -> 2022 range to 2019 -> 2023 range ([8306d43](https://gitlab.com/ccrpc/sset/commit/8306d432b090831f47293424382eca421cfd1670))

## [1.2.4](https://gitlab.com/ccrpc/sset/compare/v1.2.3...v1.2.4) (2024-07-30)


### Bug Fixes

* **frontend:** index.html was pulling in the latest ionic core ([7a06c7b](https://gitlab.com/ccrpc/sset/commit/7a06c7bbed1f77472865eb80e9f2e64714d55273))
* **frontend:** Overwriting hidden button visibility ([5dedd04](https://gitlab.com/ccrpc/sset/commit/5dedd0495d13167c8668804d78f9b3f6a457a99a))

## [1.2.3](https://gitlab.com/ccrpc/sset/compare/v1.2.2...v1.2.3) (2023-09-20)


### Features

* Applying type renames to countermeasures ([68adef9](https://gitlab.com/ccrpc/sset/commit/68adef92933f7e7319603f56079b50fb2864b506))


### Bug Fixes

* **docx:** Number of crashes now depend on crash severity ([39952b6](https://gitlab.com/ccrpc/sset/commit/39952b6dc8efb1d04e7f0ef15873a0e88b7e2090))
* Mimicing sleet data cleaning ([82532d9](https://gitlab.com/ccrpc/sset/commit/82532d9bf5a6d7db5067df72eebc000d8de680bb))
* Not exculding FTF or FTR ([a513491](https://gitlab.com/ccrpc/sset/commit/a51349106bcbc22cb3733a223d4bcbac5bbe0687))

## [1.2.2](https://gitlab.com/ccrpc/sset/compare/v1.2.1...v1.2.2) (2023-07-18)


### Bug Fixes

* **frontend:** All should not include wet pavement ([b95e9ac](https://gitlab.com/ccrpc/sset/commit/b95e9ac2ed264a95ff6a8aa12714bdbbd67c0325))

## [1.2.1](https://gitlab.com/ccrpc/sset/compare/v1.2.0...v1.2.1) (2023-07-18)


### Features

* **frontend:** Added suport for custom units ([10b83b7](https://gitlab.com/ccrpc/sset/commit/10b83b7d030b917527251f165d06973cb2d093da))
* **frontend:** Adding user defined countermeasures ([533c0f5](https://gitlab.com/ccrpc/sset/commit/533c0f542723289ae00971c8c55ce7ad070b1bd3))
* **frontend:** Code for user defined is now auto calculated ([82085b1](https://gitlab.com/ccrpc/sset/commit/82085b1e883f6958ccfd43080980ee882641e0fd))
* **frontend:** Edit button and condition to display it ([46f5171](https://gitlab.com/ccrpc/sset/commit/46f51719000c9a65d36b57d74d8c16135b1e34e9))
* **frontend:** User defined can be edited again ([df05b38](https://gitlab.com/ccrpc/sset/commit/df05b387d008bc6c42ec8d53e920a4df1fc6f667))
* **frontend:** User Generated all input ([c8b670c](https://gitlab.com/ccrpc/sset/commit/c8b670ca278e128624b23e2743d5a72ae3fb6fd2))


### Bug Fixes

* **frontend:** Affected crash types checked ([44f483c](https://gitlab.com/ccrpc/sset/commit/44f483c2a19e3ca1f58151d726ead93da34b665f))
* **frontend:** Aligning countermeasure information ([528e773](https://gitlab.com/ccrpc/sset/commit/528e773a98ff28f9c681375aa61e12e26784dec2))
* **frontend:** Centering all checkbox ([61a8112](https://gitlab.com/ccrpc/sset/commit/61a8112ef4b185944c302949f92a631eff91e424))
* **frontend:** No loading existing countermeasures into defined ([14e6ffa](https://gitlab.com/ccrpc/sset/commit/14e6ffabdd8c2720e3f9755c1cdf6ebf79d4a931))

## [1.2.0](https://gitlab.com/ccrpc/sset/compare/v1.1.2...v1.2.0) (2023-06-20)


### Features

* **BCLib:** Added aadtMinor support to rest of application ([a4d4165](https://gitlab.com/ccrpc/sset/commit/a4d41657cf821e0c1294601f65210b5d4643d2cb))
* **BCLib:** Added more calculated countermeasures ([6483122](https://gitlab.com/ccrpc/sset/commit/64831221336cb6b997c8ef8f480a90d76f173577))
* **BCLib:** Added more suport for aadtMinor testing ([f1d35b1](https://gitlab.com/ccrpc/sset/commit/f1d35b1de793808da8cd2d7acea4d94c1c9d263a))
* **BCLib:** Finished calculated countermeasures ([f2b124a](https://gitlab.com/ccrpc/sset/commit/f2b124af8eb151ad1484b6a5fd6780a6e13a0004))
* **BCLib:** More calculated countermeasures ([b775944](https://gitlab.com/ccrpc/sset/commit/b7759448357288d6ef954d8048745645554ae827))
* **docx:** Added pre-filled fields ([9164f39](https://gitlab.com/ccrpc/sset/commit/9164f39d3864bd115f95db3092e499177b79de1c))
* **frontend:** Added 3.1.1.I5.1 ([7560d16](https://gitlab.com/ccrpc/sset/commit/7560d1660c42a18b0424c2397dd8df3040487992))
* **frontend:** Added a close button for countermeasures ([676d0d5](https://gitlab.com/ccrpc/sset/commit/676d0d53975ddb410ced25ad2b3edd87b9efc68d))
* **frontend:** Added aadtMinor support to interface ([a40098b](https://gitlab.com/ccrpc/sset/commit/a40098b78f02ddfe5707475d331c60ae04efe0f7))
* **frontend:** Added an x button for features ([6db15bf](https://gitlab.com/ccrpc/sset/commit/6db15bfda3c8b8777aa429b98ba51f76700ebb17))
* **frontend:** Adding aadtMinor ([8e5e907](https://gitlab.com/ccrpc/sset/commit/8e5e907f3f5ee92ac2f9bcd5272d42e0b65f37f9))
* **frontend:** Asks user for confirmation ([3b6c915](https://gitlab.com/ccrpc/sset/commit/3b6c91577c6066d288681af90df97874200730ce))
* **frontend:** Can now enter service life ([771621d](https://gitlab.com/ccrpc/sset/commit/771621d28d4e7b239ce54567d6bcc3c67b6dba86))


### Bug Fixes

* **BCLib:** Fixing read end ([66e93c7](https://gitlab.com/ccrpc/sset/commit/66e93c7c9271ed4c8d55e6d805c0482fd468ef21))
* **BCLib:** Fixing read end ([77d4420](https://gitlab.com/ccrpc/sset/commit/77d4420b247e943ca39438f00cded51cc0026de9))
* **BCLib:** Remove 4.6.12.S1.1 ([0789b8d](https://gitlab.com/ccrpc/sset/commit/0789b8d057db8420b2803c5f5e04145a6422f4ec))
* **BCLib:** Some segment adjusted were intersection ([bd5fe60](https://gitlab.com/ccrpc/sset/commit/bd5fe604719f682f17452b1e7a6cc3e9d1697cf2))
* **BCLib:** Using unrounded intput for Turning crash type ([0c72243](https://gitlab.com/ccrpc/sset/commit/0c722437673ddf1cb06ffc784cfc8b70d1436df6))
* **docx:** if no speed limit, don't have mph hangin ([5af335f](https://gitlab.com/ccrpc/sset/commit/5af335fc16cb7177923a9a9456adab01f619d179))
* Fix pipeline ([6f75bc9](https://gitlab.com/ccrpc/sset/commit/6f75bc9de70acf6253ee47e9133660bcf2e744c1))
* **frontend:** Adding value to shaders ([9974796](https://gitlab.com/ccrpc/sset/commit/997479626330b1c834a7b5a9f262ebe83e69b601))
* **frontend:** Undoing checkbox ([e1ffe47](https://gitlab.com/ccrpc/sset/commit/e1ffe47ccb79ee1f17ef7d2292fa8cc261273d1a))

## [1.1.2](https://gitlab.com/ccrpc/sset/compare/v1.1.1...v1.1.2) (2023-05-22)

## [1.1.1](https://gitlab.com/ccrpc/sset/compare/v1.1.0...v1.1.1) (2023-05-22)


### Features

* **frontend:** Adjusting title padding ([033a59b](https://gitlab.com/ccrpc/sset/commit/033a59be00a798231d3ff7859808e46578f69b47))
* **frontend:** Calculated countermeasure now populates pending ([8b0ee3b](https://gitlab.com/ccrpc/sset/commit/8b0ee3b6abdf914eba96e808d8cf195970e527d3))
* **frontend:** Editing colors ([f02106b](https://gitlab.com/ccrpc/sset/commit/f02106b1b9c353d83d45514513341e9e17757136))


### Bug Fixes

* **frontend:** Wiring up user input to calculate logic ([2d6eaa8](https://gitlab.com/ccrpc/sset/commit/2d6eaa8e1dc94de2965293d79f2793b508bc7ce8))
* **infra:** If the default.conf is not in nginx it errors ([49cbb59](https://gitlab.com/ccrpc/sset/commit/49cbb591efb313a68e9060f6a7ef8b18eabf8560))

## [1.1.0](https://gitlab.com/ccrpc/sset/compare/v1.0.1...v1.1.0) (2023-05-16)


### Features

* Adding project type and project system ([ef337e0](https://gitlab.com/ccrpc/sset/commit/ef337e0b84056aab71965b25167c7d9819b2d4a4))
* **BCLib:** Added Increased Pavement Friction countermeasure ([9180505](https://gitlab.com/ccrpc/sset/commit/91805059b80e42513da683bd14fb408af339e765))
* **BCLib:** Adding aggregate input to createCrashTable ([81fd1c8](https://gitlab.com/ccrpc/sset/commit/81fd1c83e1df1f65bfdbd66774d21d2c23c0732d))
* **BCLib:** AdjustedCMFs are not calculated ([398903f](https://gitlab.com/ccrpc/sset/commit/398903f9715cf94f02e646308fe1e3375b5e4edf))
* **BCLib:** Aggregation logic correct now ([995c7fc](https://gitlab.com/ccrpc/sset/commit/995c7fc6466451338870498bae3c57b6127b26c8))
* **BCLib:** Benefit now takes wp and ngt into account ([6dcaff1](https://gitlab.com/ccrpc/sset/commit/6dcaff19b31d0a175623f195918df99ed2c8e5b0))
* **BCLib:** Changed aggregation logic ([beb7801](https://gitlab.com/ccrpc/sset/commit/beb78019977cff14d7667abd9eae713c0f11f353))
* **BCLib:** Tests now take start and end year from .env file ([8e4fd02](https://gitlab.com/ccrpc/sset/commit/8e4fd023f46d8583db5242c7275b74b6feaaef48))
* **docx:** Added conditionality to crash details ([a7b1c91](https://gitlab.com/ccrpc/sset/commit/a7b1c91d41cb8c110ebb728ffa8a3066b24f3865))
* **frontend:** Added Severity.N to crash table ([016b615](https://gitlab.com/ccrpc/sset/commit/016b615ea6e09966c46174c997eea97ffda55771))
* **frontend:** Added toggle all buttson ([25bc358](https://gitlab.com/ccrpc/sset/commit/25bc358d92123391df1b2968e62b4aad71abfa63))
* **frontend:** Adding NGT and WP aggregation to frontend ([594dfeb](https://gitlab.com/ccrpc/sset/commit/594dfebffd545bd9001f32faaa782a5dc526248a))
* **frontend:** Countermeasure 4.1.17.AL.1 implemented ([3141e13](https://gitlab.com/ccrpc/sset/commit/3141e13e2da60a58740ecd25b6f2d6f96c72d208))
* **frontend:** Crashes countAs changes are persisted ([f3f88df](https://gitlab.com/ccrpc/sset/commit/f3f88df2d606a3f03bcabac3afe8b36908cb57b2))
* **frontend:** Non aggregated night time behavior added ([23ef69d](https://gitlab.com/ccrpc/sset/commit/23ef69d53dc2a85f4384ca78ca64f5ac10d69b04))
* **frontend:** Starting work on crash list ([b9e869a](https://gitlab.com/ccrpc/sset/commit/b9e869a6f3571d60430c9b439a80c7e580d9b3a2))
* **frontend:** Toggles working ([dbf5744](https://gitlab.com/ccrpc/sset/commit/dbf57442a993c76f665d7aa1fc79ab23dd858402))
* **python:** Added python to grab adjustedCMF ([79108db](https://gitlab.com/ccrpc/sset/commit/79108db0675dc6b700483c6fa07702b833c3aa54))


### Bug Fixes

* **BCLib:** Calculated combined cmf had an incorrect escape ([93ff6d4](https://gitlab.com/ccrpc/sset/commit/93ff6d4c329d6791b84c94ae944c7c7263729aee))
* **BCLib:** cmf loader not reading NGT or WP ([4543f50](https://gitlab.com/ccrpc/sset/commit/4543f5028794f35e941de054c0c5903a594071d3))
* **BCLib:** FIxing more dates in tests ([9f05a52](https://gitlab.com/ccrpc/sset/commit/9f05a5224500a579c45e12196a03790fa7fc12f3))
* **BCLib:** In the tests the date was for a 4 year periods ([50ae73d](https://gitlab.com/ccrpc/sset/commit/50ae73dca995c3d3a7d95e66a91bf02636cf5f9d))
* **BCLib:** OVT was listed twice ([c4cf24d](https://gitlab.com/ccrpc/sset/commit/c4cf24d5057187b28f0104d1fcf1951010f6cfaa))
* **docx:** Crash severity distribution Night and Wet Pavement ([cd764b7](https://gitlab.com/ccrpc/sset/commit/cd764b7c466b3283dfabdc9c3ccad8fb5bcbb5db))

## [1.0.1](https://gitlab.com/ccrpc/sset/compare/v1.0.0...v1.0.1) (2023-05-03)


### Features

* **docx:** added Number of vehicles ([fd3f115](https://gitlab.com/ccrpc/sset/commit/fd3f11560d7fd3dc9f6b51b5fbaeef1e083c9667))
* **docx:** Splitting up raw crash data ([56fe895](https://gitlab.com/ccrpc/sset/commit/56fe895681457ecf4e446ddccf4f3340c3afcea6))
* **frontend:** Switching to 2020 imagery ([6a95e9e](https://gitlab.com/ccrpc/sset/commit/6a95e9eaf4e88880aafbdba9231c73fa22e382e2))
* **infra:** Preparing export profile for potential cloud use ([666732f](https://gitlab.com/ccrpc/sset/commit/666732f56c530cb003b5b03c06b0261de61c5d6d))


### Bug Fixes

* Changing wet weather to wet pavement ([de88e77](https://gitlab.com/ccrpc/sset/commit/de88e77eee83396427646a59e3e4f8714e11290e))
* **chart:** Crashes hour starts at 0 ([8681ce0](https://gitlab.com/ccrpc/sset/commit/8681ce09a97d79020e9eb7607430a02df8d14d95))
* **frontend:** getCountermeasureBenefit was bugged ([0cf94b6](https://gitlab.com/ccrpc/sset/commit/0cf94b6687e1138df4ecdc58aec85447a311ee8b))

## [1.0.0](https://gitlab.com/ccrpc/sset/compare/e84b067fa380478209c18278fb658557d04733a9...v1.0.0) (2023-05-02)


### Features

* add code to countermeasure load ([d6f3e62](https://gitlab.com/ccrpc/sset/commit/d6f3e6291a7abbb73ec1d15b9a78bfb12d48b90a))
* **app events:** a few UI improvement ([6c181f5](https://gitlab.com/ccrpc/sset/commit/6c181f58af3687ea97e453211864bcae70d9d487)), closes [#70](https://gitlab.com/ccrpc/sset/issues/70) [#74](https://gitlab.com/ccrpc/sset/issues/74) [#73](https://gitlab.com/ccrpc/sset/issues/73)
* **assets:** updated assets ([7172785](https://gitlab.com/ccrpc/sset/commit/717278568abc38372b81ff1e391943dc917387c9))
* **basemap:** changed default basemap to hybrid ([48d9b77](https://gitlab.com/ccrpc/sset/commit/48d9b77fd5912aaf25bd7f400c8914974068789e))
* **BC tool:** added BC Lib starter code ([e84b067](https://gitlab.com/ccrpc/sset/commit/e84b067fa380478209c18278fb658557d04733a9))
* **BC Tool:** foundational refactor ([cbd051a](https://gitlab.com/ccrpc/sset/commit/cbd051a016ca131b79e220b8366eead577edf066))
* **BC Tool:** implemented same rounding used in excel tool ([7187bc6](https://gitlab.com/ccrpc/sset/commit/7187bc6f664a15c87023292868d26b92b25815b9))
* **BCLib:** 4.7.4.SR.1 ([81070fd](https://gitlab.com/ccrpc/sset/commit/81070fdab7db60278bbadec70cfa4592c76be1aa))
* **BCLib:** Added bases of cost calculator ([97d310c](https://gitlab.com/ccrpc/sset/commit/97d310cce7de4fc1bed3a58df15e786bce9789d9))
* **BCLib:** Added fatalities prevented (annual and total) ([e83b3c0](https://gitlab.com/ccrpc/sset/commit/e83b3c07b81cfb8d7273dfdfc07b49084e3bd3db))
* **BCLib:** Added the 5 crash types new in 2019 ([2e54a5a](https://gitlab.com/ccrpc/sset/commit/2e54a5a3fe735ed1402241e8807f98bdab5bb85a))
* **BCLib:** basic start to cost breakdown ([ba5a9de](https://gitlab.com/ccrpc/sset/commit/ba5a9ded6dd71c310dafd8687a188e199cc197b9))
* **BCLib:** benefit calculation tests run and pass ([048b03e](https://gitlab.com/ccrpc/sset/commit/048b03eae64b5e8ebbae4349faae23e6a2a22a0a))
* **BCLib:** Calculated countermeasures ignore case ([29a6699](https://gitlab.com/ccrpc/sset/commit/29a6699ddc972668451992304339b1907b7719c0))
* **BCLib:** Flatten sideslopes rural multilane done ([373b284](https://gitlab.com/ccrpc/sset/commit/373b2848d9ddce37b1edddbc8bc3a3c5360dfba9))
* **BCLib:** Implemented 3.2.23.IU.8 ([f115dc7](https://gitlab.com/ccrpc/sset/commit/f115dc7f429cbc72e8cbc34e00b2c9af11bd7722))
* **BCLib:** Implemented stop sign  cmf values ([29ca0c6](https://gitlab.com/ccrpc/sset/commit/29ca0c6ff89ccc6977a079252533564b0dbdf749))
* **BCLib:** Implementing 4.7.7.S1.1 ([3b76e39](https://gitlab.com/ccrpc/sset/commit/3b76e39fde8cd7bcc491fee848499a6638b60071))
* **BCLib:** Put in skeleton of validate function ([241cf32](https://gitlab.com/ccrpc/sset/commit/241cf32a60597df03bf738cc7b28868dfc5318ef))
* **components:** intermediate ([82e33bf](https://gitlab.com/ccrpc/sset/commit/82e33bf3e73b337d82dd8de93bbf7f6bc4d40592))
* **countermeasure data:** added countermeasure data from curated list ([f5ff147](https://gitlab.com/ccrpc/sset/commit/f5ff1475818759dbb42d6134834077c981da5900))
* **countermeasure selection:** wip ([a352ced](https://gitlab.com/ccrpc/sset/commit/a352cedf1355bcd7dd723dbf0c8c5c20d4eafb5b))
* **crash / injury tables:** added animal crash count and section sep ([90e7afd](https://gitlab.com/ccrpc/sset/commit/90e7afd650fdb9aa597397a3a94a81754f4c0860))
* **crash info:** created crash info tables ([af808f8](https://gitlab.com/ccrpc/sset/commit/af808f85d8cb28e5e2089ecd17b69e20620684ef))
* **crash table:** added wet weather/darkness overview ([c2f6d10](https://gitlab.com/ccrpc/sset/commit/c2f6d10131d729e379ee86805fa30683dcc1c279))
* **crash table:** moved animal row into normal crash types ([620b3e7](https://gitlab.com/ccrpc/sset/commit/620b3e79b541622c27db4d9d3c3168e0cadaee3a))
* **crash/injury tables:** included rollup total ([1f67e30](https://gitlab.com/ccrpc/sset/commit/1f67e30e9762f60efacc9a71ba0f61f150bfe327))
* **data export:** added env file to hold app labels ([a0008e0](https://gitlab.com/ccrpc/sset/commit/a0008e02dc09a7aa2f3b31bf2e3e572cfb8b1643))
* **defined more form fields:** more form fields ([bf768c5](https://gitlab.com/ccrpc/sset/commit/bf768c5bd70eee5915b3fdc871b722d0d51ba3d7))
* **dependencies:** added webmapgl as dep ([d6dfbff](https://gitlab.com/ccrpc/sset/commit/d6dfbff68a74d8d19485c01b715a98f966c9c554))
* **dev:** added dev dependencies ([cefaff8](https://gitlab.com/ccrpc/sset/commit/cefaff89b4c3450b8deda21d38f7093f49e65609))
* **docx export:** completed document structure ([267a877](https://gitlab.com/ccrpc/sset/commit/267a8774cba75b3a99446ab1a30f286ab9c0aa1c))
* **docx export:** initial functionality ([14d0ca3](https://gitlab.com/ccrpc/sset/commit/14d0ca3092aee12e0c0e50f28fe17180b8c9935d))
* **docx:** Added benefit over cost ratio ([1de2fce](https://gitlab.com/ccrpc/sset/commit/1de2fce5677c1f448640706524c6dfcda8f23233))
* **docx:** Added Borders to Beneift / Cost ([109fa7f](https://gitlab.com/ccrpc/sset/commit/109fa7f9620cd817c79106d3b29d26f03a13d50a))
* **docx:** Added borders to crash severity ([f46aec8](https://gitlab.com/ccrpc/sset/commit/f46aec875789a30bfcbdc7460bf5e5c5880f8b7d))
* **docx:** Added borders to timeline ([0e1e8ba](https://gitlab.com/ccrpc/sset/commit/0e1e8baa24a7c7fba1d6002ada6810c476454e4e))
* **docx:** Added buffer to fit bounds ([148990c](https://gitlab.com/ccrpc/sset/commit/148990c68128c69c579506e7a44e9a6e3989a394))
* **docx:** Added growth factor and interest rate presets ([ce979fc](https://gitlab.com/ccrpc/sset/commit/ce979fc1b97e00b85fed85ed97d290235e4f8f71))
* **docx:** Added header to severity distribution ([4379ba7](https://gitlab.com/ccrpc/sset/commit/4379ba72730ea6e5cff9ae559fd1df95b2d88ad6))
* **docx:** Added max height to screenshots ([61fbe6b](https://gitlab.com/ccrpc/sset/commit/61fbe6b9c6cd6e366aba3fa8709c26eba35aaae5))
* **docx:** Added selecting to location map ([0a0c516](https://gitlab.com/ccrpc/sset/commit/0a0c51655da7a227088cb3acd6f935fc36e973bf))
* **docx:** Added shading to crash severity ([ab16405](https://gitlab.com/ccrpc/sset/commit/ab16405ad33f9fb549ea220bd527bd3c900920ea))
* **docx:** Added startStudyYear and endStudyYear ([54f0b0f](https://gitlab.com/ccrpc/sset/commit/54f0b0fd581ea806a7cfd76fb79fcfde7d8002fc))
* **docx:** Added total project benefit and total project cost ([400c7d7](https://gitlab.com/ccrpc/sset/commit/400c7d715e5771debfa4191114be61ce78af3a83))
* **docx:** Adding blank rows for the user to fill in ([d4606a9](https://gitlab.com/ccrpc/sset/commit/d4606a9af63b22613d7192bb1c794e0227de6640))
* **docx:** Adding countermeasure cmf ([d1ad5f3](https://gitlab.com/ccrpc/sset/commit/d1ad5f3af84c5cdb232892d90c7012b617baa883))
* **docx:** Adding lines after titles ([92bb8e8](https://gitlab.com/ccrpc/sset/commit/92bb8e8a055b50f5ba06f3cac796cb014ec8c0af))
* **docx:** Adding shading ([0edaf03](https://gitlab.com/ccrpc/sset/commit/0edaf038e41eb313ecc511699049e789a7b18443))
* **docx:** Adding weather chart ([b4e3974](https://gitlab.com/ccrpc/sset/commit/b4e3974975ed9226f76552d21046c95abef6b2dc))
* **docx:** Adjusting sizes for pictures ([5f4c807](https://gitlab.com/ccrpc/sset/commit/5f4c807add8118666f9a02f0cff101649641b730))
* **docx:** Crash Details table separated ([3575d75](https://gitlab.com/ccrpc/sset/commit/3575d75bcb40df453703a2adc56c0449a160f001))
* **docx:** Decided on colors ([5409b6d](https://gitlab.com/ccrpc/sset/commit/5409b6df5e74455eac728580e975449b80ef3221))
* **docx:** Get graph dimensions from image ([f798887](https://gitlab.com/ccrpc/sset/commit/f798887106042004b32271ed84446ce16b1e25a4))
* **docx:** Hiding segments when focusing on an intersection ([9a95004](https://gitlab.com/ccrpc/sset/commit/9a950047398302ebb4525cb50da73531d97961bd))
* **docx:** Image paragraphs ([0a30c84](https://gitlab.com/ccrpc/sset/commit/0a30c84d7bc7ba7cb5994634c878f88c7096720b))
* **docx:** Increased maxzoom and added names ([c9a4c2c](https://gitlab.com/ccrpc/sset/commit/c9a4c2cee3b2e2a9d1f47cf92d2b09949df8f8b6))
* **docx:** Intersection highlighting adjusted ([5d012a2](https://gitlab.com/ccrpc/sset/commit/5d012a2f3125373b2f59da58ce50bd0585f45a55))
* **docx:** Just adding some info ([4b7ce45](https://gitlab.com/ccrpc/sset/commit/4b7ce459c7afb916d6d9ecc711274a26da47b943))
* **docx:** Listing both segment and intersection crashes ([b6ba5ca](https://gitlab.com/ccrpc/sset/commit/b6ba5cafc73c15959f3a46738c6b43756cfb895d))
* **docx:** Location filters crashes ([71564e9](https://gitlab.com/ccrpc/sset/commit/71564e901a473e40c04b78f47077d72b8a871b5d))
* **docx:** Location map for intersections uses buffer ([d284482](https://gitlab.com/ccrpc/sset/commit/d284482c1b375731da9fc16e2f4fbc4d35160d09))
* **docx:** Location map has key ([4b8d602](https://gitlab.com/ccrpc/sset/commit/4b8d60212e9eb521b82620d9b333df93e01a496b))
* **docx:** Location map now zooms in on intersections ([eab7b66](https://gitlab.com/ccrpc/sset/commit/eab7b66a7045d5aa98d0eb9e6a73bd07d6ab618f))
* **docx:** Map returns after pictures ([6709e1b](https://gitlab.com/ccrpc/sset/commit/6709e1b6d84a03356e88e4606b9f64595fc5c4b1))
* **docx:** Prefill District with 5 ([6256f0b](https://gitlab.com/ccrpc/sset/commit/6256f0b3aa0f881a0e42843ef52af1c43da05c44))
* **docx:** Preliminary charts ([1aca6c3](https://gitlab.com/ccrpc/sset/commit/1aca6c37c9243ad7f8987fadc92790827c070efc))
* **docx:** presentWorth and euac for same counter measure ([364abe0](https://gitlab.com/ccrpc/sset/commit/364abe052049c0cbea0fdcfef6d6609cf87217e5))
* **docx:** Returning AffectedCrashTypes in doc ([aa23de8](https://gitlab.com/ccrpc/sset/commit/aa23de8f83ed6fface49584e880585a92d460521))
* **docx:** Segments for screenshot outline only ([0c5568a](https://gitlab.com/ccrpc/sset/commit/0c5568ab5baae97ac0a5206f78fe1e5758eb481d))
* **docx:** Shading tasks ([1157845](https://gitlab.com/ccrpc/sset/commit/1157845b15b2ab4ca04af77e06af02d8f40d585c))
* **docx:** Shortened legend, gave more padding ([c0974a9](https://gitlab.com/ccrpc/sset/commit/c0974a9b92896fc86fcf5bde9f72dad3a75c05a6))
* **docx:** Showing presentWorth and EUAC cost ([e6c7dd2](https://gitlab.com/ccrpc/sset/commit/e6c7dd24343bd54306e70ecafa53caa960a6248a))
* **docx:** Start of location map ([dd4763a](https://gitlab.com/ccrpc/sset/commit/dd4763a8a3fe7c0a5d87032159f014df062ba966))
* **docx:** Starting crash severity borders ([cea80df](https://gitlab.com/ccrpc/sset/commit/cea80dfb9905fed2c09c4b457fd35bcbb10e74a4))
* **docx:** Starting timeline table outline ([090e0c9](https://gitlab.com/ccrpc/sset/commit/090e0c91a042087297120324f9f1d582ea29d446))
* **docx:** The docx now uses the costCalculator ([7b434c7](https://gitlab.com/ccrpc/sset/commit/7b434c77d410b7a467035720c0a75a17501389a4))
* **docx:** Using toBounds radius in segments ([f07bbe7](https://gitlab.com/ccrpc/sset/commit/f07bbe7913e623e228c98eb004a03617628fa3aa))
* **docx:** Zooming and basic screenshot ([fb7a404](https://gitlab.com/ccrpc/sset/commit/fb7a404981f8255ce84f31dc9cb271e8c7307b98))
* **drawer:** reimplemented drawer ([3768f5d](https://gitlab.com/ccrpc/sset/commit/3768f5d3da267441bf33e7937218f9fb41a22073))
* **export:** added logic to field load ([0508ba0](https://gitlab.com/ccrpc/sset/commit/0508ba0210504d8e839340ca19dbc8e2757fe25d))
* **feature style:** color-coded features based on # of risk factors ([a26540f](https://gitlab.com/ccrpc/sset/commit/a26540fbb6ab06d358c7a2ef36dc74c886dc6dd1))
* **frontend:** Added 4.8.3.S1.1's ability to define service life ([b089cb9](https://gitlab.com/ccrpc/sset/commit/b089cb9953304ca7242e8816aa8dba292cb36626))
* **frontend:** Added padding to ion-select options ([945043d](https://gitlab.com/ccrpc/sset/commit/945043da3ce796131e45b8fee10a778391a606ee))
* **frontend:** Added recalculate button ([9d71662](https://gitlab.com/ccrpc/sset/commit/9d7166264f0e7a2469e9acda49f8de7eed9373a5))
* **frontend:** Added user input display ([7ab9758](https://gitlab.com/ccrpc/sset/commit/7ab9758dee9ee0b6ea821dcabbe93aa4dd06a005))
* **frontend:** Adding more calculations ([773e2e7](https://gitlab.com/ccrpc/sset/commit/773e2e79211d135dc99ba3cab6fd1fec65824777))
* **frontend:** Automatic AADT and setup for choose ([8f3bf66](https://gitlab.com/ccrpc/sset/commit/8f3bf661cebcc1deb17da0aeeab39d78ced60403))
* **frontend:** Basic AADT Selection ([69827de](https://gitlab.com/ccrpc/sset/commit/69827de238faa0d02f512cdbbd9fa113112b11ea))
* **frontend:** basic calculatedCountermeasure info ([c7ab132](https://gitlab.com/ccrpc/sset/commit/c7ab132e54b6d233539bd17fb5fd0927da2da1e7))
* **frontend:** Basic user countermeasure input ([02093bf](https://gitlab.com/ccrpc/sset/commit/02093bf957e1bee60097b5b92a05b7201d4b0b1f))
* **frontend:** Clicking on feature in left menu selects it ([cd33a33](https://gitlab.com/ccrpc/sset/commit/cd33a335ef44c75ccfb633091452be4ebda62b61))
* **frontend:** Closing drawer before exporting doc ([64de0d7](https://gitlab.com/ccrpc/sset/commit/64de0d7521117de54972c9040a2a05c31fd7f3cf))
* **frontend:** Countermeasure options are filtered by RoadwayType ([867c7d5](https://gitlab.com/ccrpc/sset/commit/867c7d5224baf6e7a6a38dfbb392e510327d794a))
* **frontend:** Countermeasure options display category ([bb0b8de](https://gitlab.com/ccrpc/sset/commit/bb0b8de271f39975a02e2e71fad03c2e1eb279f5))
* **frontend:** Countermeasures have own benefit over cost ([a117027](https://gitlab.com/ccrpc/sset/commit/a117027d4f13fcc4a8ada1cbff2e47691b33d274))
* **frontend:** DIsable rotation ([722860b](https://gitlab.com/ccrpc/sset/commit/722860b6bed0144d96432be7d18f3ec5ed4d8757))
* **frontend:** Finished calculated metadata ([24feb7c](https://gitlab.com/ccrpc/sset/commit/24feb7c0d0ae0fd40066932ad44fdf992c29bc2f))
* **frontend:** Finished getting macro cases ([9797196](https://gitlab.com/ccrpc/sset/commit/97971963f168ae48fc3bc129a092b5bc2b18ca6e))
* **frontend:** Frontend now looks at tiles served on currrent url ([4b9b68a](https://gitlab.com/ccrpc/sset/commit/4b9b68a110fc4ed43b1dc658ee7d21729c273ad3))
* **frontend:** Local segments finished ([08f2554](https://gitlab.com/ccrpc/sset/commit/08f255480d65025198d82ba578c18a97ed4f5800))
* **frontend:** More calculations written ([b669d3c](https://gitlab.com/ccrpc/sset/commit/b669d3c1b79152338960309af3c5694667ca629f))
* **frontend:** Segment zoom to avoid unrendered crashes and array ([e3beecc](https://gitlab.com/ccrpc/sset/commit/e3beecc3ba93f60be46ddeb7b1505520c71adf36))
* **frontend:** Segments and intersections are now below labels ([933675f](https://gitlab.com/ccrpc/sset/commit/933675f9b1301836459ca6543d981964d8374e44))
* **frontend:** select options cleanup ([21c5349](https://gitlab.com/ccrpc/sset/commit/21c53492c825eabef8bbd81b8210cd01fd436377))
* **frontend:** Selected Line more visible ([5c08129](https://gitlab.com/ccrpc/sset/commit/5c0812920d54dff0a971c49c74b9aae8bb2575bd))
* **frontend:** Selected segment is highlighted via outline ([0f8f485](https://gitlab.com/ccrpc/sset/commit/0f8f4856b79a632c07e73f2b1862fcc895bc3400))
* **frontend:** Start of calculated counter measures list ([cec6f24](https://gitlab.com/ccrpc/sset/commit/cec6f241146fa41e0d872fb76b679112fae2154a))
* **graph api:** created starter API ([652b441](https://gitlab.com/ccrpc/sset/commit/652b44113536599a150b967966330c4d923df49e))
* **infra:** added secret example ([8870801](https://gitlab.com/ccrpc/sset/commit/887080123f981c73c5b886acc83627bd64a10052))
* **infra:** Added standard network policy ([961f498](https://gitlab.com/ccrpc/sset/commit/961f49808fe5d5b3bd191d3085f646a1f539a8ef))
* **infra:** Adding postgis secret ([228813a](https://gitlab.com/ccrpc/sset/commit/228813ac94d782d032fb623a39e8ef1d3f701212))
* **infra:** Adding registry pull secret and example ([a9fcf84](https://gitlab.com/ccrpc/sset/commit/a9fcf846973717244f74056fc7ee0d8224b90bbb))
* **infra:** Converting from loop to kustomize ([f2200a7](https://gitlab.com/ccrpc/sset/commit/f2200a744a73a6efe2072dab84ed13f53fc7bdec))
* **infra:** Skaffold file ([0c1e7e0](https://gitlab.com/ccrpc/sset/commit/0c1e7e0a72e150897c8a8eedcb4bf5a5eae84002))
* **infra:** skaffold profile to run the upload job ([ad4e0e3](https://gitlab.com/ccrpc/sset/commit/ad4e0e3db5da8b189cf0baa77c5f71affc9c80d8))
* **intermediate:** fixed labels ([4299ecf](https://gitlab.com/ccrpc/sset/commit/4299ecfe0e62079932ef9bcfd537361128eba7fc))
* **layers:** separated layer theme into assets file ([04855ba](https://gitlab.com/ccrpc/sset/commit/04855bae1a5a0a2ec39cc922767e513e1b70bebd))
* **map features:** highlight features when clicked ([a983c48](https://gitlab.com/ccrpc/sset/commit/a983c483e98540a995e2bb9b4913246f0f3d1152)), closes [#21](https://gitlab.com/ccrpc/sset/issues/21)
* **map:** first working build of map ([9f10eb8](https://gitlab.com/ccrpc/sset/commit/9f10eb80904f52561044b642e9abe641c7b4e7f7))
* **massive:** massive ([35107d9](https://gitlab.com/ccrpc/sset/commit/35107d9e7b3ab7ccdf092166bb8cfe0fc535a189))
* **modified export scripts:** modified export scripts ([3511c38](https://gitlab.com/ccrpc/sset/commit/3511c3888129a7dcbb8945bd320966eb518e8655))
* **overhaul:** sweeping changes in app flow ([0af2a10](https://gitlab.com/ccrpc/sset/commit/0af2a10be96a784a73cefab2d8bdde695d606687))
* **page traversal:** implemented routing system with persistent globals ([06e3e9d](https://gitlab.com/ccrpc/sset/commit/06e3e9dc051459ac4cedc15f7e66a86b29ff3569))
* **python:** removed python cache ([2d78f41](https://gitlab.com/ccrpc/sset/commit/2d78f4178130427800726fb0ff326f5d6857893c))
* **refactored study group list:** now consistent with global state ([56f1398](https://gitlab.com/ccrpc/sset/commit/56f1398764205021af7a6e82b9c7d6a85097e151))
* **refactor:** sweeping refactor and bc integration ([7f12a66](https://gitlab.com/ccrpc/sset/commit/7f12a663e0b88ff583c994aeb09f4f814639d7b7))
* **risk factor table:** created risk factor table ([430df8f](https://gitlab.com/ccrpc/sset/commit/430df8f7d4e997dd4d062fae655b8ed0f92b8164))
* **roadway info:** created table that displays feature information ([7491a10](https://gitlab.com/ccrpc/sset/commit/7491a104922782940d03fbb19fda3b7e45da0cf8))
* **stencil store:** added store to enforce rerendering on state change ([6cde45d](https://gitlab.com/ccrpc/sset/commit/6cde45d2913318d1a435568608caf673c27c7e7b))
* **study group:** added study group / stats panel ([0a2033f](https://gitlab.com/ccrpc/sset/commit/0a2033f9d465c5abc69ae8a306aee878ee202b0d))
* **study-group-list:** hooked up events emitters and receivers ([5f453f6](https://gitlab.com/ccrpc/sset/commit/5f453f6cc49641674d035ee34e03c4343094606f))
* **style, deploy:** tweaked style and made deployment ([b1c63a0](https://gitlab.com/ccrpc/sset/commit/b1c63a00a2c05ac8d686174a228dfd5975bbbeaf))
* **style:** applied cubic bezier scaling to features ([f23d295](https://gitlab.com/ccrpc/sset/commit/f23d2954cec6c4b0d29c685641d96ad13bb2d416))
* **style:** fixed highlighting bug ([99320be](https://gitlab.com/ccrpc/sset/commit/99320be8bdaa9e881350623f09cd7acae1c3fe27))
* **style:** many changes ([d75e6c0](https://gitlab.com/ccrpc/sset/commit/d75e6c0465da8a29405206410101efc0472cf4b0))
* **style:** text aligned ([424c428](https://gitlab.com/ccrpc/sset/commit/424c4285da320d91c7ac7b2c72bdc17a3c740be5))
* **Tiles:** Added Tile export process ([53dd592](https://gitlab.com/ccrpc/sset/commit/53dd5927a1aa18b03664e0f623a9198bcbc5adc3))
* **tiles:** tweaked zoom thresholds and filtered crashes ([357f443](https://gitlab.com/ccrpc/sset/commit/357f443fb75c73802af3002c1a2b15a52761899c))
* **ui:** basic start to ui ([c308afa](https://gitlab.com/ccrpc/sset/commit/c308afabcfd11d54af813f71c610ed81fbb58322))
* **ui:** created tab interface in drawer component ([402246e](https://gitlab.com/ccrpc/sset/commit/402246e3a29f080554a348c4e21e5753c67ff85a))
* **upkeep:** small changes ([155fc21](https://gitlab.com/ccrpc/sset/commit/155fc21fd3832d0bd94398f15c366faa87bb5208))


### Bug Fixes

* **BC Tool:** Fixed bugs in library ([b2dd3cf](https://gitlab.com/ccrpc/sset/commit/b2dd3cfd6cf8a8411a102b4536c5b67eed5b82d9))
* **BCLib:** Benefit test now accurate ([87cede0](https://gitlab.com/ccrpc/sset/commit/87cede071a2762eb6555fc5b3fb311ea4efe2c56))
* **BCLib:** calculateCombinedCMF was changing default ([c793f25](https://gitlab.com/ccrpc/sset/commit/c793f25546a8947a1376b433cce68c1ad91fd7f2))
* **BCLib:** CMF < 0.6 now correct ([52987fa](https://gitlab.com/ccrpc/sset/commit/52987faac00f5b6e88bd9e68cbfdbeed9ea37b0f))
* **BCLib:** costCalculator was using ^ instead of Math.pow ([f7301ef](https://gitlab.com/ccrpc/sset/commit/f7301ef656820369a59e126a3a3a8f8fee89f310))
* **BCLib:** Fixed mispelling of default ([e7a5f41](https://gitlab.com/ccrpc/sset/commit/e7a5f410a0109f4c9f3716e2fb868f70f9eed191))
* **BCLib:** Fixed more spelling of defaultVector ([03ad9ee](https://gitlab.com/ccrpc/sset/commit/03ad9eeeaeb4565748c6fb2beb49eccf4881ed95))
* **BCLib:** Fixed spelling error of default vector ([16773de](https://gitlab.com/ccrpc/sset/commit/16773dee52c44c371b420059b8446c26cf885cd6))
* **BCLib:** The spreadsheet uses banker's rounding ([14095cc](https://gitlab.com/ccrpc/sset/commit/14095ccad3adbff424c9438675f92e2a9c95164f))
* **crash layer:** fixed duplication issue ([bae6349](https://gitlab.com/ccrpc/sset/commit/bae6349ea3bef7dfe0b958eed94fb634d6a37afd))
* **crash table:** fixed alignment bug ([6c02f17](https://gitlab.com/ccrpc/sset/commit/6c02f17ce08208e68834aecb0191c074de26c60d))
* **date:** fixed default date ([97061e9](https://gitlab.com/ccrpc/sset/commit/97061e92b8c8d48f24443b21b651f5c4d36eda92))
* **docx:** getCrashSevirty was failing ([45e18df](https://gitlab.com/ccrpc/sset/commit/45e18dfb936682ab25586dbed28564a450cd94a0))
* **docx:** Ignore unknown crash types in crash total ([c0cd7ff](https://gitlab.com/ccrpc/sset/commit/c0cd7ff93fdb5d301d7892fecc399896094c62e2))
* **docx:** PDO Was mapping to Fatalities ([2fe46f2](https://gitlab.com/ccrpc/sset/commit/2fe46f226fd3bcb3d00315a02c78b64c3ea756a0))
* **docx:** Removing unnecessary import ([8c552dc](https://gitlab.com/ccrpc/sset/commit/8c552dccf66787b964c894faf9a69af5bd668e43))
* **fixed bug in doc export:** fixed bug in doc export ([cda3bbc](https://gitlab.com/ccrpc/sset/commit/cda3bbc1c7444849d68d1383bdfadf223513c666))
* **frontend:** Calculating cost via CostCalculator ([19c6947](https://gitlab.com/ccrpc/sset/commit/19c6947f91c1d9affa72b1e0059b9e4daec950e1))
* **frontend:** Checking for Number.isNan ([21922de](https://gitlab.com/ccrpc/sset/commit/21922de1905dc337b242ac1e8cbb418e3114962e))
* **frontend:** Fixing ion-label where possible ([a158f13](https://gitlab.com/ccrpc/sset/commit/a158f13ad4bd57d2c8c86a6779761c2da2aed1f7))
* **frontend:** infraIdLabel was not populating on intersections ([1a12859](https://gitlab.com/ccrpc/sset/commit/1a12859b21b2a853fe10357b9497dc839de3dae9))
* **frontend:** Intersection counter measure cost calculation ([afcd8fb](https://gitlab.com/ccrpc/sset/commit/afcd8fbd5f376691560b0513012855ed38b3b5f0))
* **frontend:** using EAUC in list ([47a573c](https://gitlab.com/ccrpc/sset/commit/47a573c5a9e0a0a958d26aeb89a906d8ec410f44))
* **injury table:** nenamed and reordered fatality column ([c2a87f7](https://gitlab.com/ccrpc/sset/commit/c2a87f7bdceb6c6fa2f95a18d73616ba0d516698))
* **test:** capped CMF to 0.6 ([8031449](https://gitlab.com/ccrpc/sset/commit/8031449158f60ff058de19890225a9307286db2a))
* **tiles:** Export scripts now import and use labels.env ([48ee257](https://gitlab.com/ccrpc/sset/commit/48ee2570221bd9b88abcdcb0e33ec5b0ced87b9c))
