import { Chart, registerables } from "chart.js";
import { Crash } from "../BCLIb/models/classes/Crash";
import { ProjectElements } from "../BCLIb/models/types";
import { IRoadwayElement } from "../BCLIb/models/interfaces";
import { RoadwayType } from "../BCLIb/models/enums";
import { Severity } from "../BCLIb/models/enums";

Chart.register(...registerables);

export function lightChart(proj: ProjectElements, type: RoadwayType): string {
  const test_array: any[] = proj
    .get(type)
    .flatMap((roadwayElem: IRoadwayElement) =>
      roadwayElem.crashes.map((crash) => crash.lightCondition)
    );
  const allinput: string[] = [
    // similar lighting condition name
    "Dawn",
    "Unknown",
    "Darkness / Lighted Road",
    "Daylight",
    "Dusk",
    "Darkness",
  ];
  const countMap: Map<string, number> = new Map<string, number>();

  test_array.forEach((item: string) => {
    if (countMap.has(item)) {
      countMap.set(item, countMap.get(item) + 1);
    } else {
      countMap.set(item, 1);
    }
  });

  const percentageArray: number[] = allinput.map((item: string) => {
    const count: number = countMap.get(item) || 0;
    return (count / test_array.length) * 100;
  });
  const ctx = document.createElement("canvas");
  document.documentElement.appendChild(ctx);
  ctx.setAttribute("width", "400");
  ctx.setAttribute("height", "400");
  const chart = new Chart(ctx, {
    type: "pie",
    data: {
      labels: [
        "Dawn",
        "Unknown",
        "Darkness / Lighted Road",
        "Daylight",
        "Dusk",
        "Darkness",
      ],
      datasets: [
        {
          label: "Percentage of crashes by road light condition",
          data: percentageArray,
          backgroundColor: [
            "rgba(255, 99, 132, 0.8)",
            "rgba(54, 162, 235, 0.8)",
            "rgba(255, 206, 86, 0.8)",
            "rgba(75, 192, 192, 0.8)",
            "rgba(153, 102, 255, 0.8)",
            "rgba(255, 159, 64, 0.8)",
          ],
          borderColor: [
            "rgba(255, 99, 132, 0.8)",
            "rgba(54, 162, 235, 0.8)",
            "rgba(255, 206, 86, 0.8)",
            "rgba(75, 192, 192, 0.8)",
            "rgba(153, 102, 255, 0.8)",
            "rgba(255, 159, 64, 0.8)",
          ],
          borderWidth: 1,
        },
      ],
    },
    options: {
      responsive: false,
      animation: false,
    },
  });

  const base64 = chart.toBase64Image();

  ctx.remove();

  return base64;
}
function getType(crash: Crash): Severity {
  if (crash.injuryProfile.get(Severity.F) > 0) return Severity.F;
  if (crash.injuryProfile.get(Severity.A) > 0) return Severity.A;
  if (crash.injuryProfile.get(Severity.B) > 0) return Severity.B;
  if (crash.injuryProfile.get(Severity.C) > 0) return Severity.C;
  return Severity.N;
}
