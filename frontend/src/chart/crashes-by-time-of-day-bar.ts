import { Chart, registerables } from "chart.js";
import { Crash } from "../BCLIb/models/classes/Crash";
import { ProjectElements } from "../BCLIb/models/types";
import { IRoadwayElement } from "../BCLIb/models/interfaces";
import { RoadwayType } from "../BCLIb/models/enums";
import { Severity } from "../BCLIb/models/enums";

Chart.register(...registerables);

export function daytimeChart(proj: ProjectElements, type: RoadwayType): string {
  const test_array: any[] = proj
    .get(type)
    .flatMap((roadwayElem: IRoadwayElement) =>
      roadwayElem.crashes.map((crash) => crash.crashHour)
    );

  const ctx = document.createElement("canvas");
  document.documentElement.appendChild(ctx);
  ctx.setAttribute("width", "800");
  ctx.setAttribute("height", "400");
  const chart = new Chart(ctx, {
    type: "bar",
    data: {
      labels: [...new Array(24).keys()].map((month) => String(month)),
      datasets: [
        {
          label: "Number of crashes by day time",
          data: [...new Array(24).keys()].map(
            (month) => test_array.filter((value) => value === month).length
          ),

          backgroundColor: [
            "rgba(255, 99, 132, 0.8)",
            "rgba(54, 162, 235, 0.8)",
            "rgba(255, 206, 86, 0.8)",
            "rgba(75, 192, 192, 0.8)",
            "rgba(153, 102, 255, 0.8)",
            "rgba(255, 159, 64, 0.8)",
            "rgba(255, 99, 132, 0.6)",
            "rgba(54, 162, 235, 0.6)",
            "rgba(255, 206, 86, 0.6)",
            "rgba(75, 192, 192, 0.6)",
            "rgba(153, 102, 255, 0.6)",
            "rgba(255, 159, 64, 0.6)",
            "rgba(255, 99, 132, 0.4)",
            "rgba(54, 162, 235, 0.4)",
            "rgba(255, 206, 86, 0.4)",
            "rgba(75, 192, 192, 0.4)",
            "rgba(153, 102, 255, 0.4)",
            "rgba(255, 159, 64, 0.4)",
            "rgba(255, 99, 132, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
          ],
          borderColor: [
            "rgba(255, 99, 132, 0.8)",
            "rgba(54, 162, 235, 0.8)",
            "rgba(255, 206, 86, 0.8)",
            "rgba(75, 192, 192, 0.8)",
            "rgba(153, 102, 255, 0.8)",
            "rgba(255, 159, 64, 0.8)",
            "rgba(255, 99, 132, 0.6)",
            "rgba(54, 162, 235, 0.6)",
            "rgba(255, 206, 86, 0.6)",
            "rgba(75, 192, 192, 0.6)",
            "rgba(153, 102, 255, 0.6)",
            "rgba(255, 159, 64, 0.6)",
            "rgba(255, 99, 132, 0.4)",
            "rgba(54, 162, 235, 0.4)",
            "rgba(255, 206, 86, 0.4)",
            "rgba(75, 192, 192, 0.4)",
            "rgba(153, 102, 255, 0.4)",
            "rgba(255, 159, 64, 0.4)",
            "rgba(255, 99, 132, 0.2)",
            "rgba(54, 162, 235, 0.2)",
            "rgba(255, 206, 86, 0.2)",
            "rgba(75, 192, 192, 0.2)",
            "rgba(153, 102, 255, 0.2)",
            "rgba(255, 159, 64, 0.2)",
          ],
          borderWidth: 1,
        },
      ],
    },
    options: {
      responsive: false,
      animation: false,
    },
  });

  const base64 = chart.toBase64Image();

  ctx.remove();

  return base64;
}
function getType(crash: Crash): Severity {
  if (crash.injuryProfile.get(Severity.F) > 0) return Severity.F;
  if (crash.injuryProfile.get(Severity.A) > 0) return Severity.A;
  if (crash.injuryProfile.get(Severity.B) > 0) return Severity.B;
  if (crash.injuryProfile.get(Severity.C) > 0) return Severity.C;
  return Severity.N;
}
