import { Chart, registerables } from "chart.js";
import { Crash } from "../BCLIb/models/classes/Crash";
import { ProjectElements } from "../BCLIb/models/types";
import { IRoadwayElement } from "../BCLIb/models/interfaces";
import { RoadwayType } from "../BCLIb/models/enums";
import { Severity } from "../BCLIb/models/enums";
Chart.register(...registerables);
export function lineChart(proj: ProjectElements, type: RoadwayType): string {
  const test_array: any[] = proj
    .get(type)
    .flatMap((roadwayElem: IRoadwayElement) =>
      roadwayElem.crashes.map((crash) => crash.crashYear)
    );
  const ctx = document.createElement("canvas");
  document.documentElement.appendChild(ctx);
  ctx.setAttribute("width", "400");
  ctx.setAttribute("height", "400");
  const chart = new Chart(ctx, {
    type: "line",
    data: {
      labels: ["2019", "2020", "2021", "2022", "2023"],
      datasets: [
        {
          label: "Number of crashes by year",
          data: [
            test_array.filter((value) => value === 2019).length,
            test_array.filter((value) => value === 2020).length,
            test_array.filter((value) => value === 2021).length,
            test_array.filter((value) => value === 2022).length,
            test_array.filter((value) => value === 2023).length,
          ],
          backgroundColor: [
            "rgba(255, 99, 132, 0.8)",
            "rgba(54, 162, 235, 0.8)",
            "rgba(255, 206, 86, 0.8)",
            "rgba(75, 192, 192, 0.8)",
            "rgba(153, 102, 255, 0.8)",
          ],
          borderColor: [
            "rgba(255, 99, 132, 0.8)",
            "rgba(54, 162, 235, 0.8)",
            "rgba(255, 206, 86, 0.8)",
            "rgba(75, 192, 192, 0.8)",
            "rgba(153, 102, 255, 0.8)",
          ],
          borderWidth: 1,
        },
      ],
    },
    options: {
      responsive: false,
      animation: false,
    },
  });

  const base64 = chart.toBase64Image();

  ctx.remove();

  return base64;
}
function getType(crash: Crash): Severity {
  if (crash.injuryProfile.get(Severity.F) > 0) return Severity.F;
  if (crash.injuryProfile.get(Severity.A) > 0) return Severity.A;
  if (crash.injuryProfile.get(Severity.B) > 0) return Severity.B;
  if (crash.injuryProfile.get(Severity.C) > 0) return Severity.C;
  return Severity.N;
}
