import { Chart, ChartConfiguration, registerables } from "chart.js";
import { Crash } from "../BCLIb/models/classes/Crash";
import { ProjectElements } from "../BCLIb/models/types";
import { IRoadwayElement } from "../BCLIb/models/interfaces";
import { RoadwayType } from "../BCLIb/models/enums";
import { Severity } from "../BCLIb/models/enums";
Chart.register(...registerables);
export function weatherChart(proj: ProjectElements, type: RoadwayType): string {
  const test_array: any[] = proj
    .get(type)
    .flatMap((roadwayElem: IRoadwayElement) =>
      roadwayElem.crashes.map((crash) => crash.weatherCondition)
    );
  const allinput: string[] = [
    "Other",
    "Unknown",
    "Snow",
    "Severe Cross Wind",
    "Sleet/Hail",
    "Freezing Rain or Freezing Drizzle",
    "Cloudy/Overcast",
    "Clear",
    "Rain",
    "Fog/Smoke/Haze",
    "Blowing Snow",
  ];
  const countMap: Map<string, number> = new Map<string, number>();
  test_array.forEach((item: string) => {
    if (item === "Freezing Rain") {
      item = "Freezing Rain or Freezing Drizzle";
    }
    if (countMap.has(item)) {
      countMap.set(item, countMap.get(item) + 1);
    } else {
      countMap.set(item, 1);
    }
  });
  const percentageArray: number[] = allinput.map((item: string) => {
    const count: number = countMap.get(item) || 0;
    return (count / test_array.length) * 100;
  });

  const ctx = document.createElement("canvas");
  document.documentElement.appendChild(ctx);
  ctx.setAttribute("width", "400");
  ctx.setAttribute("height", "400");
  const chart = new Chart(ctx, {
    type: "pie",
    data: {
      labels: [
        "Other",
        "Unknown",
        "Snow",
        "Severe Cross Wind",
        "Sleet/Hail",
        "Freezing Rain or Freezing Drizzle",
        "Cloudy/Overcast",
        "Clear",
        "Rain",
        "Fog/Smoke/Haze",
        "Blowing Snow",
      ],
      datasets: [
        {
          label: "Percentage of crashes by road weather condition",
          data: percentageArray,
          backgroundColor: [
            "rgba(255, 0, 0, 0.8)", // bright red //1
            "rgba(0, 255, 255, 0.8)", // bright cyan //2
            "rgba(255, 200, 0, 0.8)", // bright yellow-orange //3
            "rgba(0, 128, 255, 0.8)", // medium blue //4
            "rgba(255, 102, 255, 0.8)", // bright magenta //5
            "rgba(255, 128, 0, 0.8)", // bright orange //6
            "rgba(255, 0, 0, 0.6)", // bright red (lighter) //7
            "rgba(0, 255, 255, 0.2)", // bright cyan (lighter) //8
            "rgba(255, 200, 200, 0.6)", // light pink //9
            "rgba(0, 128, 128, 0.6)", // dark teal //10
            "rgba(153, 0, 255, 0.6)", // deep purple //11
            "rgba(255, 64, 0, 0.6)", // bright orange (darker) //12
          ],
          borderColor: [
            "rgba(255, 0, 0, 0.8)", // bright red
            "rgba(0, 255, 255, 0.8)", // bright cyan
            "rgba(255, 200, 0, 0.8)", // bright yellow-orange
            "rgba(0, 128, 255, 0.8)", // medium blue
            "rgba(255, 102, 255, 0.8)", // bright magenta
            "rgba(255, 128, 0, 0.8)", // bright orange
            "rgba(255, 0, 0, 0.6)", // bright red (lighter)
            "rgba(0, 255, 255, 0.6)", // bright cyan (lighter)
            "rgba(255, 200, 200, 0.6)", // light pink
            "rgba(0, 128, 128, 0.6)", // dark teal
            "rgba(153, 0, 255, 0.6)", // deep purple
            "rgba(255, 64, 0, 0.6)", // bright orange (darker)
          ],
          borderWidth: 1,
        },
      ],
    },
    options: {
      responsive: false,
      animation: false,
    },
  });

  const base64 = chart.toBase64Image();
  ctx.remove();
  return base64;
}
function getType(crash: Crash): Severity {
  if (crash.injuryProfile.get(Severity.F) > 0) return Severity.F;
  if (crash.injuryProfile.get(Severity.A) > 0) return Severity.A;
  if (crash.injuryProfile.get(Severity.B) > 0) return Severity.B;
  if (crash.injuryProfile.get(Severity.C) > 0) return Severity.C;
  return Severity.N;
}
