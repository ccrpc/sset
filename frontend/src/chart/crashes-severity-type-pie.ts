import { Chart, registerables } from "chart.js";
import { Crash } from "../BCLIb/models/classes/Crash";
import { ProjectElements } from "../BCLIb/models/types";
import { IRoadwayElement } from "../BCLIb/models/interfaces";
import { RoadwayType } from "../BCLIb/models/enums";
import { Severity } from "../BCLIb/models/enums";
Chart.register(...registerables);

export function pieChart(proj: ProjectElements, type: RoadwayType): string {
  const segmentCrashes: Crash[] = proj
    .get(RoadwayType.Segment)
    .map((element: IRoadwayElement) => element.crashes)
    .flat();
  const types = proj
    .get(type)
    .flatMap((roadwayElem: IRoadwayElement) =>
      roadwayElem.crashes.flatMap((crash: Crash) => [getType(crash)])
    );
  const test_array: any[] = [
    Severity.F,
    Severity.A,
    Severity.B,
    Severity.C,
    Severity.N,
  ].map((p: Severity) => types.filter((type) => type === p).length);

  const ctx = document.createElement("canvas");
  document.documentElement.appendChild(ctx);
  ctx.setAttribute("width", "400");
  ctx.setAttribute("height", "400");
  const chart = new Chart(ctx, {
    type: "pie",
    data: {
      labels: [
        "Total Fatalities",
        "Total A-injuries",
        "Total B-injuries",
        "Total C-injuries",
        "Total Non-injuries",
      ],
      datasets: [
        {
          label: "# of Votes",
          data: test_array,
          backgroundColor: [
            "rgba(255, 0, 0, 0.8)", // bright red //1
            "rgba(0, 255, 255, 0.8)", // bright cyan //2
            "rgba(255, 200, 0, 0.8)", // bright yellow-orange //3
            "rgba(0, 128, 255, 0.8)", // medium blue //4
            "rgba(255, 102, 255, 0.8)", // bright magenta //5
          ],
          borderColor: [
            "rgba(255, 0, 0, 0.8)", // bright red //1
            "rgba(0, 255, 255, 0.8)", // bright cyan //2
            "rgba(255, 200, 0, 0.8)", // bright yellow-orange //3
            "rgba(0, 128, 255, 0.8)", // medium blue //4
            "rgba(255, 102, 255, 0.8)", // bright magenta //5
          ],
          borderWidth: 1,
        },
      ],
    },
    options: {
      responsive: false,
      animation: false,
    },
  });

  const base64 = chart.toBase64Image();

  ctx.remove();

  return base64;
}

function getType(crash: Crash): Severity {
  if (crash.injuryProfile.get(Severity.F) > 0) return Severity.F;
  if (crash.injuryProfile.get(Severity.A) > 0) return Severity.A;
  if (crash.injuryProfile.get(Severity.B) > 0) return Severity.B;
  if (crash.injuryProfile.get(Severity.C) > 0) return Severity.C;
  return Severity.N;
}
