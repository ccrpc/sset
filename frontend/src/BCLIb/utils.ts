import { Countermeasure } from "./models/classes/Countermeasure";
import { Crash } from "./models/classes/Crash";
import {
  CrashType,
  Severity,
  CrashInjuryType,
  ConditionType,
} from "./models/enums";
import { CmfVector, CrashTable } from "./models/types";

/**
 * Getting the short-version label from the full name of the crash type.
 * @param {CrashType} ct the type of crash
 */
export function labelToCode(ct: CrashType): string {
  const lookup = new Map([
    [CrashType.AG, "AG"],
    [CrashType.AN, "AN"],
    [CrashType.FO, "FO"],
    [CrashType.LT, "LT"],
    [CrashType.NGT, "NGT"],
    [CrashType.OVT, "OVT"],
    [CrashType.OtherNC, "OtherNC"],
    [CrashType.OtherO, "OtherO"],
    [CrashType.PD, "PD"],
    [CrashType.PDC, "PDC"],
    [CrashType.PKV, "PKV"],
    [CrashType.ROR, "ROR"],
    [CrashType.RT, "RT"],
    [CrashType.SOD, "SOD"],
    [CrashType.SSD, "SSD"],
    [CrashType.T, "T"],
    [CrashType.TR, "TR"],
    [CrashType.WP, "WP"],
    [CrashType.FTF, "FTF"],
    [CrashType.FTR, "FTR"],
    [CrashType.RTF, "RTF"],
    [CrashType.RTR, "RTR"],
    [CrashType.RTS, "RTS"],
  ]);

  return lookup.get(ct);
}

export function incrementDueToTrafficGrowth(
  P: number,
  i: number,
  n: number
): number {
  return (
    P /
    (Math.pow(1 + i, n) - 1) /
    (i * i * Math.pow(1 + i, n) - n / (i * Math.pow(1 + i, n)))
  );
}

export function annualizedGradiant(G: number, i: number, n: number): number {
  return G * (1 / i - n / (Math.pow(1 + i, n) - 1));
}

const crashOrder: CrashType[] = [
  CrashType.AG,
  CrashType.AN,
  CrashType.FO,
  CrashType.FTF,
  CrashType.LT,
  CrashType.OtherNC,
  CrashType.OtherO,
  CrashType.OVT,
  CrashType.PD,
  CrashType.PDC,
  CrashType.PKV,
  CrashType.FTR,
  CrashType.RT,
  CrashType.SSD,
  CrashType.SOD,
  CrashType.T,
  CrashType.TR,
];

/**
 * /**
 * For each crash type in a, multiply it by the cmf of the same crash type in b.
 * If either a or b does not have that CMF, treat that value as 1 for the multiplication (aka do not factor it into the result)
 *
 * For example say a is <AG: 0.9, LT: 0.8, PD: 0.7> and b is <AG: 0.7, LT: 0.6>, you get  <AG: 0.9 * 0.7, LT: 0.8 * 0.6, PD: 0.7 * 1> = <AG: 0.63, LT: 0.48, PD: 0.7>
 *
 * Currently if b has a value that is not in a, it is ignored. Aka if a is <AG: 0.7, LT: 0.6> and b is <AG: 0.9, LT: 0.8, PD: 0.7>, the result is <AG: 0.63, LT: 0.48>. The PD: 0.7 is discarded.
 * That said, where this function is actually used (a reduce call in calculateCombinedCMF), the initial value of a is the defualtVector which is every crash type at 1.
 * Therefore, as a has all of the possible counter measures, this function works as intended.
 * @param previousValue
 * @param currentValue
 * @returns
 */
function plainMultiplication(
  previousValue: CmfVector,
  currentValue: CmfVector
): CmfVector {
  for (const [crashType, cmf] of previousValue) {
    previousValue.set(
      crashType,
      (cmf || 1.0) * (currentValue.get(crashType) || 1.0)
    );
  }

  return previousValue;
}

function nightTimeMultiplication(
  previousValue: CmfVector,
  currentValue: CmfVector
): CmfVector {
  for (const [crashType, cmf] of previousValue) {
    let multiplyBy: number;

    if (
      currentValue.get(CrashType.NGT) &&
      currentValue.get(CrashType.NGT) !== 1
    ) {
      multiplyBy = currentValue.get(CrashType.NGT);
    } else if (currentValue.get(crashType)) {
      multiplyBy = currentValue.get(crashType);
    } else {
      multiplyBy = 1;
    }

    previousValue.set(crashType, (cmf || 1.0) * multiplyBy);
  }

  return previousValue;
}

function wetPavementMultiplication(
  previousValue: CmfVector,
  currentValue: CmfVector
): CmfVector {
  for (const [crashType, cmf] of previousValue) {
    let multiplyBy: number;

    if (
      currentValue.get(CrashType.NGT) &&
      currentValue.get(CrashType.NGT) !== 1
    ) {
      multiplyBy = currentValue.get(CrashType.NGT);
    } else if (currentValue.get(crashType)) {
      multiplyBy = currentValue.get(crashType);
    } else {
      multiplyBy = 1;
    }

    previousValue.set(crashType, (cmf || 1.0) * multiplyBy);
  }

  return previousValue;
}

function multiplyCMFs(
  countermeasures: Countermeasure[],
  start: CmfVector,
  multiplicationFunction: (a: CmfVector, b: CmfVector) => CmfVector
): CmfVector {
  const resultVector = countermeasures
    .map((cm) => cm.cmfVector)
    .reduce(multiplicationFunction, start);

  for (const [crashType, cmf] of resultVector) {
    if (cmf < 0.6) {
      resultVector.set(
        crashType,
        Math.min(
          0.6,
          ...countermeasures.map(
            (countermeasure) => countermeasure.cmfVector.get(crashType) || 1.0
          )
        )
      );
    }
  }

  return resultVector;
}

/**
 * Each countermeasure has a CmfVector object which maps each crash type to a cmf value.
 * This function takes all of the countermeasures and for each crash type, multiplies their cmf values together.
 * For example if the first counter measure had a cmfVector of <AG: 0.9, LT: 0.8> and the second had <AG: 0.7, LT: 0.6> the result would be <AG: 0.9 * 0.7, LT: 0.8 * 0.6> which is <AG: 0.63, LT: 0.48>
 * That said if a cmf value is less than 0.6, it is set to 0.6.
 *
 * Therefore the result of our example would actually be <AG: 0.63, LT: 0.6>.
 *
 * @param {Countermeasure[]} countermeasures The array of counter measures to combine.
 * @returns {CmfVector} resultVector - The resulting vector with all of the cmf values as attributes
 */
export function calculateCombinedCMF(
  countermeasures: Countermeasure[]
): Map<ConditionType, CmfVector> {
  if (!countermeasures.length)
    return new Map([
      [ConditionType.plain, Countermeasure.defaultVector],
      [ConditionType.NGT, Countermeasure.defaultVector],
      [ConditionType.WP, Countermeasure.defaultVector],
    ]);

  const plainVector: CmfVector = multiplyCMFs(
    countermeasures,
    new Map(Countermeasure.defaultVector),
    plainMultiplication
  );
  const nightTimeVector = multiplyCMFs(
    countermeasures,
    new Map(Countermeasure.defaultVector),
    nightTimeMultiplication
  );
  const wetPavementVector = multiplyCMFs(
    countermeasures,
    new Map(Countermeasure.defaultVector),
    wetPavementMultiplication
  );

  for (const [crashType, cmf] of nightTimeVector) {
    if (plainVector.get(CrashType.NGT) < cmf) {
      nightTimeVector.set(crashType, plainVector.get(CrashType.NGT));
    }
  }

  for (const [crashType, cmf] of wetPavementVector) {
    if (plainVector.get(CrashType.WP) < cmf) {
      wetPavementVector.set(crashType, plainVector.get(CrashType.WP));
    }
  }

  // We have some crash types that the spreadsheet does not take into account so we need to set
  // those to 1 so that our tool does not create any benefit the spreadsheet would not.
  for (const type of [CrashType.RTF, CrashType.RTR, CrashType.RTS]) {
    plainVector.set(type, 1.0);
    nightTimeVector.set(type, 1.0);
    wetPavementVector.set(type, 1.0);
  }

  return new Map([
    [ConditionType.plain, plainVector],
    [ConditionType.NGT, nightTimeVector],
    [ConditionType.WP, wetPavementVector],
  ]);
}

/**
 * Crash data is currently read from the mbtiles and not imported from a local json file, hence this function is not implemented.
 * @param json
 * @returns
 */
export function loadCrashes(json: any): Crash[] {
  throw new Error("Not Implemented!!!");
}

/**
 * This loads the hard-coded countermeasure data from a json.
 * In our case it will be src/BCLIb/assets/cmf_data/countermeasure_data.json
 * @param data An object which contains the data (not a file path)
 * @returns {Countermeasure[]} An array of countermeasure objects instantiated from the data.
 */
export function loadCountermeasures(data: any): Countermeasure[] {
  const countermeasures: Countermeasure[] = [];

  for (const cm of data) {
    const cmfv: CmfVector = new Map(Countermeasure.defaultVector);
    for (const [crashType, defaultCmf] of cmfv) {
      cmfv.set(crashType, cm[crashType] || defaultCmf);
    }

    countermeasures.push(
      new Countermeasure(
        cm["Countermeasure"],
        cm["Category"],
        cm["Service Life"],
        cmfv,
        cm["Setting Type"],
        cm["Facility Type"],
        cm["Unit"],
        cm["Code"],
        cm["Affected Crash Types"],
        cm["Project Type"],
        cm["Project System"]
      )
    );
  }

  return countermeasures;
}

const typeConversion: Record<Severity, CrashInjuryType> = {
  "Total Fatalities": CrashInjuryType.F,
  "Total A-injuries": CrashInjuryType.A,
  "Total B-injuries": CrashInjuryType.B,
  "Total C-injuries": CrashInjuryType.C,
  "Total Non-injuries": CrashInjuryType.N,
};

function setSeverityDistribution(
  crashes: Crash[],
  crashTable: CrashTable,
  type: ConditionType
): void {
  const crashTypes: CrashType[] = [
    ...new Set(crashes.map((crash) => crash.crashType)),
  ];

  crashTable.set(type, new Map<CrashType, Map<Severity, number>>());

  for (const crashType of crashTypes) {
    crashTable.get(type).set(
      crashType,
      new Map(
        [Severity.F, Severity.A, Severity.B, Severity.C, Severity.N].map(
          (sev) => {
            return [
              sev,
              crashes
                .filter((crash) => crash.crashType === crashType)
                .filter((crash) => crash.crashSeverity === typeConversion[sev])
                .length,
            ];
          }
        )
      )
    );
  }
}

export function createCrashTable(
  crashes: Crash[],
  aggregate: boolean
): CrashTable {
  const crashTable: CrashTable = new Map<
    ConditionType,
    Map<CrashType, Map<Severity, number>>
  >();

  const normalCrashes = crashes.filter(
    (crash) => !crash.countAsNightTime && !crash.countAsWetPavement
  );
  setSeverityDistribution(normalCrashes, crashTable, ConditionType.plain);

  const nightCrashes = crashes.filter((crash) => crash.countAsNightTime);
  if (nightCrashes.length != 0) {
    setSeverityDistribution(nightCrashes, crashTable, ConditionType.NGT);
  }

  const wetPavementCrashes = crashes.filter(
    (crash) => crash.countAsWetPavement
  );

  if (wetPavementCrashes.length != 0) {
    if (aggregate) {
      crashTable.set(
        ConditionType.WP,
        new Map<CrashType, Map<Severity, number>>()
      );

      for (const crashType of crashOrder) {
        const crashIndex = crashOrder.indexOf(crashType);
        const remainingTypes = crashOrder.slice(crashIndex + 1);

        crashTable.get(ConditionType.WP).set(
          crashType,
          new Map(
            [Severity.F, Severity.A, Severity.B, Severity.C, Severity.N].map(
              (sev) => {
                return [
                  sev,
                  remainingTypes
                    .map(
                      (remainingType) =>
                        wetPavementCrashes
                          .filter((crash) => crash.crashType === remainingType)
                          .filter(
                            (crash) =>
                              crash.crashSeverity === typeConversion[sev]
                          ).length
                    )
                    .reduce((a, b) => a + b, 0) ||
                    wetPavementCrashes
                      .filter((crash) => crash.crashType === crashType)
                      .filter(
                        (crash) => crash.crashSeverity === typeConversion[sev]
                      ).length,
                ];
              }
            )
          )
        );
      }
    } else {
      setSeverityDistribution(wetPavementCrashes, crashTable, ConditionType.WP);
    }
  }

  return crashTable;
}

export function findMode(arr: any[]) {
  return Object.values(
    arr.reduce((count, e) => {
      if (!(e in count)) {
        count[e] = [0, e];
      }

      count[e][0]++;
      return count;
    }, {})
  ).reduce((a, v) => (v[0] < a[0] ? a : v), [0, null])[1];
}

export function roundNumber(num: number, scale: number) {
  if (!String(num).includes("e")) {
    return Number(Math.round(Number(num + "e+" + scale)) + "e-" + scale);
  } else {
    const arr = String(num).split("e");
    let sig = "";
    if (Number(arr[1]) + scale > 0) {
      sig = "+";
    }
    return Number(
      Math.round(Number(arr[0] + "e" + sig + (Number(arr[1]) + scale))) +
        "e-" +
        scale
    );
  }
}
