import {
  adjustedCMFVector,
  createDistributionTag,
  calculatedAdjustedCMFVector,
} from "./adjustedCMF";
import { CrashType, RoadwayType } from "./models/enums";
import { CmfVector } from "./models/types";
import { roundNumber } from "./utils";

export class InvalidInputError extends Error {
  constructor(message: string) {
    super(message);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, InvalidInputError);
    }
    this.name = "InvalidInputError";
  }
}

// Lookup variables

const shoulderTypeCMF: Map<number, Map<string, number>> = new Map<
  number,
  Map<string, number>
>([
  [
    1,
    new Map<string, number>([
      ["paved", 1.0],
      ["gravel", 1.0],
      ["composite", 1.01],
      ["turf", 1.01],
    ]),
  ],
  [
    2,
    new Map<string, number>([
      ["paved", 1.0],
      ["gravel", 1.01],
      ["composite", 1.02],
      ["turf", 1.03],
    ]),
  ],
  [
    3,
    new Map<string, number>([
      ["paved", 1.0],
      ["gravel", 1.01],
      ["composite", 1.02],
      ["turf", 1.04],
    ]),
  ],
  [
    4,
    new Map<string, number>([
      ["paved", 1.0],
      ["gravel", 1.01],
      ["composite", 1.03],
      ["turf", 1.05],
    ]),
  ],
  [
    5,
    new Map<string, number>([
      ["paved", 1.0],
      ["gravel", 1.015],
      ["composite", 1.035],
      ["turf", 1.065],
    ]),
  ],
  [
    6,
    new Map<string, number>([
      ["paved", 1.0],
      ["gravel", 1.02],
      ["composite", 1.04],
      ["turf", 1.08],
    ]),
  ],
  [
    7,
    new Map<string, number>([
      ["paved", 1.0],
      ["gravel", 1.02],
      ["composite", 1.05],
      ["turf", 1.095],
    ]),
  ],
  [
    8,
    new Map<string, number>([
      ["paved", 1.0],
      ["gravel", 1.02],
      ["composite", 1.06],
      ["turf", 1.11],
    ]),
  ],
  [
    9,
    new Map<string, number>([
      ["paved", 1.0],
      ["gravel", 1.025],
      ["composite", 1.065],
      ["turf", 1.125],
    ]),
  ],
  [
    10,
    new Map<string, number>([
      ["paved", 1.0],
      ["gravel", 1.03],
      ["composite", 1.07],
      ["turf", 1.14],
    ]),
  ],
]);

const sideslopeCMFTwoLane: Map<string, Map<string, number>> = new Map<
  string,
  Map<string, number>
>([
  [
    "1V:2H",
    new Map<string, number>([
      ["1V:4H", 0.94],
      ["1V:5H", 0.91],
      ["1V:6H", 0.88],
      ["1V:7H", 0.85],
    ]),
  ],
  [
    "1V:3H",
    new Map<string, number>([
      ["1V:4H", 0.95],
      ["1V:5H", 0.92],
      ["1V:6H", 0.89],
      ["1V:7H", 0.85],
    ]),
  ],
  [
    "1V:4H",
    new Map<string, number>([
      ["1V:5H", 0.97],
      ["1V:6H", 0.93],
      ["1V:7H", 0.89],
    ]),
  ],
  [
    "1V:5H",
    new Map<string, number>([
      ["1V:6H", 0.97],
      ["1V:7H", 0.92],
    ]),
  ],
  ["1V:6H", new Map<string, number>([["1V:7H", 0.95]])],
]);

const sideslopeCMFMultilane: Map<string, number> = new Map<string, number>([
  ["1V:7H", 1.0],
  ["1V:6H", 1.05],
  ["1V:5H", 1.09],
  ["1V:4H", 1.12],
  ["1V:3H", 1.15],
  ["1V:2H", 1.18],
]);

const roadsideDistance: number[][] = [
  [0, 1],
  [3.3, 1],
  [16.7, 0.78],
  [30.01, 0.44],
];

const medianRuralFullAccess: number[][] = [
  [0, 1.0],
  [9.99, 1.0],
  [19.99, 0.86],
  [29.99, 0.74],
  [39.99, 0.63],
  [49.99, 0.54],
  [59.99, 0.46],
  [69.99, 0.4],
  [79.99, 0.34],
  [89.99, 0.29],
  [99.99, 0.25],
];

const medianRuralNoAccess: number[][] = [
  [0, 1.0],
  [9.99, 1.0],
  [19.99, 0.84],
  [29.99, 0.71],
  [39.99, 0.6],
  [49.99, 0.51],
  [59.99, 0.43],
  [69.99, 0.36],
  [79.99, 0.31],
  [89.99, 0.26],
  [99.99, 0.22],
];

const medianUrbanFullAccess: number[][] = [
  [0, 1.0],
  [9.99, 1.0],
  [19.99, 0.89],
  [29.99, 0.8],
  [39.99, 0.71],
  [49.99, 0.64],
  [59.99, 0.57],
  [69.99, 0.51],
  [79.99, 0.46],
  [89.99, 0.41],
  [99.99, 0.36],
];

const medianUrbanNoAccess: number[][] = [
  [0, 1.0],
  [9.99, 1.0],
  [19.99, 0.87],
  [29.99, 0.76],
  [39.99, 0.67],
  [49.99, 0.59],
  [59.99, 0.51],
  [69.99, 0.45],
  [79.99, 0.39],
  [89.99, 0.34],
  [99.99, 0.3],
];

const modifyAccessPointDensity: number[][] = [
  [0, 0.37],
  [10, 0.49],
  [24, 0.71],
  [48, 1],
];

function stepFunction(array: number[][], input: number): number {
  const end = array.length - 1;
  for (let i = end; i >= 0; i--) {
    if (input > array[i][0]) {
      return array[i][1];
    }
  }
  return Number.NaN;
}

//_______________________________________________

// Validation Helpers

interface validationData {
  code: string;
  correctNumberOfInputs: number;
  correctTypeArray: string[];
  aadtMajorRequired: boolean;
  aadtMinorRequired: boolean;
  providedInputs: (string | number)[];
  aadtMajor: number;
  aadtMinor: number;
}

function validate(data: validationData) {
  validateInputLength(
    data.code,
    data.correctNumberOfInputs,
    data.providedInputs
  );
  validateInputType(data.code, data.correctTypeArray, data.providedInputs);
  validateAADT(
    data.code,
    data.aadtMajorRequired,
    data.aadtMajor,
    data.aadtMinorRequired,
    data.aadtMinor
  );
}

function validateInputLength(
  code: string,
  correctInputLength: number,
  inputs: (string | number)[]
) {
  if (inputs.length !== correctInputLength) {
    throw new InvalidInputError(
      `${code} takes ${correctInputLength} inputs but ${inputs.length} ${
        inputs.length === 1 ? "was" : "were"
      } provided`
    );
  }
}

function validateInputType(
  code: string,
  correctInputTypes: string[],
  inputs: (string | number)[]
) {
  let valid = true;
  for (let index = 0; index < inputs.length; index++) {
    if (typeof inputs[index] !== correctInputTypes[index]) {
      valid = false;
      break;
    }
    if (correctInputTypes[index] === "number" && Number.isNaN(inputs[index])) {
      inputs[index] = "Not a number";
      valid = false;
      break;
    }
  }

  if (!valid) {
    throw new TypeError(
      `Input for ${code} is the incorrect type. Input needs to be of type [${correctInputTypes}], but is of type [${inputs.map(
        (input) =>
          typeof input === "number" && Number.isNaN(input)
            ? "string"
            : typeof input
      )}]`
    );
  }
}

function validateAADT(
  code: string,
  aadtMajorRequired: boolean,
  aadtMajor: number,
  aadtMinorRequired: boolean,
  aadtMinor: number
) {
  if (aadtMajorRequired && typeof aadtMajor !== "number") {
    throw new InvalidInputError(`The AADT Major must be provided for ${code}`);
  }
  if (aadtMinorRequired && typeof aadtMinor !== "number") {
    throw new InvalidInputError(`The AADT Minor must be provided for ${code}`);
  }
}

//_______________________________________________

// Calculated Countermeasures

export interface CalculatedCounterMeasure {
  /** The name of the counter measure */
  name: string;
  /** The code for the counter measure */
  code: string;
  /** What to prompt the user with  */
  prompt: string;
  /** The number of user inputs the calculation for this countermeasure needs  */
  numberOfInputs: number;
  /** Whether the calculation of this counter measure needs aadt Major data  */
  aadtMajorRequired: boolean;
  /** Whether the calculation needs the aadtMinor */
  aadtMinorRequired: boolean;
  /** What to ask the user for for the inputs of the calculation for this counter measure  */
  inputPrompts: string[];
  /** The types of the inputs  */
  inputTypes: ("string" | "number")[];
  /** A function to calculate the CmfVector given the user inputs and, potentially, aadt information */
  calculate: (
    input: (string | number)[],
    aadtMajor: number,
    aadtMinor: number
  ) => CmfVector;
  /** A function to validate the input of the user and give input-specific feedback if the countermeasure provides this  */
  validate: (
    input: (string | number)[],
    aadtMajor: number,
    aadtMinor: number
  ) => void;
}

export const calculatedCounterMeasures: CalculatedCounterMeasure[] = [
  {
    // Macro case: 2A
    name: "Modify Lane Width",
    code: "4.1.1.S1.1",
    prompt: "Lane Width Variables",
    numberOfInputs: 2,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Lane Width (in feet, round to nearest 1')\nRefer to HSIM 1st ed - 13.4.2.1",
      "Input Future Lane Width (in feet, round to nearest 1')\nRefer to HSIM 1st ed - 13.4.2.1",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // CMF=AK823/AJ823
      // Where AK823 = IF($AI823<=9,IF($AF823<400,1.05,IF($AF823>2000,1.5,1.05+2.81*0.0001*($AF823-400))),IF($AI823=10,IF($AF823<400,1.02,IF($AF823>2000,1.3,1.02+1.75*0.0001*($AF823-400))),IF($AI823=11,IF($AF823<400,1.01,IF($AF823>2000,1.05,1.01+2.5*0.00001*($AF823-400))),1)))
      // AI823 = User input 2
      // AF823 = AADT Major
      // And AJ823 = IF($AH823<=9,IF($AF823<400,1.05,IF($AF823>2000,1.5,1.05+2.81*0.0001*($AF823-400))),IF($AH823=10,IF($AF823<400,1.02,IF($AF823>2000,1.3,1.02+1.75*0.0001*($AF823-400))),IF($AH823=11,IF($AF823<400,1.01,IF($AF823>2000,1.05,1.01+2.5*0.00001*($AF823-400))),1)))
      // Where A823 = User input 1

      let AK: number;
      if ((inputs[1] as number) <= 9) {
        if (aadtMajor < 400) {
          AK = 1.05;
        } else if (aadtMajor > 2000) {
          AK = 1.5;
        } else {
          AK = 1.05 + 2.81 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 10) {
        if (aadtMajor < 400) {
          AK = 1.02;
        } else if (aadtMajor > 2000) {
          AK = 1.3;
        } else {
          AK = 1.02 + 1.75 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 11) {
        if (aadtMajor < 400) {
          AK = 1.01;
        } else if (aadtMajor > 2000) {
          AK = 1.05;
        } else {
          AK = 1.01 + 2.5 * 0.00001 * (aadtMajor - 400);
        }
      } else {
        AK = 1;
      }

      let AJ: number;
      if ((inputs[0] as number) <= 9) {
        if (aadtMajor < 400) {
          AJ = 1.05;
        } else if (aadtMajor > 2000) {
          AJ = 1.5;
        } else {
          AJ = 1.05 + 2.81 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 10) {
        if (aadtMajor < 400) {
          AJ = 1.02;
        } else if (aadtMajor > 2000) {
          AJ = 1.3;
        } else {
          AJ = 1.02 + 1.75 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 11) {
        if (aadtMajor < 400) {
          AJ = 1.01;
        } else if (aadtMajor > 2000) {
          AJ = 1.05;
        } else {
          AJ = 1.01 + 2.5 * 0.00001 * (aadtMajor - 400);
        }
      } else {
        AJ = 1;
      }

      let CMF = AK / AJ;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, CMF],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, CMF],
        [CrashType.RT, 1],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.1.S1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2B
    name: "Modify Lane Width",
    code: "4.1.1.S6.1",
    prompt: "Lane Width Variables",
    numberOfInputs: 2,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Lane Width (in feet, round to nearest 1')\nRefer to HSM 1st ed - 13.4.2.1",
      "Input Future Lane Width (in feet, round to nearest 1')\nRefer to HSIM 1st ed - 13.4.2.1",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // CMF = AK824/AJ824
      // Where AK824 = IF($AI824<=9,IF($AF824<400,1.03,IF($AF824>2000,1.25,1.03+1.38*0.0001*($AF824-400))),IF($AI824=10,IF($AF824<400,1.01,IF($AF824>2000,1.15,1.01+8.75*0.00001*($AF824-400))),IF($AI824=11,IF($AF824<400,1.01,IF($AF824>2000,1.03,1.01+1.25*0.00001*($AF824-400))),1)))
      // Where AI824 = user input 2
      // And AF825 = aadt major
      // Where AJ824 = IF($AH824<=9,IF($AF824<400,1.03,IF($AF824>2000,1.25,1.03+1.38*0.0001*($AF824-400))),IF($AH824=10,IF($AF824<400,1.01,IF($AF824>2000,1.15,1.01+8.75*0.00001*($AF824-400))),IF($AH824=11,IF($AF824<400,1.01,IF($AF824>2000,1.03,1.01+1.25*0.00001*($AF824-400))),1)))
      // Where AH824 = user input 1

      let AK: number;

      if ((inputs[1] as number) <= 9) {
        if (aadtMajor < 400) {
          AK = 1.03;
        } else if (aadtMajor > 2000) {
          AK = 1.25;
        } else {
          AK = 1.03 + 1.38 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 10) {
        if (aadtMajor < 400) {
          AK = 1.01;
        } else if (aadtMajor > 2000) {
          AK = 1.15;
        } else {
          AK = 1.01 + 8.75 * 0.00001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 11) {
        if (aadtMajor < 400) {
          AK = 1.01;
        } else if (aadtMajor > 2000) {
          AK = 1.03;
        } else {
          AK = 1.01 + 1.25 * 0.00001 * (aadtMajor - 400);
        }
      } else {
        AK = 1;
      }

      let AJ: number;

      if ((inputs[0] as number) <= 9) {
        if (aadtMajor < 400) {
          AJ = 1.03;
        } else if (aadtMajor > 2000) {
          AJ = 1.25;
        } else {
          AJ = 1.03 + 1.38 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 10) {
        if (aadtMajor < 400) {
          AJ = 1.01;
        } else if (aadtMajor > 2000) {
          AJ = 1.15;
        } else {
          AJ = 1.01 + 8.75 * 0.00001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 11) {
        if (aadtMajor < 400) {
          AJ = 1.01;
        } else if (aadtMajor > 2000) {
          AJ = 1.03;
        } else {
          AJ = 1.01 + 1.25 * 0.00001 * (aadtMajor - 400);
        }
      } else {
        AJ = 1;
      }

      let CMF = AK / AJ;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, CMF],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, CMF],
        [CrashType.RT, 1],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.1.S6.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Decrease lane width from 11 ft to 10 ft",
    code: "4.1.2.SU.1",
    prompt:
      "This countermeasure is calculated from the AADT and does not use a prompt",
    numberOfInputs: 0,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // =EXP(-0.515+0.00012*AF825) Where AF832 is the AADT Major
      let CMF = Math.exp(-0.515 + 0.00012 * aadtMajor);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.2.SU.1",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Decrease lane width from 11 ft to 9 ft",
    code: "4.1.2.SU.2",
    prompt: "This countermeasure is hard coded in the spreadsheet",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF = 0.41;
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.2.SU.2",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Decrease lane width from 12 ft to 10 ft",
    code: "4.1.2.SU.3",
    prompt: "This countermeasure is hard coded in the spreadsheet",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF = 1.32;
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.2.SU.3",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Decrease lane width from 12 ft to 11 ft",
    code: "4.1.2.SU.4",
    prompt:
      "This countermeasure is calculated from the AADT and does not use a prompt",
    numberOfInputs: 0,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF = Math.exp(-0.425 + 0.00009 * aadtMajor);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.2.SU.4",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Increase lane width from 10 ft to 11 ft",
    code: "4.1.2.SU.5",
    prompt:
      "This countermeasure is calculated from the AADT and does not use a prompt",
    numberOfInputs: 0,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF = 1 / Math.exp(-0.515 + 0.00012 * aadtMajor);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.2.SU.5",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Increase lane width from 9 ft to 11 ft",
    code: "4.1.2.SU.6",
    prompt: "This countermeasure is hard coded in the spreadsheet",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF = 1 / 0.41;
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.2.SU.6",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Increase lane width from 10 ft to 12 ft",
    code: "4.1.2.SU.7",
    prompt: "This countermeasure is hard coded in the spreadsheet",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF = 1 / 1.32;
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.2.SU.7",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Increase lane width from 11 ft to 12 ft",
    code: "4.1.2.SU.8",
    prompt:
      "This countermeasure is calculated from the AADT and does not use a prompt",
    numberOfInputs: 0,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // 1/EXP(-0.425+0.00009*AF832) Where AF832 is the AADT Major
      let CMF = 1 / Math.exp(-0.425 + 0.00009 * aadtMajor);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.2.SU.8",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2D
    name: "Add or Widen Paved Shoulder",
    code: "4.1.3.S1.1",
    prompt: "Shoulder Width Variables",
    numberOfInputs: 2,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Shoulder Width (in feet, round to nearest 1')\nRefer to HSIM 1st ed - 13.4.2.4",
      "Input Future Shoulder Width (in feet, round to nearest 1')\nRefer to HSIM 1st ed - 13.4.2.4",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // CMF = AK833 / AJ833
      // Where AK833 = IF(AI833>=8,IF($AF833<400,0.98,IF($AF833>2000,0.87,0.98-6.875*0.00001*($AF833-400))),IF(AI833=7,IF($AF833<400,0.99,IF($AF833>2000,0.935,0.99-3.44*0.00001*($AF833-400))),IF(AI833=6,1,IF(AI833=5,IF($AF833<400,1.01,IF($AF833>2000,1.075,1.01+4.06*0.00001*($AF833-400))),IF(AI833=4,IF($AF833<400,1.02,IF($AF833>2000,1.15,1.02+8.125*0.00001*($AF833-400))),IF(AI833=3,IF($AF833<400,1.045,IF($AF833>2000,1.225,1.045+1.1125*0.0001*($AF833-400))),IF(AI833=2,IF($AF833<400,1.07,IF($AF833>2000,1.3,1.07+1.43*0.0001*($AF833-400))),IF(AI833=1,IF($AF833<400,1.085,IF($AF833>2000,1.4,1.085+1.969*0.0001*($AF833-400))),IF($AF833<400,1.1,IF($AF833>2000,1.5,1.1+2.5*0.0001*($AF833-400)))))))))))
      // Where AI833 = user input 2
      // Where AF833 = aadt major
      // Where AJ833 = IF(AH833>=8,IF($AF833<400,0.98,IF($AF833>2000,0.87,0.98-6.875*0.00001*($AF833-400))),IF(AH833=7,IF($AF833<400,0.99,IF($AF833>2000,0.935,0.99-3.44*0.00001*($AF833-400))),IF(AH833=6,1,IF(AH833=5,IF($AF833<400,1.01,IF($AF833>2000,1.075,1.01+4.06*0.00001*($AF833-400))),IF(AH833=4,IF($AF833<400,1.02,IF($AF833>2000,1.15,1.02+8.125*0.00001*($AF833-400))),IF(AH833=3,IF($AF833<400,1.045,IF($AF833>2000,1.225,1.045+1.1125*0.0001*($AF833-400))),IF(AH833=2,IF($AF833<400,1.07,IF($AF833>2000,1.3,1.07+1.43*0.0001*($AF833-400))),IF(AH833=1,IF($AF833<400,1.085,IF($AF833>2000,1.4,1.085+1.969*0.0001*($AF833-400))),IF($AF833<400,1.1,IF($AF833>2000,1.5,1.1+2.5*0.0001*($AF833-400)))))))))))
      // Where AH833 = User input 1
      let AK: number;

      if ((inputs[1] as number) >= 8) {
        if (aadtMajor < 400) {
          AK = 0.98;
        } else if (aadtMajor > 2000) {
          AK = 0.87;
        } else {
          AK = 0.98 - 6.875 * 0.00001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 7) {
        if (aadtMajor < 400) {
          AK = 0.99;
        } else if (aadtMajor > 2000) {
          AK = 0.935;
        } else {
          AK = 0.99 - 3.44 * 0.00001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 6) {
        AK = 1;
      } else if (inputs[1] === 5) {
        if (aadtMajor < 400) {
          AK = 1.01;
        } else if (aadtMajor > 2000) {
          AK = 1.075;
        } else {
          AK = 1.01 + 4.06 * 0.00001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 4) {
        if (aadtMajor < 400) {
          AK = 1.02;
        } else if (aadtMajor > 2000) {
          AK = 1.15;
        } else {
          AK = 1.02 + 8.125 * 0.00001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 3) {
        if (aadtMajor < 400) {
          AK = 1.045;
        } else if (aadtMajor > 2000) {
          AK = 1.225;
        } else {
          AK = 1.045 + 1.1125 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 2) {
        if (aadtMajor < 400) {
          AK = 1.07;
        } else if (aadtMajor > 2000) {
          AK = 1.3;
        } else {
          AK = 1.07 + 1.43 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 1) {
        if (aadtMajor < 400) {
          AK = 1.085;
        } else if (aadtMajor > 2000) {
          AK = 1.4;
        } else {
          AK = 1.085 + 1.969 * 0.0001 * (aadtMajor - 400);
        }
      } else {
        if (aadtMajor < 400) {
          AK = 1.1;
        } else if (aadtMajor > 2000) {
          AK = 1.5;
        } else {
          AK = 1.1 + 2.5 * 0.0001 * (aadtMajor - 400);
        }
      }

      let AJ: number;

      if ((inputs[0] as number) >= 8) {
        if (aadtMajor < 400) {
          AJ = 0.98;
        } else if (aadtMajor > 2000) {
          AJ = 0.87;
        } else {
          AJ = 0.98 - 6.875 * 0.00001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 7) {
        if (aadtMajor < 400) {
          AJ = 0.99;
        } else if (aadtMajor > 2000) {
          AJ = 0.935;
        } else {
          AJ = 0.99 - 3.44 * 0.00001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 6) {
        AJ = 1;
      } else if (inputs[0] === 5) {
        if (aadtMajor < 400) {
          AJ = 1.01;
        } else if (aadtMajor > 2000) {
          AJ = 1.075;
        } else {
          AJ = 1.01 + 4.06 * 0.00001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 4) {
        if (aadtMajor < 400) {
          AJ = 1.02;
        } else if (aadtMajor > 2000) {
          AJ = 1.15;
        } else {
          AJ = 1.02 + 8.125 * 0.00001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 3) {
        if (aadtMajor < 400) {
          AJ = 1.045;
        } else if (aadtMajor > 2000) {
          AJ = 1.225;
        } else {
          AJ = 1.045 + 1.1125 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 2) {
        if (aadtMajor < 400) {
          AJ = 1.07;
        } else if (aadtMajor > 2000) {
          AJ = 1.3;
        } else {
          AJ = 1.07 + 1.43 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 1) {
        if (aadtMajor < 400) {
          AJ = 1.085;
        } else if (aadtMajor > 2000) {
          AJ = 1.4;
        } else {
          AJ = 1.085 + 1.969 * 0.0001 * (aadtMajor - 400);
        }
      } else {
        if (aadtMajor < 400) {
          AJ = 1.1;
        } else if (aadtMajor > 2000) {
          AJ = 1.5;
        } else {
          AJ = 1.1 + 2.5 * 0.0001 * (aadtMajor - 400);
        }
      }

      let CMF = AK / AJ;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, CMF],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, CMF],
        [CrashType.RT, 1],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.3.S1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2F
    name: "Add or Widen Paved Shoulder",
    code: "4.1.3.S6.1",
    prompt:
      "Shoulder Width Variables - NOTE: Spreadsheet ignores user input, behavior copied here but likely incorrect",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    // These would be the prompts if the countermeasure did not ignore user input.
    // [
    //   "Input Existing Shoulder Width (in feet, round to nearest 1')\nRefer to HSIM 1st ed - 13.4.2.4",
    //   "Input Future Shoulder Width (in feet, round to nearest 1')\nRefer to HSIM 1st ed - 13.4.2.4",
    // ],
    inputTypes: [],
    // These would be the input types if the countermeasure did not ignore user input.
    // ["number", "number"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // CMF = H834
      // Where H834 = INDEX($Module02d.$BL$18:$CF$77,MATCH($AV834,$Module02d.$BJ$18:$BJ$77,0),MATCH(H$4,$Module02d.$BL$17:$CF$17,0))
      // Where AV834 = 4A
      // That said each type has a different-yet-similar lookup. For example AG is INDEX($Module02d.$BL$18:$CF$77,MATCH($AV834,$Module02d.$BJ$18:$BJ$77,0),MATCH(I$4,$Module02d.$BL$17:$CF$17,0))
      // Which is different from all and is also different from SOD
      // I am not sure whether this countermeasure relies on user input or not.
      // Going to use what is in the spreadsheet to replicate this, but also contact IDOT.
      // TODO: Contact IDOT
      return new Map([
        [CrashType.AG, 0.35],
        [CrashType.AN, 0.35],
        [CrashType.FO, 0.67],
        [CrashType.FTF, 0.67],
        [CrashType.LT, 0.35],
        [CrashType.OtherNC, 0.35],
        [CrashType.OtherO, 0.52],
        [CrashType.OVT, 0.67],
        [CrashType.PD, 0.52],
        [CrashType.PDC, 0.52],
        [CrashType.PKV, 0.52],
        [CrashType.FTR, 0.35],
        [CrashType.RT, 0.35],
        [CrashType.SSD, 0.67],
        [CrashType.SOD, 0.67],
        [CrashType.T, 0.35],
        [CrashType.TR, 0.52],
        [CrashType.NGT, 1],
        [CrashType.ROR, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.3.S6.1",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2G
    name: "Modify Shoulder Type",
    code: "4.1.12.S1.1",
    prompt: "Change shoulder type variables",
    numberOfInputs: 3,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Shoulder Width (Whole numbers between 1 and 10)\nRefer to HSM 1st ed - 13.4.2.5",
      "Existing Shoulder Type (Paved, Gravel, Composite, or Turf)\nRefer to HSM 1st ed - 13.4.2.5",
      "Future Shoulder Type (Paved, Gravel, Composite, or Turf)\nRefer to HSM 1st ed - 13.4.2.5",
    ],
    inputTypes: ["number", "string", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // CMF = (HLOOKUP(AH850,$Module02b.$C$116:$L$120,VLOOKUP(AJ850,$Module02b.$N$108:$O$112,2,0),0))/HLOOKUP(AH850,$Module02b.$C$116:$L$120,VLOOKUP(AI850,$Module02b.$N$108:$O$112,2,0),0)
      // Where AH850 is user input 1 (shoulder width)
      // AI850 is user input 2 (existing shoulder type)
      // AJ850 is user input 3 (future shoulder type)

      inputs[1] = (inputs[1] as string).toLowerCase();
      inputs[2] = (inputs[2] as string).toLowerCase();

      let CMF =
        shoulderTypeCMF.get(inputs[0] as number).get(inputs[2] as string) /
        shoulderTypeCMF.get(inputs[0] as number).get(inputs[1] as string);
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, CMF],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, 1],
        [CrashType.RT, 1],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.12.S1.1",
        correctNumberOfInputs: 3,
        correctTypeArray: ["number", "string", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      inputs[1] = (inputs[1] as string).toLowerCase();
      inputs[2] = (inputs[2] as string).toLowerCase();

      if ((inputs[0] as number) < 1 || (inputs[0] as number) > 10) {
        throw new InvalidInputError(
          "Shoulder width must be between 1 and 10. If shoulder width is greater than 10, please input 10."
        );
      }
      if (
        inputs[1] !== "paved" &&
        inputs[1] !== "composite" &&
        inputs[1] !== "turf" &&
        inputs[1] !== "gravel"
      ) {
        throw new InvalidInputError(
          "Invalid shoulder type selected. Shoulder Type Options are Paved, Composite, Turf, and Gravel."
        );
      }
      if (
        inputs[2] !== "paved" &&
        inputs[2] !== "composite" &&
        inputs[2] !== "turf" &&
        inputs[2] !== "gravel"
      ) {
        throw new InvalidInputError(
          "Invalid shoulder type selected. Shoulder Type Options are Paved, Composite, Turf, and Gravel."
        );
      }
    },
  },
  {
    // Macro case: 2I
    name: "Provide TWLTL",
    code: "4.1.13.S6.1",
    prompt:
      "TWLTL Variables - Note: Crash proportion is ignored in calculation due to IDOT Spreadsheet",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Driveway Density (driveways/mi)\nRefer to HSM 1st ed - 16.5.2.1",
      "Input Proportion of Crashes that are Driveway Related (in decimal)\nRefer to HSM 1st ed - 16.5.2.1",
    ],
    inputTypes: ["number", "number"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      let CMF =
        1 -
        0.7 *
          0.5 *
          ((0.0047 * (inputs[0] as number) +
            0.0024 * Math.pow(inputs[0] as number, 2)) /
            (1.199 +
              0.0047 * (inputs[0] as number) +
              0.0024 * Math.pow(inputs[0] as number, 2)));
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, 1],
        [CrashType.FO, 1],
        [CrashType.FTF, 1],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, 1],
        [CrashType.RT, 1],
        [CrashType.SSD, 1],
        [CrashType.SOD, 1],
        [CrashType.T, CMF],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.13.S6.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[1] as number) > 1) {
        throw new InvalidInputError(
          "Crash Proportion is in decimal form (crash proportion must be < 1)"
        );
      }
    },
  },
  {
    // Macro case: 2K
    name: "Provide TWLTL",
    code: "4.1.13.S12.1",
    prompt:
      "TWLTL Variables - Note: Crash proportion is ignored in calculation due to IDOT Spreadsheet",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Driveway Density (driveways/mi)\nRefer to HSM 1st ed - 16.5.2.1",
      "Input Proportion of Crashes that are Driveway Related (in decimal)\nRefer to HSM 1st ed - 16.5.2.1",
    ],
    inputTypes: ["number", "number"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      let CMF =
        1 -
        0.7 *
          0.5 *
          ((0.0047 * (inputs[0] as number) +
            0.0024 * Math.pow(inputs[0] as number, 2)) /
            (1.199 +
              0.0047 * (inputs[0] as number) +
              0.0024 * Math.pow(inputs[0] as number, 2)));
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, 1],
        [CrashType.FO, 1],
        [CrashType.FTF, 1],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, 1],
        [CrashType.RT, 1],
        [CrashType.SSD, 1],
        [CrashType.SOD, 1],
        [CrashType.T, CMF],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.13.S12.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[1] as number) > 1) {
        throw new InvalidInputError(
          "Crash Proportion is in decimal form (crash proportion must be < 1)"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF: 4B
    name: "Increased pavement friction",
    code: "4.1.17.AL.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Peer Group (number)",
      "Locality (either 'local' or 'state')",
    ],
    inputTypes: ["number", "string"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Segment
      );
      return adjustedCMFVector("4B", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.17.AL.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }

      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 4C
    name: "Install TWLTL (two-way left turn lane) on two lane road",
    code: "4.1.19.S8.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Segment
      );
      return adjustedCMFVector("4C", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.19.S8.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 4D
    name: "Install TWLTL (two-way left turn lane) on two lane road",
    code: "4.1.19.S1.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Segment
      );
      return adjustedCMFVector("4D", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.19.S1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 4E
    name: "Install bicycle lanes",
    code: "4.1.21.S8.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Segment
      );
      return adjustedCMFVector("4E", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.21.S8.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 4E
    name: "Install bicycle lanes",
    code: "4.1.21.S12.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Segment
      );
      return adjustedCMFVector("4E", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.1.21.S12.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: 2L
    name: "Change Width of Existing Median (with Full Access Control)",
    code: "4.2.5.S6.1",
    prompt: "Median Width Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Median Width (Round to nearest 1')\nRefer to HSM 1st ed - 13.4.2.7",
      "Input Future Median Width (Round to nearest 1')\nRefer to HSM 1st ed - 13.4.2.7",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF =
        stepFunction(medianRuralFullAccess, inputs[1] as number) /
        stepFunction(medianRuralFullAccess, inputs[0] as number);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, 1],
        [CrashType.FTF, CMF],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, 1],
        [CrashType.RT, 1],
        [CrashType.SSD, 1],
        [CrashType.SOD, CMF],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.2.5.S6.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2M
    name: "Change Width of Existing Median (with Partial/No Access Control)",
    code: "4.2.5.S6.2",
    prompt: "Median Width Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Median Width (Round to nearest 1')\nRefer to HSM 1st ed - 13.4.2.7",
      "Input Future Median Width (Round to nearest 1')\nRefer to HSM 1st ed - 13.4.2.7",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF =
        stepFunction(medianRuralNoAccess, inputs[1] as number) /
        stepFunction(medianRuralNoAccess, inputs[0] as number);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, 1],
        [CrashType.FTF, CMF],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, 1],
        [CrashType.RT, 1],
        [CrashType.SSD, 1],
        [CrashType.SOD, CMF],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.2.5.S6.2",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2N
    name: "Change Width of Existing Median (with Full Access Control)",
    code: "4.2.5.S12.1",
    prompt: "Median Width Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Median Width (Round to nearest 1')\nRefer to HSM 1st ed - 13.4.2.7",
      "Input Future Median Width (Round to nearest 1')\nRefer to HSM 1st ed - 13.4.2.7",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF =
        stepFunction(medianUrbanFullAccess, inputs[1] as number) /
        stepFunction(medianUrbanFullAccess, inputs[0] as number);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, 1],
        [CrashType.FTF, CMF],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, 1],
        [CrashType.RT, 1],
        [CrashType.SSD, 1],
        [CrashType.SOD, CMF],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.2.5.S12.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2O
    name: "Change Width of Existing Median (with Partial/No Access Control)",
    code: "4.2.5.S12.2",
    prompt: "Median Width Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Median Width (Round to nearest 1')\nRefer to HSM 1st ed - 13.4.2.7",
      "Input Future Median Width (Round to nearest 1')\nRefer to HSM 1st ed - 13.4.2.7",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF =
        stepFunction(medianUrbanNoAccess, inputs[1] as number) /
        stepFunction(medianUrbanNoAccess, inputs[0] as number);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, 1],
        [CrashType.FTF, CMF],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, 1],
        [CrashType.RT, 1],
        [CrashType.SSD, 1],
        [CrashType.SOD, CMF],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.2.5.S12.2",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 4F
    name: "Convert an open median to a directional median",
    code: "4.2.7.S12.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Segment
      );
      return adjustedCMFVector("4F", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.2.7.S12.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 4G
    name: "Convert an open median to a left-in only median",
    code: "4.2.7.S12.2",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Segment
      );
      return adjustedCMFVector("4G", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.2.7.S12.2",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: 4A
    name: "Change in median opening density from X to Y median openings",
    code: "4.2.10.S12.1",
    prompt: "Median Opening Density Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing median opening density (Median Openings/Mile)\nRefer to CMF Clearinghouse #2492",
      "Proposed median opening density (Median Openings/Mile)\nRefer to CMF Clearinghouse #2492",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      const CMF1 = roundNumber(
        Math.exp(0.0985 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF2 = roundNumber(
        Math.exp(0.0481 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF3 = roundNumber(
        Math.exp(0.1129 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );

      return new Map([
        [CrashType.AG, CMF1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF2],
        [CrashType.FTF, CMF3],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, CMF2],
        [CrashType.ROR, CMF2],
        [CrashType.RT, 1],
        [CrashType.SSD, CMF2],
        [CrashType.SOD, 1],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.2.10.S12.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 4B
    name: "Change bridge width (bridge minus roadway width) from X to Y",
    code: "4.5.13.AL.1",
    prompt: "Bridge Width Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing bridge width minus roadway width (Feet)\nRefer to CMF Clearinghouse #2839",
      "Proposed bridge width minus roadway width (Feet)\nRefer to CMF Clearinghouse #2839",
    ],
    inputTypes: ["number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      const CMF = roundNumber(
        Math.exp(-0.116 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.5.13.AL.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2P
    name: "Improve Superelevation on Curve",
    code: "4.6.1.S1.1",
    prompt: "Superelevation Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Superelevation Variance (Decimal)\nRefer to HSM 1st ed - 13.6.2.2",
      "Input Future Superelevation Variance (Decimal)\nRefer to HSM 1st ed - 13.6.2.2",
    ],
    inputTypes: ["number", "number"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // AK940/AJ940
      // AK = IF(AI940<0.01,1,IF((AI940)>0.02,1.06+3*((AI940)-0.02),(1+6*((AI940)-0.01))))
      // Where AI940 = user input 2
      // AJ = IF(AH940<0.01,1,IF((AH940)>0.02,1.06+3*((AH940)-0.02),(1+6*((AH940)-0.01))))
      // Where IH940 = user input 2

      let AK: number;
      if ((inputs[1] as number) < 0.01) {
        AK = 1;
      } else {
        if ((inputs[1] as number) > 0.02) {
          AK = 1.06 + 3 * ((inputs[1] as number) - 0.02);
        } else {
          AK = 1 + 6 * ((inputs[1] as number) - 0.01);
        }
      }

      let AJ: number;
      if ((inputs[0] as number) < 0.01) {
        AJ = 1;
      } else {
        if ((inputs[0] as number) > 0.02) {
          AJ = 1.06 + 3 * ((inputs[0] as number) - 0.02);
        } else {
          AJ = 1 + 6 * ((inputs[0] as number) - 0.01);
        }
      }

      let CMF = AK / AJ;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.6.1.S1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if (
        (inputs[0] as number) < 0 ||
        (inputs[1] as number) < 0 ||
        (inputs[0] as number) > 0.4 ||
        (inputs[1] as number) > 0.4
      ) {
        throw new InvalidInputError(
          "Input invalid. Please enter a decimal number between 0 and 0.4."
        );
      }
    },
  },

  {
    // Macro case: 2Q
    name: "Modify Horizontal Curve Radius and Length w/ Spiral Transitions",
    code: "4.6.9.S1.1",
    prompt: "Horizontal Curve Variables",
    numberOfInputs: 4,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Curve Length (mi)\nRefer to HSM 1st ed - 13.6.2.1",
      "Input Existing Radius of Curvature (feet)\nRefer to HSM 1st ed - 13.6.2.1",
      "Input Future Curve Length (mi)\nRefer to HSM 1st ed - 13.6.2.1",
      "Input Future Radius of Curvature (feet)\nRefer to HSM 1st ed - 13.6.2.1",
    ],
    inputTypes: ["number", "number", "number", "number"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // CMF = AK948/AJ948
      // Where AK =((1.55*" & var3 & ")+(80.2/" & var4 & ")-(0.012*1))/(1.55*" & var3 & ")
      // Where AJ = ((1.55*" & var1 & ")+(80.2/" & var2 & ")-(0.012*1))/(1.55*" & var1 & ")

      const AK: number =
        (1.55 * (inputs[2] as number) +
          80.2 / (inputs[3] as number) -
          0.012 * 1) /
        (1.55 * (inputs[2] as number));
      const AJ: number =
        (1.55 * (inputs[0] as number) +
          80.2 / (inputs[1] as number) -
          0.012 * 1) /
        (1.55 * (inputs[0] as number));

      let CMF = AK / AJ;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.6.9.S1.1",
        correctNumberOfInputs: 4,
        correctTypeArray: ["number", "number", "number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2R
    name: "Modify Horizontal Curve Radius and Length w/o Spiral Transitions",
    code: "4.6.9.S1.2",
    prompt: "Horizontal Curve Variables",
    numberOfInputs: 4,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Curve Length (mi)\nRefer to HSM 1st ed - 13.6.2.1",
      "Input Existing Radius of Curvature (feet)\nRefer to HSM 1st ed - 13.6.2.1",
      "Input Future Curve Length (mi)\nRefer to HSM 1st ed - 13.6.2.1",
      "Input Future Radius of Curvature (feet)\nRefer to HSM 1st ed - 13.6.2.1",
    ],
    inputTypes: ["number", "number", "number", "number"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // CMF = AK948/AJ948
      // Where AK = ((1.55*" & var3 & ")+(80.2/" & var4 & ")-(0.012*0))/(1.55*" & var3 & ")
      // Where AJ = ((1.55*" & var1 & ")+(80.2/" & var2 & ")-(0.012*0))/(1.55*" & var1 & ")

      const AK: number =
        (1.55 * (inputs[2] as number) +
          80.2 / (inputs[3] as number) -
          0.012 * 0) /
        (1.55 * (inputs[2] as number));

      const AJ: number =
        (1.55 * (inputs[0] as number) +
          80.2 / (inputs[1] as number) -
          0.012 * 0) /
        (1.55 * (inputs[0] as number));

      let CMF = AK / AJ;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.6.9.S1.2",
        correctNumberOfInputs: 4,
        correctTypeArray: ["number", "number", "number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2S
    name: "Flatten Sideslopes",
    code: "4.7.3.S1.1",
    prompt: "Sideslope Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only).\nRefer to HSM 1st ed - 13.5.2.1",
      "Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only).\nRefer to HSM 1st ed - 13.5.2.1",
    ],
    inputTypes: ["string", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // CMF = VLOOKUP(AH961,$Module02b.$B$21:$F$25,VLOOKUP(AI961,$Module02b.$H$20:$I$23,2,0),0)
      // Where AH961 is user input 1 (sideslope before)
      // Where AI961 is user input 2 (sideslope after)

      inputs[0] = (inputs[0] as string).toUpperCase();
      inputs[1] = (inputs[1] as string).toUpperCase();

      let CMF = sideslopeCMFTwoLane
        .get(inputs[0] as string)
        .get(inputs[1] as string);

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.7.3.S1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["string", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      inputs[0] = (inputs[0] as string).toUpperCase();
      inputs[1] = (inputs[1] as string).toUpperCase();

      if (
        inputs[0] !== "1V:2H" &&
        inputs[0] !== "1V:3H" &&
        inputs[0] !== "1V:4H" &&
        inputs[0] !== "1V:5H" &&
        inputs[0] !== "1V:6H"
      ) {
        throw new InvalidInputError(
          "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
        );
      }
      if (
        inputs[1] !== "1V:4H" &&
        inputs[1] !== "1V:5H" &&
        inputs[1] !== "1V:6H" &&
        inputs[1] !== "1V:7H"
      ) {
        throw new InvalidInputError(
          "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
        );
      }

      const startingShallowness = Number((inputs[0] as string).charAt(3));
      const futureShallowness = Number((inputs[1] as string).charAt(3));
      if (startingShallowness > futureShallowness) {
        throw new InvalidInputError(
          "Error, flattening a sideslope cannot make it less flat. Existing sideslope must be steeper than future sideslope."
        );
      }
    },
  },
  {
    // Macro case: 2AT
    name: "Flatten Sideslopes",
    code: "4.7.3.S6.1",
    prompt: "Sideslope Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only).\nRefer to HSM 1st ed - 13.5.2.1",
      "Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only).\nRefer to HSM 1st ed - 13.5.2.1",
    ],
    inputTypes: ["string", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // CMF = AK962/AJ962
      // Where AK962 = VLOOKUP(AI962,$Module02b.$B$30:$C$35,2,0)
      // Where AI962 = User input 2 (future sideslope)
      // Where AJ962 = VLOOKUP(AH962,$Module02b.$B$30:$C$35,2,0)
      // Where AH962 = User input 1 (current sideslope)

      inputs[0] = (inputs[0] as string).toUpperCase();
      inputs[1] = (inputs[1] as string).toUpperCase();

      let CMF =
        sideslopeCMFMultilane.get(inputs[1] as string) /
        sideslopeCMFMultilane.get(inputs[0] as string);
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.7.3.S6.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["string", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      inputs[0] = (inputs[0] as string).toUpperCase();
      inputs[1] = (inputs[1] as string).toUpperCase();

      if (
        inputs[0] !== "1V:2H" &&
        inputs[0] !== "1V:3H" &&
        inputs[0] !== "1V:4H" &&
        inputs[0] !== "1V:5H" &&
        inputs[0] !== "1V:6H"
      ) {
        throw new InvalidInputError(
          "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
        );
      }
      if (
        inputs[1] !== "1V:4H" &&
        inputs[1] !== "1V:5H" &&
        inputs[1] !== "1V:6H" &&
        inputs[1] !== "1V:7H"
      ) {
        throw new InvalidInputError(
          "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
        );
      }

      const startingShallowness = Number((inputs[0] as string).charAt(3));
      const futureShallowness = Number((inputs[1] as string).charAt(3));
      if (startingShallowness > futureShallowness) {
        throw new InvalidInputError(
          "Error, flattening a sideslope cannot make it less flat. Existing sideslope must be steeper than future sideslope."
        );
      }
    },
  },
  {
    // Macro case: 2AV
    name: "Flatten Sideslopes",
    code: "4.7.3.S8.1",
    prompt: "Sideslope Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only).\nRefer to HSM 1st ed - 13.5.2.1",
      "Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only).\nRefer to HSM 1st ed - 13.5.2.1",
    ],
    inputTypes: ["string", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // Uses the exact same lookup that rural two-lane does.

      inputs[0] = (inputs[0] as string).toUpperCase();
      inputs[1] = (inputs[1] as string).toUpperCase();

      let CMF = sideslopeCMFTwoLane
        .get(inputs[0] as string)
        .get(inputs[1] as string);
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.7.3.S8.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["string", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      inputs[0] = (inputs[0] as string).toUpperCase();
      inputs[1] = (inputs[1] as string).toUpperCase();

      if (
        inputs[0] !== "1V:2H" &&
        inputs[0] !== "1V:3H" &&
        inputs[0] !== "1V:4H" &&
        inputs[0] !== "1V:5H" &&
        inputs[0] !== "1V:6H"
      ) {
        throw new InvalidInputError(
          "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
        );
      }
      if (
        inputs[1] !== "1V:4H" &&
        inputs[1] !== "1V:5H" &&
        inputs[1] !== "1V:6H" &&
        inputs[1] !== "1V:7H"
      ) {
        throw new InvalidInputError(
          "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
        );
      }

      const startingShallowness = Number((inputs[0] as string).charAt(3));
      const futureShallowness = Number((inputs[1] as string).charAt(3));
      if (startingShallowness > futureShallowness) {
        throw new InvalidInputError(
          "Error, flattening a sideslope cannot make it less flat. Existing sideslope must be steeper than future sideslope."
        );
      }
    },
  },
  {
    // Macro case: 2AW
    name: "Flatten Sideslopes",
    code: "4.7.3.S12.1",
    prompt:
      "Sideslope Variables - NOTE: Spreadsheet ignores user input, behavior copied here but likely incorrect",
    numberOfInputs: 1, // If the countermeasure in the spreadsheet did not ignore existing sideslope this value would be 2
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only).\nRefer to HSM 1st ed - 13.5.2.1",
    ],
    // These would be the prompts if the countermeasure did not ignore Existing Sideslope
    //  [
    //   "Input Existing Sideslope (in 1V:xH format, whole numbers between 1V:2H and 1V:6H only).\nRefer to HSM 1st ed - 13.5.2.1",
    //   "Input Future Sideslope (in 1V:xH format, whole numbers between 1V:4H and 1V:7H only).\nRefer to HSM 1st ed - 13.5.2.1",
    // ],
    inputTypes: ["string"],
    // These would be the prompts if the countermeasure did not ignore Existing Sideslope
    // ["string", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // NOTE: I think the spreadsheet is incorrect here. I believe AJ should be a lookup rather than hard-coded to 1.12
      // TODO: Contact IDOT about this.

      inputs[0] = (inputs[0] as string).toUpperCase();

      let CMF = sideslopeCMFMultilane.get(inputs[0] as string) / 1.12;
      // If the countermeasure did not ignore existing sideslope the CMF would look something like:
      // sideslopeCMFMultilane.get(inputs[1] as string) / sideslopeCMFMultilane.get(inputs[0] as string)
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.7.3.S12.1",
        correctNumberOfInputs: 1,
        correctTypeArray: ["string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      // The following logic is correct because the countermeasure ignores the existing sideslope

      inputs[0] = (inputs[0] as string).toUpperCase();

      if (
        inputs[0] !== "1V:4H" &&
        inputs[0] !== "1V:5H" &&
        inputs[0] !== "1V:6H" &&
        inputs[0] !== "1V:7H"
      ) {
        throw new InvalidInputError(
          "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
        );
      }

      /* The following logic applies if the countermeasure did not ignore the existing sideslope
      if (
        inputs[0] !== "1V:2H" &&
        inputs[0] !== "1V:3H" &&
        inputs[0] !== "1V:4H" &&
        inputs[0] !== "1V:5H" &&
        inputs[0] !== "1V:6H"
      ) {
        throw new InvalidInputError(
          "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H."
        );
      }
      if (
        inputs[1] !== "1V:4H" &&
        inputs[1] !== "1V:5H" &&
        inputs[1] !== "1V:6H" &&
        inputs[1] !== "1V:7H"
      ) {
        throw new InvalidInputError(
          "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H."
        );
      }

      const startingShallowness = Number((inputs[0] as string).charAt(3));
      const futureShallowness = Number((inputs[1] as string).charAt(3));
      if (startingShallowness > futureShallowness) {
        throw new InvalidInputError(
          "Error, flattening a sideslope cannot make it less flat. Existing sideslope must be steeper than future sideslope."
        );
      }
      **/
    },
  },
  {
    // Macro case: 2U
    name: "Increase Distance to Roadside Features",
    code: "4.7.4.SR.1",
    prompt: "Roadside Feature Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Distance to Roadside Features (in feet)\nRefer to HSM 1st ed - 13.5.2.2",
      "Input Future Distance to Roadside Features (in feet)\nRefer to HSM 1st ed - 13.5.2.2",
    ],
    inputTypes: ["number", "number"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // CMF = AK965 / AJ965
      // Where AK and AJ are lookups
      const AK = stepFunction(roadsideDistance, inputs[1] as number);
      const AJ = stepFunction(roadsideDistance, inputs[0] as number);
      let CMF = AK / AJ;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.7.4.SR.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2V
    name: "Reduce Roadside Hazard Rating",
    code: "4.7.7.S1.1",
    prompt: "Roadside Hazard Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing Roadside Hazard Rating (1-7)\nRefer to HSM 1st ed - 13.5.2.6",
      "Future Roadside Hazard Rating (1-7)\nRefer to HSM 1st ed - 13.5.2.6",
    ],
    inputTypes: ["number", "number"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      // CMF=AK968/AJ968
      // Where AK = EXP(-0.6869+(0.0668* user input 2))/EXP(-0.4865)
      // Where AJ = EXP(-0.6869+(0.0668* user input 1))/EXP(-0.4865)

      const AK =
        Math.exp(-0.6869 + 0.0668 * (inputs[1] as number)) / Math.exp(-0.4865);
      const AJ =
        Math.exp(-0.6869 + 0.0668 * (inputs[0] as number)) / Math.exp(-0.4865);
      let CMF = AK / AJ;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.7.7.S1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if (
        (inputs[0] as number) < 1 ||
        (inputs[1] as number) < 1 ||
        (inputs[0] as number) > 7 ||
        (inputs[1] as number) > 7
      ) {
        throw new InvalidInputError(
          "Roadside Hazard Rating must be Between 1-7"
        );
      }
    },
  },
  {
    // Macro case: 2W
    name: "Convert Angle Parking to Parallel Parking (Residential)",
    code: "4.7.11.SU.1",
    prompt: "Parking Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing Roadside Hazard Rating (1-7)\nRefer to HSM 1st ed - 13.5.2.6",
      "Future Roadside Hazard Rating (1-7)\nRefer to HSM 1st ed - 13.5.2.6",
    ],
    inputTypes: ["number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      const AK: number =
        1 +
        ((0.5 * (inputs[0] as number)) / (inputs[1] as number)) * (1.465 - 1);
      const AJ: number =
        1 +
        ((0.5 * (inputs[0] as number)) / (inputs[1] as number)) * (3.428 - 1);

      const CMF = roundNumber(AK / AJ, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.7.11.SU.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2AA
    name: "Convert Angle Parking to Parallel Parking (Commercial/Industrial)",
    code: "4.7.11.SU.2",
    prompt: "Bridge Width Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing bridge width minus roadway width (Feet)\nRefer to CMF Clearinghouse #2839",
      "Proposed bridge width minus roadway width (Feet)\nRefer to CMF Clearinghouse #2839",
    ],
    inputTypes: ["number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      const AJ: number =
        1 +
        ((0.5 * (inputs[0] as number)) / (inputs[1] as number)) * (4.853 - 1);
      const AK: number =
        1 +
        ((0.5 * (inputs[0] as number)) / (inputs[1] as number)) * (2.074 - 1);

      const CMF = roundNumber(AK / AJ, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.7.11.SU.2",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 2AE
    name: "Modify Access Point Density",
    code: "4.8.3.S1.1",
    prompt: "Access Variables",
    numberOfInputs: 3,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing Driveway Density (Dwy/Mi)\nRefer to HSM 1st ed - 13.14.2.1",
      "Future Driveway Density  (Dwy/Mi)\nRefer to HSM 1st ed - 13.14.2.1",
      "Effective Service Life (Whole number years, 1-50)\nRefer to HSM 1st ed - 13.14.2.1",
    ],
    inputTypes: ["number", "number", "number"],
    calculate(
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector {
      // CMF=AK968/AJ968
      // Where AK = (0.322+AI986*(0.05-0.005*LN($AF$986)))/(0.322+(5*(0.05-0.005*LN($AF$986))))
      // Where AI986 is user input 2
      // And AF986 is aadt
      // Where AJ = (0.322+AH986*(0.05-0.005*LN(AF986)))/(0.322+(5*(0.05-0.005*LN(AF986))))
      // Where AH986 is user input 1

      const AK =
        (0.322 + (inputs[1] as number) * (0.05 - 0.005 * Math.log(aadtMajor))) /
        (0.322 + 5 * (0.05 - 0.005 * Math.log(aadtMajor)));

      const AJ =
        (0.322 + (inputs[0] as number) * (0.05 - 0.005 * Math.log(aadtMajor))) /
        (0.322 + 5 * (0.05 - 0.005 * Math.log(aadtMajor)));

      let CMF = AK / AJ;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.8.3.S1.1",
        correctNumberOfInputs: 3,
        correctTypeArray: ["number", "number", "number"],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if (aadtMajor === 0) {
        throw new InvalidInputError(
          "The AADT for 4.8.3.S1.1 cannot be zero as it uses the natural log of the AADT to calculate its CMF (and the natural log of 0 is -infinity)"
        );
      }

      if ((inputs[2] as number) < 1 || (inputs[2] as number) > 50) {
        throw new InvalidInputError(
          "Effective service life must be a number between 1 and 50"
        );
      }
    },
  },
  {
    // Macro case: None
    name: "Install Speed Humps",
    code: "4.8.3.SU.1",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // CMF is hard coded to 0.6

      const CMF = 0.6;
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.8.3.SU.1",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  // start of new
  {
    // Macro case: 2AF
    name: "Modify Access Point Density",
    code: "4.8.3.S8.1",
    prompt: "Access Variables",
    numberOfInputs: 3,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing Driveway Density (Dwy/Mi)\nRefer to HSM 1st ed - 13.14.2.1",
      "Future Driveway Density  (Dwy/Mi)\nRefer to HSM 1st ed - 13.14.2.1",
      "Effective Service Life (Whole number years, 1-50)\nRefer to HSM 1st ed - 13.14.2.1",
    ],
    inputTypes: ["number", "number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      const AK = stepFunction(modifyAccessPointDensity, inputs[1] as number);
      const AJ = stepFunction(modifyAccessPointDensity, inputs[0] as number);

      let CMF = AK / AJ;

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.8.3.S8.1",
        correctNumberOfInputs: 3,
        correctTypeArray: ["number", "number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[2] as number) < 1 || (inputs[2] as number) > 50) {
        throw new InvalidInputError(
          "Effective service life must be a number between 1 and 50"
        );
      }
    },
  },
  {
    // Macro case: 2AH
    name: "Modify Access Point Density",
    code: "4.8.3.S12.1",
    prompt: "Access Variables",
    numberOfInputs: 3,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing Driveway Density (Dwy/Mi)\nRefer to HSM 1st ed - 13.14.2.1",
      "Future Driveway Density  (Dwy/Mi)\nRefer to HSM 1st ed - 13.14.2.1",
      "Effective Service Life (Whole number years, 1-50)\nRefer to HSM 1st ed - 13.14.2.1",
    ],
    inputTypes: ["number", "number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      const AK = stepFunction(modifyAccessPointDensity, inputs[1] as number);
      const AJ = stepFunction(modifyAccessPointDensity, inputs[0] as number);

      let CMF = AK / AJ;

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.8.3.S12.1",
        correctNumberOfInputs: 3,
        correctTypeArray: ["number", "number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[2] as number) < 1 || (inputs[2] as number) > 50) {
        throw new InvalidInputError(
          "Effective service life must be a number between 1 and 50"
        );
      }
    },
  },
  {
    // Macro case: 2AI
    name: "Modify Work Zone Duration",
    code: "4.8.8.S6.1",
    prompt: "Work Zone Duration Variables",
    numberOfInputs: 3,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing Work Zone Duration (Days)\nRefer to HSM 1st ed - 16.4.2.1",
      "Proposed Work Zone Duration (Days)\nRefer to HSM 1st ed - 16.4.2.1",
      "Effective Service Life (Whole number years, 1-50)\nRefer to HSM 1st ed - 16.4.2.1",
    ],
    inputTypes: ["number", "number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      const ratio: number = (inputs[1] as number) / (inputs[0] as number);
      const intermediateResult: number = ((ratio - 1) * 100 * 1.11) / 100;
      let CMF: number =
        1 + intermediateResult < 0.01
          ? 0.01
          : 1 + ((ratio - 1) * 100 * 1.11) / 100;

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.8.8.S6.1",
        correctNumberOfInputs: 3,
        correctTypeArray: ["number", "number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[2] as number) < 1 || (inputs[2] as number) > 50) {
        throw new InvalidInputError(
          "Effective service life must be a number between 1 and 50"
        );
      }
    },
  },
  {
    // Macro case: 2AK
    name: "Modify Work Zone Duration",
    code: "4.8.8.S12.1",
    prompt: "Work Zone Duration Variables",
    numberOfInputs: 3,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing Work Zone Duration (Days)\nRefer to HSM 1st ed - 16.4.2.1",
      "Proposed Work Zone Duration (Days)\nRefer to HSM 1st ed - 16.4.2.1",
      "Effective Service Life (Whole number years, 1-50)\nRefer to HSM 1st ed - 16.4.2.1",
    ],
    inputTypes: ["number", "number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      const ratio: number = (inputs[1] as number) / (inputs[0] as number);
      const intermediateResult: number = ((ratio - 1) * 100 * 1.11) / 100;
      let CMF: number =
        1 + intermediateResult < 0.01
          ? 0.01
          : 1 + ((ratio - 1) * 100 * 1.11) / 100;

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.8.8.S12.1",
        correctNumberOfInputs: 3,
        correctTypeArray: ["number", "number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[2] as number) < 1 || (inputs[2] as number) > 50) {
        throw new InvalidInputError(
          "Effective service life must be a number between 1 and 50"
        );
      }
    },
  },
  {
    // Macro case: 2AN
    name: "Modify Work Zone Length",
    code: "4.8.9.S6.1",
    prompt: "Work Zone Length Variables",
    numberOfInputs: 3,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing Work Zone Length (Miles)\nRefer to HSM 1st ed - 16.4.2.1",
      "Proposed Work Zone Length (Miles)\nRefer to HSM 1st ed - 16.4.2.1",
      "Effective Service Life (Whole number years, 1-50)\nRefer to HSM 1st ed - 16.4.2.1",
    ],
    inputTypes: ["number", "number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      const ratio: number = (inputs[1] as number) / (inputs[0] as number);
      const intermediateResult: number = ((ratio - 1) * 100 * 0.67) / 100;
      let CMF: number =
        1 + intermediateResult < 0.01
          ? 0.01
          : 1 + ((ratio - 1) * 100 * 0.67) / 100;

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.8.9.S6.1",
        correctNumberOfInputs: 3,
        correctTypeArray: ["number", "number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[2] as number) < 1 || (inputs[2] as number) > 50) {
        throw new InvalidInputError(
          "Effective service life must be a number between 1 and 50"
        );
      }
    },
  },
  {
    // Macro case: 2AP
    name: "Modify Work Zone Length",
    code: "4.8.9.S12.1",
    prompt: "Work Zone Length Variables",
    numberOfInputs: 3,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing Work Zone Length (Miles)\nRefer to HSM 1st ed - 16.4.2.1",
      "Proposed Work Zone Length (Miles)\nRefer to HSM 1st ed - 16.4.2.1",
      "Effective Service Life (Whole number years, 1-50)\nRefer to HSM 1st ed - 16.4.2.1",
    ],
    inputTypes: ["number", "number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      const ratio: number = (inputs[1] as number) / (inputs[0] as number);
      const intermediateResult: number = ((ratio - 1) * 100 * 0.67) / 100;
      let CMF: number =
        1 + intermediateResult < 0.01
          ? 0.01
          : 1 + ((ratio - 1) * 100 * 0.67) / 100;

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.8.9.S12.1",
        correctNumberOfInputs: 3,
        correctTypeArray: ["number", "number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[2] as number) < 1 || (inputs[2] as number) > 50) {
        throw new InvalidInputError(
          "Effective service life must be a number between 1 and 50"
        );
      }
    },
  },
  // end of new
  {
    // Macro case: 4C
    name: "Change driveway density from X to Y (driveways/mile for segment)",
    code: "4.8.23.S1.1",
    prompt: "Driveway Density Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing driveway density (Driveways/Mile)\nRefer to CMF Clearinghouse #2248",
      "Proposed driveway density (Driveways/Mile)\nRefer to CMF Clearinghouse #2248",
    ],
    inputTypes: ["number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      // CMF=EXP(0.0232*(AI1005-AH1005))
      // Where AI1005 is user input 2
      // And AH1005 is user input 1

      let CMF = Math.exp(
        0.0232 * ((inputs[1] as number) - (inputs[0] as number))
      );

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "4.8.23.S1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  // start of new
  {
    // Macro case: 4C
    name: "Change in driveway density from X to Y driveways per mile",
    code: "4.8.24.S12.1",
    prompt: "Driveway Density Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing driveway density (Driveways/Mile)\nRefer to CMF Clearinghouse #2248",
      "Proposed driveway density (Driveways/Mile)\nRefer to CMF Clearinghouse #2248",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      const CMF1: number = roundNumber(
        Math.exp(0.0087 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF2: number = roundNumber(
        Math.exp(0.0096 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF3: number = roundNumber(
        Math.exp(0.003 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );

      return new Map([
        [CrashType.AG, CMF1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF2],
        [CrashType.FTF, CMF2],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, CMF3],
        [CrashType.ROR, CMF2],
        [CrashType.RT, 1],
        [CrashType.SSD, CMF2],
        [CrashType.SOD, 1],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.8.24.S12.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 4DA
    name: "Change in signal spacing from X 1000's feet to Y 1000's feet",
    code: "4.8.25.S12.1",
    prompt: "Traffic Signal Spacing Variables (1000ft per Signal)",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing traffic signal spacing (x*1000ft/Traffic Signal)\nRefer to CMF Clearinghouse #2497",
      "Proposed traffic signal spacing (x*1000ft/Traffic Signal\nRefer to CMF Clearinghouse #2497",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      const CMF1: number = roundNumber(
        Math.exp(-0.1144 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF2: number = roundNumber(
        Math.exp(0.0977 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF3: number = roundNumber(
        Math.exp(-0.2493 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF4: number = roundNumber(
        Math.exp(-0.1684 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );

      return new Map([
        [CrashType.AG, CMF1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF2],
        [CrashType.FTF, CMF1],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, CMF3],
        [CrashType.ROR, CMF2],
        [CrashType.RT, 1],
        [CrashType.SSD, CMF4],
        [CrashType.SOD, 1],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.8.25.S12.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 4E
    name: "Change in unsignalized cross roads from X to Y unsignalized cross roads per mile",
    code: "4.8.26.S12.1",
    prompt: "Unsignalized Cross Roads Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing unsignalized cross roads density (# Cross Roads/Mile)\nRefer to CMF Clearinghouse #2501",
      "Proposed unsignalized cross roads density (# Cross Roads/Mile)\nRefer to CMF Clearinghouse #2501",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      const CMF1: number = roundNumber(
        Math.exp(0.0126 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF2: number = roundNumber(
        Math.exp(0.0333 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF3: number = roundNumber(
        Math.exp(0.023 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF4: number = roundNumber(
        Math.exp(0.0254 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );

      return new Map([
        [CrashType.AG, CMF1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF1],
        [CrashType.FTF, CMF2],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, CMF3],
        [CrashType.ROR, CMF1],
        [CrashType.RT, 1],
        [CrashType.SSD, CMF4],
        [CrashType.SOD, 1],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.8.26.S12.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 4FA
    name: "Change number of 3-leg intersections from X to Y",
    code: "4.8.27.SU.1",
    prompt: "3-Leg Intersection Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing number of 3-leg intersections in study area\nRefer to CMF Clearinghouse #3944",
      "Proposed number of 3-leg intersections in study area\nRefer to CMF Clearinghouse #3944",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      const CMF1: number = roundNumber(
        Math.exp(-0.001 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF2: number = roundNumber(
        Math.exp(-0.004 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF3: number = roundNumber(
        Math.exp(0.002 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );

      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF1],
        [CrashType.FTF, 1],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, CMF2],
        [CrashType.PDC, CMF3],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, 1],
        [CrashType.RT, 1],
        [CrashType.SSD, 1],
        [CrashType.SOD, 1],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.8.27.SU.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 4FB
    name: "Change number of 4-or-more-leg intersections from X to Y",
    code: "4.8.28.SU.1",
    prompt: "4-Leg Intersection Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing number of 4-leg intersections in study area\nRefer to CMF Clearinghouse #3939",
      "Proposed number of 4-leg intersections in study area\nRefer to CMF Clearinghouse #3939",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      const CMF1: number = roundNumber(
        Math.exp(0.006 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF2: number = roundNumber(
        Math.exp(0.009 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF3: number = roundNumber(
        Math.exp(0.013 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );
      const CMF4: number = roundNumber(
        Math.exp(0.004 * ((inputs[1] as number) - (inputs[0] as number))),
        9
      );

      return new Map([
        [CrashType.AG, CMF1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF2],
        [CrashType.FTF, CMF1],
        [CrashType.LT, CMF1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, CMF2],
        [CrashType.PDC, CMF3],
        [CrashType.PKV, CMF4],
        [CrashType.FTR, CMF1],
        [CrashType.ROR, CMF1],
        [CrashType.RT, CMF1],
        [CrashType.SSD, CMF1],
        [CrashType.SOD, 1],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.8.28.SU.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 4G
    name: "Change in median opening density from X to Y median openings",
    code: "4.8.30.S1.1",
    prompt: "4-Leg Intersection Variables",
    numberOfInputs: 1,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing traffic volume of study area (AADT)\nRefer to CMF Clearinghouse #2978",
    ],
    inputTypes: ["number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      const CMF = roundNumber(
        Math.exp(0.1123 - 0.0003 * (inputs[0] as number)),
        9
      );

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.8.30.S1.1",
        correctNumberOfInputs: 1,
        correctTypeArray: ["number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 4DB
    // This one is both calculated and adjusted
    // Adjustment Flag 4I
    name: "Change in median opening density from X to Y median openings",
    code: "4.8.31.S12.1",
    prompt: "Traffic Signal Spacing Variables (Signals per Mile)",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Existing traffic signal spacing (Signals/Mile)\nRefer to CMF Clearinghouse #2217, #2218",
      "Proposed traffic signal spacing (Signals/Mile)\nRefer to CMF Clearinghouse #2217, #2218",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      const calculatedValue =
        Math.round(
          Math.exp(0.919 * ((inputs[1] as number) - (inputs[0] as number))) *
            100
        ) / 100;
      const distributionTag = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Segment
      );

      return calculatedAdjustedCMFVector(
        "4I",
        distributionTag,
        calculatedValue
      );
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "4.8.31.S12.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  // end of new

  // Start of code 3

  {
    // Macro case: None
    name: "Convert Four-leg Intersection to Two Three-Leg Intersections",
    code: "3.1.1.I5.1",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: true,
    aadtMinorRequired: true,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // =IF(AG535>0.3*(AF535+AG535),0.67,IF(AG535<0.15*(AF535+AG535),1.35,0.75))

      let CMF =
        aadtMinor > 0.3 * (aadtMajor + aadtMinor)
          ? 0.67
          : aadtMinor < 0.15 * (aadtMajor + aadtMinor)
          ? 1.35
          : 0.75;

      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.1.1.I5.1",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: true,
        aadtMinorRequired: true,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Convert a 3-leg unsignalized intersection at a driveway to a regular 3-leg unsignalized intersection",
    code: "3.1.2.I5.1",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // CMF is hard coded to 1.41

      const CMF = 1.41;
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.1.2.I5.1",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Convert a 4-leg unsignalized intersection at driveways to a regular 4-leg unsignalized intersection",
    code: "3.1.3.I5.1",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // CMF is hard coded to 1.11

      const CMF = 1.11;
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.1.3.I5.1",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 1A
    name: "Reduce Intersection Skew Angle (3 Leg - Two Lane Facility)",
    code: "3.2.18.I1.1",
    prompt: "Skew Angle Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Skew Angle (Degrees)\nRefer to HSM 1st ed - 14.6.2.1",
      "Input Future Skew Angle (Degrees)\nRefer to HSM 1st ed - 14.6.2.1",
    ],
    inputTypes: ["number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      // CMF=AK578/AJ578
      // Where AK578 = EXP(0.004*AI578)
      // And AJ578 =EXP(0.004*AH578)
      // And AI578 is user input 2
      // And AH578 is user input 1

      let CMF =
        Math.exp(0.004 * (inputs[1] as number)) /
        Math.exp(0.004 * (inputs[0] as number));

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.18.I1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 1B
    name: "Reduce Intersection Skew Angle (4 Leg - Two Lane Facility)",
    code: "3.2.18.I1.2",
    prompt: "Skew Angle Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Skew Angle (Degrees)\nRefer to HSM 1st ed - 14.6.2.1",
      "Input Future Skew Angle (Degrees)\nRefer to HSM 1st ed - 14.6.2.1",
    ],
    inputTypes: ["number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      // CMF=AK578/AJ578
      // Where AK578 = EXP(0.0054*AI578)
      // And AJ578 =EXP(0.0054*AH578)
      // And AI578 is user input 2
      // And AH578 is user input 1

      let CMF =
        Math.exp(0.0054 * (inputs[1] as number)) /
        Math.exp(0.0054 * (inputs[0] as number));

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.18.I1.2",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 1C
    name: "Reduce Intersection Skew Angle (3 Leg - Multilane Facility)",
    code: "3.2.18.I1.3",
    prompt: "Skew Angle Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Skew Angle (Degrees)\nRefer to HSM 1st ed - 14.6.2.1",
      "Input Future Skew Angle (Degrees)\nRefer to HSM 1st ed - 14.6.2.1",
    ],
    inputTypes: ["number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      // CMF=AK/AJ
      // Where AK = ((0.016*AI580)/(0.98+(0.16*AI580)))+1
      // And AJ = ((0.016*AH580)/(0.98+(0.16*AH580)))+1
      // And AI580 is user input 2
      // And AH580 is user input 1

      const AK =
        (0.016 * (inputs[1] as number)) /
          (0.98 + 0.16 * (inputs[1] as number)) +
        1;
      const AJ =
        (0.016 * (inputs[0] as number)) /
          (0.98 + 0.16 * (inputs[0] as number)) +
        1;

      let CMF = AK / AJ;

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.18.I1.3",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: 1D
    name: "Reduce Intersection Skew Angle (4 Leg - Multilane Facility)",
    code: "3.2.18.I1.4",
    prompt: "Skew Angle Variables",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Skew Angle (Degrees)\nRefer to HSM 1st ed - 14.6.2.1",
      "Input Future Skew Angle (Degrees)\nRefer to HSM 1st ed - 14.6.2.1",
    ],
    inputTypes: ["number", "number"],
    calculate(inputs: (string | number)[]): CmfVector {
      // CMF=AK/AJ
      // Where AK = ((0.053*AI581)/(1.43+(0.53*AI581)))+1
      // And AJ = ((0.053*AH581)/(1.43+(0.53*AH581)))+1
      // And AI581 is user input 2
      // And AH581 is user input 1

      const AK =
        (0.053 * (inputs[1] as number)) /
          (1.43 + 0.53 * (inputs[1] as number)) +
        1;
      const AJ =
        (0.053 * (inputs[0] as number)) /
          (1.43 + 0.53 * (inputs[0] as number)) +
        1;

      let CMF = AK / AJ;

      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.18.I1.4",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },

  {
    // Macro case: 2A
    name: "Modify Lane Width",
    code: "3.2.22.IR.1",
    prompt: "Lane Width Variables",
    numberOfInputs: 2,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [
      "Input Existing Lane Width (in feet, round to nearest 1')\nRefer to HSIM 1st ed - 13.4.2.1",
      "Input Future Lane Width (in feet, round to nearest 1')\nRefer to HSIM 1st ed - 13.4.2.1",
    ],
    inputTypes: ["number", "number"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // CMF = AK586/AJ586
      // AK586 = IF($AI586<=9,IF($AF586<400,1.05,IF($AF586>2000,1.5,1.05+2.81*0.0001*($AF586-400))),IF($AI586=10,IF($AF586<400,1.02,IF($AF586>2000,1.3,1.02+1.75*0.0001*($AF586-400))),IF($AI586=11,IF($AF586<400,1.01,IF($AF586>2000,1.05,1.01+2.5*0.00001*($AF586-400))),1)))
      // AJ586 = IF($AH586<=9,IF($AF586<400,1.05,IF($AF586>2000,1.5,1.05+2.81*0.0001*($AF586-400))),IF($AH586=10,IF($AF586<400,1.02,IF($AF586>2000,1.3,1.02+1.75*0.0001*($AF586-400))),IF($AH586=11,IF($AF586<400,1.01,IF($AF586>2000,1.05,1.01+2.5*0.00001*($AF586-400))),1)))
      // Where AI586 is user input 2
      // Where AH586 is user input 1
      // Where AF586 is aadtMajor

      let AK: number;

      if ((inputs[1] as number) <= 9) {
        if (aadtMajor < 400) {
          AK = 1.05;
        } else if (aadtMajor > 2000) {
          AK = 1.5;
        } else {
          AK = 1.05 + 2.81 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 10) {
        if (aadtMajor < 400) {
          AK = 1.02;
        } else if (aadtMajor > 2000) {
          AK = 1.3;
        } else {
          AK = 1.02 + 1.75 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[1] === 11) {
        if (aadtMajor < 400) {
          AK = 1.01;
        } else if (aadtMajor > 2000) {
          AK = 1.05;
        } else {
          AK = 1.01 + 2.5 * 0.00001 * (aadtMajor - 400);
        }
      } else {
        AK = 1;
      }

      let AJ: number;

      if ((inputs[0] as number) <= 9) {
        if (aadtMajor < 400) {
          AJ = 1.05;
        } else if (aadtMajor > 2000) {
          AJ = 1.5;
        } else {
          AJ = 1.05 + 2.81 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 10) {
        if (aadtMajor < 400) {
          AJ = 1.02;
        } else if (aadtMajor > 2000) {
          AJ = 1.3;
        } else {
          AJ = 1.02 + 1.75 * 0.0001 * (aadtMajor - 400);
        }
      } else if (inputs[0] === 11) {
        if (aadtMajor < 400) {
          AJ = 1.01;
        } else if (aadtMajor > 2000) {
          AJ = 1.05;
        } else {
          AJ = 1.01 + 2.5 * 0.00001 * (aadtMajor - 400);
        }
      } else {
        AJ = 1;
      }

      let CMF: number = AK / AJ;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, CMF],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.ROR, CMF],
        [CrashType.RT, 1],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.22.IR.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "number"],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Decrease lane width from 11 ft to 10 ft",
    code: "3.2.23.IU.1",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF = Math.exp(-0.515 + 0.00012 * aadtMajor);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.2.23.IU.1",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Decrease lane width from 11 ft to 9 ft",
    code: "3.2.23.IU.2",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // CMF is hard coded to 0.41

      const CMF = 0.41;
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.2.23.IU.2",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },

  {
    // Macro case: None
    name: "Decrease lane width from 12 ft to 10 ft",
    code: "3.2.23.IU.3",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // CMF is hard coded to 1.31

      const CMF = 1.31;
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.2.23.IU.3",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Decrease lane width from 12 ft to 11 ft",
    code: "3.2.23.IU.4",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF = Math.exp(-0.425 + 0.00009 * aadtMajor);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.2.23.IU.4",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Increase lane width from 10 ft to 11 ft",
    code: "3.2.23.IU.5",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF = 1 / Math.exp(-0.515 + 0.00012 * aadtMajor);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.2.23.IU.5",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Increase lane width from 9 ft to 11 ft",
    code: "3.2.23.IU.6",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // Hard Coded
      let CMF = 1 / 0.41;
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.2.23.IU.6",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Increase lane width from 10 ft to 12 ft",
    code: "3.2.23.IU.7",
    prompt: "",
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // Hard Coded
      let CMF = 1 / 1.32;
      CMF = roundNumber(CMF, 9);

      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.2.23.IU.7",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    name: "Increase lane width from 11 ft to 12 ft",
    code: "3.2.23.IU.8",
    prompt: "Does not have a use prompt but is calculated from AADT Major",
    numberOfInputs: 0,
    aadtMajorRequired: true,
    aadtMinorRequired: false,
    inputPrompts: [],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // =1/EXP(-0.425+0.00009*AF594) Where AF594 is AADT Major
      let CMF = 1 / Math.exp(-0.425 + 0.00009 * aadtMajor);
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.23.IU.8",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: true,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3A
    name: "Improve left-turn lane offset to create positive offset",
    code: "3.2.26.I3.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3A", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.26.I3.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3B
    name: "Improve left-turn lane offset to create positive offset",
    code: "3.2.26.I7.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3B", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.26.I7.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AU
    name: "Introduce raised/curb left-turn channelization",
    code: "3.2.27.I1.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AU", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.27.I1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3C
    name: "Improve left-turn lane offset to create positive offset",
    code: "3.2.28.I1.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3C", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.28.I1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3G
    name: "Install bicycle lanes",
    code: "3.2.32.IU.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3G", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.2.32.IU.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3H
    name: 'Prohibit left-turns with "No Left Turn" sign',
    code: "3.3.15.I5.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3H", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.15.I5.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3I
    name: 'Prohibit left-turns and U-turns with "No Left Turn" and "No U-Turn" signs',
    code: "3.3.16.I5.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3I", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.16.I5.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3J
    name: "Provide flashing beacons at stop controlled intersections",
    code: "3.3.17.I1.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3J", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.17.I1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3K
    name: "Provide flashing beacons at stop controlled intersections",
    code: "3.3.17.I2.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3K", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.17.I2.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3L
    name: "Provide flashing beacons at stop controlled intersections",
    code: "3.3.17.I5.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3L", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.17.I5.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3M
    name: "Provide flashing beacons at stop controlled intersections",
    code: "3.3.17.I6.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3M", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.17.I6.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3N
    name: "Increase retroreflectivity of STOP signs",
    code: "3.3.19.I1.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3N", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.19.I1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3O
    name: "Increase retroreflectivity of STOP signs",
    code: "3.3.19.I2.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3O", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.19.I2.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3P
    name: "Increase retroreflectivity of STOP signs",
    code: "3.3.19.I5.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3P", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.19.I5.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3Q
    name: "Increase retroreflectivity of STOP signs",
    code: "3.3.19.I6.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3Q", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.19.I6.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });

      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3R
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (overhead signs at intersection on major, loops on minor)',
    code: "3.3.20.I1.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3R", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3S
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (overhead signs at intersection on minor, loops on major)',
    code: "3.3.20.I1.2",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3S", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I1.2",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3T
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (advance post mounted signs on major, loops on minor)',
    code: "3.3.20.I1.3",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3T", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I1.3",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3U
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (combination of overhead, advance post mounted signs)',
    code: "3.3.20.I1.4",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3U", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I1.4",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3V
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (overhead signs at intersection on major, loops on minor)',
    code: "3.3.20.I2.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3V", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I2.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3W
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (overhead signs at intersection on minor, loops on major)',
    code: "3.3.20.I2.2",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3W", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I2.2",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3X
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (advance post mounted signs on major, loops on minor)',
    code: "3.3.20.I2.3",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3X", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I2.3",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3Y
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (combination of overhead, advance post mounted signs)',
    code: "3.3.20.I2.4",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3Y", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I2.4",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3Z
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (overhead signs at intersection on major, loops on minor)',
    code: "3.3.20.I5.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3Z", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I5.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AA
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (overhead signs at intersection on minor, loops on major)',
    code: "3.3.20.I5.2",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AA", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I5.2",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AB
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (advance post mounted signs on major, loops on minor)',
    code: "3.3.20.I5.3",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AB", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I5.3",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AC
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (combination of overhead, advance post mounted signs)',
    code: "3.3.20.I5.4",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AC", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I5.4",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AD
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (overhead signs at intersection on major, loops on minor)',
    code: "3.3.20.I6.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AD", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I6.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AE
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (overhead signs at intersection on minor, loops on major)',
    code: "3.3.20.I6.2",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AE", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I6.2",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AF
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (advance post mounted signs on major, loops on minor)',
    code: "3.3.20.I6.3",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AF", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I6.3",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AG
    name: 'Install a "Vehicles Entering When Flashing" (VEWF) system (combination of overhead, advance post mounted signs)',
    code: "3.3.20.I6.4",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AG", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.20.I6.4",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AH
    name: "Install dynamic signal warning flashers",
    code: "3.3.21.I3.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AH", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.21.I3.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AI
    name: "Install dynamic signal warning flashers",
    code: "3.3.21.I7.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AI", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.3.21.I7.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AJ
    name: "Replace Night-Time Flash with Steady Operation",
    code: "3.4.14.I3.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AJ", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.14.I3.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AK
    name: "Replace Night-Time Flash with Steady Operation",
    code: "3.4.14.I7.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AK", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.14.I7.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AL
    name: "Convert from yield signal control to signalized control",
    code: "3.4.24.I7.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AL", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.24.I7.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AM
    name: "Replace Incandescent Traffic Signal Bulbs with Light Emitting Diodes (LEDs)",
    code: "3.4.25.I7.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AM", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.25.I7.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AN
    name: "Installation of an actuated advance warning dilemma zone protection system at high-speed signalized intersections",
    code: "3.4.26.I7.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AN", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.26.I7.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AO
    name: "Changing left turn phasing from protected to flashing yellow arrow (FYA)",
    code: "3.4.28.I3.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AO", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.28.I3.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AO
    name: "Changing left turn phasing from protected to flashing yellow arrow (FYA)",
    code: "3.4.28.I7.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AO", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.28.I7.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AP
    name: "Changing left turn phasing from at least one permissive approach to flashing yellow arrow (FYA)",
    code: "3.4.29.I3.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AP", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.29.I3.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AP
    name: "Changing left turn phasing from at least one permissive approach to flashing yellow arrow (FYA)",
    code: "3.4.29.I7.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AP", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.29.I7.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AQ
    name: "Changing left turn phasing from protected-permissive to flashing yellow arrow (FYA)",
    code: "3.4.30.I3.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AQ", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.30.I3.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AQ
    name: "Changing left turn phasing from protected-permissive to flashing yellow arrow (FYA)",
    code: "3.4.30.I7.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AQ", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.30.I7.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AR
    name: "Convert a conventional signalized intersection to a signalized superstreet",
    code: "3.4.32.I3.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AR", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.32.I3.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AT
    name: "Convert a conventional unsignalized intersection to an unsignalized superstreet",
    code: "3.4.33.I1.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AT", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.33.I1.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    // Adjusted CMF Tag: 3AS
    name: "Installation of a High intensity Activated crossWalK (HAWK) pedestrian-activated beacon at an intersection",
    code: "3.4.35.I5.1",
    prompt: "Adjusted cmf determined by locality and peer group",
    numberOfInputs: 2,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    inputPrompts: ["Peer Group", "Locality"],
    inputTypes: ["number", "string"],
    calculate: (inputs: (string | number)[]): CmfVector => {
      const distributionLabel = createDistributionTag(
        inputs[0] as number,
        inputs[1] as "state" | "local",
        RoadwayType.Intersection
      );
      return adjustedCMFVector("3AS", distributionLabel);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ) => {
      validate({
        code: "3.4.35.I5.1",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
      if ((inputs[0] as number) <= 0) {
        throw new InvalidInputError("Peer group must be a positive integer");
      }
      if (inputs[1] !== "local" && inputs[1] !== "state") {
        throw new InvalidInputError(
          "Locality must be either 'local' or 'state'"
        );
      }
    },
  },
  {
    // Macro case: None
    name: "Convert a 3-leg unsignalized intersection at a driveway to a regular 3-leg unsignalized intersection",
    code: "3.4.46.I7.1",
    prompt: "",
    // numberOfInputs: 2,
    numberOfInputs: 0,
    aadtMajorRequired: false,
    aadtMinorRequired: false,
    // inputPrompts: ["Existing number of signal cycles per hour", "Proposed number of signal cycles per hour"],
    inputPrompts: [],
    // inputTypes: ['number', 'number'],
    inputTypes: [],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      // CMF is hard coded to 0 (which means in our cmf vector we use a 1)
      // Do to it not prompting users for input

      // let CMF = 100 * (1 - Math.exp(-0.0444 * (inputs[1] - inputs[0])));

      const CMF = 1;
      return new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, 1],
        [CrashType.FTF, 1],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, 1],
        [CrashType.SSD, 1],
        [CrashType.SOD, 1],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "3.4.46.I7.1",
        correctNumberOfInputs: 0,
        correctTypeArray: [],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor,
        aadtMinor: aadtMinor,
      });
    },
  },

  /*
  {
    // Macro case: Placeholder
    name: "Placeholder",
    code: "Placeholder",
    prompt: "Placeholder",
    numberOfInputs: 2,
    aadtMajorRequired: false, aadtMinorRequired: false,
    inputPrompts: ["Placeholder", "Placeholder"],
    inputTypes: ["number", "string"],
    calculate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): CmfVector => {
      let CMF = 5;
      CMF = roundNumber(CMF, 9);
      return new Map([
        [CrashType.AG, CMF],
        [CrashType.AN, CMF],
        [CrashType.FO, CMF],
        [CrashType.FTF, CMF],
        [CrashType.LT, CMF],
        [CrashType.OtherNC, CMF],
        [CrashType.OtherO, CMF],
        [CrashType.OVT, CMF],
        [CrashType.PD, CMF],
        [CrashType.PDC, CMF],
        [CrashType.PKV, CMF],
        [CrashType.FTR, CMF],
        [CrashType.ROR, 1],
        [CrashType.RT, CMF],
        [CrashType.SSD, CMF],
        [CrashType.SOD, CMF],
        [CrashType.T, CMF],
        [CrashType.TR, CMF],
        [CrashType.NGT, 1],
        [CrashType.WP, 1],
      ]);
    },
    validate: (
      inputs: (string | number)[],
      aadtMajor: number,
      aadtMinor: number
    ): void => {
      validate({
        code: "Placeholder",
        correctNumberOfInputs: 2,
        correctTypeArray: ["number", "string"],
        aadtMajorRequired: false,
        aadtMinorRequired: false,
        providedInputs: inputs,
        aadtMajor: aadtMajor, aadtMinor: aadtMinor
      });
    },
  },
  */
];
