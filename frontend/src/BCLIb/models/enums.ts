/**
 * The labels one uses in the excel spreadsheet with to their full meaning.
 *
 * Note: With the 2022 crash data 3 crash types were notably renamed
 * - Train -> Railway Train
 * - Rear End -> Front to Rear
 * - Head On -> Front to Front
 */
export enum CrashType {
  AG = "Angle", // Angle
  AN = "Animal", // Animal
  FO = "Fixed Object", // Fixed Object
  LT = "Left Turn", // Left Turn
  OtherNC = "Other Non-Collision", // Other Non collision
  OtherO = "Other Object", // Other Object
  OVT = "Overturned", // Overturned
  PD = "Pedestrian", // Pedestrian
  PDC = "Pedalcyclist", // Pedalcyclist
  PKV = "Parked Motor Vehicle", // Parked Vehicle
  ROR = "Run off Road", // Run off Road
  RT = "Right Turn", // Right Turn
  SSD = "Sideswipe Same Direction", // Sideswipe Same Direction
  SOD = "Sideswipe Opposite Direction", // Sideswipe Opposite Direction
  T = "Turning", // Turning
  TR = "Railway Train", // Used to be called just "Train"

  // Added in 2019, used after the 2022 crash data clean
  FTF = "Front to Front", // This replaced "Head On"
  FTR = "Front to Rear", // This replaced "Rear End"

  // Added in 2019 Unused
  RTF = "Rear to Front",
  RTR = "Rear to Rear",
  RTS = "Rear to side",

  // "Special" Case
  NGT = "Night Time", // Night Time Crash
  WP = "Wet Pavement", // Wet Pavement
}

export enum ConditionType {
  NGT,
  WP,
  plain,
}

export enum Severity {
  F = "Total Fatalities",
  A = "Total A-injuries",
  B = "Total B-injuries",
  C = "Total C-injuries",
  N = "Total Non-injuries",
}

export enum CrashInjuryType {
  F = "Fatal Crash",
  A = "A Injury Crash",
  B = "B Injury Crash",
  C = "C Injury Crash",
  N = "No Injuries",
}

export enum Locality {
  Local,
  State,
}

export enum RoadwayType {
  Segment = "Segment",
  Intersection = "Intersection",
}

export enum UnitType {
  Units = "units",
  Miles = "miles",
}
