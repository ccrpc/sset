import { Countermeasure } from "./classes/Countermeasure";
import { Crash } from "./classes/Crash";
import { Locality, RoadwayType, UnitType } from "./enums";

export interface IAppliedCountermeasure {
  /** The countermeasures applied */
  countermeasure: Countermeasure;
  /** The unit cost of this countermeasure (as entered by user) */
  unitCost: number;
  /** The quantity of this countermeasure (as entered by user)*/
  quantity: number;
  /** The Present Worth array of the countermeasure (defined as [1st cots, 2nd cost, 3rtd cost, et cetera]).
   *
   * This is calculated and requires knowledge of the longest service life of all applied counter measures.
   */
  presentWorth: number[];
  /** The EUAC Cost of this countermeasure
   *
   * This is calculated and requires knowledge of the longest service life of all applied counter measures.
   */
  EAUCCost: number;
}

export interface IRoadwayElement {
  readonly id: number;
  readonly name: string;
  readonly locality: Locality;
  readonly peerGroup: number;
  readonly roadwayType: RoadwayType;
  readonly infoProps: Map<string, string | number>;
  readonly riskFactors: Map<string, boolean | number>;
  crashes: Crash[];
  appliedCountermeasures: IAppliedCountermeasure[];

  get infoPropsTable(): [string, string | number][];
  get riskFactorsTable(): [string, boolean | number][];

  addAppliedCountermeasure(acm: IAppliedCountermeasure): void;
  removeAppliedCountermeasure(index: number): void;
}
