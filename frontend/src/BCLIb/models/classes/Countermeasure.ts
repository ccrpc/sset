import { CrashType } from "../enums";
import { CmfVector } from "../types";

export class Countermeasure {
  static readonly defaultVector: CmfVector = new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 1],
    [CrashType.FTF, 1],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 1],
    [CrashType.SOD, 1],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
    [CrashType.RTF, 1.0],
    [CrashType.RTR, 1.0],
    [CrashType.RTS, 1.0],
  ]);

  name: string;
  category: string;
  serviceLife: number;
  cmfVector: CmfVector;
  settingType: string;
  facilityType: string;
  units: string;
  code: string;
  userInputs: (string | number)[];
  aadtMajor: number;
  aadtMinor: number;
  affectedCrashTypes: string[];
  projectType: string;
  projectSystem: string;

  constructor(
    name: string = null,
    category: string = null,
    serviceLife: number = null,
    cmfVector: CmfVector = null,
    settingType = "",
    facilityType = "",
    units = "",
    code = "",
    affectedCrashTypes: string[] = [],
    projectType = "",
    projectSystem = ""
  ) {
    this.name = name;
    this.category = category;
    this.serviceLife = serviceLife;
    this.cmfVector = cmfVector;
    this.settingType = settingType;
    this.facilityType = facilityType;
    this.units = units;
    this.code = code;
    this.affectedCrashTypes = affectedCrashTypes;
    this.projectType = projectType;
    this.projectSystem = projectSystem;

    // The following are used for calculated countermeasures but are not
    // attributes of the countermeasure themselves.
    this.userInputs = undefined;
    this.aadtMajor = undefined;
    this.aadtMinor = undefined;
  }

  get fullName() {
    return [this.code, this.category, this.name].join(" - ");
  }
}
