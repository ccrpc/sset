import { INTX_PROP_LABELS } from "../consts";
import { Locality, RoadwayType } from "../enums";
import { IAppliedCountermeasure, IRoadwayElement } from "../interfaces";
import { Crash } from "./Crash";

export class Intersection implements IRoadwayElement {
  readonly id: number;
  readonly name: string;
  readonly locality: Locality;
  readonly peerGroup: number;
  readonly roadwayType: RoadwayType;
  readonly infoProps: Map<string, string | number>;
  readonly riskFactors: Map<string, boolean | number>;
  crashes: Crash[];
  appliedCountermeasures: IAppliedCountermeasure[];

  constructor(
    id: number,
    name: string,
    locality: Locality,
    peerGroup: number,
    roadwayType: RoadwayType,
    displayProps: Map<string, string | number>,
    riskFactors: Map<string, boolean | number>,
    crashes?: Crash[],
    appliedCountermeasures?: IAppliedCountermeasure[],
  ) {
    this.id = id
    this.name = name
    this.locality = locality
    this.peerGroup = peerGroup
    this.roadwayType = roadwayType
    this.infoProps = displayProps
    this.riskFactors = riskFactors
    this.crashes = crashes || []
    this.appliedCountermeasures = appliedCountermeasures || []
  }

  static fromFeatureProps(props: any) : Intersection {
    return new Intersection(
      props[INTX_PROP_LABELS.ID],
      props[INTX_PROP_LABELS.NAME],
      Locality.Local,
      props[INTX_PROP_LABELS.PEER_GROUP],
      RoadwayType.Intersection,
      new Map([
        INTX_PROP_LABELS.ID,
        INTX_PROP_LABELS.NAME,
        INTX_PROP_LABELS.SETTING,
        INTX_PROP_LABELS.PEER_GROUP,
        INTX_PROP_LABELS.AADT_MAJOR,
        INTX_PROP_LABELS.AADT_MINOR,
        INTX_PROP_LABELS.SKEW_ANGLE,
        INTX_PROP_LABELS.LEFT_TURN_LANE_APPROACH_COUNT,
        INTX_PROP_LABELS.LIGHTING,
        INTX_PROP_LABELS.INTERSECTION_TYPE,
      ].map(k => [k, props[k]])),
      new Map([
        INTX_PROP_LABELS.RISK_INTERSECTION_TYPE,
        INTX_PROP_LABELS.RISK_SKEW_ANGLE_CATEGORY,
        INTX_PROP_LABELS.RISK_AADT_MAJOR,
        INTX_PROP_LABELS.RISK_AADT_MINOR,
        INTX_PROP_LABELS.RISK_SUM,
      ].map(k => [k, props[k]]))
    )
  }

  get infoPropsTable(): [string, string | number][] {
    return [...this.infoProps.entries()]
  }

  get riskFactorsTable(): [string, number | boolean][] {
    return [...this.riskFactors.entries()]    
  }

  addAppliedCountermeasure(acm: IAppliedCountermeasure): void {
    throw new Error("Method not implemented.");
  }

  removeAppliedCountermeasure(index: number): void {
    throw new Error("Method not implemented.");
  }
}