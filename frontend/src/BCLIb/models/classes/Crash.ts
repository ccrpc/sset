import { CRASH_PROP_LABELS } from "../consts";
import { CrashType, Severity, RoadwayType, CrashInjuryType } from "../enums";
import { InjuryProfile } from "../types";

export class Crash {
  readonly id: number;
  readonly crashType: CrashType;
  readonly crashSeverity: CrashInjuryType;
  readonly surfaceCondition: string;
  readonly weatherCondition: string;
  readonly lightCondition: string;
  readonly crashYear: number;
  readonly crashMonth: number;
  readonly crashDay: number;
  readonly crashHour: number;
  readonly crashWeekday: number;
  readonly roadwayId: number;
  readonly roadwayType: RoadwayType;
  readonly totalInjuries: number;
  readonly injuryProfile: InjuryProfile;
  readonly numberOfVehicles: number;
  countAsNightTime: boolean;
  countAsWetPavement: boolean;

  constructor(
    id: number,
    crashType: CrashType,
    crashSeverity: CrashInjuryType,
    surfaceCondition: string,
    weatherCondition: string,
    lightCondition: string,
    crashYear: number,
    crashMonth: number,
    crashDay: number,
    crashHour: number,
    crashWeekday: number,
    roadwayId: number,
    roadwayType: RoadwayType,
    totalInjuries: number,
    totalFatals: number,
    totalAInjuries: number,
    totalBInjuries: number,
    totalCInjuries: number,
    totalNonInjuries: number,
    numberOfVehicles: number
  ) {
    this.id = id;
    this.crashType = crashType;
    this.crashSeverity = crashSeverity;
    this.surfaceCondition = surfaceCondition;
    this.weatherCondition = weatherCondition;
    this.lightCondition = lightCondition;
    this.crashYear = crashYear;
    this.crashMonth = crashMonth;
    this.crashDay = crashDay;
    this.crashHour = crashHour;
    this.crashWeekday = crashWeekday;
    this.roadwayId = roadwayId;
    this.roadwayType = roadwayType;
    this.totalInjuries = totalInjuries;
    this.injuryProfile = new Map([
      [Severity.F, totalFatals],
      [Severity.A, totalAInjuries],
      [Severity.B, totalBInjuries],
      [Severity.C, totalCInjuries],
      [Severity.N, totalNonInjuries],
    ]);
    this.numberOfVehicles = numberOfVehicles;
    this.countAsNightTime = false;
    this.countAsWetPavement = false;
  }

  static fromFeatureProps(props: any): Crash {
    return new Crash(
      props[CRASH_PROP_LABELS.ID],
      props[CRASH_PROP_LABELS.TYPEOFFIRSTCRASH],
      props[CRASH_PROP_LABELS.CRASHINJURIESSEVERITY],
      props[CRASH_PROP_LABELS.ROADSURFACECOND],
      props[CRASH_PROP_LABELS.WEATHERCONDITION],
      props[CRASH_PROP_LABELS.LIGHTCONDITION],
      props[CRASH_PROP_LABELS.CRASHYEAR],
      props[CRASH_PROP_LABELS.CRASHMONTH],
      props[CRASH_PROP_LABELS.CRASHDAY],
      props[CRASH_PROP_LABELS.CRASHHOUR],
      props[CRASH_PROP_LABELS.DAYOFWEEK],
      props[CRASH_PROP_LABELS.SSET_ID] ||
        props[CRASH_PROP_LABELS.INTERSECTION_ID],
      props[CRASH_PROP_LABELS.SSET_INFRA],
      props[CRASH_PROP_LABELS.TOTALINJURED],
      props[CRASH_PROP_LABELS.TOTALFATALS],
      props[CRASH_PROP_LABELS.AINJURIES],
      props[CRASH_PROP_LABELS.BINJURIES],
      props[CRASH_PROP_LABELS.CINJURIES],
      props[CRASH_PROP_LABELS.NOINJURIES],
      props[CRASH_PROP_LABELS.NUMBEROFVEHICLES]
    );
  }

  static mergeInjuryProfiles(crashes: Crash[]): InjuryProfile {
    return new Map(
      Object.values(Severity).map((sev: Severity) => {
        return [
          sev,
          crashes
            .map((crash) => crash.injuryProfile.get(sev) || 0)
            .reduce((a, b) => a + b, 0),
        ];
      })
    );
  }
}
