import { SEG_PROP_LABELS } from "../consts";
import { Locality, RoadwayType } from "../enums";
import { IAppliedCountermeasure, IRoadwayElement } from "../interfaces";
import { Crash } from "./Crash";


export class Segment implements IRoadwayElement {
    readonly id: number;
    readonly name: string;
    readonly locality: Locality;
    readonly peerGroup: number;
    readonly roadwayType: RoadwayType;
    readonly infoProps: Map<string, string | number>;
    readonly riskFactors: Map<string, number | boolean>;
    crashes: Crash[];
    appliedCountermeasures: IAppliedCountermeasure[];

    constructor(
      id: number,
      name: string,
      locality: Locality,
      peerGroup: number,
      roadwayType: RoadwayType,
      displayProps: Map<string, string | number>,
      riskFactors: Map<string, boolean | number>,
      crashes?: Crash[],
      appliedCountermeasures?: IAppliedCountermeasure[],
    ) {
      this.id = id
      this.name = name
      this.locality = locality
      this.peerGroup = peerGroup
      this.roadwayType = roadwayType
      this.infoProps = displayProps
      this.riskFactors = riskFactors
      this.crashes = crashes || []
      this.appliedCountermeasures = appliedCountermeasures || []
    }

    get infoPropsTable(): [string, string | number][] {
      return [...this.infoProps.entries()]
    }

    get riskFactorsTable(): [string, number | boolean][] {
      return [...this.riskFactors.entries()]
    }

    addAppliedCountermeasure(acm: IAppliedCountermeasure): void {
      throw new Error("Method not implemented.");
    }

    removeAppliedCountermeasure(index: number): void {
      throw new Error("Method not implemented.");
    }

    static fromFeatureProps(props: any) {
      return new Segment(
        props[SEG_PROP_LABELS.ID],
        props[SEG_PROP_LABELS.NAME],
        Locality.Local,
        props[SEG_PROP_LABELS.PEER_GROUP],
        RoadwayType.Segment,
        new Map([
          SEG_PROP_LABELS.ID,
          SEG_PROP_LABELS.NAME,
          SEG_PROP_LABELS.SETTING,
          SEG_PROP_LABELS.PEER_GROUP,
          SEG_PROP_LABELS.COUNTY_HIGHWAY,
          SEG_PROP_LABELS.TOTAL_LANES,
          SEG_PROP_LABELS.LANE_WIDTH_FT,
          SEG_PROP_LABELS.SPEED_LIMIT,
          SEG_PROP_LABELS.MEDIAN_TYPE,
          SEG_PROP_LABELS.PAVED_SHOULDER_WIDTH_FT,
          SEG_PROP_LABELS.UNPAVED_SHOULDER_WIDTH_FT,
          SEG_PROP_LABELS.OUTSIDE_SHOULDER_1_TYPE,
          SEG_PROP_LABELS.OUTSIDE_SHOULDER_1_WIDTH_FT,
          SEG_PROP_LABELS.OUTSIDE_SHOULDER_2_TYPE,
          SEG_PROP_LABELS.OUTSIDE_SHOULDER_2_WIDTH_FT,
          SEG_PROP_LABELS.SHOULDER_RUMBLE_STRIPS,
          SEG_PROP_LABELS.CURVATURE,
          SEG_PROP_LABELS.VERTICAL_ALIGNMENT_VARIATION,
          SEG_PROP_LABELS.ACCESS_POINT_DENSITY,
          SEG_PROP_LABELS.OVERTAKING_DEMAND,
          SEG_PROP_LABELS.AADT,
          SEG_PROP_LABELS.AADT_YEAR,
          SEG_PROP_LABELS.SURFACE_TYPE,
          SEG_PROP_LABELS.SURFACE_WIDTH_FT,
          SEG_PROP_LABELS.TWO_WAY_LEFT_TURN_LANE,
          SEG_PROP_LABELS.SEGMENT_LIGHTING,
          SEG_PROP_LABELS.AUTO_SPEED_ENFORCEMENT,
          SEG_PROP_LABELS.BRIDGE_DECK_WIDTH_FT,
          SEG_PROP_LABELS.BRIDGE_ROADWAY_WIDTH_FT,
          SEG_PROP_LABELS.BRIDGE_LENGTH_FT,
          SEG_PROP_LABELS.CROSS_RAIL,
          SEG_PROP_LABELS.FUNCTIONAL_CLASSIFICATION,
          SEG_PROP_LABELS.LENGTH_FT,
        ].map(k => [k, props[k]])),
        new Map([
          SEG_PROP_LABELS.RISK_FUNCTIONAL_CLASSIFICATION,
          SEG_PROP_LABELS.RISK_AADT,
          SEG_PROP_LABELS.RISK_SHOULDER_WIDTH,
          SEG_PROP_LABELS.RISK_ACCESS_POINT_DENSITY,
          SEG_PROP_LABELS.RISK_OVERTAKING_DEMAND,
          SEG_PROP_LABELS.RISK_SPEED_LIMIT,
          SEG_PROP_LABELS.RISK_SUM,
        ].map(k => [k, props[k]])),
      )
    }
}