import {
  RoadwayType,
  CrashType,
  Severity,
  Locality,
  ConditionType,
} from "./enums";
import { IRoadwayElement } from "./interfaces";

export type ProjectElements = Map<RoadwayType, IRoadwayElement[]>;

export type CmfVector = Map<CrashType, number>;
export type InjuryProfile = Map<Severity, number>;

export type CrashTable = Map<
  ConditionType,
  Map<CrashType, Map<Severity, number>>
>;

export type CrashProfile = {
  locality: Locality;
  roadwayType: RoadwayType;
  peerGroup?: number;
  periodStart?: Date;
  periodEnd?: Date;
  crashes: CrashTable;
};
