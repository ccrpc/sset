/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Countermeasure } from "./models/classes/Countermeasure";
import { INJURY_COST } from "./models/consts";
import {
  CrashType,
  Severity,
  Locality,
  RoadwayType,
  ConditionType,
} from "./models/enums";
import {
  CmfVector,
  CrashProfile,
  CrashTable,
  ProjectElements,
} from "./models/types";
import { calculateCombinedCMF, createCrashTable } from "./utils";
import { IAppliedCountermeasure } from "./models/interfaces";
import { Crash } from "./models/classes/Crash";

export enum CrashBenefitMetric {
  TotalPrevented = "Total Prevented",
  AnnualPrevented = "Annual Prevented",
  TotalSaved = "Total Saved",
  AnnualSaved = "Annual Saved",
}

export type BenefitBreakdown = Map<
  CrashType,
  Map<Severity, Map<CrashBenefitMetric, number>>
>;

/**
 * What we need for this is the cost of the crash * number of crashes / year * (1 - totalCMF for that crash type)
 * @param crashProfile
 * @param countermeasures
 * @returns
 */
export function calculateBenefitBreakdown(
  crashProfile: CrashProfile,
  countermeasures: Countermeasure[]
): BenefitBreakdown {
  const effectiveCMF: Map<ConditionType, CmfVector> =
    calculateCombinedCMF(countermeasures);
  const periodInYears: number = Math.round(
    (crashProfile.periodEnd.getTime() - crashProfile.periodStart.getTime()) /
      (1000 * // seconds
        60 * // minutes
        60 * // hours
        24 * // days
        365) // years
  );
  const benefitBreakdown: BenefitBreakdown = new Map<
    CrashType,
    Map<Severity, Map<CrashBenefitMetric, number>>
  >();

  /* eslint-disable no-unexpected-multiline */
  for (const conditionType of [
    ConditionType.NGT,
    ConditionType.WP,
    ConditionType.plain,
  ]) {
    if (!crashProfile.crashes.get(conditionType)) {
      continue;
    }
    for (const [crashType, severityCounts] of crashProfile.crashes.get(
      conditionType
    )) {
      if (!benefitBreakdown.get(crashType)) {
        benefitBreakdown.set(crashType, new Map());
      }
      for (const [severity, count] of severityCounts) {
        {
          // This rounding scheme is designed to mimic that of the spreadsheet tool
          const preventedPerYear =
            (count / periodInYears) *
            (1 - (effectiveCMF.get(conditionType).get(crashType) || 1.0));
          const nPrevented = preventedPerYear * periodInYears;
          const costOfInjury =
            INJURY_COST.get(crashProfile.locality)!
              .get(crashProfile.roadwayType)!
              [crashProfile.peerGroup - 1].get(severity) || 0;

          if (!benefitBreakdown.get(crashType).get(severity)) {
            benefitBreakdown.get(crashType).set(
              severity,
              new Map([
                [CrashBenefitMetric.TotalPrevented, nPrevented],
                [CrashBenefitMetric.AnnualPrevented, preventedPerYear],
                [CrashBenefitMetric.TotalSaved, nPrevented * costOfInjury],
                [
                  CrashBenefitMetric.AnnualSaved,
                  preventedPerYear * costOfInjury,
                ],
              ])
            );
          } else {
            benefitBreakdown
              .get(crashType)
              .get(severity)
              .set(
                CrashBenefitMetric.TotalPrevented,
                benefitBreakdown
                  .get(crashType)
                  .get(severity)
                  .get(CrashBenefitMetric.TotalPrevented) + nPrevented
              );

            benefitBreakdown
              .get(crashType)
              .get(severity)
              .set(
                CrashBenefitMetric.AnnualPrevented,
                benefitBreakdown
                  .get(crashType)
                  .get(severity)
                  .get(CrashBenefitMetric.AnnualPrevented) + preventedPerYear
              );

            benefitBreakdown
              .get(crashType)
              .get(severity)
              .set(
                CrashBenefitMetric.TotalSaved,
                benefitBreakdown
                  .get(crashType)
                  .get(severity)
                  .get(CrashBenefitMetric.TotalSaved) +
                  nPrevented * costOfInjury
              );

            benefitBreakdown
              .get(crashType)
              .get(severity)
              .set(
                CrashBenefitMetric.AnnualSaved,
                benefitBreakdown
                  .get(crashType)
                  .get(severity)
                  .get(CrashBenefitMetric.AnnualSaved) +
                  preventedPerYear * costOfInjury
              );
          }
        }
      }
    }
  }

  /* eslint-enable no-unexpected-multiline */
  return benefitBreakdown;
}

export function calculateTotalBenefit(breakdown: BenefitBreakdown) {
  return calculateTotalAttribute(breakdown, CrashBenefitMetric.AnnualSaved);
}

export function calculateTotalFatalitiesPrevented(breakdown: BenefitBreakdown) {
  const fatalitiesBreakdown: BenefitBreakdown =
    getFatalitiesBreakdown(breakdown);
  return calculateTotalAttribute(
    fatalitiesBreakdown,
    CrashBenefitMetric.TotalPrevented
  );
}

export function calculateAnnualFatalitiesPrevented(
  breakdown: BenefitBreakdown
) {
  const fatalitiesBreakdown: BenefitBreakdown =
    getFatalitiesBreakdown(breakdown);
  return calculateTotalAttribute(
    fatalitiesBreakdown,
    CrashBenefitMetric.AnnualPrevented
  );
}

/**
 * Returns a breakdown with only the fatal crashes
 * @param breakdown the breakdown whose fatal crashes to extract
 * @returns The breakdown with only the fatal crashes
 */
function getFatalitiesBreakdown(breakdown: BenefitBreakdown): BenefitBreakdown {
  const fatalitiesBreakdown: BenefitBreakdown = new Map();
  for (const [crashType] of breakdown) {
    const fatalitiesInformation = breakdown.get(crashType).get(Severity.F);
    if (fatalitiesInformation) {
      const fatalityMetrics: Map<
        Severity,
        Map<CrashBenefitMetric, number>
      > = new Map();
      fatalityMetrics.set(Severity.F, breakdown.get(crashType).get(Severity.F));
      fatalitiesBreakdown.set(crashType, fatalityMetrics);
    }
  }
  return fatalitiesBreakdown;
}

function calculateTotalAttribute(
  breakdown: BenefitBreakdown,
  key: CrashBenefitMetric
): number {
  const vals = [...[...breakdown.values()].map((a) => [...a.values()])];

  const joined = vals.reduce((a, b) => a.concat(b), []);
  const attributeArray = joined.map((x: Map<CrashBenefitMetric, number>) =>
    x.get(key)
  );

  const total = attributeArray.reduce((a, b) => (a || 0) + (b || 0), 0) || 0;

  return total;
}

export interface IBenefitCalculator {
  breakdown: BenefitBreakdown;
  totalBenefit: number;
  totalFatalitiesPrevented: number;
  annualFatalitiesPrevented: number;
}
export class BenefitCalculator implements IBenefitCalculator {
  periodStart: Date;
  periodEnd: Date;
  crashProfile: CrashProfile;
  countermeasures: Countermeasure[];

  constructor(
    crashProfile: CrashProfile,
    countermeasures: Countermeasure[] = []
  ) {
    this.periodStart = crashProfile.periodStart;
    this.periodEnd = crashProfile.periodEnd;
    this.crashProfile = crashProfile;
    this.countermeasures = countermeasures;
  }

  public get breakdown(): BenefitBreakdown {
    return calculateBenefitBreakdown(this.crashProfile, this.countermeasures);
  }

  public get totalBenefit(): number {
    return Math.floor(calculateTotalBenefit(this.breakdown) / 50) * 50;
  }

  public get totalFatalitiesPrevented(): number {
    return calculateTotalFatalitiesPrevented(this.breakdown);
  }

  public get annualFatalitiesPrevented(): number {
    return calculateAnnualFatalitiesPrevented(this.breakdown);
  }
}

/**
 * This returns the total benefit of the project when applying all counter measures to all crashes.
 *
 * This matches the way the BCTool spreadsheet currently handles a project.
 * @param studyAreaFeatures The features to which countermeasures are applied
 * @param endStudyYear The year the study ends
 * @returns The total benefit of the project
 */
export function getTotalProjectBenefit(
  studyAreaFeatures: ProjectElements,
  endStudyYear: number
): number {
  return [RoadwayType.Intersection, RoadwayType.Segment]
    .map(
      (type: RoadwayType) =>
        getTypedBenefit(studyAreaFeatures, endStudyYear, type).benefit
    )
    .reduce(
      (accumulator: number, currentValue: number) => accumulator + currentValue,
      0
    );
}

export function whetherToAggregate(
  countermeasures: IAppliedCountermeasure[]
): boolean {
  return (
    countermeasures
      .flatMap(
        (countermeasure) => countermeasure.countermeasure.affectedCrashTypes
      )
      .filter((type) => type === "All").length !== 0
  );
}

/**
 * Returns the benefit of the countermeasures that are applied to a given RoadwayType (i.e. segment or intersection).
 * @param {studyAreaFeatures} studyAreaFeatures The features with the countermeasures applied to them.
 * @param {number} endStudyYear The end of the study year.
 * @param {RoadwayType} type Whether to look at segments or intersections.
 * @returns
 */
export function getTypedBenefit(
  studyAreaFeatures: ProjectElements,
  endStudyYear: number,
  type: RoadwayType
): { benefit: number; totalPrevented: number; annualPrevented: number } {
  // The countermeasures applied to the provided type (Segments or Intersections)
  const typedCounterMeasures: IAppliedCountermeasure[] = [];

  // The crashes on the provided type of roadway element.
  // In this instance organized by peer group. Aka the data structure is Map<Peer Group, Array of Crashes>
  const typedCrashes: Map<number, Crash[]> = new Map<number, Crash[]>();

  // Iterate through the roadway elements of the given type and populate the countermeasures and crashes.
  studyAreaFeatures.get(type).forEach((roadwayElement) => {
    typedCounterMeasures.push(...roadwayElement.appliedCountermeasures);
    if (!typedCrashes.get(roadwayElement.peerGroup)) {
      typedCrashes.set(roadwayElement.peerGroup, []);
    }
    typedCrashes.get(roadwayElement.peerGroup).push(...roadwayElement.crashes);
  });

  // For each peer group we apply all of the countermeasures to the crashes of that type to find the benefit
  let benefit = 0;
  let totalPrevented = 0;
  let annualPrevented = 0;
  for (const [PeerGroup, crashes] of typedCrashes) {
    const crashTable = createCrashTable(
      crashes,
      whetherToAggregate(typedCounterMeasures)
    );
    const crashProfile: CrashProfile = {
      locality: Locality.Local,
      roadwayType: type,
      peerGroup: PeerGroup,
      crashes: crashTable,
      periodStart: new Date(`${endStudyYear - 4}-01-01`),
      periodEnd: new Date(`${endStudyYear}-12-31`),
    };
    const benefitCalculator = new BenefitCalculator(
      crashProfile,
      typedCounterMeasures.map(
        (appliedCountermeasure) => appliedCountermeasure.countermeasure
      )
    );

    benefit = benefit + benefitCalculator.totalBenefit;
    totalPrevented =
      totalPrevented + benefitCalculator.totalFatalitiesPrevented;
    annualPrevented =
      annualPrevented + benefitCalculator.annualFatalitiesPrevented;
  }

  return {
    benefit: benefit,
    totalPrevented: totalPrevented,
    annualPrevented: annualPrevented,
  };
}

/**
 * This returns the total benefit of a project by looking at the benefits of each roadway element and the counter measures
 * applied to that roadway element.
 *
 * This works when looking to do the "partial application of countermeasures" approach on page 31 of the BCTool User Manual.
 * @param studyAreaFeatures The features to which countermeasures are applied
 * @param endStudyYear The year the study ends
 * @returns {number} The total benefit of the project
 */
export function getTotalProjectBenefitPartialApplication(
  studyAreaFeatures: ProjectElements,
  endStudyYear: number
): number {
  return [...studyAreaFeatures.values()]
    .flatMap((arr) => arr)
    .map((re) => {
      const crashTables: CrashTable = createCrashTable(
        re.crashes,
        whetherToAggregate(
          [RoadwayType.Segment, RoadwayType.Intersection]
            .map((type) => studyAreaFeatures.get(type))
            .map((roadwayElements) =>
              roadwayElements.map(
                (roadwayElement) => roadwayElement.appliedCountermeasures
              )
            )
            .flat()
            .flat()
        )
      );
      const crashProfile: CrashProfile = {
        locality: Locality.Local,
        roadwayType: re.roadwayType,
        peerGroup: re.peerGroup,
        crashes: crashTables,
        periodStart: new Date(`${endStudyYear - 4}-01-01`),
        periodEnd: new Date(`${endStudyYear}-12-31`),
      };
      const countermeasures = re.appliedCountermeasures.map(
        (acm) => acm.countermeasure
      );
      const bc = new BenefitCalculator(crashProfile, countermeasures);
      return bc.totalBenefit;
    })
    .reduce((a, b) => a + b, 0.0);
}
/* eslint-enable @typescript-eslint/no-non-null-assertion */
