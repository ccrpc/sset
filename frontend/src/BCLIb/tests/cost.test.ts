import { Countermeasure } from "../models/classes/Countermeasure";
import { IAppliedCountermeasure } from "../models/interfaces";
import { CostCalculator } from "../CostBreakdown";

type costCompare = {
  presentWorth: number[];
  EUACCost: number;
  EUACRounded50: number;
};

/**
 * This test inputs the countermeasure found in issue 81 (https://gitlab.com/ccrpc/sset/-/issues/81) Test Case1 : Seg_172001_Countermeasure 1
 * and tests for the cost accuracy when given a single counter measure (aka when present worth is not a factor).
 */
test("EUAC Cost For Rumble Strips", () => {
  const appliedCountermeasures: IAppliedCountermeasure[] = [
    {
      unitCost: 75117,
      quantity: 0.4,
      countermeasure: new Countermeasure(
        "Install Rumble Strips (shoulder)",
        "Pavement Treatments",
        8,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
  ];

  // The expected presentWorth and EAUC are taken from the spreadsheet.
  const expectedCosts: costCompare[] = [
    {
      presentWorth: [30047],
      EUACCost: 4463,
      EUACRounded50: 4500,
    },
  ];

  const costCalculator: CostCalculator = new CostCalculator(
    appliedCountermeasures
  );

  const result: costCompare[] = appliedCountermeasures.map(
    (appliedCountermeasure): costCompare => {
      const calculatedCountermeasure: IAppliedCountermeasure =
        costCalculator.calculateAppliedCountermeasure(appliedCountermeasure);
      return {
        presentWorth: calculatedCountermeasure.presentWorth,
        EUACCost: calculatedCountermeasure.EAUCCost,
        EUACRounded50: CostCalculator.round50(
          calculatedCountermeasure.EAUCCost
        ),
      };
    }
  );

  expect(result).toEqual(expectedCosts);
});

/** Using the three countermeasures found in the un-edited HSIP Benefit to Cost Tool, 06-11-19.xls */
test("EUAC Cost For Four Countermeasures", () => {
  const appliedCountermeasures: IAppliedCountermeasure[] = [
    {
      unitCost: 88235,
      quantity: 8.5,
      countermeasure: new Countermeasure(
        "Add or Widen Paved Shoulder",
        "Pavement Treatments",
        15,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
    {
      unitCost: 75295,
      quantity: 8.5,
      countermeasure: new Countermeasure(
        "Install Rumble Strips (shoulder)",
        "Pavement Treatments",
        8,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
    {
      unitCost: 2353,
      quantity: 4.25,
      countermeasure: new Countermeasure(
        "Install Rumble (centerline)",
        "User Defined",
        8,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
    {
      unitCost: 115165,
      quantity: 8.5,
      countermeasure: new Countermeasure(
        "Flatten Sideslopes",
        "Roadside Safety",
        20,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
  ];

  // The expected presentWorth and EAUC are taken from the spreadsheet.
  const expectedCosts: costCompare[] = [
    {
      presentWorth: [749998],
      EUACCost: 55186,
      EUACRounded50: 55200,
    },
    {
      presentWorth: [640008, 467647],
      EUACCost: 81503,
      EUACRounded50: 81550,
    },
    {
      presentWorth: [10000, 7307],
      EUACCost: 1273,
      EUACRounded50: 1300,
    },
    {
      presentWorth: [978903],
      EUACCost: 72029,
      EUACRounded50: 72050,
    },
  ];

  const costCalculator: CostCalculator = new CostCalculator(
    appliedCountermeasures
  );

  const result: costCompare[] = appliedCountermeasures.map(
    (appliedCountermeasure): costCompare => {
      const calculatedCountermeasure: IAppliedCountermeasure =
        costCalculator.calculateAppliedCountermeasure(appliedCountermeasure);
      return {
        presentWorth: calculatedCountermeasure.presentWorth,
        EUACCost: calculatedCountermeasure.EAUCCost,
        EUACRounded50: CostCalculator.round50(
          calculatedCountermeasure.EAUCCost
        ),
      };
    }
  );

  // Check Present Worth of Rumble Strips
  for (const idx in expectedCosts[1].presentWorth) {
    expect(result[1].presentWorth[idx]).toEqual(
      expectedCosts[1].presentWorth[idx]
    );
  }

  // Check everything
  expect(result).toEqual(expectedCosts);
});

/** Issue 81 Test Case 3 # 1*/
test("Issue 81 Test Case 3 # 1", () => {
  const appliedCountermeasures: IAppliedCountermeasure[] = [
    {
      unitCost: 128000,
      quantity: 1.18,
      countermeasure: new Countermeasure(
        "Add or Widen Paved Shoulder",
        "Pavement Treatments",
        15,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
  ];

  // The expected presentWorth and EAUC are taken from the spreadsheet.
  const expectedCosts: costCompare[] = [
    {
      presentWorth: [151040],
      EUACCost: 13585,
      EUACRounded50: 13600,
    },
  ];

  const costCalculator: CostCalculator = new CostCalculator(
    appliedCountermeasures
  );

  const result: costCompare[] = appliedCountermeasures.map(
    (appliedCountermeasure): costCompare => {
      const calculatedCountermeasure: IAppliedCountermeasure =
        costCalculator.calculateAppliedCountermeasure(appliedCountermeasure);
      return {
        presentWorth: calculatedCountermeasure.presentWorth,
        EUACCost: calculatedCountermeasure.EAUCCost,
        EUACRounded50: CostCalculator.round50(
          calculatedCountermeasure.EAUCCost
        ),
      };
    }
  );

  expect(result).toEqual(expectedCosts);
});

/** Issue 81 Test Case 3 # 2 (the one with two counter measures*/
test("Issue 81 Test Case 3 # 2", () => {
  const appliedCountermeasures: IAppliedCountermeasure[] = [
    {
      unitCost: 128000,
      quantity: 0.844,
      countermeasure: new Countermeasure(
        "Add or Widen Paved Shoulder",
        "Pavement Treatments",
        15,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
    {
      unitCost: 1000,
      quantity: 0.844,
      countermeasure: new Countermeasure(
        "Install Rumble Strips (Shoulder)",
        "Pavement Treatments",
        8,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
  ];

  // The expected presentWorth and EAUC are taken from the spreadsheet.
  const expectedCosts: costCompare[] = [
    {
      presentWorth: [108032],
      EUACCost: 9717,
      EUACRounded50: 9750,
    },
    {
      presentWorth: [844, 617],
      EUACCost: 131,
      EUACRounded50: 150,
    },
  ];

  const costCalculator: CostCalculator = new CostCalculator(
    appliedCountermeasures
  );

  const result: costCompare[] = appliedCountermeasures.map(
    (appliedCountermeasure): costCompare => {
      const calculatedCountermeasure: IAppliedCountermeasure =
        costCalculator.calculateAppliedCountermeasure(appliedCountermeasure);
      return {
        presentWorth: calculatedCountermeasure.presentWorth,
        EUACCost: calculatedCountermeasure.EAUCCost,
        EUACRounded50: CostCalculator.round50(
          calculatedCountermeasure.EAUCCost
        ),
      };
    }
  );

  expect(result).toEqual(expectedCosts);
});

// Using a max age that ends up for 3.5 to check banker's rounding behavior
test("EUAC Cost For Four Countermeasures With longest life 28", () => {
  const appliedCountermeasures: IAppliedCountermeasure[] = [
    {
      unitCost: 88235,
      quantity: 8.5,
      countermeasure: new Countermeasure(
        "Add or Widen Paved Shoulder",
        "Pavement Treatments",
        15,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
    {
      unitCost: 75295,
      quantity: 8.5,
      countermeasure: new Countermeasure(
        "Install Rumble Strips (shoulder)",
        "Pavement Treatments",
        8,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
    {
      unitCost: 2353,
      quantity: 4.25,
      countermeasure: new Countermeasure(
        "Install Rumble (centerline)",
        "User Defined",
        8,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
    {
      unitCost: 115165,
      quantity: 8.5,
      countermeasure: new Countermeasure(
        "Flatten Sideslopes",
        "Roadside Safety",
        28,
        null
      ),
      presentWorth: null,
      EAUCCost: null,
    },
  ];

  // The expected presentWorth and EAUC are taken from the spreadsheet.
  const expectedCosts: costCompare[] = [
    {
      presentWorth: [749998, 416447],
      EUACCost: 70002,
      EUACRounded50: 70050,
    },
    {
      presentWorth: [640008, 467647, 341705, 249681],
      EUACCost: 101965,
      EUACRounded50: 102000,
    },
    {
      presentWorth: [10000, 7307, 5339, 3901],
      EUACCost: 1593,
      EUACRounded50: 1600,
    },
    {
      presentWorth: [978903],
      EUACCost: 58747,
      EUACRounded50: 58750,
    },
  ];

  const costCalculator: CostCalculator = new CostCalculator(
    appliedCountermeasures
  );
  const result: costCompare[] = appliedCountermeasures.map(
    (appliedCountermeasure): costCompare => {
      const calculatedCountermeasure: IAppliedCountermeasure =
        costCalculator.calculateAppliedCountermeasure(appliedCountermeasure);
      return {
        presentWorth: calculatedCountermeasure.presentWorth,
        EUACCost: calculatedCountermeasure.EAUCCost,
        EUACRounded50: CostCalculator.round50(
          calculatedCountermeasure.EAUCCost
        ),
      };
    }
  );

  // Check Present Worth of Rumble Strips
  for (const idx in expectedCosts[1].presentWorth) {
    expect(result[1].presentWorth[idx]).toEqual(
      expectedCosts[1].presentWorth[idx]
    );
  }

  // Check everything
  expect(result).toEqual(expectedCosts);
});
