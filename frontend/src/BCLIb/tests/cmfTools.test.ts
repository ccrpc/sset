import { Countermeasure } from "../models/classes/Countermeasure";
import { ConditionType, CrashType } from "../models/enums";
import { calculateCombinedCMF } from "../utils";

test("2 CMF Vectors of equal length", () => {
  const countermeasures = [
    new Countermeasure(
      "Resurfacing Alone",
      "Pavement Treatments",
      10,
      new Map([
        [CrashType.AG, 0.95],
        [CrashType.AN, 0.95],
        [CrashType.FO, 0.95],
        [CrashType.FTF, 0.95],
        [CrashType.LT, 0.95],
        [CrashType.OtherNC, 0.95],
        [CrashType.OtherO, 0.95],
        [CrashType.OVT, 0.95],
        [CrashType.PD, 0.95],
        [CrashType.PDC, 0.95],
        [CrashType.PKV, 0.95],
        [CrashType.FTR, 0.95],
        [CrashType.ROR, 0.95],
        [CrashType.RT, 0.95],
        [CrashType.SSD, 0.95],
        [CrashType.SOD, 0.95],
        [CrashType.T, 0.95],
        [CrashType.TR, 0.95],
        [CrashType.NGT, 0.8],
        [CrashType.WP, 0.95],
      ])
    ),
    new Countermeasure(
      "Increased pavement friction",
      "Pavement Treatments",
      5,
      new Map([
        [CrashType.AG, 0.76],
        [CrashType.AN, 0.75],
        [CrashType.FO, 0.75],
        [CrashType.FTF, 0.75],
        [CrashType.LT, 0.76],
        [CrashType.OtherNC, 0.76],
        [CrashType.OtherO, 0.76],
        [CrashType.OVT, 0.75],
        [CrashType.PD, 0.76],
        [CrashType.PDC, 0.76],
        [CrashType.PKV, 0.76],
        [CrashType.FTR, 0.83],
        [CrashType.ROR, 1.0],
        [CrashType.RT, 0.76],
        [CrashType.SSD, 0.76],
        [CrashType.SOD, 0.75],
        [CrashType.T, 0.76],
        [CrashType.TR, 0.76],
        [CrashType.NGT, 0.7],
        [CrashType.WP, 0.43],
      ])
    ),
  ];

  const result = calculateCombinedCMF(countermeasures);
  const expected = new Map([
    [CrashType.AG, 0.722],
    [CrashType.AN, 0.7125],
    [CrashType.FO, 0.7125],
    [CrashType.FTF, 0.7125],
    [CrashType.LT, 0.722],
    [CrashType.OtherNC, 0.722],
    [CrashType.OtherO, 0.722],
    [CrashType.OVT, 0.7125],
    [CrashType.PD, 0.722],
    [CrashType.PDC, 0.722],
    [CrashType.PKV, 0.722],
    [CrashType.FTR, 0.7885],
    [CrashType.ROR, 0.95],
    [CrashType.RT, 0.722],
    [CrashType.SSD, 0.722],
    [CrashType.SOD, 0.7125],
    [CrashType.T, 0.722],
    [CrashType.TR, 0.722],
    [CrashType.NGT, 0.6],
    [CrashType.WP, 0.43],
    [CrashType.RTF, 1.0],
    [CrashType.RTR, 1.0],
    [CrashType.RTS, 1.0],
  ]);

  for (const [crashType, cmf] of result.get(ConditionType.plain)) {
    expect(cmf).toBeCloseTo(expected.get(crashType));
  }
});

test("2 CMF Vectors of unequal length", () => {
  const countermeasures = [
    new Countermeasure(
      "Resurfacing Alone",
      "Pavement Treatments",
      10,
      new Map([
        [CrashType.AG, 0.95],
        [CrashType.AN, 0.95],
        [CrashType.FO, 0.95],
        [CrashType.FTF, 0.95],
        [CrashType.LT, 0.95],
        [CrashType.OtherNC, 0.95],
        [CrashType.OtherO, 0.95],
        [CrashType.OVT, 0.95],
        [CrashType.PD, 0.95],
        [CrashType.PDC, 0.95],
        [CrashType.PKV, 0.95],
        [CrashType.FTR, 0.95],
        [CrashType.ROR, 0.95],
        [CrashType.RT, 0.95],
        [CrashType.SSD, 0.95],
        [CrashType.SOD, 0.95],
        [CrashType.T, 0.95],
        [CrashType.TR, 0.95],
        [CrashType.NGT, 0.95],
        [CrashType.WP, 0.95],
      ])
    ),
    new Countermeasure(
      "Automatic Railroad Gates (Crossing w/o Lights and Sound)",
      "Railroad Crossings",
      15,
      new Map([
        [CrashType.FO, 0.33],
        [CrashType.OVT, 0.33],
        [CrashType.FTR, 0.33],
        [CrashType.TR, 0.33],
      ])
    ),
  ];

  const result = calculateCombinedCMF(countermeasures);
  const expected = new Map([
    [CrashType.AG, 0.95],
    [CrashType.AN, 0.95],
    [CrashType.FO, 0.33],
    [CrashType.FTF, 0.95],
    [CrashType.LT, 0.95],
    [CrashType.OtherNC, 0.95],
    [CrashType.OtherO, 0.95],
    [CrashType.OVT, 0.33],
    [CrashType.PD, 0.95],
    [CrashType.PDC, 0.95],
    [CrashType.PKV, 0.95],
    [CrashType.FTR, 0.33],
    [CrashType.ROR, 0.95],
    [CrashType.RT, 0.95],
    [CrashType.SSD, 0.95],
    [CrashType.SOD, 0.95],
    [CrashType.T, 0.95],
    [CrashType.TR, 0.33],
    [CrashType.NGT, 0.95],
    [CrashType.WP, 0.95],
    [CrashType.RTF, 1.0],
    [CrashType.RTR, 1.0],
    [CrashType.RTS, 1.0],
  ]);

  for (const [crashType, cmf] of result.get(ConditionType.plain)) {
    expect(cmf).toBeCloseTo(expected.get(crashType));
  }
});

test("Night Time and Wet Pavement CMF", () => {
  const countermeasures = [
    new Countermeasure(
      "Resurfacing Alone",
      "Pavement Treatments",
      10,
      new Map([
        [CrashType.AG, 0.95],
        [CrashType.AN, 0.95],
        [CrashType.FO, 0.95],
        [CrashType.FTF, 0.95],
        [CrashType.LT, 0.95],
        [CrashType.OtherNC, 0.95],
        [CrashType.OtherO, 0.95],
        [CrashType.OVT, 0.95],
        [CrashType.PD, 0.95],
        [CrashType.PDC, 0.95],
        [CrashType.PKV, 0.95],
        [CrashType.FTR, 0.95],
        [CrashType.ROR, 1.0],
        [CrashType.RT, 0.95],
        [CrashType.SSD, 0.95],
        [CrashType.SOD, 0.95],
        [CrashType.T, 0.95],
        [CrashType.TR, 0.95],
        [CrashType.NGT, 0.95],
        [CrashType.WP, 1],
      ])
    ),
    new Countermeasure(
      "Increased pavement friction",
      "Pavement Treatments",
      5,
      new Map([
        [CrashType.AG, 0.7],
        [CrashType.AN, 0.7],
        [CrashType.FO, 0.7],
        [CrashType.FTF, 0.7],
        [CrashType.LT, 0.7],
        [CrashType.OtherNC, 0.7],
        [CrashType.OtherO, 0.7],
        [CrashType.OVT, 0.7],
        [CrashType.PD, 0.7],
        [CrashType.PDC, 0.7],
        [CrashType.PKV, 0.7],
        [CrashType.FTR, 0.7],
        [CrashType.ROR, 1.0],
        [CrashType.RT, 0.7],
        [CrashType.SSD, 0.7],
        [CrashType.SOD, 0.7],
        [CrashType.T, 0.7],
        [CrashType.TR, 0.7],
        [CrashType.NGT, 0.8],
        [CrashType.WP, 1],
      ])
    ),
    new Countermeasure(
      "Increased pavement friction 2",
      "Pavement Treatments",
      5,
      new Map([
        [CrashType.AG, 0.8],
        [CrashType.AN, 0.9],
        [CrashType.FO, 0.8],
        [CrashType.FTF, 0.9],
        [CrashType.LT, 0.8],
        [CrashType.OtherNC, 0.9],
        [CrashType.OtherO, 0.8],
        [CrashType.OVT, 0.9],
        [CrashType.PD, 0.8],
        [CrashType.PDC, 0.9],
        [CrashType.PKV, 0.8],
        [CrashType.FTR, 0.9],
        [CrashType.ROR, 1.0],
        [CrashType.RT, 0.8],
        [CrashType.SSD, 0.9],
        [CrashType.SOD, 0.8],
        [CrashType.T, 0.9],
        [CrashType.TR, 0.8],
        [CrashType.NGT, 1.0],
        [CrashType.WP, 0.43],
      ])
    ),
  ];

  const result = calculateCombinedCMF(countermeasures);
  const expectedNGT = new Map([
    [CrashType.AG, 0.608],
    [CrashType.AN, 0.684],
    [CrashType.FO, 0.608],
    [CrashType.FTF, 0.684],
    [CrashType.LT, 0.608],
    [CrashType.OtherNC, 0.684],
    [CrashType.OtherO, 0.608],
    [CrashType.OVT, 0.684],
    [CrashType.PD, 0.608],
    [CrashType.PDC, 0.684],
    [CrashType.PKV, 0.608],
    [CrashType.FTR, 0.684],
    [CrashType.ROR, 0.76],
    [CrashType.RT, 0.608],
    [CrashType.SSD, 0.684],
    [CrashType.SOD, 0.608],
    [CrashType.T, 0.684],
    [CrashType.TR, 0.608],
    [CrashType.NGT, 0.76],
    [CrashType.WP, 0.43],
    [CrashType.RTF, 1.0],
    [CrashType.RTR, 1.0],
    [CrashType.RTS, 1.0],
  ]);

  const expectedWP = new Map([
    [CrashType.AG, 0.43],
    [CrashType.AN, 0.43],
    [CrashType.FO, 0.43],
    [CrashType.FTF, 0.43],
    [CrashType.LT, 0.43],
    [CrashType.OtherNC, 0.43],
    [CrashType.OtherO, 0.43],
    [CrashType.OVT, 0.43],
    [CrashType.PD, 0.43],
    [CrashType.PDC, 0.43],
    [CrashType.PKV, 0.43],
    [CrashType.FTR, 0.43],
    [CrashType.ROR, 0.43],
    [CrashType.RT, 0.43],
    [CrashType.SSD, 0.43],
    [CrashType.SOD, 0.43],
    [CrashType.T, 0.43],
    [CrashType.TR, 0.43],
    [CrashType.NGT, 0.43],
    [CrashType.WP, 0.43],
    [CrashType.RTF, 1.0],
    [CrashType.RTR, 1.0],
    [CrashType.RTS, 1.0],
  ]);

  for (const [crashType, cmf] of result.get(ConditionType.NGT)) {
    expect(cmf).toBeCloseTo(expectedNGT.get(crashType));
  }

  expect(result.get(ConditionType.WP)).toEqual(expectedWP);
});
