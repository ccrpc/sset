import {
  calculatedCounterMeasures,
  CalculatedCounterMeasure,
} from "../calculatedCounterMeasures";
import { CrashType, RoadwayType } from "../models/enums";
import { CmfVector } from "../models/types";
import { roundNumber } from "../utils";

interface counterMeasureTestData {
  code: string;
  expectedCMF: CmfVector;
  correctInputs: (string | number)[];
  incorrectInputsDueToType: (string | number)[][];
  incorrectInputsDueToLength: (string | number)[];
  aadtMajorRequired: boolean;
  aadtMinorRequired: boolean;
  aadtMajor: number;
  aadtMinor: number;
  correctNumberOfInputs: number;
  correctTypeArray: string[];
  inputValidation: boolean;
  incorrectInputsDueToValue: (number | string)[][];
  expectedValueErrors: string[];
}

const counterMeasuresToTest: string[] = [
  "4.1.1.S1.1",
  "4.1.1.S6.1",
  "4.1.2.SU.1",
  "4.1.2.SU.2",
  "4.1.2.SU.3",
  "4.1.2.SU.4",
  "4.1.2.SU.5",
  "4.1.2.SU.6",
  "4.1.2.SU.7",
  "4.1.2.SU.8",
  "4.1.3.S1.1",
  "4.1.3.S6.1",
  "4.1.12.S1.1",
  "4.1.13.S6.1",
  "4.1.13.S12.1",
  "4.1.17.AL.1",
  "4.1.19.S8.1",
  "4.1.19.S1.1",
  "4.1.21.S8.1",
  "4.1.21.S12.1",
  "4.2.5.S6.1",
  "4.2.5.S6.2",
  "4.2.5.S12.1",
  "4.2.5.S12.2",
  "4.2.7.S12.1",
  "4.2.7.S12.2",
  "4.2.10.S12.1",
  "4.5.13.AL.1",
  "4.6.1.S1.1",
  "4.6.9.S1.1",
  "4.6.9.S1.2",
  "4.7.3.S1.1",
  "4.7.3.S6.1",
  "4.7.3.S8.1",
  "4.7.3.S12.1",
  "4.7.4.SR.1",
  "4.7.7.S1.1",
  "4.7.11.SU.1",
  "4.7.11.SU.2",
  "4.8.3.S1.1",
  "4.8.3.SU.1",
  "4.8.3.S8.1",
  "4.8.3.S12.1",
  "4.8.8.S6.1",
  "4.8.8.S12.1",
  "4.8.9.S6.1",
  "4.8.9.S12.1",
  "4.8.23.S1.1",
  "4.8.24.S12.1",
  "4.8.25.S12.1",
  "4.8.26.S12.1",
  "4.8.27.SU.1",
  "4.8.28.SU.1",
  "4.8.30.S1.1",
  "4.8.31.S12.1",
  "3.1.1.I5.1",
  "3.1.2.I5.1",
  "3.1.3.I5.1",
  "3.2.18.I1.1",
  "3.2.18.I1.2",
  "3.2.18.I1.3",
  "3.2.18.I1.4",
  "3.2.22.IR.1",
  "3.2.23.IU.1",
  "3.2.23.IU.2",
  "3.2.23.IU.3",
  "3.2.23.IU.4",
  "3.2.23.IU.5",
  "3.2.23.IU.6",
  "3.2.23.IU.7",
  "3.2.23.IU.8",
  "3.2.26.I3.1",
  "3.2.26.I7.1",
  "3.2.27.I1.1",
  "3.2.28.I1.1",
  "3.2.32.IU.1",
  "3.3.15.I5.1",
  "3.3.16.I5.1",
  "3.3.17.I1.1",
  "3.3.17.I2.1",
  "3.3.17.I5.1",
  "3.3.17.I6.1",
  "3.3.19.I1.1",
  "3.3.19.I2.1",
  "3.3.19.I5.1",
  "3.3.19.I6.1",
  "3.3.20.I1.1",
  "3.3.20.I1.2",
  "3.3.20.I1.3",
  "3.3.20.I1.4",
  "3.3.20.I2.1",
  "3.3.20.I2.2",
  "3.3.20.I2.3",
  "3.3.20.I2.4",
  "3.3.20.I5.1",
  "3.3.20.I5.2",
  "3.3.20.I5.3",
  "3.3.20.I5.4",
  "3.3.20.I6.1",
  "3.3.20.I6.2",
  "3.3.20.I6.3",
  "3.3.20.I6.4",
  "3.3.21.I3.1",
  "3.3.21.I7.1",
  "3.4.14.I3.1",
  "3.4.14.I7.1",
  "3.4.24.I7.1",
  "3.4.25.I7.1",
  "3.4.26.I7.1",
  "3.4.28.I3.1",
  "3.4.28.I7.1",
  "3.4.29.I3.1",
  "3.4.29.I7.1",
  "3.4.30.I3.1",
  "3.4.30.I7.1",
  "3.4.32.I3.1",
  "3.4.33.I1.1",
  "3.4.35.I5.1",
  "3.4.46.I7.1",
];

let testedCountermeasures: string[];

beforeAll(() => {
  expect(counterMeasuresToTest.length).toEqual(119);
  testedCountermeasures = [];
});

afterAll(() => {
  expect(testedCountermeasures).toEqual(counterMeasuresToTest);
});

// Helper functions

function validateCMFVector(
  code: string,
  expectedCMF: CmfVector,
  inputs: (number | string)[],
  aadtMajor: number,
  aadtMinor: number
): () => void {
  return () => {
    const calculatedCMF: CalculatedCounterMeasure =
      calculatedCounterMeasures.filter((calcCMF) => calcCMF.code === code)[0];

    expect(calculatedCMF).toBeDefined();

    const resultingCMF: CmfVector = calculatedCMF.calculate(
      inputs,
      aadtMajor,
      aadtMinor
    );
    expect(resultingCMF).toEqual(expectedCMF);

    testedCountermeasures.push(code);
  };
}

function validateCorrectInput(
  code: string,
  inputs: (number | string)[],
  aadtMajor: number,
  aadtMinor: number
): () => void {
  return () => {
    const calculatedCMF: CalculatedCounterMeasure =
      calculatedCounterMeasures.filter((calcCMF) => calcCMF.code === code)[0];

    expect(calculatedCMF).toBeDefined();

    expect(() =>
      calculatedCMF.validate(inputs, aadtMajor, aadtMinor)
    ).not.toThrowError();
  };
}

function validateAadtRequired(
  code: string,
  inputs: (number | string)[],
  majorRequired: boolean,
  minorRequired: boolean
): () => void {
  return () => {
    expect.assertions(
      majorRequired && minorRequired
        ? 5
        : majorRequired || minorRequired
        ? 3
        : 2
    );
    const calculatedCMF: CalculatedCounterMeasure =
      calculatedCounterMeasures.filter((calcCMF) => calcCMF.code === code)[0];

    expect(calculatedCMF).toBeDefined();

    if (!majorRequired && !minorRequired) {
      expect(() =>
        calculatedCMF.validate(inputs, undefined, undefined)
      ).not.toThrowError();
    } else {
      if (majorRequired) {
        try {
          calculatedCMF.validate(inputs, undefined, 700);
        } catch (error) {
          expect(error.name).toEqual("InvalidInputError");
          expect(error.message).toEqual(
            `The AADT Major must be provided for ${code}`
          );
        }
      }
      if (minorRequired) {
        try {
          calculatedCMF.validate(inputs, 700, undefined);
        } catch (error) {
          expect(error.name).toEqual("InvalidInputError");
          expect(error.message).toEqual(
            `The AADT Minor must be provided for ${code}`
          );
        }
      }
    }
  };
}

function validateInputType(
  code: string,
  incorrectTypeInputs: (number | string)[][],
  correctTypeArray: string[],
  aadtMajor: number,
  aadtMinor: number
): () => void {
  return () => {
    expect.assertions(1 + incorrectTypeInputs.length * 2);
    const calculatedCMF: CalculatedCounterMeasure =
      calculatedCounterMeasures.filter((calcCMF) => calcCMF.code === code)[0];

    expect(calculatedCMF).toBeDefined();

    for (const incorrectTypeInput of incorrectTypeInputs) {
      try {
        calculatedCMF.validate(incorrectTypeInput, aadtMajor, aadtMinor);
      } catch (error) {
        expect(error.name).toEqual("TypeError");
        expect(error.message).toEqual(
          `Input for ${code} is the incorrect type. Input needs to be of type [${correctTypeArray}], but is of type [${incorrectTypeInput.map(
            (input) =>
              typeof input === "number" && Number.isNaN(input)
                ? "string"
                : typeof input
          )}]`
        );
      }
    }
  };
}

function validateInputLength(
  code: string,
  incorrectInputs: (number | string)[],
  correctInputCount: number,
  aadtMajor: number,
  aadtMinor: number
): () => void {
  return () => {
    expect.assertions(3);
    const calculatedCMF: CalculatedCounterMeasure =
      calculatedCounterMeasures.filter((calcCMF) => calcCMF.code === code)[0];

    expect(calculatedCMF).toBeDefined();

    try {
      calculatedCMF.validate(incorrectInputs, aadtMajor, aadtMinor);
    } catch (error) {
      expect(error.name).toEqual("InvalidInputError");
      expect(error.message).toEqual(
        `${code} takes ${correctInputCount} inputs but ${
          incorrectInputs.length
        } ${incorrectInputs.length === 1 ? "was" : "were"} provided`
      );
    }
  };
}

function validateInputValues(
  code: string,
  incorrectInputs: (number | string)[][],
  expectedErrors: string[],
  aadtMajor: number,
  aadtMinor: number
): () => void {
  return () => {
    expect.assertions(incorrectInputs.length * 2 + 1);
    const calculatedCMF: CalculatedCounterMeasure =
      calculatedCounterMeasures.filter((calcCMF) => calcCMF.code === code)[0];

    expect(calculatedCMF).toBeDefined();

    for (let i = 0; i < incorrectInputs.length; i++) {
      try {
        calculatedCMF.validate(incorrectInputs[i], aadtMajor, aadtMinor);
      } catch (error) {
        expect(error.name).toEqual("InvalidInputError");
        expect(error.message).toEqual(expectedErrors[i]);
      }
    }
  };
}

function validateObjectAttributes(
  code: string,
  aadtMajorRequired: boolean,
  aadtMinorRequired: boolean,
  correctNumberOfInputs: number,
  correctTypeArray: string[]
): () => void {
  return () => {
    const calculatedCMF: CalculatedCounterMeasure =
      calculatedCounterMeasures.filter((calcCMF) => calcCMF.code === code)[0];
    expect(calculatedCMF).toBeDefined();

    expect(calculatedCMF.aadtMajorRequired).toEqual(aadtMajorRequired);
    expect(calculatedCMF.aadtMinorRequired).toEqual(aadtMinorRequired);
    expect(calculatedCMF.numberOfInputs).toEqual(correctNumberOfInputs);
    expect(calculatedCMF.inputPrompts.length).toEqual(correctNumberOfInputs);
    expect(calculatedCMF.inputTypes.length).toEqual(correctNumberOfInputs);
    expect(calculatedCMF.inputTypes).toEqual(correctTypeArray);
  };
}

function testCounterMeasure(testData: counterMeasureTestData) {
  test(
    `Attribute Validation for ${testData.code}`,
    validateObjectAttributes(
      testData.code,
      testData.aadtMajorRequired,
      testData.aadtMinorRequired,
      testData.correctNumberOfInputs,
      testData.correctTypeArray
    )
  );

  test(
    `Validate ${testData.code} correctly`,
    validateCorrectInput(
      testData.code,
      testData.correctInputs,
      testData.aadtMajor,
      testData.aadtMinor
    )
  );

  // If the countermeasure has 0 expected inputs, the type does not matter
  if (testData.correctNumberOfInputs != 0) {
    test(
      `Validate ${testData.code} type of inputs`,
      validateInputType(
        testData.code,
        testData.incorrectInputsDueToType,
        testData.correctTypeArray,
        testData.aadtMajor,
        testData.aadtMinor
      )
    );
  }

  test(
    `Validate ${testData.code} number of inputs`,
    validateInputLength(
      testData.code,
      testData.incorrectInputsDueToLength,
      testData.correctNumberOfInputs,
      testData.aadtMajor,
      testData.aadtMinor
    )
  );

  test(
    `Validate ${testData.code} AADT Missing`,
    validateAadtRequired(
      testData.code,
      testData.correctInputs,
      testData.aadtMajorRequired,
      testData.aadtMinorRequired
    )
  );

  if (testData.inputValidation) {
    test(
      `Validate ${testData.code} inputs`,
      validateInputValues(
        testData.code,
        testData.incorrectInputsDueToValue,
        testData.expectedValueErrors,
        testData.aadtMajor,
        testData.aadtMinor
      )
    );
  }

  test(
    `Calculated CmfVector for ${testData.code}`,
    validateCMFVector(
      testData.code,
      testData.expectedCMF,
      testData.correctInputs,
      testData.aadtMajor,
      testData.aadtMinor
    )
  );
}

// _____________________________________________________________________________________________

// 4.1.1.S1.1
// Macro case 2A

testCounterMeasure({
  code: "4.1.1.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 0.866666667],
    [CrashType.FTF, 0.866666667],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 0.866666667],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.ROR, 0.866666667],
    [CrashType.RT, 1],
    [CrashType.SSD, 0.866666667],
    [CrashType.SOD, 0.866666667],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [5, 10],
  incorrectInputsDueToType: [
    [5, "Hello"],
    ["Fish", 3],
    [Number.NaN, 2],
    [2, Number.NaN],
  ],
  incorrectInputsDueToLength: [5],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3750,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.1.1.S6.1
// Macro case 2B

testCounterMeasure({
  code: "4.1.1.S6.1",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 0.970873786],
    [CrashType.FTF, 0.970873786],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 0.970873786],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.ROR, 0.970873786],
    [CrashType.RT, 1],
    [CrashType.SSD, 0.970873786],
    [CrashType.SOD, 0.970873786],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [3, 14],
  incorrectInputsDueToType: [
    [3, "World"],
    ["Fish", 14],
    [Number.NaN, 14],
    [3, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 0,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.1.2.SU.1
// Macro case none

testCounterMeasure({
  code: "4.1.2.SU.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.856415177483613, 9)],
    [CrashType.AN, roundNumber(0.856415177483613, 9)],
    [CrashType.FO, roundNumber(0.856415177483613, 9)],
    [CrashType.FTF, roundNumber(0.856415177483613, 9)],
    [CrashType.LT, roundNumber(0.856415177483613, 9)],
    [CrashType.OtherNC, roundNumber(0.856415177483613, 9)],
    [CrashType.OtherO, roundNumber(0.856415177483613, 9)],
    [CrashType.OVT, roundNumber(0.856415177483613, 9)],
    [CrashType.PD, roundNumber(0.856415177483613, 9)],
    [CrashType.PDC, roundNumber(0.856415177483613, 9)],
    [CrashType.PKV, roundNumber(0.856415177483613, 9)],
    [CrashType.FTR, roundNumber(0.856415177483613, 9)],
    [CrashType.RT, roundNumber(0.856415177483613, 9)],
    [CrashType.SSD, roundNumber(0.856415177483613, 9)],
    [CrashType.SOD, roundNumber(0.856415177483613, 9)],
    [CrashType.T, roundNumber(0.856415177483613, 9)],
    [CrashType.TR, roundNumber(0.856415177483613, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: undefined,
  incorrectInputsDueToLength: [1],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3000,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// 4.1.2.SU.2
// Macro case none

testCounterMeasure({
  code: "4.1.2.SU.2",
  expectedCMF: new Map([
    [CrashType.AG, 0.41],
    [CrashType.AN, 0.41],
    [CrashType.FO, 0.41],
    [CrashType.FTF, 0.41],
    [CrashType.LT, 0.41],
    [CrashType.OtherNC, 0.41],
    [CrashType.OtherO, 0.41],
    [CrashType.OVT, 0.41],
    [CrashType.PD, 0.41],
    [CrashType.PDC, 0.41],
    [CrashType.PKV, 0.41],
    [CrashType.FTR, 0.41],
    [CrashType.RT, 0.41],
    [CrashType.SSD, 0.41],
    [CrashType.SOD, 0.41],
    [CrashType.T, 0.41],
    [CrashType.TR, 0.41],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: undefined,
  incorrectInputsDueToLength: [1],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// 4.1.2.SU.3
// Macro case none

testCounterMeasure({
  code: "4.1.2.SU.3",
  expectedCMF: new Map([
    [CrashType.AG, 1.32],
    [CrashType.AN, 1.32],
    [CrashType.FO, 1.32],
    [CrashType.FTF, 1.32],
    [CrashType.LT, 1.32],
    [CrashType.OtherNC, 1.32],
    [CrashType.OtherO, 1.32],
    [CrashType.OVT, 1.32],
    [CrashType.PD, 1.32],
    [CrashType.PDC, 1.32],
    [CrashType.PKV, 1.32],
    [CrashType.FTR, 1.32],
    [CrashType.RT, 1.32],
    [CrashType.SSD, 1.32],
    [CrashType.SOD, 1.32],
    [CrashType.T, 1.32],
    [CrashType.TR, 1.32],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: undefined,
  incorrectInputsDueToLength: [1],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// 4.1.2.SU.4
// Macro case none

testCounterMeasure({
  code: "4.1.2.SU.4",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.856415177483613, 9)],
    [CrashType.AN, roundNumber(0.856415177483613, 9)],
    [CrashType.FO, roundNumber(0.856415177483613, 9)],
    [CrashType.FTF, roundNumber(0.856415177483613, 9)],
    [CrashType.LT, roundNumber(0.856415177483613, 9)],
    [CrashType.OtherNC, roundNumber(0.856415177483613, 9)],
    [CrashType.OtherO, roundNumber(0.856415177483613, 9)],
    [CrashType.OVT, roundNumber(0.856415177483613, 9)],
    [CrashType.PD, roundNumber(0.856415177483613, 9)],
    [CrashType.PDC, roundNumber(0.856415177483613, 9)],
    [CrashType.PKV, roundNumber(0.856415177483613, 9)],
    [CrashType.FTR, roundNumber(0.856415177483613, 9)],
    [CrashType.RT, roundNumber(0.856415177483613, 9)],
    [CrashType.SSD, roundNumber(0.856415177483613, 9)],
    [CrashType.SOD, roundNumber(0.856415177483613, 9)],
    [CrashType.T, roundNumber(0.856415177483613, 9)],
    [CrashType.TR, roundNumber(0.856415177483613, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: undefined,
  incorrectInputsDueToLength: [1],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3000,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// 4.1.2.SU.5
// Macro case none

testCounterMeasure({
  code: "4.1.2.SU.5",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(1.16765796110513, 9)],
    [CrashType.AN, roundNumber(1.16765796110513, 9)],
    [CrashType.FO, roundNumber(1.16765796110513, 9)],
    [CrashType.FTF, roundNumber(1.16765796110513, 9)],
    [CrashType.LT, roundNumber(1.16765796110513, 9)],
    [CrashType.OtherNC, roundNumber(1.16765796110513, 9)],
    [CrashType.OtherO, roundNumber(1.16765796110513, 9)],
    [CrashType.OVT, roundNumber(1.16765796110513, 9)],
    [CrashType.PD, roundNumber(1.16765796110513, 9)],
    [CrashType.PDC, roundNumber(1.16765796110513, 9)],
    [CrashType.PKV, roundNumber(1.16765796110513, 9)],
    [CrashType.FTR, roundNumber(1.16765796110513, 9)],
    [CrashType.RT, roundNumber(1.16765796110513, 9)],
    [CrashType.SSD, roundNumber(1.16765796110513, 9)],
    [CrashType.SOD, roundNumber(1.16765796110513, 9)],
    [CrashType.T, roundNumber(1.16765796110513, 9)],
    [CrashType.TR, roundNumber(1.16765796110513, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: undefined,
  incorrectInputsDueToLength: [1],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3000,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// 4.1.2.SU.6
// Macro case none

testCounterMeasure({
  code: "4.1.2.SU.6",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(2.4390243902439, 9)],
    [CrashType.AN, roundNumber(2.4390243902439, 9)],
    [CrashType.FO, roundNumber(2.4390243902439, 9)],
    [CrashType.FTF, roundNumber(2.4390243902439, 9)],
    [CrashType.LT, roundNumber(2.4390243902439, 9)],
    [CrashType.OtherNC, roundNumber(2.4390243902439, 9)],
    [CrashType.OtherO, roundNumber(2.4390243902439, 9)],
    [CrashType.OVT, roundNumber(2.4390243902439, 9)],
    [CrashType.PD, roundNumber(2.4390243902439, 9)],
    [CrashType.PDC, roundNumber(2.4390243902439, 9)],
    [CrashType.PKV, roundNumber(2.4390243902439, 9)],
    [CrashType.FTR, roundNumber(2.4390243902439, 9)],
    [CrashType.RT, roundNumber(2.4390243902439, 9)],
    [CrashType.SSD, roundNumber(2.4390243902439, 9)],
    [CrashType.SOD, roundNumber(2.4390243902439, 9)],
    [CrashType.T, roundNumber(2.4390243902439, 9)],
    [CrashType.TR, roundNumber(2.4390243902439, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: undefined,
  incorrectInputsDueToLength: [1],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// 4.1.2.SU.7
// Macro case none

testCounterMeasure({
  code: "4.1.2.SU.7",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.757575757575757, 9)],
    [CrashType.AN, roundNumber(0.757575757575757, 9)],
    [CrashType.FO, roundNumber(0.757575757575757, 9)],
    [CrashType.FTF, roundNumber(0.757575757575757, 9)],
    [CrashType.LT, roundNumber(0.757575757575757, 9)],
    [CrashType.OtherNC, roundNumber(0.757575757575757, 9)],
    [CrashType.OtherO, roundNumber(0.757575757575757, 9)],
    [CrashType.OVT, roundNumber(0.757575757575757, 9)],
    [CrashType.PD, roundNumber(0.757575757575757, 9)],
    [CrashType.PDC, roundNumber(0.757575757575757, 9)],
    [CrashType.PKV, roundNumber(0.757575757575757, 9)],
    [CrashType.FTR, roundNumber(0.757575757575757, 9)],
    [CrashType.RT, roundNumber(0.757575757575757, 9)],
    [CrashType.SSD, roundNumber(0.757575757575757, 9)],
    [CrashType.SOD, roundNumber(0.757575757575757, 9)],
    [CrashType.T, roundNumber(0.757575757575757, 9)],
    [CrashType.TR, roundNumber(0.757575757575757, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: undefined,
  incorrectInputsDueToLength: [1],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.1.2.SU.8
// Macro case none

testCounterMeasure({
  code: "4.1.2.SU.8",
  expectedCMF: new Map([
    [CrashType.AG, 1.091442264],
    [CrashType.AN, 1.091442264],
    [CrashType.FO, 1.091442264],
    [CrashType.FTF, 1.091442264],
    [CrashType.LT, 1.091442264],
    [CrashType.OtherNC, 1.091442264],
    [CrashType.OtherO, 1.091442264],
    [CrashType.OVT, 1.091442264],
    [CrashType.PD, 1.091442264],
    [CrashType.PDC, 1.091442264],
    [CrashType.PKV, 1.091442264],
    [CrashType.FTR, 1.091442264],
    [CrashType.ROR, 1],
    [CrashType.RT, 1.091442264],
    [CrashType.SSD, 1.091442264],
    [CrashType.SOD, 1.091442264],
    [CrashType.T, 1.091442264],
    [CrashType.TR, 1.091442264],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: undefined,
  incorrectInputsDueToLength: [1],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3750,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.1.3.S1.1
// Macro Case 2D

testCounterMeasure({
  code: "4.1.3.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 0.816326531],
    [CrashType.FTF, 0.816326531],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 0.816326531],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.ROR, 0.816326531],
    [CrashType.RT, 1],
    [CrashType.SSD, 0.816326531],
    [CrashType.SOD, 0.816326531],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [3, 6],
  incorrectInputsDueToType: [
    ["string", 6],
    [3, "string"],
    [Number.NaN, 6],
    [3, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3750,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.1.3.S6.1
// Macro Case 2F

// Note: The following test copies the spreadsheet's behavior on returning a fixed CMF vector without doing any calculations on user input
// I believe this is incorrect for this counter measure but we must first match the spreadsheet and then contact IDOT about this error.

testCounterMeasure({
  code: "4.1.3.S6.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.35],
    [CrashType.AN, 0.35],
    [CrashType.FO, 0.67],
    [CrashType.FTF, 0.67],
    [CrashType.LT, 0.35],
    [CrashType.OtherNC, 0.35],
    [CrashType.OtherO, 0.52],
    [CrashType.OVT, 0.67],
    [CrashType.PD, 0.52],
    [CrashType.PDC, 0.52],
    [CrashType.PKV, 0.52],
    [CrashType.FTR, 0.35],
    [CrashType.RT, 0.35],
    [CrashType.SSD, 0.67],
    [CrashType.SOD, 0.67],
    [CrashType.T, 0.35],
    [CrashType.TR, 0.52],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: undefined,
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.1.12.S1.1
// Macro case 2G

testCounterMeasure({
  code: "4.1.12.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 0.962264151],
    [CrashType.FTF, 0.962264151],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 0.962264151],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.ROR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 0.962264151],
    [CrashType.SOD, 0.962264151],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [8, "composite", "Gravel"],
  incorrectInputsDueToType: [
    ["string", "composite", "gravel"],
    [8, 8, "gravel"],
    [8, "composite", 8],
    [Number.NaN, "composite", "gravel"],
  ],
  incorrectInputsDueToLength: [3, 5],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 3,
  correctTypeArray: ["number", "string", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [11, "paved", "gravel"],
    [5, "paved", "wrong"],
    [5, "wrong", "gravel"],
  ],
  expectedValueErrors: [
    "Shoulder width must be between 1 and 10. If shoulder width is greater than 10, please input 10.",
    "Invalid shoulder type selected. Shoulder Type Options are Paved, Composite, Turf, and Gravel.",
    "Invalid shoulder type selected. Shoulder Type Options are Paved, Composite, Turf, and Gravel.",
  ],
});

// _____________________________________________________________________________________________

// 4.1.13.S6.1
// Macro Case 2I

testCounterMeasure({
  code: "4.1.13.S6.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.989880132825788, 9)],
    [CrashType.AN, 1],
    [CrashType.FO, 1],
    [CrashType.FTF, 1],
    [CrashType.LT, roundNumber(0.989880132825788, 9)],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 1],
    [CrashType.SOD, 1],
    [CrashType.T, roundNumber(0.989880132825788, 9)],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [3, 0.5],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [[3, 2]],
  expectedValueErrors: [
    "Crash Proportion is in decimal form (crash proportion must be < 1)",
  ],
});

// _____________________________________________________________________________________________

// 4.1.13.S12.1
// Macro Case 2K

testCounterMeasure({
  code: "4.1.13.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.881914893617021, 9)],
    [CrashType.AN, 1],
    [CrashType.FO, 1],
    [CrashType.FTF, 1],
    [CrashType.LT, roundNumber(0.881914893617021, 9)],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 1],
    [CrashType.SOD, 1],
    [CrashType.T, roundNumber(0.881914893617021, 9)],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [15, 0.2],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [[3, 2]],
  expectedValueErrors: [
    "Crash Proportion is in decimal form (crash proportion must be < 1)",
  ],
});

// _____________________________________________________________________________________________

// 4.1.17.AL.1
// Macro case None
// Adjusted CMF: 4B

testCounterMeasure({
  code: "4.1.17.AL.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.76],
    [CrashType.AN, 0.752193784],
    [CrashType.FO, 0.752193784],
    [CrashType.FTF, 0.752193784],
    [CrashType.LT, 0.76],
    [CrashType.OtherNC, 0.76],
    [CrashType.OtherO, 0.76],
    [CrashType.OVT, 0.752193784],
    [CrashType.PD, 0.76],
    [CrashType.PDC, 0.76],
    [CrashType.PKV, 0.76],
    [CrashType.FTR, 0.83],
    [CrashType.RT, 0.76],
    [CrashType.SSD, 0.76],
    [CrashType.SOD, 0.752193784],
    [CrashType.T, 0.76],
    [CrashType.TR, 0.76],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 0.43],
  ]),
  correctInputs: [5, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [1],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 4.1.19.S8.1
// Macro Case None
// Adjusted CMF Tag: 4C

testCounterMeasure({
  code: "4.1.19.S8.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.739],
    [CrashType.AN, roundNumber(0.742127659574468, 9)],
    [CrashType.FO, roundNumber(0.742127659574468, 9)],
    [CrashType.FTF, 0.739],
    [CrashType.LT, 0.739],
    [CrashType.OtherNC, roundNumber(0.742127659574468, 9)],
    [CrashType.OtherO, 0.739],
    [CrashType.OVT, roundNumber(0.742127659574468, 9)],
    [CrashType.PD, 0.739],
    [CrashType.PDC, 0.739],
    [CrashType.PKV, 0.739],
    [CrashType.FTR, 0.613],
    [CrashType.RT, 0.739],
    [CrashType.SSD, 0.739],
    [CrashType.SOD, 0.739],
    [CrashType.T, 0.739],
    [CrashType.TR, 0.739],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 4.1.19.S1.1
// Macro Case None
// Adjusted CMF Tag: 4D

testCounterMeasure({
  code: "4.1.19.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.739],
    [CrashType.AN, roundNumber(0.742127659574468, 9)],
    [CrashType.FO, roundNumber(0.742127659574468, 9)],
    [CrashType.FTF, 0.739],
    [CrashType.LT, 0.739],
    [CrashType.OtherNC, roundNumber(0.742127659574468, 9)],
    [CrashType.OtherO, 0.739],
    [CrashType.OVT, roundNumber(0.742127659574468, 9)],
    [CrashType.PD, 0.739],
    [CrashType.PDC, 0.739],
    [CrashType.PKV, 0.739],
    [CrashType.FTR, 0.613],
    [CrashType.RT, 0.739],
    [CrashType.SSD, 0.739],
    [CrashType.SOD, 0.739],
    [CrashType.T, 0.739],
    [CrashType.TR, 0.739],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 4.1.21.S8.1
// Macro Case None
// Adjusted CMF Tag: 4E

testCounterMeasure({
  code: "4.1.21.S8.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.946],
    [CrashType.AN, roundNumber(0.946241980474198, 9)],
    [CrashType.FO, roundNumber(0.946241980474198, 9)],
    [CrashType.FTF, 0.946],
    [CrashType.LT, 0.946],
    [CrashType.OtherNC, roundNumber(0.946241980474198, 9)],
    [CrashType.OtherO, 0.946],
    [CrashType.OVT, roundNumber(0.946241980474198, 9)],
    [CrashType.PD, 0.855],
    [CrashType.PDC, 1.509],
    [CrashType.PKV, 0.946],
    [CrashType.FTR, 0.946],
    [CrashType.RT, 0.946],
    [CrashType.SSD, 0.946],
    [CrashType.SOD, 0.946],
    [CrashType.T, 0.946],
    [CrashType.TR, 0.946],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 4.1.21.S12.1
// Macro Case None
// Adjusted CMF Tag: 4E

testCounterMeasure({
  code: "4.1.21.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.946],
    [CrashType.AN, roundNumber(0.946241980474198, 9)],
    [CrashType.FO, roundNumber(0.946241980474198, 9)],
    [CrashType.FTF, 0.946],
    [CrashType.LT, 0.946],
    [CrashType.OtherNC, roundNumber(0.946241980474198, 9)],
    [CrashType.OtherO, 0.946],
    [CrashType.OVT, roundNumber(0.946241980474198, 9)],
    [CrashType.PD, 0.855],
    [CrashType.PDC, 1.509],
    [CrashType.PKV, 0.946],
    [CrashType.FTR, 0.946],
    [CrashType.RT, 0.946],
    [CrashType.SSD, 0.946],
    [CrashType.SOD, 0.946],
    [CrashType.T, 0.946],
    [CrashType.TR, 0.946],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 4.2.5.S6.1
// Macro Case 2L

testCounterMeasure({
  code: "4.2.5.S6.1",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 1],
    [CrashType.FTF, 0.34],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 1],
    [CrashType.SOD, 0.34],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, 80],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.2.5.S6.2
// Macro Case 2L

testCounterMeasure({
  code: "4.2.5.S6.2",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 1],
    [CrashType.FTF, 0.71],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 1],
    [CrashType.SOD, 0.71],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [10, 30],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.2.5.S12.1
// Macro Case 2N

testCounterMeasure({
  code: "4.2.5.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 1],
    [CrashType.FTF, 0.8],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 1],
    [CrashType.SOD, 0.8],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [10, 30],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.2.5.S12.2
// Macro Case 2O

testCounterMeasure({
  code: "4.2.5.S12.2",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 1],
    [CrashType.FTF, 0.76],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 1],
    [CrashType.SOD, 0.76],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [10, 30],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.2.7.S12.1
// Macro Case None
// Adjusted CMF Tag: 4F

testCounterMeasure({
  code: "4.2.7.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.93],
    [CrashType.AN, roundNumber(0.930693000693001, 9)],
    [CrashType.FO, roundNumber(0.930693000693001, 9)],
    [CrashType.FTF, 0.93],
    [CrashType.LT, 0.43],
    [CrashType.OtherNC, roundNumber(0.930693000693001, 9)],
    [CrashType.OtherO, 0.93],
    [CrashType.OVT, roundNumber(0.930693000693001, 9)],
    [CrashType.PD, 0.93],
    [CrashType.PDC, 0.93],
    [CrashType.PKV, 0.93],
    [CrashType.FTR, 0.93],
    [CrashType.RT, 0.93],
    [CrashType.SSD, 0.93],
    [CrashType.SOD, 0.93],
    [CrashType.T, 0.63],
    [CrashType.TR, 0.93],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 4.2.7.S12.2
// Macro Case None
// Adjusted CMF Tag: 4G

testCounterMeasure({
  code: "4.2.7.S12.2",
  expectedCMF: new Map([
    [CrashType.AG, 0.95],
    [CrashType.AN, roundNumber(0.950554400554401, 9)],
    [CrashType.FO, roundNumber(0.950554400554401, 9)],
    [CrashType.FTF, 0.95],
    [CrashType.LT, 0.55],
    [CrashType.OtherNC, roundNumber(0.950554400554401, 9)],
    [CrashType.OtherO, 0.95],
    [CrashType.OVT, roundNumber(0.950554400554401, 9)],
    [CrashType.PD, 0.95],
    [CrashType.PDC, 0.95],
    [CrashType.PKV, 0.95],
    [CrashType.FTR, 0.95],
    [CrashType.RT, 0.95],
    [CrashType.SSD, 0.95],
    [CrashType.SOD, 0.95],
    [CrashType.T, 0.71],
    [CrashType.TR, 0.95],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 4.2.10.S12.1
// Macro Case 4A

testCounterMeasure({
  code: "4.2.10.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(1.21774404070591, 9)],
    [CrashType.AN, 1],
    [CrashType.FO, roundNumber(1.10097923782343, 9)],
    [CrashType.FTF, roundNumber(1.25332497521497, 9)],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, roundNumber(1.10097923782343, 9)],
    [CrashType.RT, 1],
    [CrashType.SSD, roundNumber(1.10097923782343, 9)],
    [CrashType.SOD, 1],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, roundNumber(1.10097923782343, 9)],
    [CrashType.WP, 1],
  ]),
  correctInputs: [5, 7],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.5.13.AL.1
// Macro Case 4B

testCounterMeasure({
  code: "4.5.13.AL.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.030807411032751, 9)],
    [CrashType.AN, roundNumber(0.030807411032751, 9)],
    [CrashType.FO, roundNumber(0.030807411032751, 9)],
    [CrashType.FTF, roundNumber(0.030807411032751, 9)],
    [CrashType.LT, roundNumber(0.030807411032751, 9)],
    [CrashType.OtherNC, roundNumber(0.030807411032751, 9)],
    [CrashType.OtherO, roundNumber(0.030807411032751, 9)],
    [CrashType.OVT, roundNumber(0.030807411032751, 9)],
    [CrashType.PD, roundNumber(0.030807411032751, 9)],
    [CrashType.PDC, roundNumber(0.030807411032751, 9)],
    [CrashType.PKV, roundNumber(0.030807411032751, 9)],
    [CrashType.FTR, roundNumber(0.030807411032751, 9)],
    [CrashType.RT, roundNumber(0.030807411032751, 9)],
    [CrashType.SSD, roundNumber(0.030807411032751, 9)],
    [CrashType.SOD, roundNumber(0.030807411032751, 9)],
    [CrashType.T, roundNumber(0.030807411032751, 9)],
    [CrashType.TR, roundNumber(0.030807411032751, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [50, 80],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.6.1.S1.1
// Macro Case 2P

testCounterMeasure({
  code: "4.6.1.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.892857143],
    [CrashType.AN, 0.892857143],
    [CrashType.FO, 0.892857143],
    [CrashType.FTF, 0.892857143],
    [CrashType.LT, 0.892857143],
    [CrashType.OtherNC, 0.892857143],
    [CrashType.OtherO, 0.892857143],
    [CrashType.OVT, 0.892857143],
    [CrashType.PD, 0.892857143],
    [CrashType.PDC, 0.892857143],
    [CrashType.PKV, 0.892857143],
    [CrashType.FTR, 0.892857143],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.892857143],
    [CrashType.SSD, 0.892857143],
    [CrashType.SOD, 0.892857143],
    [CrashType.T, 0.892857143],
    [CrashType.TR, 0.892857143],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [0.04, 0.01],
  incorrectInputsDueToType: [
    ["string", 0.3],
    [Number.NaN, 0.3],
    [0.2, "string"],
    [0.2, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [1, 0.3],
    [-1, 0.3],
    [0.2, 7],
    [0.2, -3],
  ],
  expectedValueErrors: [
    "Input invalid. Please enter a decimal number between 0 and 0.4.",
    "Input invalid. Please enter a decimal number between 0 and 0.4.",
    "Input invalid. Please enter a decimal number between 0 and 0.4.",
    "Input invalid. Please enter a decimal number between 0 and 0.4.",
  ],
});

// _____________________________________________________________________________________________

// 4.6.9.S1.1
// Macro Case 2Q

testCounterMeasure({
  code: "4.6.9.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.972156446],
    [CrashType.AN, 0.972156446],
    [CrashType.FO, 0.972156446],
    [CrashType.FTF, 0.972156446],
    [CrashType.LT, 0.972156446],
    [CrashType.OtherNC, 0.972156446],
    [CrashType.OtherO, 0.972156446],
    [CrashType.OVT, 0.972156446],
    [CrashType.PD, 0.972156446],
    [CrashType.PDC, 0.972156446],
    [CrashType.PKV, 0.972156446],
    [CrashType.FTR, 0.972156446],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.972156446],
    [CrashType.SSD, 0.972156446],
    [CrashType.SOD, 0.972156446],
    [CrashType.T, 0.972156446],
    [CrashType.TR, 0.972156446],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [0.5, 1500, 0.75, 2000],
  incorrectInputsDueToType: [
    ["string", 1500, 0.75, 2000],
    [Number.NaN, 1500, 0.75, 2000],
    [0.5, "string", 0.75, 2000],
    [0.5, Number.NaN, 0.75, 2000],
    [0.5, 1500, "string", 2000],
    [0.5, 1500, Number.NaN, 2000],
    [0.5, 1500, 0.75, "string"],
    [0.5, 1500, 0.75, Number.NaN],
  ],
  incorrectInputsDueToLength: [0.5, 1500],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 4,
  correctTypeArray: ["number", "number", "number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.6.9.S1.2
// Macro Case 2R

testCounterMeasure({
  code: "4.6.9.S1.2",
  expectedCMF: new Map([
    [CrashType.AG, 0.904493889],
    [CrashType.AN, 0.904493889],
    [CrashType.FO, 0.904493889],
    [CrashType.FTF, 0.904493889],
    [CrashType.LT, 0.904493889],
    [CrashType.OtherNC, 0.904493889],
    [CrashType.OtherO, 0.904493889],
    [CrashType.OVT, 0.904493889],
    [CrashType.PD, 0.904493889],
    [CrashType.PDC, 0.904493889],
    [CrashType.PKV, 0.904493889],
    [CrashType.FTR, 0.904493889],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.904493889],
    [CrashType.SSD, 0.904493889],
    [CrashType.SOD, 0.904493889],
    [CrashType.T, 0.904493889],
    [CrashType.TR, 0.904493889],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [0.2, 1800, 0.75, 2000],
  incorrectInputsDueToType: [
    ["string", 1800, 0.75, 2000],
    [Number.NaN, 1800, 0.75, 2000],
    [0.2, "string", 0.75, 2000],
    [0.2, Number.NaN, 0.75, 2000],
    [0.2, 1800, "string", 2000],
    [0.2, 1800, Number.NaN, 2000],
    [0.2, 1800, 0.75, "string"],
    [0.2, 1800, 0.75, Number.NaN],
  ],
  incorrectInputsDueToLength: [0.2, 1800],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 4,
  correctTypeArray: ["number", "number", "number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.7.3.S1.1
// Macro case 2S

testCounterMeasure({
  code: "4.7.3.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.92],
    [CrashType.AN, 0.92],
    [CrashType.FO, 0.92],
    [CrashType.FTF, 0.92],
    [CrashType.LT, 0.92],
    [CrashType.OtherNC, 0.92],
    [CrashType.OtherO, 0.92],
    [CrashType.OVT, 0.92],
    [CrashType.PD, 0.92],
    [CrashType.PDC, 0.92],
    [CrashType.PKV, 0.92],
    [CrashType.FTR, 0.92],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.92],
    [CrashType.SSD, 0.92],
    [CrashType.SOD, 0.92],
    [CrashType.T, 0.92],
    [CrashType.TR, 0.92],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: ["1v:3h", "1V:5H"],
  incorrectInputsDueToType: [
    ["1V:3H", 6],
    [6, "1V:5H"],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["string", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    ["1V:1H", "1V:5H"],
    ["Wrong", "1V:5H"],
    ["1V:3H", "1V:8h"],
    ["1V:6H", "1V:4H"],
  ],
  expectedValueErrors: [
    "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H.",
    "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H.",
    "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H.",
    "Error, flattening a sideslope cannot make it less flat. Existing sideslope must be steeper than future sideslope.",
  ],
});

// _____________________________________________________________________________________________

// 4.7.3.S6.1
// Macro Case 2AT

testCounterMeasure({
  code: "4.7.3.S6.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.947826087],
    [CrashType.AN, 0.947826087],
    [CrashType.FO, 0.947826087],
    [CrashType.FTF, 0.947826087],
    [CrashType.LT, 0.947826087],
    [CrashType.OtherNC, 0.947826087],
    [CrashType.OtherO, 0.947826087],
    [CrashType.OVT, 0.947826087],
    [CrashType.PD, 0.947826087],
    [CrashType.PDC, 0.947826087],
    [CrashType.PKV, 0.947826087],
    [CrashType.FTR, 0.947826087],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.947826087],
    [CrashType.SSD, 0.947826087],
    [CrashType.SOD, 0.947826087],
    [CrashType.T, 0.947826087],
    [CrashType.TR, 0.947826087],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: ["1V:3H", "1v:5H"],
  incorrectInputsDueToType: [
    ["1V:3H", 6],
    [6, "1V:5H"],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["string", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    ["1V:1H", "1V:5H"],
    ["Wrong", "1V:5H"],
    ["1V:3H", "1V:8h"],
    ["1V:6H", "1V:4H"],
  ],
  expectedValueErrors: [
    "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H.",
    "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H.",
    "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H.",
    "Error, flattening a sideslope cannot make it less flat. Existing sideslope must be steeper than future sideslope.",
  ],
});

// _____________________________________________________________________________________________

// 4.7.3.S8.1
// Macro Case 2AV

testCounterMeasure({
  code: "4.7.3.S8.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.93],
    [CrashType.AN, 0.93],
    [CrashType.FO, 0.93],
    [CrashType.FTF, 0.93],
    [CrashType.LT, 0.93],
    [CrashType.OtherNC, 0.93],
    [CrashType.OtherO, 0.93],
    [CrashType.OVT, 0.93],
    [CrashType.PD, 0.93],
    [CrashType.PDC, 0.93],
    [CrashType.PKV, 0.93],
    [CrashType.FTR, 0.93],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.93],
    [CrashType.SSD, 0.93],
    [CrashType.SOD, 0.93],
    [CrashType.T, 0.93],
    [CrashType.TR, 0.93],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: ["1v:4H", "1V:6H"],
  incorrectInputsDueToType: [
    ["1V:4H", 6],
    [6, "1V:6H"],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["string", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    ["1V:1H", "1V:5H"],
    ["Wrong", "1V:5H"],
    ["1V:3H", "1V:8h"],
    ["1V:6H", "1V:4H"],
  ],
  expectedValueErrors: [
    "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H.",
    "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H.",
    "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H.",
    "Error, flattening a sideslope cannot make it less flat. Existing sideslope must be steeper than future sideslope.",
  ],
});

// _____________________________________________________________________________________________

// 4.7.3.S12.1
// Macro Case 2AW

// Note, the following test assumes the countermeasure has 1.12 hard-coded for InterimCalc1, which I believe is incorrect.
// Need to contact IDOT about this.
testCounterMeasure({
  code: "4.7.3.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.9375],
    [CrashType.AN, 0.9375],
    [CrashType.FO, 0.9375],
    [CrashType.FTF, 0.9375],
    [CrashType.LT, 0.9375],
    [CrashType.OtherNC, 0.9375],
    [CrashType.OtherO, 0.9375],
    [CrashType.OVT, 0.9375],
    [CrashType.PD, 0.9375],
    [CrashType.PDC, 0.9375],
    [CrashType.PKV, 0.9375],
    [CrashType.FTR, 0.9375],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.9375],
    [CrashType.SSD, 0.9375],
    [CrashType.SOD, 0.9375],
    [CrashType.T, 0.9375],
    [CrashType.TR, 0.9375],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: ["1V:6h"],
  incorrectInputsDueToType: [[5]],
  incorrectInputsDueToLength: ["1V:4H", "1V:6H"],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 1,
  correctTypeArray: ["string"],
  inputValidation: true,
  /* The following would be correct if the countermeasure did not ignore existing sideslope
  [
    ["1V:1H", "1V:5H"],
    ["Wrong", "1V:5H"],
    ["1V:3H", "1V:8h"],
    ["1V:6H", "1V:4H"],
  ],
  [
    "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H.",
    "Existing Sideslope must be 1V:2H, 1V:3H, 1V:4H, 1V:5H, or 1V:6H. For Slopes steeper than 1V:2H, use 1V:2H. For slopes shallower than 1V:6H, use 1V:6H.",
    "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H.",
    "Error, flattening a sideslope cannot make it less flat. Existing sideslope must be steeper than future sideslope.",
  ]
  */
  // The following is correct only because the countermeasure ignores existing sideslope
  incorrectInputsDueToValue: [["1V:3H"]],
  expectedValueErrors: [
    "Future Sideslope must be  1V:4H, 1V:5H, 1V:6H, or 1V:7H. For slopes shallower than 1V:7H, use 1V:7H.",
  ],
});

// _____________________________________________________________________________________________

// 4.7.4.SR.1
// Macro Case 2U

testCounterMeasure({
  code: "4.7.4.SR.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.78],
    [CrashType.AN, 0.78],
    [CrashType.FO, 0.78],
    [CrashType.FTF, 0.78],
    [CrashType.LT, 0.78],
    [CrashType.OtherNC, 0.78],
    [CrashType.OtherO, 0.78],
    [CrashType.OVT, 0.78],
    [CrashType.PD, 0.78],
    [CrashType.PDC, 0.78],
    [CrashType.PKV, 0.78],
    [CrashType.FTR, 0.78],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.78],
    [CrashType.SSD, 0.78],
    [CrashType.SOD, 0.78],
    [CrashType.T, 0.78],
    [CrashType.TR, 0.78],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [10, 20],
  incorrectInputsDueToType: [
    ["string", 20],
    [10, "string"],
    [Number.NaN, 20],
    [10, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.7.7.S1.1
// Macro Case 2V

testCounterMeasure({
  code: "4.7.7.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.716053782],
    [CrashType.AN, 0.716053782],
    [CrashType.FO, 0.716053782],
    [CrashType.FTF, 0.716053782],
    [CrashType.LT, 0.716053782],
    [CrashType.OtherNC, 0.716053782],
    [CrashType.OtherO, 0.716053782],
    [CrashType.OVT, 0.716053782],
    [CrashType.PD, 0.716053782],
    [CrashType.PDC, 0.716053782],
    [CrashType.PKV, 0.716053782],
    [CrashType.FTR, 0.716053782],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.716053782],
    [CrashType.SSD, 0.716053782],
    [CrashType.SOD, 0.716053782],
    [CrashType.T, 0.716053782],
    [CrashType.TR, 0.716053782],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [7, 2],
  incorrectInputsDueToType: [
    ["string", 2],
    [Number.NaN, 2],
    [7, "string"],
    [7, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [1, 8],
    [9, 3],
  ],
  expectedValueErrors: [
    "Roadside Hazard Rating must be Between 1-7",
    "Roadside Hazard Rating must be Between 1-7",
  ],
});

// _____________________________________________________________________________________________

// 4.7.11.SU.1
// Macro Case 2W

testCounterMeasure({
  code: "4.7.11.SU.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.735729671513193, 9)],
    [CrashType.AN, roundNumber(0.735729671513193, 9)],
    [CrashType.FO, roundNumber(0.735729671513193, 9)],
    [CrashType.FTF, roundNumber(0.735729671513193, 9)],
    [CrashType.LT, roundNumber(0.735729671513193, 9)],
    [CrashType.OtherNC, roundNumber(0.735729671513193, 9)],
    [CrashType.OtherO, roundNumber(0.735729671513193, 9)],
    [CrashType.OVT, roundNumber(0.735729671513193, 9)],
    [CrashType.PD, roundNumber(0.735729671513193, 9)],
    [CrashType.PDC, roundNumber(0.735729671513193, 9)],
    [CrashType.PKV, roundNumber(0.735729671513193, 9)],
    [CrashType.FTR, roundNumber(0.735729671513193, 9)],
    [CrashType.RT, roundNumber(0.735729671513193, 9)],
    [CrashType.SSD, roundNumber(0.735729671513193, 9)],
    [CrashType.SOD, roundNumber(0.735729671513193, 9)],
    [CrashType.T, roundNumber(0.735729671513193, 9)],
    [CrashType.TR, roundNumber(0.735729671513193, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [600, 1500],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.7.11.SU.2
// Macro Case 2AA

testCounterMeasure({
  code: "4.7.11.SU.2",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.646122500955049, 9)],
    [CrashType.AN, roundNumber(0.646122500955049, 9)],
    [CrashType.FO, roundNumber(0.646122500955049, 9)],
    [CrashType.FTF, roundNumber(0.646122500955049, 9)],
    [CrashType.LT, roundNumber(0.646122500955049, 9)],
    [CrashType.OtherNC, roundNumber(0.646122500955049, 9)],
    [CrashType.OtherO, roundNumber(0.646122500955049, 9)],
    [CrashType.OVT, roundNumber(0.646122500955049, 9)],
    [CrashType.PD, roundNumber(0.646122500955049, 9)],
    [CrashType.PDC, roundNumber(0.646122500955049, 9)],
    [CrashType.PKV, roundNumber(0.646122500955049, 9)],
    [CrashType.FTR, roundNumber(0.646122500955049, 9)],
    [CrashType.RT, roundNumber(0.646122500955049, 9)],
    [CrashType.SSD, roundNumber(0.646122500955049, 9)],
    [CrashType.SOD, roundNumber(0.646122500955049, 9)],
    [CrashType.T, roundNumber(0.646122500955049, 9)],
    [CrashType.TR, roundNumber(0.646122500955049, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [500, 1000],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.8.3.S1.1
// Macro Case 2AE

testCounterMeasure({
  code: "4.8.3.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.917311181],
    [CrashType.AN, 0.917311181],
    [CrashType.FO, 0.917311181],
    [CrashType.FTF, 0.917311181],
    [CrashType.LT, 0.917311181],
    [CrashType.OtherNC, 0.917311181],
    [CrashType.OtherO, 0.917311181],
    [CrashType.OVT, 0.917311181],
    [CrashType.PD, 0.917311181],
    [CrashType.PDC, 0.917311181],
    [CrashType.PKV, 0.917311181],
    [CrashType.FTR, 0.917311181],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.917311181],
    [CrashType.SSD, 0.917311181],
    [CrashType.SOD, 0.917311181],
    [CrashType.T, 0.917311181],
    [CrashType.TR, 0.917311181],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [12, 8, 25],
  incorrectInputsDueToType: [
    ["string", 8, 25],
    [Number.NaN, 8, 25],
    [12, "string", 25],
    [12, Number.NaN, 25],
    [12, 8, "string"],
    [12, 8, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3750,
  aadtMinor: undefined,
  correctNumberOfInputs: 3,
  correctTypeArray: ["number", "number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [12, 8, 0],
    [12, 8, 55],
  ],
  expectedValueErrors: [
    "Effective service life must be a number between 1 and 50",
    "Effective service life must be a number between 1 and 50",
  ],
});

// _____________________________________________________________________________________________

// 4.8.3.SU.1
// Macro Case None

testCounterMeasure({
  code: "4.8.3.SU.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.6],
    [CrashType.AN, 0.6],
    [CrashType.FO, 0.6],
    [CrashType.FTF, 0.6],
    [CrashType.LT, 0.6],
    [CrashType.OtherNC, 0.6],
    [CrashType.OtherO, 0.6],
    [CrashType.OVT, 0.6],
    [CrashType.PD, 0.6],
    [CrashType.PDC, 0.6],
    [CrashType.PKV, 0.6],
    [CrashType.FTR, 0.6],
    [CrashType.RT, 0.6],
    [CrashType.SSD, 0.6],
    [CrashType.SOD, 0.6],
    [CrashType.T, 0.6],
    [CrashType.TR, 0.6],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// start of new

// _____________________________________________________________________________________________

// 4.8.3.S8.1
// Macro Case 2AF

testCounterMeasure({
  code: "4.8.3.S8.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.37],
    [CrashType.AN, 0.37],
    [CrashType.FO, 0.37],
    [CrashType.FTF, 0.37],
    [CrashType.LT, 0.37],
    [CrashType.OtherNC, 0.37],
    [CrashType.OtherO, 0.37],
    [CrashType.OVT, 0.37],
    [CrashType.PD, 0.37],
    [CrashType.PDC, 0.37],
    [CrashType.PKV, 0.37],
    [CrashType.FTR, 0.37],
    [CrashType.RT, 0.37],
    [CrashType.SSD, 0.37],
    [CrashType.SOD, 0.37],
    [CrashType.T, 0.37],
    [CrashType.TR, 0.37],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [50, 9, 25],
  incorrectInputsDueToType: [
    ["string", 8, 25],
    [Number.NaN, 8, 25],
    [12, "string", 25],
    [12, Number.NaN, 25],
    [12, 8, "string"],
    [12, 8, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 3,
  correctTypeArray: ["number", "number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [12, 8, 0],
    [12, 8, 55],
  ],
  expectedValueErrors: [
    "Effective service life must be a number between 1 and 50",
    "Effective service life must be a number between 1 and 50",
  ],
});

// _____________________________________________________________________________________________

// 4.8.3.S12.1
// Macro Case 2AH

testCounterMeasure({
  code: "4.8.3.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.755102040816326, 9)],
    [CrashType.AN, roundNumber(0.755102040816326, 9)],
    [CrashType.FO, roundNumber(0.755102040816326, 9)],
    [CrashType.FTF, roundNumber(0.755102040816326, 9)],
    [CrashType.LT, roundNumber(0.755102040816326, 9)],
    [CrashType.OtherNC, roundNumber(0.755102040816326, 9)],
    [CrashType.OtherO, roundNumber(0.755102040816326, 9)],
    [CrashType.OVT, roundNumber(0.755102040816326, 9)],
    [CrashType.PD, roundNumber(0.755102040816326, 9)],
    [CrashType.PDC, roundNumber(0.755102040816326, 9)],
    [CrashType.PKV, roundNumber(0.755102040816326, 9)],
    [CrashType.FTR, roundNumber(0.755102040816326, 9)],
    [CrashType.RT, roundNumber(0.755102040816326, 9)],
    [CrashType.SSD, roundNumber(0.755102040816326, 9)],
    [CrashType.SOD, roundNumber(0.755102040816326, 9)],
    [CrashType.T, roundNumber(0.755102040816326, 9)],
    [CrashType.TR, roundNumber(0.755102040816326, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [12, 9, 25],
  incorrectInputsDueToType: [
    ["string", 8, 25],
    [Number.NaN, 8, 25],
    [12, "string", 25],
    [12, Number.NaN, 25],
    [12, 8, "string"],
    [12, 8, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 3,
  correctTypeArray: ["number", "number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [12, 8, 0],
    [12, 8, 55],
  ],
  expectedValueErrors: [
    "Effective service life must be a number between 1 and 50",
    "Effective service life must be a number between 1 and 50",
  ],
});

// _____________________________________________________________________________________________

// 4.8.8.S6.1
// Macro Case 2AI

testCounterMeasure({
  code: "4.8.8.S6.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.63],
    [CrashType.AN, 0.63],
    [CrashType.FO, 0.63],
    [CrashType.FTF, 0.63],
    [CrashType.LT, 0.63],
    [CrashType.OtherNC, 0.63],
    [CrashType.OtherO, 0.63],
    [CrashType.OVT, 0.63],
    [CrashType.PD, 0.63],
    [CrashType.PDC, 0.63],
    [CrashType.PKV, 0.63],
    [CrashType.FTR, 0.63],
    [CrashType.RT, 0.63],
    [CrashType.SSD, 0.63],
    [CrashType.SOD, 0.63],
    [CrashType.T, 0.63],
    [CrashType.TR, 0.63],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [15, 10, 25],
  incorrectInputsDueToType: [
    ["string", 8, 25],
    [Number.NaN, 8, 25],
    [12, "string", 25],
    [12, Number.NaN, 25],
    [12, 8, "string"],
    [12, 8, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 3,
  correctTypeArray: ["number", "number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [12, 8, 0],
    [12, 8, 55],
  ],
  expectedValueErrors: [
    "Effective service life must be a number between 1 and 50",
    "Effective service life must be a number between 1 and 50",
  ],
});

// _____________________________________________________________________________________________

// 4.8.8.S12.1
// Macro Case 2AK

testCounterMeasure({
  code: "4.8.8.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.705510204081633, 9)],
    [CrashType.AN, roundNumber(0.705510204081633, 9)],
    [CrashType.FO, roundNumber(0.705510204081633, 9)],
    [CrashType.FTF, roundNumber(0.705510204081633, 9)],
    [CrashType.LT, roundNumber(0.705510204081633, 9)],
    [CrashType.OtherNC, roundNumber(0.705510204081633, 9)],
    [CrashType.OtherO, roundNumber(0.705510204081633, 9)],
    [CrashType.OVT, roundNumber(0.705510204081633, 9)],
    [CrashType.PD, roundNumber(0.705510204081633, 9)],
    [CrashType.PDC, roundNumber(0.705510204081633, 9)],
    [CrashType.PKV, roundNumber(0.705510204081633, 9)],
    [CrashType.FTR, roundNumber(0.705510204081633, 9)],
    [CrashType.RT, roundNumber(0.705510204081633, 9)],
    [CrashType.SSD, roundNumber(0.705510204081633, 9)],
    [CrashType.SOD, roundNumber(0.705510204081633, 9)],
    [CrashType.T, roundNumber(0.705510204081633, 9)],
    [CrashType.TR, roundNumber(0.705510204081633, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [490, 360, 25],
  incorrectInputsDueToType: [
    ["string", 8, 25],
    [Number.NaN, 8, 25],
    [12, "string", 25],
    [12, Number.NaN, 25],
    [12, 8, "string"],
    [12, 8, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 3,
  correctTypeArray: ["number", "number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [12, 8, 0],
    [12, 8, 55],
  ],
  expectedValueErrors: [
    "Effective service life must be a number between 1 and 50",
    "Effective service life must be a number between 1 and 50",
  ],
});

// _____________________________________________________________________________________________

// 4.8.9.S6.1
// Macro Case 2AN

testCounterMeasure({
  code: "4.8.9.S6.1",
  expectedCMF: new Map([
    [CrashType.AG, 3.68],
    [CrashType.AN, 3.68],
    [CrashType.FO, 3.68],
    [CrashType.FTF, 3.68],
    [CrashType.LT, 3.68],
    [CrashType.OtherNC, 3.68],
    [CrashType.OtherO, 3.68],
    [CrashType.OVT, 3.68],
    [CrashType.PD, 3.68],
    [CrashType.PDC, 3.68],
    [CrashType.PKV, 3.68],
    [CrashType.FTR, 3.68],
    [CrashType.RT, 3.68],
    [CrashType.SSD, 3.68],
    [CrashType.SOD, 3.68],
    [CrashType.T, 3.68],
    [CrashType.TR, 3.68],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [0.2, 1, 25],
  incorrectInputsDueToType: [
    ["string", 8, 25],
    [Number.NaN, 8, 25],
    [12, "string", 25],
    [12, Number.NaN, 25],
    [12, 8, "string"],
    [12, 8, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 3,
  correctTypeArray: ["number", "number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [12, 8, 0],
    [12, 8, 55],
  ],
  expectedValueErrors: [
    "Effective service life must be a number between 1 and 50",
    "Effective service life must be a number between 1 and 50",
  ],
});

// _____________________________________________________________________________________________

// 4.8.9.S12.1
// Macro Case 2AP

testCounterMeasure({
  code: "4.8.9.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.866],
    [CrashType.AN, 0.866],
    [CrashType.FO, 0.866],
    [CrashType.FTF, 0.866],
    [CrashType.LT, 0.866],
    [CrashType.OtherNC, 0.866],
    [CrashType.OtherO, 0.866],
    [CrashType.OVT, 0.866],
    [CrashType.PD, 0.866],
    [CrashType.PDC, 0.866],
    [CrashType.PKV, 0.866],
    [CrashType.FTR, 0.866],
    [CrashType.RT, 0.866],
    [CrashType.SSD, 0.866],
    [CrashType.SOD, 0.866],
    [CrashType.T, 0.866],
    [CrashType.TR, 0.866],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1.5, 1.2, 25],
  incorrectInputsDueToType: [
    ["string", 8, 25],
    [Number.NaN, 8, 25],
    [12, "string", 25],
    [12, Number.NaN, 25],
    [12, 8, "string"],
    [12, 8, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 3,
  correctTypeArray: ["number", "number", "number"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [12, 8, 0],
    [12, 8, 55],
  ],
  expectedValueErrors: [
    "Effective service life must be a number between 1 and 50",
    "Effective service life must be a number between 1 and 50",
  ],
});

// end of new

// _____________________________________________________________________________________________

// 4.8.23.S1.1
// Macro Case 4C

testCounterMeasure({
  code: "4.8.23.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.689905947],
    [CrashType.AN, 0.689905947],
    [CrashType.FO, 0.689905947],
    [CrashType.FTF, 0.689905947],
    [CrashType.LT, 0.689905947],
    [CrashType.OtherNC, 0.689905947],
    [CrashType.OtherO, 0.689905947],
    [CrashType.OVT, 0.689905947],
    [CrashType.PD, 0.689905947],
    [CrashType.PDC, 0.689905947],
    [CrashType.PKV, 0.689905947],
    [CrashType.FTR, 0.689905947],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.689905947],
    [CrashType.SSD, 0.689905947],
    [CrashType.SOD, 0.689905947],
    [CrashType.T, 0.689905947],
    [CrashType.TR, 0.689905947],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [18, 2],
  incorrectInputsDueToType: [
    ["string", 2],
    [Number.NaN, 2],
    [18, "string"],
    [18, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.8.24.S12.1
// Macro Case 4C

testCounterMeasure({
  code: "4.8.24.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.87005400025596, 9)],
    [CrashType.AN, 1],
    [CrashType.FO, roundNumber(0.857614998410769, 9)],
    [CrashType.FTF, roundNumber(0.857614998410769, 9)],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, roundNumber(0.953133787077505, 9)],
    [CrashType.RT, 1],
    [CrashType.SSD, roundNumber(0.857614998410769, 9)],
    [CrashType.SOD, 1],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, roundNumber(0.857614998410769, 9)],
    [CrashType.WP, 1],
  ]),
  correctInputs: [18, 2],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.8.25.S12.1
// Macro Case 4DA

testCounterMeasure({
  code: "4.8.25.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(2.4972746568941, 9)],
    [CrashType.AN, 1],
    [CrashType.FO, roundNumber(0.457673148134016, 9)],
    [CrashType.FTF, roundNumber(2.4972746568941, 9)],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, roundNumber(7.34779302920597, 9)],
    [CrashType.RT, 1],
    [CrashType.SSD, roundNumber(3.84663984621594, 9)],
    [CrashType.SOD, 1],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, roundNumber(0.457673148134016, 9)],
    [CrashType.WP, 1],
  ]),
  correctInputs: [18, 10],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.8.26.S12.1
// Macro Case 4E

testCounterMeasure({
  code: "4.8.26.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.817421831289724, 9)],
    [CrashType.AN, 1],
    [CrashType.FO, roundNumber(0.817421831289724, 9)],
    [CrashType.FTF, roundNumber(0.586959180942734, 9)],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, roundNumber(0.69211718168873, 9)],
    [CrashType.RT, 1],
    [CrashType.SSD, roundNumber(0.666043696655623, 9)],
    [CrashType.SOD, 1],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, roundNumber(0.817421831289724, 9)],
    [CrashType.WP, 1],
  ]),
  correctInputs: [18, 2],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});
// _____________________________________________________________________________________________

// 4.8.27.SU.1
// Macro Case 4FA

testCounterMeasure({
  code: "4.8.27.SU.1",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, roundNumber(0.951229424500714, 9)],
    [CrashType.FTF, 1],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, roundNumber(0.818730753077982, 9)],
    [CrashType.PDC, roundNumber(1.10517091807565, 9)],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 1],
    [CrashType.SOD, 1],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),

  correctInputs: [50, 100],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});
// _____________________________________________________________________________________________

// 4.8.28.SU.1
// Macro Case 4FB

testCounterMeasure({
  code: "4.8.28.SU.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.941764533584249, 9)],
    [CrashType.AN, 1],
    [CrashType.FO, roundNumber(0.913931185271228, 9)],
    [CrashType.FTF, roundNumber(0.941764533584249, 9)],
    [CrashType.LT, roundNumber(0.941764533584249, 9)],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, roundNumber(0.913931185271228, 9)],
    [CrashType.PDC, roundNumber(0.878095430920561, 9)],
    [CrashType.PKV, roundNumber(0.960789439152323, 9)],
    [CrashType.FTR, roundNumber(0.941764533584249, 9)],
    [CrashType.RT, roundNumber(0.941764533584249, 9)],
    [CrashType.SSD, roundNumber(0.941764533584249, 9)],
    [CrashType.SOD, 1],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.ROR, roundNumber(0.941764533584249, 9)],
    [CrashType.WP, 1],
  ]),
  correctInputs: [50, 40],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});
// _____________________________________________________________________________________________

// 4.8.30.S1.1
// Macro Case 4G

testCounterMeasure({
  code: "4.8.30.S1.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.614037056530872, 9)],
    [CrashType.AN, roundNumber(0.614037056530872, 9)],
    [CrashType.FO, roundNumber(0.614037056530872, 9)],
    [CrashType.FTF, roundNumber(0.614037056530872, 9)],
    [CrashType.LT, roundNumber(0.614037056530872, 9)],
    [CrashType.OtherNC, roundNumber(0.614037056530872, 9)],
    [CrashType.OtherO, roundNumber(0.614037056530872, 9)],
    [CrashType.OVT, roundNumber(0.614037056530872, 9)],
    [CrashType.PD, roundNumber(0.614037056530872, 9)],
    [CrashType.PDC, roundNumber(0.614037056530872, 9)],
    [CrashType.PKV, roundNumber(0.614037056530872, 9)],
    [CrashType.FTR, roundNumber(0.614037056530872, 9)],
    [CrashType.RT, roundNumber(0.614037056530872, 9)],
    [CrashType.SSD, roundNumber(0.614037056530872, 9)],
    [CrashType.SOD, roundNumber(0.614037056530872, 9)],
    [CrashType.T, roundNumber(0.614037056530872, 9)],
    [CrashType.TR, roundNumber(0.614037056530872, 9)],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [2000],
  incorrectInputsDueToType: [["string"], [Number.NaN]],
  incorrectInputsDueToLength: [3, 2],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 1,
  correctTypeArray: ["number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 4.8.31.S12.1
// This one is both calculated and adjusted!
// Flag 4I
// Macro Case 4DB

testCounterMeasure({
  code: "4.8.31.S12.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.06],
    [CrashType.AN, 0.06],
    [CrashType.FO, 0.06],
    [CrashType.FTF, 0.06],
    [CrashType.LT, 0.06],
    [CrashType.OtherNC, 0.06],
    [CrashType.OtherO, 0.06],
    [CrashType.OVT, 0.06],
    [CrashType.PD, 0.06],
    [CrashType.PDC, 0.06],
    [CrashType.PKV, 0.06],
    [CrashType.FTR, 0.06],
    [CrashType.RT, 0.06],
    [CrashType.SSD, 0.06],
    [CrashType.SOD, 0.06],
    [CrashType.T, 0.06],
    [CrashType.TR, 0.06],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [8, 5],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.1.1.I5.1
// Macro Case None

testCounterMeasure({
  code: "3.1.1.I5.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.75],
    [CrashType.AN, 0.75],
    [CrashType.FO, 0.75],
    [CrashType.FTF, 0.75],
    [CrashType.LT, 0.75],
    [CrashType.OtherNC, 0.75],
    [CrashType.OtherO, 0.75],
    [CrashType.OVT, 0.75],
    [CrashType.PD, 0.75],
    [CrashType.PDC, 0.75],
    [CrashType.PKV, 0.75],
    [CrashType.FTR, 0.75],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.75],
    [CrashType.SSD, 0.75],
    [CrashType.SOD, 0.75],
    [CrashType.T, 0.75],
    [CrashType.TR, 0.75],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [[5]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: true,
  aadtMinorRequired: true,
  aadtMajor: 3750,
  aadtMinor: 750,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.1.2.I5.1
// Macro Case Placeholder
testCounterMeasure({
  code: "3.1.2.I5.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.41],
    [CrashType.AN, 1.41],
    [CrashType.FO, 1.41],
    [CrashType.FTF, 1.41],
    [CrashType.LT, 1.41],
    [CrashType.OtherNC, 1.41],
    [CrashType.OtherO, 1.41],
    [CrashType.OVT, 1.41],
    [CrashType.PD, 1.41],
    [CrashType.PDC, 1.41],
    [CrashType.PKV, 1.41],
    [CrashType.FTR, 1.41],
    [CrashType.ROR, 1],
    [CrashType.RT, 1.41],
    [CrashType.SSD, 1.41],
    [CrashType.SOD, 1.41],
    [CrashType.T, 1.41],
    [CrashType.TR, 1.41],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});
// _____________________________________________________________________________________________

// 3.1.3.I5.1
// Macro Case None
testCounterMeasure({
  code: "3.1.3.I5.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.11],
    [CrashType.AN, 1.11],
    [CrashType.FO, 1.11],
    [CrashType.FTF, 1.11],
    [CrashType.LT, 1.11],
    [CrashType.OtherNC, 1.11],
    [CrashType.OtherO, 1.11],
    [CrashType.OVT, 1.11],
    [CrashType.PD, 1.11],
    [CrashType.PDC, 1.11],
    [CrashType.PKV, 1.11],
    [CrashType.FTR, 1.11],
    [CrashType.ROR, 1],
    [CrashType.RT, 1.11],
    [CrashType.SSD, 1.11],
    [CrashType.SOD, 1.11],
    [CrashType.T, 1.11],
    [CrashType.TR, 1.11],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.18.I1.1
// Macro Case 1A

testCounterMeasure({
  code: "3.2.18.I1.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.941764534],
    [CrashType.AN, 0.941764534],
    [CrashType.FO, 0.941764534],
    [CrashType.FTF, 0.941764534],
    [CrashType.LT, 0.941764534],
    [CrashType.OtherNC, 0.941764534],
    [CrashType.OtherO, 0.941764534],
    [CrashType.OVT, 0.941764534],
    [CrashType.PD, 0.941764534],
    [CrashType.PDC, 0.941764534],
    [CrashType.PKV, 0.941764534],
    [CrashType.FTR, 0.941764534],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.941764534],
    [CrashType.SSD, 0.941764534],
    [CrashType.SOD, 0.941764534],
    [CrashType.T, 0.941764534],
    [CrashType.TR, 0.941764534],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [15, 0],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.18.I1.2
// Macro Case 1B

testCounterMeasure({
  code: "3.2.18.I1.2",
  expectedCMF: new Map([
    [CrashType.AG, 0.775846956],
    [CrashType.AN, 0.775846956],
    [CrashType.FO, 0.775846956],
    [CrashType.FTF, 0.775846956],
    [CrashType.LT, 0.775846956],
    [CrashType.OtherNC, 0.775846956],
    [CrashType.OtherO, 0.775846956],
    [CrashType.OVT, 0.775846956],
    [CrashType.PD, 0.775846956],
    [CrashType.PDC, 0.775846956],
    [CrashType.PKV, 0.775846956],
    [CrashType.FTR, 0.775846956],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.775846956],
    [CrashType.SSD, 0.775846956],
    [CrashType.SOD, 0.775846956],
    [CrashType.T, 0.775846956],
    [CrashType.TR, 0.775846956],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [62, 15],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.18.I1.3
// Macro Case 1C

testCounterMeasure({
  code: "3.2.18.I1.3",
  expectedCMF: new Map([
    [CrashType.AG, 1.029069952],
    [CrashType.AN, 1.029069952],
    [CrashType.FO, 1.029069952],
    [CrashType.FTF, 1.029069952],
    [CrashType.LT, 1.029069952],
    [CrashType.OtherNC, 1.029069952],
    [CrashType.OtherO, 1.029069952],
    [CrashType.OVT, 1.029069952],
    [CrashType.PD, 1.029069952],
    [CrashType.PDC, 1.029069952],
    [CrashType.PKV, 1.029069952],
    [CrashType.FTR, 1.029069952],
    [CrashType.ROR, 1],
    [CrashType.RT, 1.029069952],
    [CrashType.SSD, 1.029069952],
    [CrashType.SOD, 1.029069952],
    [CrashType.T, 1.029069952],
    [CrashType.TR, 1.029069952],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [10, 80],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.18.I1.4
// Macro Case 1D

testCounterMeasure({
  code: "3.2.18.I1.4",
  expectedCMF: new Map([
    [CrashType.AG, 0.990343692],
    [CrashType.AN, 0.990343692],
    [CrashType.FO, 0.990343692],
    [CrashType.FTF, 0.990343692],
    [CrashType.LT, 0.990343692],
    [CrashType.OtherNC, 0.990343692],
    [CrashType.OtherO, 0.990343692],
    [CrashType.OVT, 0.990343692],
    [CrashType.PD, 0.990343692],
    [CrashType.PDC, 0.990343692],
    [CrashType.PKV, 0.990343692],
    [CrashType.FTR, 0.990343692],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.990343692],
    [CrashType.SSD, 0.990343692],
    [CrashType.SOD, 0.990343692],
    [CrashType.T, 0.990343692],
    [CrashType.TR, 0.990343692],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [38, 13],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.22.IR.1
// Macro Case 2A

testCounterMeasure({
  code: "3.2.22.IR.1",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 0.952380952],
    [CrashType.FTF, 0.952380952],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 0.952380952],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.ROR, 0.952380952],
    [CrashType.RT, 1],
    [CrashType.SSD, 0.952380952],
    [CrashType.SOD, 0.952380952],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [11, 12],
  incorrectInputsDueToType: [["string", 6]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3750,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.23.IU.1
// Macro Case None
testCounterMeasure({
  code: "3.2.23.IU.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.937067463],
    [CrashType.AN, 0.937067463],
    [CrashType.FO, 0.937067463],
    [CrashType.FTF, 0.937067463],
    [CrashType.LT, 0.937067463],
    [CrashType.OtherNC, 0.937067463],
    [CrashType.OtherO, 0.937067463],
    [CrashType.OVT, 0.937067463],
    [CrashType.PD, 0.937067463],
    [CrashType.PDC, 0.937067463],
    [CrashType.PKV, 0.937067463],
    [CrashType.FTR, 0.937067463],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.937067463],
    [CrashType.SSD, 0.937067463],
    [CrashType.SOD, 0.937067463],
    [CrashType.T, 0.937067463],
    [CrashType.TR, 0.937067463],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3750,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.23.IU.2
// Macro Case None
testCounterMeasure({
  code: "3.2.23.IU.2",
  expectedCMF: new Map([
    [CrashType.AG, 0.41],
    [CrashType.AN, 0.41],
    [CrashType.FO, 0.41],
    [CrashType.FTF, 0.41],
    [CrashType.LT, 0.41],
    [CrashType.OtherNC, 0.41],
    [CrashType.OtherO, 0.41],
    [CrashType.OVT, 0.41],
    [CrashType.PD, 0.41],
    [CrashType.PDC, 0.41],
    [CrashType.PKV, 0.41],
    [CrashType.FTR, 0.41],
    [CrashType.ROR, 1],
    [CrashType.RT, 0.41],
    [CrashType.SSD, 0.41],
    [CrashType.SOD, 0.41],
    [CrashType.T, 0.41],
    [CrashType.TR, 0.41],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.23.IU.3
// Macro Case None

testCounterMeasure({
  code: "3.2.23.IU.3",
  expectedCMF: new Map([
    [CrashType.AG, 1.31],
    [CrashType.AN, 1.31],
    [CrashType.FO, 1.31],
    [CrashType.FTF, 1.31],
    [CrashType.LT, 1.31],
    [CrashType.OtherNC, 1.31],
    [CrashType.OtherO, 1.31],
    [CrashType.OVT, 1.31],
    [CrashType.PD, 1.31],
    [CrashType.PDC, 1.31],
    [CrashType.PKV, 1.31],
    [CrashType.FTR, 1.31],
    [CrashType.ROR, 1],
    [CrashType.RT, 1.31],
    [CrashType.SSD, 1.31],
    [CrashType.SOD, 1.31],
    [CrashType.T, 1.31],
    [CrashType.TR, 1.31],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.23.IU.4
// Macro Case None

testCounterMeasure({
  code: "3.2.23.IU.4",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.916218871650878, 9)],
    [CrashType.AN, roundNumber(0.916218871650878, 9)],
    [CrashType.FO, roundNumber(0.916218871650878, 9)],
    [CrashType.FTF, roundNumber(0.916218871650878, 9)],
    [CrashType.LT, roundNumber(0.916218871650878, 9)],
    [CrashType.OtherNC, roundNumber(0.916218871650878, 9)],
    [CrashType.OtherO, roundNumber(0.916218871650878, 9)],
    [CrashType.OVT, roundNumber(0.916218871650878, 9)],
    [CrashType.PD, roundNumber(0.916218871650878, 9)],
    [CrashType.PDC, roundNumber(0.916218871650878, 9)],
    [CrashType.PKV, roundNumber(0.916218871650878, 9)],
    [CrashType.FTR, roundNumber(0.916218871650878, 9)],
    [CrashType.ROR, 1],
    [CrashType.RT, roundNumber(0.916218871650878, 9)],
    [CrashType.SSD, roundNumber(0.916218871650878, 9)],
    [CrashType.SOD, roundNumber(0.916218871650878, 9)],
    [CrashType.T, roundNumber(0.916218871650878, 9)],
    [CrashType.TR, roundNumber(0.916218871650878, 9)],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3750,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.23.IU.5
// Macro Case None

testCounterMeasure({
  code: "3.2.23.IU.5",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(1.06715902438419, 9)],
    [CrashType.AN, roundNumber(1.06715902438419, 9)],
    [CrashType.FO, roundNumber(1.06715902438419, 9)],
    [CrashType.FTF, roundNumber(1.06715902438419, 9)],
    [CrashType.LT, roundNumber(1.06715902438419, 9)],
    [CrashType.OtherNC, roundNumber(1.06715902438419, 9)],
    [CrashType.OtherO, roundNumber(1.06715902438419, 9)],
    [CrashType.OVT, roundNumber(1.06715902438419, 9)],
    [CrashType.PD, roundNumber(1.06715902438419, 9)],
    [CrashType.PDC, roundNumber(1.06715902438419, 9)],
    [CrashType.PKV, roundNumber(1.06715902438419, 9)],
    [CrashType.FTR, roundNumber(1.06715902438419, 9)],
    [CrashType.ROR, 1],
    [CrashType.RT, roundNumber(1.06715902438419, 9)],
    [CrashType.SSD, roundNumber(1.06715902438419, 9)],
    [CrashType.SOD, roundNumber(1.06715902438419, 9)],
    [CrashType.T, roundNumber(1.06715902438419, 9)],
    [CrashType.TR, roundNumber(1.06715902438419, 9)],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3750,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.23.IU.6
// Macro Case None

testCounterMeasure({
  code: "3.2.23.IU.6",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(2.4390243902439, 9)],
    [CrashType.AN, roundNumber(2.4390243902439, 9)],
    [CrashType.FO, roundNumber(2.4390243902439, 9)],
    [CrashType.FTF, roundNumber(2.4390243902439, 9)],
    [CrashType.LT, roundNumber(2.4390243902439, 9)],
    [CrashType.OtherNC, roundNumber(2.4390243902439, 9)],
    [CrashType.OtherO, roundNumber(2.4390243902439, 9)],
    [CrashType.OVT, roundNumber(2.4390243902439, 9)],
    [CrashType.PD, roundNumber(2.4390243902439, 9)],
    [CrashType.PDC, roundNumber(2.4390243902439, 9)],
    [CrashType.PKV, roundNumber(2.4390243902439, 9)],
    [CrashType.FTR, roundNumber(2.4390243902439, 9)],
    [CrashType.ROR, 1],
    [CrashType.RT, roundNumber(2.4390243902439, 9)],
    [CrashType.SSD, roundNumber(2.4390243902439, 9)],
    [CrashType.SOD, roundNumber(2.4390243902439, 9)],
    [CrashType.T, roundNumber(2.4390243902439, 9)],
    [CrashType.TR, roundNumber(2.4390243902439, 9)],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.23.IU.7
// Macro Case None

testCounterMeasure({
  code: "3.2.23.IU.7",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.757575757575757, 9)],
    [CrashType.AN, roundNumber(0.757575757575757, 9)],
    [CrashType.FO, roundNumber(0.757575757575757, 9)],
    [CrashType.FTF, roundNumber(0.757575757575757, 9)],
    [CrashType.LT, roundNumber(0.757575757575757, 9)],
    [CrashType.OtherNC, roundNumber(0.757575757575757, 9)],
    [CrashType.OtherO, roundNumber(0.757575757575757, 9)],
    [CrashType.OVT, roundNumber(0.757575757575757, 9)],
    [CrashType.PD, roundNumber(0.757575757575757, 9)],
    [CrashType.PDC, roundNumber(0.757575757575757, 9)],
    [CrashType.PKV, roundNumber(0.757575757575757, 9)],
    [CrashType.FTR, roundNumber(0.757575757575757, 9)],
    [CrashType.ROR, 1],
    [CrashType.RT, roundNumber(0.757575757575757, 9)],
    [CrashType.SSD, roundNumber(0.757575757575757, 9)],
    [CrashType.SOD, roundNumber(0.757575757575757, 9)],
    [CrashType.T, roundNumber(0.757575757575757, 9)],
    [CrashType.TR, roundNumber(0.757575757575757, 9)],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.23.IU.8
// Macro Case None

testCounterMeasure({
  code: "3.2.23.IU.8",
  expectedCMF: new Map([
    [CrashType.AG, 1.091442264],
    [CrashType.AN, 1.091442264],
    [CrashType.FO, 1.091442264],
    [CrashType.FTF, 1.091442264],
    [CrashType.LT, 1.091442264],
    [CrashType.OtherNC, 1.091442264],
    [CrashType.OtherO, 1.091442264],
    [CrashType.OVT, 1.091442264],
    [CrashType.PD, 1.091442264],
    [CrashType.PDC, 1.091442264],
    [CrashType.PKV, 1.091442264],
    [CrashType.FTR, 1.091442264],
    [CrashType.ROR, 1],
    [CrashType.RT, 1.091442264],
    [CrashType.SSD, 1.091442264],
    [CrashType.SOD, 1.091442264],
    [CrashType.T, 1.091442264],
    [CrashType.TR, 1.091442264],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 3750,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

// _____________________________________________________________________________________________

// 3.2.26.I3.1
// Macro Case None
// Adjusted CMF Tag: 3A

testCounterMeasure({
  code: "3.2.26.I3.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.663381409196163, 9)],
    [CrashType.AN, roundNumber(0.663381409196163, 9)],
    [CrashType.FO, roundNumber(0.663381409196163, 9)],
    [CrashType.FTF, 0.662],
    [CrashType.LT, 0.62],
    [CrashType.OtherNC, roundNumber(0.663381409196163, 9)],
    [CrashType.OtherO, 0.662],
    [CrashType.OVT, roundNumber(0.663381409196163, 9)],
    [CrashType.PD, 0.662],
    [CrashType.PDC, 0.662],
    [CrashType.PKV, 0.662],
    [CrashType.FTR, 0.68],
    [CrashType.RT, roundNumber(0.663381409196163, 9)],
    [CrashType.SSD, 0.662],
    [CrashType.SOD, 0.662],
    [CrashType.T, roundNumber(0.637352563678465, 9)],
    [CrashType.TR, 0.662],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.2.26.I7.1
// Macro Case None
// Adjusted CMF Tag: 3B

testCounterMeasure({
  code: "3.2.26.I7.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.667265224625624, 9)],
    [CrashType.AN, roundNumber(0.667265224625624, 9)],
    [CrashType.FO, roundNumber(0.667265224625624, 9)],
    [CrashType.FTF, 0.662],
    [CrashType.LT, 0.62],
    [CrashType.OtherNC, roundNumber(0.667265224625624, 9)],
    [CrashType.OtherO, 0.662],
    [CrashType.OVT, roundNumber(0.667265224625624, 9)],
    [CrashType.PD, 0.662],
    [CrashType.PDC, 0.662],
    [CrashType.PKV, 0.662],
    [CrashType.FTR, 0.61],
    [CrashType.RT, roundNumber(0.667265224625624, 9)],
    [CrashType.SSD, 0.61],
    [CrashType.SOD, 0.662],
    [CrashType.T, roundNumber(0.63890608985025, 9)],
    [CrashType.TR, 0.662],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.2.27.I1.1
// Macro Case None
// Adjusted CMF Tag: 3AU

testCounterMeasure({
  code: "3.2.27.I1.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.876473186119874, 9)],
    [CrashType.AN, roundNumber(0.876473186119874, 9)],
    [CrashType.FO, roundNumber(0.876473186119874, 9)],
    [CrashType.FTF, 0.87],
    [CrashType.LT, roundNumber(0.876473186119874, 9)],
    [CrashType.OtherNC, roundNumber(0.876473186119874, 9)],
    [CrashType.OtherO, 0.87],
    [CrashType.OVT, roundNumber(0.876473186119874, 9)],
    [CrashType.PD, 0.87],
    [CrashType.PDC, 0.87],
    [CrashType.PKV, 0.87],
    [CrashType.FTR, 0.75],
    [CrashType.RT, roundNumber(0.876473186119874, 9)],
    [CrashType.SSD, 0.75],
    [CrashType.SOD, 0.87],
    [CrashType.T, roundNumber(0.876473186119874, 9)],
    [CrashType.TR, 0.87],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.2.28.I1.1
// Macro Case None
// Adjusted CMF Tag: 3C

testCounterMeasure({
  code: "3.2.28.I1.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.673236593059937, 9)],
    [CrashType.AN, roundNumber(0.673236593059937, 9)],
    [CrashType.FO, roundNumber(0.673236593059937, 9)],
    [CrashType.FTF, 0.67],
    [CrashType.LT, roundNumber(0.673236593059937, 9)],
    [CrashType.OtherNC, roundNumber(0.673236593059937, 9)],
    [CrashType.OtherO, 0.67],
    [CrashType.OVT, roundNumber(0.673236593059937, 9)],
    [CrashType.PD, 0.67],
    [CrashType.PDC, 0.67],
    [CrashType.PKV, 0.67],
    [CrashType.FTR, 0.61],
    [CrashType.RT, roundNumber(0.673236593059937, 9)],
    [CrashType.SSD, 0.61],
    [CrashType.SOD, 0.67],
    [CrashType.T, roundNumber(0.673236593059937, 9)],
    [CrashType.TR, 0.67],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.2.32.IU.1
// Macro Case None
// Adjusted CMF Tag: 3G

testCounterMeasure({
  code: "3.2.32.IU.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(1.06761802575107, 9)],
    [CrashType.AN, roundNumber(1.06761802575107, 9)],
    [CrashType.FO, roundNumber(1.06761802575107, 9)],
    [CrashType.FTF, 1.07],
    [CrashType.LT, roundNumber(1.06761802575107, 9)],
    [CrashType.OtherNC, roundNumber(1.06761802575107, 9)],
    [CrashType.OtherO, 1.07],
    [CrashType.OVT, roundNumber(1.06761802575107, 9)],
    [CrashType.PD, 1.07],
    [CrashType.PDC, 1.28],
    [CrashType.PKV, 1.07],
    [CrashType.FTR, roundNumber(1.06761802575107, 9)],
    [CrashType.RT, roundNumber(1.06761802575107, 9)],
    [CrashType.SSD, 1.07],
    [CrashType.SOD, 1.07],
    [CrashType.T, roundNumber(1.06761802575107, 9)],
    [CrashType.TR, 1.07],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.15.I5.1
// Macro Case None
// Adjusted CMF Tag: 3H

testCounterMeasure({
  code: "3.3.15.I5.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.317921914357683, 9)],
    [CrashType.AN, roundNumber(0.317921914357683, 9)],
    [CrashType.FO, roundNumber(0.317921914357683, 9)],
    [CrashType.FTF, 0.32],
    [CrashType.LT, 0.36],
    [CrashType.OtherNC, roundNumber(0.317921914357683, 9)],
    [CrashType.OtherO, 0.32],
    [CrashType.OVT, roundNumber(0.317921914357683, 9)],
    [CrashType.PD, 0.32],
    [CrashType.PDC, 0.32],
    [CrashType.PKV, 0.32],
    [CrashType.FTR, roundNumber(0.317921914357683, 9)],
    [CrashType.RT, roundNumber(0.317921914357683, 9)],
    [CrashType.SSD, 0.32],
    [CrashType.SOD, 0.32],
    [CrashType.T, roundNumber(0.343168765743073, 9)],
    [CrashType.TR, 0.32],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.16.I5.1
// Macro Case None
// Adjusted CMF Tag: 3I

testCounterMeasure({
  code: "3.3.16.I5.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.282597607052897, 9)],
    [CrashType.AN, roundNumber(0.282597607052897, 9)],
    [CrashType.FO, roundNumber(0.282597607052897, 9)],
    [CrashType.FTF, 0.28],
    [CrashType.LT, 0.23],
    [CrashType.OtherNC, roundNumber(0.282597607052897, 9)],
    [CrashType.OtherO, 0.28],
    [CrashType.OVT, roundNumber(0.282597607052897, 9)],
    [CrashType.PD, 0.28],
    [CrashType.PDC, 0.28],
    [CrashType.PKV, 0.28],
    [CrashType.FTR, roundNumber(0.282597607052897, 9)],
    [CrashType.RT, roundNumber(0.282597607052897, 9)],
    [CrashType.SSD, 0.28],
    [CrashType.SOD, 0.28],
    [CrashType.T, roundNumber(0.251039042821159, 9)],
    [CrashType.TR, 0.28],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.17.I1.1
// Macro Case None
// Adjusted CMF Tag: 3J

testCounterMeasure({
  code: "3.3.17.I1.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.87],
    [CrashType.AN, roundNumber(0.978905201342282, 9)],
    [CrashType.FO, roundNumber(0.978905201342282, 9)],
    [CrashType.FTF, 0.95],
    [CrashType.LT, roundNumber(0.978905201342282, 9)],
    [CrashType.OtherNC, roundNumber(0.978905201342282, 9)],
    [CrashType.OtherO, 0.95],
    [CrashType.OVT, roundNumber(0.978905201342282, 9)],
    [CrashType.PD, 0.95],
    [CrashType.PDC, 0.95],
    [CrashType.PKV, 0.95],
    [CrashType.FTR, 0.92],
    [CrashType.RT, roundNumber(0.978905201342282, 9)],
    [CrashType.SSD, 0.95],
    [CrashType.SOD, 0.95],
    [CrashType.T, roundNumber(0.978905201342282, 9)],
    [CrashType.TR, 0.95],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.17.I2.1
// Macro Case None
// Adjusted CMF Tag: 3K

testCounterMeasure({
  code: "3.3.17.I2.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.84],
    [CrashType.AN, roundNumber(0.989022651006712, 9)],
    [CrashType.FO, roundNumber(0.989022651006712, 9)],
    [CrashType.FTF, 0.95],
    [CrashType.LT, roundNumber(0.989022651006712, 9)],
    [CrashType.OtherNC, roundNumber(0.989022651006712, 9)],
    [CrashType.OtherO, 0.95],
    [CrashType.OVT, roundNumber(0.989022651006712, 9)],
    [CrashType.PD, 0.95],
    [CrashType.PDC, 0.95],
    [CrashType.PKV, 0.95],
    [CrashType.FTR, 0.92],
    [CrashType.RT, roundNumber(0.989022651006712, 9)],
    [CrashType.SSD, 0.95],
    [CrashType.SOD, 0.95],
    [CrashType.T, roundNumber(0.989022651006712, 9)],
    [CrashType.TR, 0.95],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.17.I5.1
// Macro Case None
// Adjusted CMF Tag: 3L

testCounterMeasure({
  code: "3.3.17.I5.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.87],
    [CrashType.AN, roundNumber(0.978905201342282, 9)],
    [CrashType.FO, roundNumber(0.978905201342282, 9)],
    [CrashType.FTF, 0.95],
    [CrashType.LT, roundNumber(0.978905201342282, 9)],
    [CrashType.OtherNC, roundNumber(0.978905201342282, 9)],
    [CrashType.OtherO, 0.95],
    [CrashType.OVT, roundNumber(0.978905201342282, 9)],
    [CrashType.PD, 0.95],
    [CrashType.PDC, 0.95],
    [CrashType.PKV, 0.95],
    [CrashType.FTR, 0.92],
    [CrashType.RT, roundNumber(0.978905201342282, 9)],
    [CrashType.SSD, 0.95],
    [CrashType.SOD, 0.95],
    [CrashType.T, roundNumber(0.978905201342282, 9)],
    [CrashType.TR, 0.95],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.17.I6.1
// Macro Case None
// Adjusted CMF Tag: 3M

testCounterMeasure({
  code: "3.3.17.I6.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.72],
    [CrashType.AN, roundNumber(1.02949244966443, 9)],
    [CrashType.FO, roundNumber(1.02949244966443, 9)],
    [CrashType.FTF, 0.95],
    [CrashType.LT, roundNumber(1.02949244966443, 9)],
    [CrashType.OtherNC, roundNumber(1.02949244966443, 9)],
    [CrashType.OtherO, 0.95],
    [CrashType.OVT, roundNumber(1.02949244966443, 9)],
    [CrashType.PD, 0.95],
    [CrashType.PDC, 0.95],
    [CrashType.PKV, 0.95],
    [CrashType.FTR, 0.92],
    [CrashType.RT, roundNumber(1.02949244966443, 9)],
    [CrashType.SSD, 0.95],
    [CrashType.SOD, 0.95],
    [CrashType.T, roundNumber(1.02949244966443, 9)],
    [CrashType.TR, 0.95],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.19.I1.1
// Macro Case None
// Adjusted CMF Tag: 3N

testCounterMeasure({
  code: "3.3.19.I1.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.01],
    [CrashType.AN, roundNumber(1.02316909735909, 9)],
    [CrashType.FO, roundNumber(1.02316909735909, 9)],
    [CrashType.FTF, 1.02],
    [CrashType.LT, roundNumber(1.02316909735909, 9)],
    [CrashType.OtherNC, roundNumber(1.02316909735909, 9)],
    [CrashType.OtherO, 1.02],
    [CrashType.OVT, roundNumber(1.02316909735909, 9)],
    [CrashType.PD, 1.02],
    [CrashType.PDC, 1.02],
    [CrashType.PKV, 1.02],
    [CrashType.FTR, roundNumber(1.02316909735909, 9)],
    [CrashType.RT, roundNumber(1.02316909735909, 9)],
    [CrashType.SSD, 1.02],
    [CrashType.SOD, 1.02],
    [CrashType.T, roundNumber(1.02316909735909, 9)],
    [CrashType.TR, 1.02],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["test", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.19.I2.1
// Macro Case None
// Adjusted CMF Tag: 3O

testCounterMeasure({
  code: "3.3.19.I2.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.01],
    [CrashType.AN, roundNumber(1.02316909735909, 9)],
    [CrashType.FO, roundNumber(1.02316909735909, 9)],
    [CrashType.FTF, 1.02],
    [CrashType.LT, roundNumber(1.02316909735909, 9)],
    [CrashType.OtherNC, roundNumber(1.02316909735909, 9)],
    [CrashType.OtherO, 1.02],
    [CrashType.OVT, roundNumber(1.02316909735909, 9)],
    [CrashType.PD, 1.02],
    [CrashType.PDC, 1.02],
    [CrashType.PKV, 1.02],
    [CrashType.FTR, roundNumber(1.02316909735909, 9)],
    [CrashType.RT, roundNumber(1.02316909735909, 9)],
    [CrashType.SSD, 1.02],
    [CrashType.SOD, 1.02],
    [CrashType.T, roundNumber(1.02316909735909, 9)],
    [CrashType.TR, 1.02],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [
    ["test", "local"],
    [5, 2],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.19.I5.1
// Macro Case None
// Adjusted CMF Tag: 3P

testCounterMeasure({
  code: "3.3.19.I5.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.01],
    [CrashType.AN, roundNumber(1.0258029168309, 9)],
    [CrashType.FO, roundNumber(1.0258029168309, 9)],
    [CrashType.FTF, 1.022],
    [CrashType.LT, roundNumber(1.0258029168309, 9)],
    [CrashType.OtherNC, roundNumber(1.0258029168309, 9)],
    [CrashType.OtherO, 1.022],
    [CrashType.OVT, roundNumber(1.0258029168309, 9)],
    [CrashType.PD, 1.022],
    [CrashType.PDC, 1.022],
    [CrashType.PKV, 1.022],
    [CrashType.FTR, roundNumber(1.0258029168309, 9)],
    [CrashType.RT, roundNumber(1.0258029168309, 9)],
    [CrashType.SSD, 1.022],
    [CrashType.SOD, 1.022],
    [CrashType.T, roundNumber(1.0258029168309, 9)],
    [CrashType.TR, 1.022],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.19.I6.1
// Macro Case None
// Adjusted CMF Tag: 3Q

testCounterMeasure({
  code: "3.3.19.I6.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.01],
    [CrashType.AN, roundNumber(1.0258029168309, 9)],
    [CrashType.FO, roundNumber(1.0258029168309, 9)],
    [CrashType.FTF, 1.022],
    [CrashType.LT, roundNumber(1.0258029168309, 9)],
    [CrashType.OtherNC, roundNumber(1.0258029168309, 9)],
    [CrashType.OtherO, 1.022],
    [CrashType.OVT, roundNumber(1.0258029168309, 9)],
    [CrashType.PD, 1.022],
    [CrashType.PDC, 1.022],
    [CrashType.PKV, 1.022],
    [CrashType.FTR, roundNumber(1.0258029168309, 9)],
    [CrashType.RT, roundNumber(1.0258029168309, 9)],
    [CrashType.SSD, 1.022],
    [CrashType.SOD, 1.022],
    [CrashType.T, roundNumber(1.0258029168309, 9)],
    [CrashType.TR, 1.022],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I1.1
// Macro Case None
// Adjusted CMF Tag: 3R

testCounterMeasure({
  code: "3.3.20.I1.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.07],
    [CrashType.AN, roundNumber(1.0549164785553, 9)],
    [CrashType.FO, roundNumber(1.0549164785553, 9)],
    [CrashType.FTF, 1.07],
    [CrashType.LT, 1.07],
    [CrashType.OtherNC, roundNumber(1.0549164785553, 9)],
    [CrashType.OtherO, 1.06],
    [CrashType.OVT, roundNumber(1.0549164785553, 9)],
    [CrashType.PD, 1.06],
    [CrashType.PDC, 1.06],
    [CrashType.PKV, 1.06],
    [CrashType.FTR, roundNumber(1.0549164785553, 9)],
    [CrashType.RT, 1.07],
    [CrashType.SSD, 1.06],
    [CrashType.SOD, 1.06],
    [CrashType.T, 1.07],
    [CrashType.TR, 1.06],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I1.2
// Macro Case None
// Adjusted CMF Tag: 3S

testCounterMeasure({
  code: "3.3.20.I1.2",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, roundNumber(0.924582392776524, 9)],
    [CrashType.FO, roundNumber(0.924582392776524, 9)],
    [CrashType.FTF, 1],
    [CrashType.LT, 1],
    [CrashType.OtherNC, roundNumber(0.924582392776524, 9)],
    [CrashType.OtherO, 0.95],
    [CrashType.OVT, roundNumber(0.924582392776524, 9)],
    [CrashType.PD, 0.95],
    [CrashType.PDC, 0.95],
    [CrashType.PKV, 0.95],
    [CrashType.FTR, roundNumber(0.924582392776524, 9)],
    [CrashType.RT, 1],
    [CrashType.SSD, 0.95],
    [CrashType.SOD, 0.95],
    [CrashType.T, 1],
    [CrashType.TR, 0.95],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I1.3
// Macro Case None
// Adjusted CMF Tag: 3T

testCounterMeasure({
  code: "3.3.20.I1.3",
  expectedCMF: new Map([
    [CrashType.AG, 0.68],
    [CrashType.AN, 0.68],
    [CrashType.FO, 0.68],
    [CrashType.FTF, 0.68],
    [CrashType.LT, 0.68],
    [CrashType.OtherNC, 0.68],
    [CrashType.OtherO, 0.68],
    [CrashType.OVT, 0.68],
    [CrashType.PD, 0.68],
    [CrashType.PDC, 0.68],
    [CrashType.PKV, 0.68],
    [CrashType.FTR, 0.68],
    [CrashType.RT, 0.68],
    [CrashType.SSD, 0.68],
    [CrashType.SOD, 0.68],
    [CrashType.T, 0.68],
    [CrashType.TR, 0.68],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I1.4
// Macro Case None
// Adjusted CMF Tag: 3U

testCounterMeasure({
  code: "3.3.20.I1.4",
  expectedCMF: new Map([
    [CrashType.AG, 0.8],
    [CrashType.AN, roundNumber(0.724582392776524, 9)],
    [CrashType.FO, roundNumber(0.724582392776524, 9)],
    [CrashType.FTF, 0.8],
    [CrashType.LT, 0.8],
    [CrashType.OtherNC, roundNumber(0.724582392776524, 9)],
    [CrashType.OtherO, 0.75],
    [CrashType.OVT, roundNumber(0.724582392776524, 9)],
    [CrashType.PD, 0.75],
    [CrashType.PDC, 0.75],
    [CrashType.PKV, 0.75],
    [CrashType.FTR, roundNumber(0.724582392776524, 9)],
    [CrashType.RT, 0.8],
    [CrashType.SSD, 0.75],
    [CrashType.SOD, 0.75],
    [CrashType.T, 0.8],
    [CrashType.TR, 0.75],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I2.1
// Macro Case None
// Adjusted CMF Tag: 3V

testCounterMeasure({
  code: "3.3.20.I2.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.07],
    [CrashType.AN, roundNumber(1.0549164785553, 9)],
    [CrashType.FO, roundNumber(1.0549164785553, 9)],
    [CrashType.FTF, 1.07],
    [CrashType.LT, 1.07],
    [CrashType.OtherNC, roundNumber(1.0549164785553, 9)],
    [CrashType.OtherO, 1.06],
    [CrashType.OVT, roundNumber(1.0549164785553, 9)],
    [CrashType.PD, 1.06],
    [CrashType.PDC, 1.06],
    [CrashType.PKV, 1.06],
    [CrashType.FTR, roundNumber(1.0549164785553, 9)],
    [CrashType.RT, 1.07],
    [CrashType.SSD, 1.06],
    [CrashType.SOD, 1.06],
    [CrashType.T, 1.07],
    [CrashType.TR, 1.06],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I2.2
// Macro Case None
// Adjusted CMF Tag: 3W

testCounterMeasure({
  code: "3.3.20.I2.2",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, roundNumber(0.924582392776524, 9)],
    [CrashType.FO, roundNumber(0.924582392776524, 9)],
    [CrashType.FTF, 1],
    [CrashType.LT, 1],
    [CrashType.OtherNC, roundNumber(0.924582392776524, 9)],
    [CrashType.OtherO, 0.95],
    [CrashType.OVT, roundNumber(0.924582392776524, 9)],
    [CrashType.PD, 0.95],
    [CrashType.PDC, 0.95],
    [CrashType.PKV, 0.95],
    [CrashType.FTR, roundNumber(0.924582392776524, 9)],
    [CrashType.RT, 1],
    [CrashType.SSD, 0.95],
    [CrashType.SOD, 0.95],
    [CrashType.T, 1],
    [CrashType.TR, 0.95],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I2.3
// Macro Case None
// Adjusted CMF Tag: 3X

testCounterMeasure({
  code: "3.3.20.I2.3",
  expectedCMF: new Map([
    [CrashType.AG, 0.68],
    [CrashType.AN, 0.68],
    [CrashType.FO, 0.68],
    [CrashType.FTF, 0.68],
    [CrashType.LT, 0.68],
    [CrashType.OtherNC, 0.68],
    [CrashType.OtherO, 0.68],
    [CrashType.OVT, 0.68],
    [CrashType.PD, 0.68],
    [CrashType.PDC, 0.68],
    [CrashType.PKV, 0.68],
    [CrashType.FTR, 0.68],
    [CrashType.RT, 0.68],
    [CrashType.SSD, 0.68],
    [CrashType.SOD, 0.68],
    [CrashType.T, 0.68],
    [CrashType.TR, 0.68],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I2.4
// Macro Case None
// Adjusted CMF Tag: 3Y

testCounterMeasure({
  code: "3.3.20.I2.4",
  expectedCMF: new Map([
    [CrashType.AG, 0.8],
    [CrashType.AN, roundNumber(0.724582392776524, 9)],
    [CrashType.FO, roundNumber(0.724582392776524, 9)],
    [CrashType.FTF, 0.8],
    [CrashType.LT, 0.8],
    [CrashType.OtherNC, roundNumber(0.724582392776524, 9)],
    [CrashType.OtherO, 0.75],
    [CrashType.OVT, roundNumber(0.724582392776524, 9)],
    [CrashType.PD, 0.75],
    [CrashType.PDC, 0.75],
    [CrashType.PKV, 0.75],
    [CrashType.FTR, roundNumber(0.724582392776524, 9)],
    [CrashType.RT, 0.8],
    [CrashType.SSD, 0.75],
    [CrashType.SOD, 0.75],
    [CrashType.T, 0.8],
    [CrashType.TR, 0.75],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I5.1
// Macro Case None
// Adjusted CMF Tag: 3Z

testCounterMeasure({
  code: "3.3.20.I5.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.07],
    [CrashType.AN, roundNumber(1.0549164785553, 9)],
    [CrashType.FO, roundNumber(1.0549164785553, 9)],
    [CrashType.FTF, 1.07],
    [CrashType.LT, 1.07],
    [CrashType.OtherNC, roundNumber(1.0549164785553, 9)],
    [CrashType.OtherO, 1.06],
    [CrashType.OVT, roundNumber(1.0549164785553, 9)],
    [CrashType.PD, 1.06],
    [CrashType.PDC, 1.06],
    [CrashType.PKV, 1.06],
    [CrashType.FTR, roundNumber(1.0549164785553, 9)],
    [CrashType.RT, 1.07],
    [CrashType.SSD, 1.06],
    [CrashType.SOD, 1.06],
    [CrashType.T, 1.07],
    [CrashType.TR, 1.06],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I5.2
// Macro Case None
// Adjusted CMF Tag: 3AA

testCounterMeasure({
  code: "3.3.20.I5.2",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, roundNumber(0.924582392776524, 9)],
    [CrashType.FO, roundNumber(0.924582392776524, 9)],
    [CrashType.FTF, 1],
    [CrashType.LT, 1],
    [CrashType.OtherNC, roundNumber(0.924582392776524, 9)],
    [CrashType.OtherO, 0.95],
    [CrashType.OVT, roundNumber(0.924582392776524, 9)],
    [CrashType.PD, 0.95],
    [CrashType.PDC, 0.95],
    [CrashType.PKV, 0.95],
    [CrashType.FTR, roundNumber(0.924582392776524, 9)],
    [CrashType.RT, 1],
    [CrashType.SSD, 0.95],
    [CrashType.SOD, 0.95],
    [CrashType.T, 1],
    [CrashType.TR, 0.95],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I5.3
// Macro Case None
// Adjusted CMF Tag: 3AB

testCounterMeasure({
  code: "3.3.20.I5.3",
  expectedCMF: new Map([
    [CrashType.AG, 0.68],
    [CrashType.AN, 0.68],
    [CrashType.FO, 0.68],
    [CrashType.FTF, 0.68],
    [CrashType.LT, 0.68],
    [CrashType.OtherNC, 0.68],
    [CrashType.OtherO, 0.68],
    [CrashType.OVT, 0.68],
    [CrashType.PD, 0.68],
    [CrashType.PDC, 0.68],
    [CrashType.PKV, 0.68],
    [CrashType.FTR, 0.68],
    [CrashType.RT, 0.68],
    [CrashType.SSD, 0.68],
    [CrashType.SOD, 0.68],
    [CrashType.T, 0.68],
    [CrashType.TR, 0.68],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I5.4
// Macro Case None
// Adjusted CMF Tag: 3AC

testCounterMeasure({
  code: "3.3.20.I5.4",
  expectedCMF: new Map([
    [CrashType.AG, 0.8],
    [CrashType.AN, roundNumber(0.724582392776524, 9)],
    [CrashType.FO, roundNumber(0.724582392776524, 9)],
    [CrashType.FTF, 0.8],
    [CrashType.LT, 0.8],
    [CrashType.OtherNC, roundNumber(0.724582392776524, 9)],
    [CrashType.OtherO, 0.75],
    [CrashType.OVT, roundNumber(0.724582392776524, 9)],
    [CrashType.PD, 0.75],
    [CrashType.PDC, 0.75],
    [CrashType.PKV, 0.75],
    [CrashType.FTR, roundNumber(0.724582392776524, 9)],
    [CrashType.RT, 0.8],
    [CrashType.SSD, 0.75],
    [CrashType.SOD, 0.75],
    [CrashType.T, 0.8],
    [CrashType.TR, 0.75],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I6.1
// Macro Case None
// Adjusted CMF Tag: 3AD

testCounterMeasure({
  code: "3.3.20.I6.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.07],
    [CrashType.AN, roundNumber(1.0549164785553, 9)],
    [CrashType.FO, roundNumber(1.0549164785553, 9)],
    [CrashType.FTF, 1.07],
    [CrashType.LT, 1.07],
    [CrashType.OtherNC, roundNumber(1.0549164785553, 9)],
    [CrashType.OtherO, 1.06],
    [CrashType.OVT, roundNumber(1.0549164785553, 9)],
    [CrashType.PD, 1.06],
    [CrashType.PDC, 1.06],
    [CrashType.PKV, 1.06],
    [CrashType.FTR, roundNumber(1.0549164785553, 9)],
    [CrashType.RT, 1.07],
    [CrashType.SSD, 1.06],
    [CrashType.SOD, 1.06],
    [CrashType.T, 1.07],
    [CrashType.TR, 1.06],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I6.2
// Macro Case None
// Adjusted CMF Tag: 3AE

testCounterMeasure({
  code: "3.3.20.I6.2",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, roundNumber(0.924582392776524, 9)],
    [CrashType.FO, roundNumber(0.924582392776524, 9)],
    [CrashType.FTF, 1],
    [CrashType.LT, 1],
    [CrashType.OtherNC, roundNumber(0.924582392776524, 9)],
    [CrashType.OtherO, 0.95],
    [CrashType.OVT, roundNumber(0.924582392776524, 9)],
    [CrashType.PD, 0.95],
    [CrashType.PDC, 0.95],
    [CrashType.PKV, 0.95],
    [CrashType.FTR, roundNumber(0.924582392776524, 9)],
    [CrashType.RT, 1],
    [CrashType.SSD, 0.95],
    [CrashType.SOD, 0.95],
    [CrashType.T, 1],
    [CrashType.TR, 0.95],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I6.3
// Macro Case None
// Adjusted CMF Tag: 3AF

testCounterMeasure({
  code: "3.3.20.I6.3",
  expectedCMF: new Map([
    [CrashType.AG, 0.68],
    [CrashType.AN, 0.68],
    [CrashType.FO, 0.68],
    [CrashType.FTF, 0.68],
    [CrashType.LT, 0.68],
    [CrashType.OtherNC, 0.68],
    [CrashType.OtherO, 0.68],
    [CrashType.OVT, 0.68],
    [CrashType.PD, 0.68],
    [CrashType.PDC, 0.68],
    [CrashType.PKV, 0.68],
    [CrashType.FTR, 0.68],
    [CrashType.RT, 0.68],
    [CrashType.SSD, 0.68],
    [CrashType.SOD, 0.68],
    [CrashType.T, 0.68],
    [CrashType.TR, 0.68],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.20.I6.4
// Macro Case None
// Adjusted CMF Tag: 3AG

testCounterMeasure({
  code: "3.3.20.I6.4",
  expectedCMF: new Map([
    [CrashType.AG, 0.8],
    [CrashType.AN, roundNumber(0.724582392776524, 9)],
    [CrashType.FO, roundNumber(0.724582392776524, 9)],
    [CrashType.FTF, 0.8],
    [CrashType.LT, 0.8],
    [CrashType.OtherNC, roundNumber(0.724582392776524, 9)],
    [CrashType.OtherO, 0.75],
    [CrashType.OVT, roundNumber(0.724582392776524, 9)],
    [CrashType.PD, 0.75],
    [CrashType.PDC, 0.75],
    [CrashType.PKV, 0.75],
    [CrashType.FTR, roundNumber(0.724582392776524, 9)],
    [CrashType.RT, 0.8],
    [CrashType.SSD, 0.75],
    [CrashType.SOD, 0.75],
    [CrashType.T, 0.8],
    [CrashType.TR, 0.75],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.21.I3.1
// Macro Case None
// Adjusted CMF Tag: 3AH

testCounterMeasure({
  code: "3.3.21.I3.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.75],
    [CrashType.AN, roundNumber(0.836995805369128, 9)],
    [CrashType.FO, roundNumber(0.836995805369128, 9)],
    [CrashType.FTF, 0.814],
    [CrashType.LT, roundNumber(0.836995805369128, 9)],
    [CrashType.OtherNC, roundNumber(0.836995805369128, 9)],
    [CrashType.OtherO, 0.814],
    [CrashType.OVT, roundNumber(0.836995805369128, 9)],
    [CrashType.PD, 0.814],
    [CrashType.PDC, 0.814],
    [CrashType.PKV, 0.814],
    [CrashType.FTR, 0.792],
    [CrashType.RT, roundNumber(0.836995805369128, 9)],
    [CrashType.SSD, 0.814],
    [CrashType.SOD, 0.814],
    [CrashType.T, roundNumber(0.836995805369128, 9)],
    [CrashType.TR, 0.814],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.3.21.I7.1
// Macro Case None
// Adjusted CMF Tag: 3AI

testCounterMeasure({
  code: "3.3.21.I7.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.75],
    [CrashType.AN, roundNumber(0.836995805369128, 9)],
    [CrashType.FO, roundNumber(0.836995805369128, 9)],
    [CrashType.FTF, 0.814],
    [CrashType.LT, roundNumber(0.836995805369128, 9)],
    [CrashType.OtherNC, roundNumber(0.836995805369128, 9)],
    [CrashType.OtherO, 0.814],
    [CrashType.OVT, roundNumber(0.836995805369128, 9)],
    [CrashType.PD, 0.814],
    [CrashType.PDC, 0.814],
    [CrashType.PKV, 0.814],
    [CrashType.FTR, 0.792],
    [CrashType.RT, roundNumber(0.836995805369128, 9)],
    [CrashType.SSD, 0.814],
    [CrashType.SOD, 0.814],
    [CrashType.T, roundNumber(0.836995805369128, 9)],
    [CrashType.TR, 0.814],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.14.I3.1
// Macro Case None
// Adjusted CMF Tag: 3AJ

testCounterMeasure({
  code: "3.4.14.I3.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.43],
    [CrashType.AN, roundNumber(0.560338101430429, 9)],
    [CrashType.FO, roundNumber(0.560338101430429, 9)],
    [CrashType.FTF, 0.43],
    [CrashType.LT, 0.43],
    [CrashType.OtherNC, roundNumber(0.560338101430429, 9)],
    [CrashType.OtherO, 0.52],
    [CrashType.OVT, roundNumber(0.560338101430429, 9)],
    [CrashType.PD, 0.52],
    [CrashType.PDC, 0.52],
    [CrashType.PKV, 0.52],
    [CrashType.FTR, roundNumber(0.560338101430429, 9)],
    [CrashType.RT, roundNumber(0.560338101430429, 9)],
    [CrashType.SSD, 0.43],
    [CrashType.SOD, 0.52],
    [CrashType.T, roundNumber(0.482135240572172, 9)],
    [CrashType.TR, 0.52],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.14.I7.1
// Macro Case None
// Adjusted CMF Tag: 3AK

testCounterMeasure({
  code: "3.4.14.I7.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.43],
    [CrashType.AN, roundNumber(0.560338101430429, 9)],
    [CrashType.FO, roundNumber(0.560338101430429, 9)],
    [CrashType.FTF, 0.43],
    [CrashType.LT, 0.43],
    [CrashType.OtherNC, roundNumber(0.560338101430429, 9)],
    [CrashType.OtherO, 0.52],
    [CrashType.OVT, roundNumber(0.560338101430429, 9)],
    [CrashType.PD, 0.52],
    [CrashType.PDC, 0.52],
    [CrashType.PKV, 0.52],
    [CrashType.FTR, roundNumber(0.560338101430429, 9)],
    [CrashType.RT, roundNumber(0.560338101430429, 9)],
    [CrashType.SSD, 0.43],
    [CrashType.SOD, 0.52],
    [CrashType.T, roundNumber(0.482135240572172, 9)],
    [CrashType.TR, 0.52],
    [CrashType.NGT, 0.66],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.24.I7.1
// Macro Case None
// Adjusted CMF Tag: 3AL

testCounterMeasure({
  code: "3.4.24.I7.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.49],
    [CrashType.AN, roundNumber(0.87148764498235, 9)],
    [CrashType.FO, roundNumber(0.87148764498235, 9)],
    [CrashType.FTF, 0.89],
    [CrashType.LT, 1.01],
    [CrashType.OtherNC, roundNumber(0.87148764498235, 9)],
    [CrashType.OtherO, 0.79],
    [CrashType.OVT, roundNumber(0.87148764498235, 9)],
    [CrashType.PD, 0.76],
    [CrashType.PDC, 0.8],
    [CrashType.PKV, 0.79],
    [CrashType.FTR, 0.89],
    [CrashType.RT, 1.01],
    [CrashType.SSD, 0.79],
    [CrashType.SOD, 0.79],
    [CrashType.T, 1.01],
    [CrashType.TR, 0.79],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.25.I7.1
// Macro Case None
// Adjusted CMF Tag: 3AM

testCounterMeasure({
  code: "3.4.25.I7.1",
  expectedCMF: new Map([
    [CrashType.AG, 1.091],
    [CrashType.AN, roundNumber(0.941931399631676, 9)],
    [CrashType.FO, roundNumber(0.941931399631676, 9)],
    [CrashType.FTF, 1.091],
    [CrashType.LT, 1.091],
    [CrashType.OtherNC, roundNumber(0.941931399631676, 9)],
    [CrashType.OtherO, 0.982],
    [CrashType.OVT, roundNumber(0.941931399631676, 9)],
    [CrashType.PD, 0.982],
    [CrashType.PDC, 0.982],
    [CrashType.PKV, 0.982],
    [CrashType.FTR, 0.827],
    [CrashType.RT, roundNumber(0.941931399631676, 9)],
    [CrashType.SSD, 0.982],
    [CrashType.SOD, 0.982],
    [CrashType.T, roundNumber(1.03137255985267, 9)],
    [CrashType.TR, 0.982],
    [CrashType.NGT, 0.926],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.26.I7.1
// Macro Case None
// Adjusted CMF Tag: 3AN

testCounterMeasure({
  code: "3.4.26.I7.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.564],
    [CrashType.AN, roundNumber(1.03289345637584, 9)],
    [CrashType.FO, roundNumber(1.03289345637584, 9)],
    [CrashType.FTF, 0.918],
    [CrashType.LT, roundNumber(1.03289345637584, 9)],
    [CrashType.OtherNC, roundNumber(1.03289345637584, 9)],
    [CrashType.OtherO, 0.918],
    [CrashType.OVT, roundNumber(1.03289345637584, 9)],
    [CrashType.PD, 0.918],
    [CrashType.PDC, 0.918],
    [CrashType.PKV, 0.918],
    [CrashType.FTR, 0.988],
    [CrashType.RT, roundNumber(1.03289345637584, 9)],
    [CrashType.SSD, 0.918],
    [CrashType.SOD, 0.918],
    [CrashType.T, roundNumber(1.03289345637584, 9)],
    [CrashType.TR, 0.918],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.28.I3.1
// Macro Case None
// Adjusted CMF Tag: 3AO

testCounterMeasure({
  code: "3.4.28.I3.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(1.29103526448363, 9)],
    [CrashType.AN, roundNumber(1.29103526448363, 9)],
    [CrashType.FO, roundNumber(1.29103526448363, 9)],
    [CrashType.FTF, 1.338],
    [CrashType.LT, 2.242],
    [CrashType.OtherNC, roundNumber(1.29103526448363, 9)],
    [CrashType.OtherO, 1.338],
    [CrashType.OVT, roundNumber(1.29103526448363, 9)],
    [CrashType.PD, 1.338],
    [CrashType.PDC, 1.338],
    [CrashType.PKV, 1.338],
    [CrashType.FTR, roundNumber(1.29103526448363, 9)],
    [CrashType.RT, roundNumber(1.29103526448363, 9)],
    [CrashType.SSD, 1.338],
    [CrashType.SOD, 1.338],
    [CrashType.T, roundNumber(1.86161410579345, 9)],
    [CrashType.TR, 1.338],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.28.I7.1
// Macro Case None
// Adjusted CMF Tag: 3AO

testCounterMeasure({
  code: "3.4.28.I7.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(1.29103526448363, 9)],
    [CrashType.AN, roundNumber(1.29103526448363, 9)],
    [CrashType.FO, roundNumber(1.29103526448363, 9)],
    [CrashType.FTF, 1.338],
    [CrashType.LT, 2.242],
    [CrashType.OtherNC, roundNumber(1.29103526448363, 9)],
    [CrashType.OtherO, 1.338],
    [CrashType.OVT, roundNumber(1.29103526448363, 9)],
    [CrashType.PD, 1.338],
    [CrashType.PDC, 1.338],
    [CrashType.PKV, 1.338],
    [CrashType.FTR, roundNumber(1.29103526448363, 9)],
    [CrashType.RT, roundNumber(1.29103526448363, 9)],
    [CrashType.SSD, 1.338],
    [CrashType.SOD, 1.338],
    [CrashType.T, roundNumber(1.86161410579345, 9)],
    [CrashType.TR, 1.338],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.29.I3.1
// Macro Case None
// Adjusted CMF Tag: 3AP

testCounterMeasure({
  code: "3.4.29.I3.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.759130352644836, 9)],
    [CrashType.AN, roundNumber(0.759130352644836, 9)],
    [CrashType.FO, roundNumber(0.759130352644836, 9)],
    [CrashType.FTF, 0.753],
    [CrashType.LT, 0.635],
    [CrashType.OtherNC, roundNumber(0.759130352644836, 9)],
    [CrashType.OtherO, 0.753],
    [CrashType.OVT, roundNumber(0.759130352644836, 9)],
    [CrashType.PD, 0.753],
    [CrashType.PDC, 0.753],
    [CrashType.PKV, 0.753],
    [CrashType.FTR, roundNumber(0.759130352644836, 9)],
    [CrashType.RT, roundNumber(0.759130352644836, 9)],
    [CrashType.SSD, 0.753],
    [CrashType.SOD, 0.753],
    [CrashType.T, roundNumber(0.684652141057935, 9)],
    [CrashType.TR, 0.753],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.29.I7.1
// Macro Case None
// Adjusted CMF Tag: 3AP

testCounterMeasure({
  code: "3.4.29.I7.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.759130352644836, 9)],
    [CrashType.AN, roundNumber(0.759130352644836, 9)],
    [CrashType.FO, roundNumber(0.759130352644836, 9)],
    [CrashType.FTF, 0.753],
    [CrashType.LT, 0.635],
    [CrashType.OtherNC, roundNumber(0.759130352644836, 9)],
    [CrashType.OtherO, 0.753],
    [CrashType.OVT, roundNumber(0.759130352644836, 9)],
    [CrashType.PD, 0.753],
    [CrashType.PDC, 0.753],
    [CrashType.PKV, 0.753],
    [CrashType.FTR, roundNumber(0.759130352644836, 9)],
    [CrashType.RT, roundNumber(0.759130352644836, 9)],
    [CrashType.SSD, 0.753],
    [CrashType.SOD, 0.753],
    [CrashType.T, roundNumber(0.684652141057935, 9)],
    [CrashType.TR, 0.753],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.30.I3.1
// Macro Case None
// Adjusted CMF Tag: 3AQ

testCounterMeasure({
  code: "3.4.30.I3.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.92802644836272, 9)],
    [CrashType.AN, roundNumber(0.92802644836272, 9)],
    [CrashType.FO, roundNumber(0.92802644836272, 9)],
    [CrashType.FTF, 0.922],
    [CrashType.LT, 0.806],
    [CrashType.OtherNC, roundNumber(0.92802644836272, 9)],
    [CrashType.OtherO, 0.922],
    [CrashType.OVT, roundNumber(0.92802644836272, 9)],
    [CrashType.PD, 0.922],
    [CrashType.PDC, 0.922],
    [CrashType.PKV, 0.922],
    [CrashType.FTR, roundNumber(0.92802644836272, 9)],
    [CrashType.RT, roundNumber(0.92802644836272, 9)],
    [CrashType.SSD, 0.922],
    [CrashType.SOD, 0.922],
    [CrashType.T, roundNumber(0.854810579345088, 9)],
    [CrashType.TR, 0.922],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.30.I7.1
// Macro Case None
// Adjusted CMF Tag: 3AQ

testCounterMeasure({
  code: "3.4.30.I7.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.92802644836272, 9)],
    [CrashType.AN, roundNumber(0.92802644836272, 9)],
    [CrashType.FO, roundNumber(0.92802644836272, 9)],
    [CrashType.FTF, 0.922],
    [CrashType.LT, 0.806],
    [CrashType.OtherNC, roundNumber(0.92802644836272, 9)],
    [CrashType.OtherO, 0.922],
    [CrashType.OVT, roundNumber(0.92802644836272, 9)],
    [CrashType.PD, 0.922],
    [CrashType.PDC, 0.922],
    [CrashType.PKV, 0.922],
    [CrashType.FTR, roundNumber(0.92802644836272, 9)],
    [CrashType.RT, roundNumber(0.92802644836272, 9)],
    [CrashType.SSD, 0.922],
    [CrashType.SOD, 0.922],
    [CrashType.T, roundNumber(0.854810579345088, 9)],
    [CrashType.TR, 0.922],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.32.I3.1
// Macro Case None
// Adjusted CMF Tag: 3AR

testCounterMeasure({
  code: "3.4.32.I3.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(2.06618260006616, 9)],
    [CrashType.AN, 3.66],
    [CrashType.FO, roundNumber(2.06618260006616, 9)],
    [CrashType.FTF, 2.11],
    [CrashType.LT, roundNumber(2.06618260006616, 9)],
    [CrashType.OtherNC, 0.44],
    [CrashType.OtherO, 2.11],
    [CrashType.OVT, roundNumber(2.06618260006616, 9)],
    [CrashType.PD, 2.11],
    [CrashType.PDC, 2.11],
    [CrashType.PKV, 2.11],
    [CrashType.FTR, roundNumber(2.06618260006616, 9)],
    [CrashType.RT, 2.93],
    [CrashType.SSD, 3.66],
    [CrashType.SOD, 2.11],
    [CrashType.T, roundNumber(2.4117095600397, 9)],
    [CrashType.TR, 2.11],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.33.I1.1
// Macro Case None
// Adjusted CMF Tag: 3AT

testCounterMeasure({
  code: "3.4.33.I1.1",
  expectedCMF: new Map([
    [CrashType.AG, 0.25],
    [CrashType.AN, roundNumber(0.666662245800177, 9)],
    [CrashType.FO, roundNumber(0.666662245800177, 9)],
    [CrashType.FTF, 0.54],
    [CrashType.LT, 0.41],
    [CrashType.OtherNC, roundNumber(0.666662245800177, 9)],
    [CrashType.OtherO, 0.54],
    [CrashType.OVT, roundNumber(0.666662245800177, 9)],
    [CrashType.PD, 0.54],
    [CrashType.PDC, 0.54],
    [CrashType.PKV, 0.54],
    [CrashType.FTR, roundNumber(0.666662245800177, 9)],
    [CrashType.RT, 0.25],
    [CrashType.SSD, 0.54],
    [CrashType.SOD, 0.54],
    [CrashType.T, 0.346],
    [CrashType.TR, 0.54],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.35.I5.1
// Macro Case None
// Adjusted CMF Tag: 3AS

testCounterMeasure({
  code: "3.4.35.I5.1",
  expectedCMF: new Map([
    [CrashType.AG, roundNumber(0.855874810548651, 9)],
    [CrashType.AN, roundNumber(0.855874810548651, 9)],
    [CrashType.FO, roundNumber(0.855874810548651, 9)],
    [CrashType.FTF, 0.849],
    [CrashType.LT, roundNumber(0.855874810548651, 9)],
    [CrashType.OtherNC, roundNumber(0.855874810548651, 9)],
    [CrashType.OtherO, 0.849],
    [CrashType.OVT, roundNumber(0.855874810548651, 9)],
    [CrashType.PD, 0.309],
    [CrashType.PDC, 0.849],
    [CrashType.PKV, 0.849],
    [CrashType.FTR, roundNumber(0.855874810548651, 9)],
    [CrashType.RT, roundNumber(0.855874810548651, 9)],
    [CrashType.SSD, 0.849],
    [CrashType.SOD, 0.849],
    [CrashType.T, roundNumber(0.855874810548651, 9)],
    [CrashType.TR, 0.849],
    [CrashType.NGT, 1],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [1, "local"],
  incorrectInputsDueToType: [["string", 2]],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "string"],
  inputValidation: true,
  incorrectInputsDueToValue: [
    [0, "local"],
    [5, "ducks"],
  ],
  expectedValueErrors: [
    "Peer group must be a positive integer",
    "Locality must be either 'local' or 'state'",
  ],
});

// _____________________________________________________________________________________________

// 3.4.46.I7.1
// Macro Case None

testCounterMeasure({
  code: "3.4.46.I7.1",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 1],
    [CrashType.FTF, 1],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.ROR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 1],
    [CrashType.SOD, 1],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: false,
  aadtMinorRequired: false,
  aadtMajor: undefined,
  aadtMinor: undefined,
  correctNumberOfInputs: 0,
  correctTypeArray: [],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});

/*
// _____________________________________________________________________________________________

// Placeholder
// Macro Case Placeholder

testCounterMeasure({
  code: "Placeholder",
  expectedCMF: new Map([
    [CrashType.AG, 1],
    [CrashType.AN, 1],
    [CrashType.FO, 1],
    [CrashType.FTF, 1],
    [CrashType.LT, 1],
    [CrashType.OtherNC, 1],
    [CrashType.OtherO, 1],
    [CrashType.OVT, 1],
    [CrashType.PD, 1],
    [CrashType.PDC, 1],
    [CrashType.PKV, 1],
    [CrashType.FTR, 1],
    [CrashType.ROR, 1],
    [CrashType.RT, 1],
    [CrashType.SSD, 1],
    [CrashType.SOD, 1],
    [CrashType.T, 1],
    [CrashType.TR, 1],
    [CrashType.NGT, 1],
    [CrashType.WP, 1],
  ]),
  correctInputs: [15, 0],
  incorrectInputsDueToType: [
    ["string", 0],
    [Number.NaN, 0],
    [15, "string"],
    [15, Number.NaN],
  ],
  incorrectInputsDueToLength: [3],
  aadtMajorRequired: true,
  aadtMinorRequired: false,
  aadtMajor: 700,
  aadtMinor: undefined,
  correctNumberOfInputs: 2,
  correctTypeArray: ["number", "number"],
  inputValidation: false,
  incorrectInputsDueToValue: undefined,
  expectedValueErrors: undefined,
});
*/
