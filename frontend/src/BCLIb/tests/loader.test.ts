import { Countermeasure } from "../models/classes/Countermeasure";
import { CrashType } from "../models/enums";
import { loadCountermeasures } from "../utils";

import { cmData } from "./testData/cmTestData.js";
// import { default as crashData } from "./testData/crashTestData.js";

test("Load Countermeasures Correctly", () => {
  const result = loadCountermeasures(cmData);

  const expected = [
    new Countermeasure(
      "Modify Lane Width",
      "Pavement Treatments",
      15,
      new Map([
        [CrashType.AG, 1.0],
        [CrashType.AN, 1.0],
        [CrashType.FO, 0.67],
        [CrashType.FTF, 0.67],
        [CrashType.LT, 1.0],
        [CrashType.OtherNC, 1.0],
        [CrashType.OtherO, 1.0],
        [CrashType.OVT, 0.67],
        [CrashType.PD, 1.0],
        [CrashType.PDC, 1.0],
        [CrashType.PKV, 1.0],
        [CrashType.FTR, 1.0],
        [CrashType.RT, 1.0],
        [CrashType.SSD, 0.67],
        [CrashType.SOD, 0.67],
        [CrashType.T, 1.0],
        [CrashType.TR, 1.0],
        [CrashType.NGT, 1.0],
        [CrashType.ROR, 0.67],
        [CrashType.WP, 1.0],
        // The following should all be 1 as we have no CMF data on them.
        [CrashType.RTF, 1.0],
        [CrashType.RTR, 1.0],
        [CrashType.RTS, 1.0],
      ]),
      "Rural",
      "Two-Lane",
      "Miles",
      "4.1.1.S1.1",
      ["ROR", "Fixed Object", "Front to Front", "OVT", "SOD", "SSD"],
      "Segment",
      "Local"
    ),
    new Countermeasure(
      "Add or Widen Paved Shoulder",
      "Pavement Treatments",
      15,
      new Map([
        [CrashType.AG, 1.0],
        [CrashType.AN, 1.0],
        [CrashType.FO, 0.77],
        [CrashType.FTF, 0.77],
        [CrashType.LT, 1.0],
        [CrashType.OtherNC, 1.0],
        [CrashType.OtherO, 1.0],
        [CrashType.OVT, 0.77],
        [CrashType.PD, 1.0],
        [CrashType.PDC, 1.0],
        [CrashType.PKV, 1.0],
        [CrashType.FTR, 1.0],
        [CrashType.RT, 1.0],
        [CrashType.SSD, 0.77],
        [CrashType.SOD, 0.77],
        [CrashType.T, 1.0],
        [CrashType.TR, 1.0],
        [CrashType.NGT, 1.0],
        [CrashType.ROR, 0.77],
        [CrashType.WP, 1.0],
        // The following should all be 1 as we have no CMF data on them.
        [CrashType.RTF, 1.0],
        [CrashType.RTR, 1.0],
        [CrashType.RTS, 1.0],
      ]),
      "Rural",
      "Two-Lane",
      "Miles",
      "4.1.3.S1.1",
      ["ROR", "Fixed Object", "Front to Front", "OVT", "SOD", "SSD"],
      "Segment",
      "Local"
    ),
    new Countermeasure(
      "Install snowplowable, permanent raised pavement markers",
      "Pavement Markings",
      4,
      new Map([
        [CrashType.AG, 1],
        [CrashType.AN, 1],
        [CrashType.FO, 1],
        [CrashType.FTF, 1],
        [CrashType.LT, 1],
        [CrashType.OtherNC, 1],
        [CrashType.OtherO, 1],
        [CrashType.OVT, 1],
        [CrashType.PD, 1],
        [CrashType.PDC, 1],
        [CrashType.PKV, 1],
        [CrashType.FTR, 1],
        [CrashType.RT, 1],
        [CrashType.SSD, 1],
        [CrashType.SOD, 1],
        [CrashType.T, 1],
        [CrashType.TR, 1],
        [CrashType.NGT, 0.67],
        [CrashType.ROR, 1],
        [CrashType.WP, 0.5],
        // The following should all be 1 as we have no CMF data on them.
        [CrashType.FTF, 1.0],
        [CrashType.FTR, 1.0],
        [CrashType.RTF, 1.0],
        [CrashType.RTR, 1.0],
        [CrashType.RTS, 1.0],
      ]),
      "Rural",
      "Multilane",
      "Miles",
      "4.3.11.S6.1",
      ["NGT"],
      "Segment",
      "State"
    ),
  ];

  expect(result.length).toEqual(expected.length);
  expect(result).toEqual(expected);
});

// Commenting out as loadCrashes was not implemented by previous team
/* test('Load Crash Data Correctly', () => {
  const result = loadCrashes(crashData);

  const expected = {};
}); */
