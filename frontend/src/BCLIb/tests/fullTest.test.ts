import { Countermeasure } from "../models/classes/Countermeasure";
import { loadCountermeasures } from "../utils";
import {
  CalculatedCounterMeasure,
  calculatedCounterMeasures,
} from "../calculatedCounterMeasures";
import {
  cmBoth,
  cmAggregation,
  cmNonAggregation,
} from "./testData/fullTestData";
import { CrashProfile } from "../models/types";
import { Crash } from "../models/classes/Crash";
import {
  CrashInjuryType,
  CrashType,
  Locality,
  RoadwayType,
} from "../models/enums";
import { createCrashTable } from "../utils";
import { BenefitCalculator } from "../BenefitBreakdown";

const periodStart = new Date(`January 1, ${process.env.STUDY_START_YEAR}`);
const periodEnd = new Date(`December 31, ${process.env.STUDY_END_YEAR}`);

let countermeasuresBoth: Countermeasure[];
let countermeasuresNonAggregation: Countermeasure[];
let countermeasuresAggregation: Countermeasure[];
let attempt1CrashesPeer5: Crash[];
let attempt1CrashesPeer3: Crash[];
let attempt1Crashes: Crash[];
let attempt2Crashes: Crash[];
let attempt3Crashes: Crash[];

function createCrash(
  id: number,
  crashType: CrashType,
  severity: CrashInjuryType,
  surfaceCond: string,
  weatherCond: string,
  lightCond: string
): Crash {
  return new Crash(
    id, // ID
    crashType, // Crash Type
    severity, // Crash Severity
    surfaceCond, // Surface Condition
    weatherCond, // Weather Condition
    lightCond, // Lighting Condition
    2020, // Crash Year
    5, // Crash Month
    9, // Crash Day
    13, // Crash Hour
    5, // Crash Weekday
    0, // Roadway ID
    RoadwayType.Segment, // Roadway Type
    0, // Total Injuries
    0, // Fatalities
    0, // A Injuries
    0, // B Injuries
    0, // C Injuries
    0, // Non Injuries
    1 // Number of Vehicles
  );
}

beforeEach(() => {
  countermeasuresBoth = loadCountermeasures(cmBoth);
  countermeasuresNonAggregation = loadCountermeasures(cmNonAggregation);
  countermeasuresAggregation = loadCountermeasures(cmAggregation);

  const pavementCounter: Countermeasure = countermeasuresBoth.find(
    (cm) => cm.code === "4.1.17.AL.1"
  );
  const pavementCalc: CalculatedCounterMeasure = calculatedCounterMeasures.find(
    (cm) => cm.code === "4.1.17.AL.1"
  );
  pavementCounter.cmfVector = pavementCalc.calculate(
    [5, "local"],
    undefined,
    undefined
  );

  attempt1CrashesPeer5 = [
    createCrash(
      0,
      CrashType.OVT,
      CrashInjuryType.N,
      "Dry",
      "Clear",
      "Darkness"
    ),
    createCrash(
      1,
      CrashType.OVT,
      CrashInjuryType.A,
      "Ice",
      "Clear",
      "Daylight"
    ),
  ];

  attempt1CrashesPeer3 = [
    createCrash(2, CrashType.FO, CrashInjuryType.N, "Dry", "Clear", "Darkness"),
    createCrash(3, CrashType.SSD, CrashInjuryType.C, "Dry", "Clear", "Dusk"),
    createCrash(
      4,
      CrashType.OVT,
      CrashInjuryType.B,
      "Dry",
      "Clear",
      "Darkness"
    ),
  ];

  attempt1Crashes = [
    createCrash(
      0,
      CrashType.OVT,
      CrashInjuryType.N,
      "Dry",
      "Clear",
      "Darkness"
    ),
    createCrash(
      1,
      CrashType.OVT,
      CrashInjuryType.A,
      "Ice",
      "Clear",
      "Daylight"
    ),
    createCrash(2, CrashType.FO, CrashInjuryType.N, "Dry", "Clear", "Darkness"),
    createCrash(3, CrashType.SSD, CrashInjuryType.C, "Dry", "Clear", "Dusk"),
    createCrash(
      4,
      CrashType.OVT,
      CrashInjuryType.B,
      "Dry",
      "Clear",
      "Darkness"
    ),
  ];

  attempt2Crashes = [
    createCrash(0, CrashType.SSD, CrashInjuryType.C, "Dry", "Clear", "Dusk"),
    createCrash(
      1,
      CrashType.OVT,
      CrashInjuryType.N,
      "Dry",
      "Clear",
      "Darkness"
    ),
    createCrash(
      2,
      CrashType.OtherO,
      CrashInjuryType.B,
      "Wet",
      "Rain",
      "Daylight"
    ),
    createCrash(
      3,
      CrashType.OVT,
      CrashInjuryType.A,
      "Dry",
      "Clear",
      "Darkness"
    ),
    createCrash(
      4,
      CrashType.OVT,
      CrashInjuryType.B,
      "Dry",
      "Clear",
      "Darkness"
    ),
  ];

  attempt3Crashes = [
    createCrash(0, CrashType.SSD, CrashInjuryType.C, "Dry", "Clear", "Dusk"),
    createCrash(
      1,
      CrashType.OVT,
      CrashInjuryType.N,
      "Dry",
      "Clear",
      "Darkness"
    ),
    createCrash(
      2,
      CrashType.OtherO,
      CrashInjuryType.B,
      "Wet",
      "Rain",
      "Daylight"
    ),
    createCrash(
      3,
      CrashType.OVT,
      CrashInjuryType.A,
      "Dry",
      "Clear",
      "Darkness"
    ),
    createCrash(
      4,
      CrashType.OVT,
      CrashInjuryType.B,
      "Dry",
      "Clear",
      "Darkness"
    ),
    createCrash(
      5,
      CrashType.OVT,
      CrashInjuryType.C,
      "Dry",
      "Clear",
      "Darkness"
    ),
  ];
});

test("Full Test  Attempt 1 Plain", () => {
  const crashProfile: CrashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 5,
    crashes: createCrashTable(attempt1Crashes, false),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  const benefitCalculator = new BenefitCalculator(crashProfile, [
    ...countermeasuresBoth,
    ...countermeasuresNonAggregation,
  ]);

  expect(benefitCalculator.totalBenefit).toEqual(46550);
});

test("Full Test  Attempt 1 Plain Taking Peer Groups into consideration", () => {
  const crashProfilePeer5: CrashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 5,
    crashes: createCrashTable(attempt1CrashesPeer5, false),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  const crashProfilePeer3: CrashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 3,
    crashes: createCrashTable(attempt1CrashesPeer3, false),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  const benefitCalculatorPeer5 = new BenefitCalculator(crashProfilePeer5, [
    ...countermeasuresBoth,
    ...countermeasuresNonAggregation,
  ]);

  const benefitCalculatorPeer3 = new BenefitCalculator(crashProfilePeer3, [
    ...countermeasuresBoth,
    ...countermeasuresNonAggregation,
  ]);

  expect(
    benefitCalculatorPeer5.totalBenefit + benefitCalculatorPeer3.totalBenefit
  ).toEqual(46400);
});

test("Full Test Attempt 2 Plain", () => {
  const crashProfile: CrashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 3,
    crashes: createCrashTable(attempt2Crashes, false),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  const benefitCalculator = new BenefitCalculator(crashProfile, [
    ...countermeasuresBoth,
    ...countermeasuresNonAggregation,
  ]);

  expect(benefitCalculator.totalBenefit).toEqual(53350);
});

test("Full Test Attempt 2 WP NGT", () => {
  for (const crash of attempt2Crashes) {
    crash.countAsNightTime = crash.lightCondition === "Darkness";
    crash.countAsWetPavement = crash.surfaceCondition !== "Dry";
  }

  const crashProfile: CrashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 3,
    crashes: createCrashTable(attempt2Crashes, false),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  const benefitCalculator = new BenefitCalculator(crashProfile, [
    ...countermeasuresBoth,
    ...countermeasuresNonAggregation,
  ]);

  expect(benefitCalculator.totalBenefit).toEqual(63200);
});

test("Full Test Attempt 3 Plain", () => {
  const crashProfile: CrashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 3,
    crashes: createCrashTable(attempt3Crashes, false),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  const benefitCalculator = new BenefitCalculator(crashProfile, [
    ...countermeasuresBoth,
    ...countermeasuresNonAggregation,
  ]);

  expect(benefitCalculator.totalBenefit).toEqual(53350);
});

test("Full Test Attempt 3 NGT WP", () => {
  for (const crash of attempt3Crashes) {
    crash.countAsNightTime = crash.lightCondition === "Darkness";
    crash.countAsWetPavement = crash.surfaceCondition !== "Dry";
  }

  const crashProfile: CrashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 3,
    crashes: createCrashTable(attempt3Crashes, false),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  const benefitCalculator = new BenefitCalculator(crashProfile, [
    ...countermeasuresBoth,
    ...countermeasuresNonAggregation,
  ]);

  expect(benefitCalculator.totalBenefit).toEqual(63200);
});

test("Full Test Attempt 3 Aggregation", () => {
  for (const crash of attempt3Crashes) {
    crash.countAsNightTime = crash.lightCondition === "Darkness";
    crash.countAsWetPavement = crash.surfaceCondition !== "Dry";
  }

  const crashProfile: CrashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 3,
    crashes: createCrashTable(attempt3Crashes, true),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  const benefitCalculator = new BenefitCalculator(crashProfile, [
    ...countermeasuresBoth,
    ...countermeasuresAggregation,
  ]);

  expect(benefitCalculator.totalBenefit).toEqual(165250);
});
