import { Crash } from "../models/classes/Crash";
import {
  ConditionType,
  CrashInjuryType,
  CrashType,
  RoadwayType,
  Severity,
} from "../models/enums";
import { CrashTable } from "../models/types";
import { createCrashTable } from "../utils";

let crashes: Crash[];
let nightCrashes: Crash[];
let wetPavementCrashes: Crash[];

beforeEach(() => {
  crashes = [
    new Crash(
      0, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      4, // Total Injuries
      0, // Fatalities
      3, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      1, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.F, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      4, // Total Injuries
      1, // Fatalities
      2, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      2, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.N, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      5, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      0, // B Injuries
      0, // C Injuries
      5, // Non Injuries
      1 // Number of Vehicles
    ),
  ];

  nightCrashes = [
    new Crash(
      3, // ID
      CrashType.FO, // Crash Type
      CrashInjuryType.B, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      5, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      1, // B Injuries
      2, // C Injuries
      3, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      4, // ID
      CrashType.AN, // Crash Type
      CrashInjuryType.C, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      0, // B Injuries
      2, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      5, // ID
      CrashType.TR, // Crash Type
      CrashInjuryType.B, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      4, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      1, // B Injuries
      0, // C Injuries
      3, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      5, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      4, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      2, // Non Injuries
      1 // Number of Vehicles
    ),
  ];

  for (const crash of nightCrashes) {
    crash.countAsNightTime = true;
  }

  wetPavementCrashes = [
    new Crash(
      6, // ID
      CrashType.FO, // Crash Type
      CrashInjuryType.B, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      5, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      1, // B Injuries
      2, // C Injuries
      3, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      7, // ID
      CrashType.AN, // Crash Type
      CrashInjuryType.C, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      0, // B Injuries
      2, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      8, // ID
      CrashType.TR, // Crash Type
      CrashInjuryType.B, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      4, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      1, // B Injuries
      0, // C Injuries
      3, // Non Injuries
      1 // Number of Vehicles
    ),
  ];

  for (const crash of wetPavementCrashes) {
    crash.countAsWetPavement = true;
  }
});

test("Create Crash Table", () => {
  const expectedTable: CrashTable = new Map([
    [
      ConditionType.plain,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 1],
            [Severity.A, 1],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 1],
          ]),
        ],
      ]),
    ],
  ]);

  expect(createCrashTable(crashes, false)).toEqual(expectedTable);
});

test("Night Time Non-Aggregated", () => {
  const expectedTable: CrashTable = new Map([
    [
      ConditionType.plain,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 1],
            [Severity.A, 1],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 1],
          ]),
        ],
      ]),
    ],
    [
      ConditionType.NGT,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.FO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.AN,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 0],
            [Severity.C, 1],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.AG,
          new Map([
            [Severity.F, 0],
            [Severity.A, 1],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.TR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
      ]),
    ],
  ]);

  expect(createCrashTable([...crashes, ...nightCrashes], false)).toEqual(
    expectedTable
  );
});

// test("Night Time Aggregated", () => {
// -- So night time, as this point, does not aggregate.
// -- As far as I can tell at least, there is a check that happens, but the resulting cell is multiplied by 0.
// });

test("Wet Pavement Non-Aggregated", () => {
  const expectedTable: CrashTable = new Map([
    [
      ConditionType.plain,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 1],
            [Severity.A, 1],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 1],
          ]),
        ],
      ]),
    ],
    [
      ConditionType.WP,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.FO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.AN,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 0],
            [Severity.C, 1],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.TR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
      ]),
    ],
  ]);

  expect(createCrashTable([...crashes, ...wetPavementCrashes], false)).toEqual(
    expectedTable
  );
});

test("Wet Pavement Aggregated", () => {
  const expectedTable: CrashTable = new Map([
    [
      ConditionType.plain,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 1],
            [Severity.A, 1],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 1],
          ]),
        ],
      ]),
    ],
    [
      ConditionType.WP,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 1],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.AN,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 1],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FTF,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.LT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OtherNC,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OtherO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OVT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PDC,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PKV,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FTR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.RT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.SSD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.SOD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.T,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.TR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
      ]),
    ],
  ]);

  expect(createCrashTable([...crashes, ...wetPavementCrashes], true)).toEqual(
    expectedTable
  );
});

test("Wet Pavement Aggregated Test 2", () => {
  const additionalWP: Crash[] = [
    new Crash(
      9, // ID
      CrashType.TR, // Crash Type
      CrashInjuryType.B, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      1, // B Injuries
      2, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      10, // ID
      CrashType.OVT, // Crash Type
      CrashInjuryType.B, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      4, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      1, // B Injuries
      0, // C Injuries
      3, // Non Injuries
      1 // Number of Vehicles
    ),
  ];

  for (const crash of additionalWP) {
    crash.countAsWetPavement = true;
  }

  const expectedTable: CrashTable = new Map([
    [
      ConditionType.plain,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 1],
            [Severity.A, 1],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 1],
          ]),
        ],
      ]),
    ],
    [
      ConditionType.WP,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 4],
            [Severity.C, 1],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.AN,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 4],
            [Severity.C, 1],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 3],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FTF,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 3],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.LT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 3],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OtherNC,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 3],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OtherO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 3],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OVT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PDC,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PKV,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FTR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.RT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.SSD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.SOD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.T,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.TR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
      ]),
    ],
  ]);

  expect(
    createCrashTable([...crashes, ...wetPavementCrashes, ...additionalWP], true)
  ).toEqual(expectedTable);
});

test("Wet Pavement Aggregated Test 3", () => {
  const differentWP: Crash[] = [
    new Crash(
      9, // ID
      CrashType.LT, // Crash Type
      CrashInjuryType.B, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      1, // B Injuries
      2, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      10, // ID
      CrashType.PD, // Crash Type
      CrashInjuryType.B, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      4, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      1, // B Injuries
      0, // C Injuries
      3, // Non Injuries
      1 // Number of Vehicles
    ),
  ];

  for (const crash of differentWP) {
    crash.countAsWetPavement = true;
  }

  const expectedTable: CrashTable = new Map([
    [ConditionType.plain, new Map<CrashType, Map<Severity, number>>()],
    [
      ConditionType.WP,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.AN,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FTF,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.LT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OtherNC,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OtherO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OVT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PDC,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PKV,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FTR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.RT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.SSD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.SOD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.T,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.TR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
      ]),
    ],
  ]);

  expect(createCrashTable(differentWP, true)).toEqual(expectedTable);
});

test("Wet Pavement Aggregated Test 4", () => {
  const additionalWP: Crash[] = [
    new Crash(
      9, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      10, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      11, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      12, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      13, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
  ];

  for (const crash of additionalWP) {
    crash.countAsWetPavement = true;
  }

  const expectedTable: CrashTable = new Map([
    [
      ConditionType.plain,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 1],
            [Severity.A, 1],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 1],
          ]),
        ],
      ]),
    ],
    [
      ConditionType.WP,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 0],
            [Severity.A, 5],
            [Severity.B, 2],
            [Severity.C, 1],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.AN,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 2],
            [Severity.C, 1],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FTF,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.LT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OtherNC,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OtherO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OVT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PDC,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PKV,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FTR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.RT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.SSD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.SOD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.T,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.TR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
      ]),
    ],
  ]);

  expect(
    createCrashTable([...crashes, ...wetPavementCrashes, ...additionalWP], true)
  ).toEqual(expectedTable);
});

test("Wet Pavement Aggregated Test 5", () => {
  const additionalWP: Crash[] = [
    new Crash(
      9, // ID
      CrashType.AN, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      10, // ID
      CrashType.AN, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      11, // ID
      CrashType.AN, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      12, // ID
      CrashType.AN, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      13, // ID
      CrashType.AN, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      1, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
  ];

  for (const crash of additionalWP) {
    crash.countAsWetPavement = true;
  }

  const expectedTable: CrashTable = new Map([
    [
      ConditionType.plain,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 1],
            [Severity.A, 1],
            [Severity.B, 0],
            [Severity.C, 0],
            [Severity.N, 1],
          ]),
        ],
      ]),
    ],
    [
      ConditionType.WP,
      new Map<CrashType, Map<Severity, number>>([
        [
          CrashType.AG,
          new Map([
            [Severity.F, 0],
            [Severity.A, 5],
            [Severity.B, 2],
            [Severity.C, 1],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.AN,
          new Map([
            [Severity.F, 0],
            [Severity.A, 5],
            [Severity.B, 2],
            [Severity.C, 1],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FTF,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.LT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OtherNC,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OtherO,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.OVT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PDC,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.PKV,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.FTR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.RT,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.SSD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.SOD,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.T,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
        [
          CrashType.TR,
          new Map([
            [Severity.F, 0],
            [Severity.A, 0],
            [Severity.B, 1],
            [Severity.C, 0],
            [Severity.N, 0],
          ]),
        ],
      ]),
    ],
  ]);

  expect(
    createCrashTable([...crashes, ...wetPavementCrashes, ...additionalWP], true)
  ).toEqual(expectedTable);
});
