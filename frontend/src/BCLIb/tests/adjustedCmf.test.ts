import {
  crashProportionAddressed,
  adjustedCrashProportionAddressed,
  getReduction,
  reductionInCrashesSpecified,
  reductionInCrashesOriginalAll,
  reductionInCrashesAdjustedAll,
  adjustedCMFValue,
  adjustedCMFVector,
  createDistributionTag,
} from "../adjustedCMF";
import { CrashType, RoadwayType } from "../models/enums";
import { CmfVector } from "../models/types";
import { roundNumber } from "../utils";

test("Crash Reduction", () => {
  const reduction = getReduction("3A", "LS_5", "LT");
  expect(reduction).toEqual(0.009375);
});

test("Crash Proportions Addressed", () => {
  let proportion = crashProportionAddressed("3Q", "LS_5");
  proportion = roundNumber(proportion, 16);
  expect(proportion).toEqual(0.0197368421052632);
});

test("Adjusted Crash Proportions Addressed", () => {
  let proportion = adjustedCrashProportionAddressed("3R", "LS_5");
  proportion = roundNumber(proportion, 16);
  expect(proportion).toEqual(0.881578947368421);
});

test("Reduction In Crashes", () => {
  let reduction = reductionInCrashesSpecified("3X", "LS_5");
  reduction = roundNumber(reduction, 16);
  expect(reduction).toEqual(0.0378947368421053);
});

test("Negative Reduction In Crashes", () => {
  let reduction = reductionInCrashesSpecified("3V", "LS_5");
  reduction = roundNumber(reduction, 17);
  expect(reduction).toEqual(-0.00828947368421053);
});

test("Reduction in Crashes Original All", () => {
  let reduction = reductionInCrashesOriginalAll("3AD", "LS_5");
  reduction = roundNumber(reduction, 2);
  expect(reduction).toEqual(-0.06);
});

test("Reduction in Crashes Adjusted All", () => {
  let reduction = reductionInCrashesAdjustedAll("4B", "LS_5");
  reduction = roundNumber(reduction, 9);
  expect(reduction).toEqual(0.222944079);
});

test("Adjusted CMF Value", () => {
  let cmfValue = adjustedCMFValue("3AL", "LS_5");
  cmfValue = roundNumber(cmfValue, 15);
  expect(cmfValue).toEqual(0.765409482758621);
});

test("Adjusted CMF Vector", () => {
  const cmfVector = adjustedCMFVector("3AK", "LS_5");
  const expectedVector: CmfVector = new Map<CrashType, number>([
    [CrashType.AG, 0.43],
    [CrashType.AN, 0.531146026],
    [CrashType.FO, 0.531146026],
    [CrashType.FTF, 0.43],
    [CrashType.LT, 0.43],
    [CrashType.OtherNC, 0.52],
    [CrashType.OtherO, 0.52],
    [CrashType.OVT, 0.531146026],
    [CrashType.PD, 0.52],
    [CrashType.PDC, 0.52],
    [CrashType.PKV, 0.52],
    [CrashType.FTR, 0.531146026],
    [CrashType.RT, 0.52],
    [CrashType.SSD, 0.43],
    [CrashType.SOD, 0.531146026],
    [CrashType.T, 0.466],
    [CrashType.TR, 0.52],
    [CrashType.NGT, 0.66],
    [CrashType.ROR, 1],
    [CrashType.WP, 1],
  ]);
  expect(cmfVector).toEqual(expectedVector);
});

test("Create Distribution Tag", () => {
  const tag = createDistributionTag(4, "local", RoadwayType.Segment);
  expect(tag).toEqual("LS_4");
});
