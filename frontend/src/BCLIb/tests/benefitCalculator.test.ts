/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { BenefitCalculator } from "../BenefitBreakdown";
import {
  CalculatedCounterMeasure,
  calculatedCounterMeasures,
} from "../calculatedCounterMeasures";
import { Countermeasure } from "../models/classes/Countermeasure";
import { Crash } from "../models/classes/Crash";
import {
  Locality,
  RoadwayType,
  CrashType,
  Severity,
  CrashInjuryType,
  ConditionType,
} from "../models/enums";
import { CrashProfile } from "../models/types";
import { createCrashTable } from "../utils";

const periodStart = new Date(`January 1, ${process.env.STUDY_START_YEAR}`);
const periodEnd = new Date(`December 31, ${process.env.STUDY_END_YEAR}`);

let crashProfile: CrashProfile;
let countermeasures: Countermeasure[];
let benefitCalculator: BenefitCalculator;

beforeEach(() => {
  crashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 5,

    crashes: new Map<ConditionType, Map<CrashType, Map<Severity, number>>>([
      [
        ConditionType.plain,
        new Map<CrashType, Map<Severity, number>>([
          [
            CrashType.AG,
            new Map([
              [Severity.F, 1],
              [Severity.A, 1],
              [Severity.N, 7],
            ]),
          ],
          [CrashType.AN, new Map([[Severity.N, 2]])],
          [
            CrashType.FO,
            new Map([
              [Severity.F, 1],
              [Severity.A, 1],
              [Severity.N, 2],
            ]),
          ],
          [
            CrashType.OtherO,
            new Map([
              [Severity.A, 1],
              [Severity.N, 1],
            ]),
          ],
          [
            CrashType.OVT,
            new Map([
              [Severity.B, 1],
              [Severity.N, 2],
            ]),
          ],
          [
            CrashType.FTR,
            new Map([
              [Severity.B, 1],
              [Severity.C, 2],
            ]),
          ],
          [CrashType.SSD, new Map([[Severity.N, 1]])],
          [
            CrashType.T,
            new Map([
              [Severity.B, 2],
              [Severity.C, 1],
              [Severity.N, 1],
            ]),
          ],
        ]),
      ],
    ]),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  countermeasures = [
    new Countermeasure(
      "Add/Widen Paved Shoulder",
      "Pavement Treatments",
      15,
      new Map([
        [CrashType.ROR, 0.77],
        [CrashType.FO, 0.77],
        [CrashType.FTF, 0.77],
        [CrashType.OVT, 0.77],
        [CrashType.SOD, 0.77],
        [CrashType.SSD, 0.77],
      ])
    ),
    new Countermeasure(
      "Install Rumble Strips (Shoulder)",
      "Pavement Treatments",
      8,
      new Map([
        [CrashType.FO, 0.67],
        [CrashType.OVT, 0.67],
        [CrashType.ROR, 0.67],
      ])
    ),
    new Countermeasure(
      "Flatten Sideslopes",
      "Roadside Safety",
      20,
      new Map([
        [CrashType.AG, 0.97],
        [CrashType.AN, 0.97],
        [CrashType.FO, 0.97],
        [CrashType.FTF, 0.97],
        [CrashType.LT, 0.97],
        [CrashType.OtherNC, 0.97],
        [CrashType.OtherO, 0.97],
        [CrashType.OVT, 0.97],
        [CrashType.PD, 0.97],
        [CrashType.PDC, 0.97],
        [CrashType.PKV, 0.97],
        [CrashType.FTR, 0.97],
        [CrashType.RT, 0.97],
        [CrashType.SSD, 0.97],
        [CrashType.SOD, 0.97],
        [CrashType.T, 0.97],
        [CrashType.TR, 0.97],
        [CrashType.NGT, 0.97],
        [CrashType.WP, 0.97],
      ])
    ),
    new Countermeasure(
      "Install/Upgrade Signs",
      "Other",
      10,
      new Map([
        [CrashType.AG, 0.82],
        [CrashType.AN, 0.82],
        [CrashType.FO, 0.82],
        [CrashType.FTF, 0.82],
        [CrashType.LT, 0.82],
        [CrashType.OtherNC, 0.82],
        [CrashType.OtherO, 0.82],
        [CrashType.OVT, 0.3],
        [CrashType.PD, 0.82],
        [CrashType.PDC, 0.82],
        [CrashType.PKV, 0.82],
        [CrashType.FTR, 0.82],
        [CrashType.RT, 0.82],
        [CrashType.SSD, 0.82],
        [CrashType.SOD, 0.82],
        [CrashType.T, 0.82],
        [CrashType.TR, 0.82],
        [CrashType.NGT, 0.82],
        [CrashType.WP, 0.82],
      ])
    ),
  ];

  benefitCalculator = new BenefitCalculator(crashProfile, countermeasures);
});

// 2020 Crash Analysis
test("2020 BC Example", () => {
  /// Apply Countermeasures to Crash Profile
  const crashProfileAG: CrashProfile = {
    locality: crashProfile.locality,
    roadwayType: crashProfile.roadwayType,
    peerGroup: crashProfile.peerGroup,
    crashes: new Map([
      [
        ConditionType.plain,
        new Map([
          [
            CrashType.AG,
            crashProfile.crashes.get(ConditionType.plain).get(CrashType.AG)!,
          ],
        ]),
      ],
    ]),
    periodStart: crashProfile.periodStart,
    periodEnd: crashProfile.periodEnd,
  };
  const benefitCalculatorAG = new BenefitCalculator(
    crashProfileAG,
    benefitCalculator.countermeasures
  );

  const crashProfileFO: CrashProfile = {
    locality: crashProfile.locality,
    roadwayType: crashProfile.roadwayType,
    peerGroup: crashProfile.peerGroup,
    crashes: new Map([
      [
        ConditionType.plain,
        new Map([
          [
            CrashType.FO,
            crashProfile.crashes.get(ConditionType.plain).get(CrashType.FO)!,
          ],
        ]),
      ],
    ]),
    periodStart: crashProfile.periodStart,
    periodEnd: crashProfile.periodEnd,
  };
  const benefitCalculatorFO = new BenefitCalculator(
    crashProfileFO,
    benefitCalculator.countermeasures
  );
  // Assert
  /// Check All relevant crash types for benefit of reduction

  /// Check total benefit
  // The formula for these is (#Crash / Period in Years) * Injury_cost for each crash type, then multiplied by 1 - CMF_Total.
  expect(benefitCalculatorAG.totalBenefit).toEqual(
    Math.floor(288966 / 50) * 50
  ); // This should be (1÷5×431671 + 1÷5 × 6630065)×(1−.97×.82) = 288966.23712
  expect(benefitCalculatorFO.totalBenefit).toEqual(
    Math.floor(564939 / 50) * 50
  ); // This should be (1÷5×431671 + 1÷5 × 6630065)×(1 − 0.77 * 0.67 * 0.82 * 0.97) = 832794.961250208 NOTE: The combined cmf needs to be changed, but that is a separate issue

  // Total Benefit:
  // Our crashes are Local, Segment, Peer Group 5

  // Numbers for that are:
  // Fatal - $6630065
  // A - $431671
  // B - $150430
  // C - $0
  // PDO - $0

  // Crashes
  // Note: as C and PDO have benefit 0, not listing them in this list
  // AG: Fatal Crash 1, A Injury 1
  // FO: Fatal 1, A 1,
  // OtherO: A 1
  // OVT: B 1
  // FTR: B 1
  // T: B 2

  // Counter Measures (currently code says 0.6 if less than 0.6, need to fix that later):
  // AG: 0.97 * 0.82 = 0.7954, so 1 - 0.7954 = 0.2046
  // FO 0.77 * 0.67 * 0.97 * 0.82 = 0.41034686 Which is < 0.6, so 0.6. That means 1 - 0.6 = 0.4
  // OtherO: 0.97 * 0.82 0.7954, so 1 - 0.7954 = 0.2046
  // OVT: 0.77 * 0.67 * 0.97 * 0.3 = 0.41034686 Which is < 0.6, so Min(0.6, the CMF values) = 0.3. So 1 - 0.3 = 0.7
  // FTR: 0.97 * 0.82 0.7954, so 1 - 0.7954 = 0.2046
  // T: 0.97 * 0.82 0.7954, so 1 - 0.7954 = 0.2046

  // (1/5)*AG*F + (1/5)*A*AG + (1/5)*F*FO + (1/5)*A*FO + (1/5)*A*OtherO + (1/5)*B*OVT + (1/5)*B*FTR + (2/5)*B*T = 911096.0812400001
  const totalBenefit = benefitCalculator.totalBenefit;
  expect(totalBenefit).toEqual(Math.floor(911096.08124 / 50) * 50);
});

// Issue 81 part 1
test("Issue 81 test case 3 #1", () => {
  const startingWidth = 2;
  const finalWidth = 6;
  const countermeasureCode = "4.1.3.S1.1";

  const crashProfile: CrashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 5,
    crashes: new Map<ConditionType, Map<CrashType, Map<Severity, number>>>([
      [
        ConditionType.plain,
        new Map<CrashType, Map<Severity, number>>([
          [CrashType.AG, new Map([[Severity.N, 1]])],
          [CrashType.FO, new Map([[Severity.F, 1]])],
          [CrashType.OVT, new Map([[Severity.A, 1]])],
          [CrashType.T, new Map([[Severity.B, 1]])],
        ]),
      ],
    ]),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  const calculatedCMF: CalculatedCounterMeasure =
    calculatedCounterMeasures.filter(
      (calcCMF) => calcCMF.code === countermeasureCode
    )[0];

  expect(calculatedCMF).toBeDefined();
  expect(() =>
    calculatedCMF.validate([startingWidth, finalWidth], 3750, 750)
  ).not.toThrowError();

  const countermeasures: Countermeasure[] = [
    new Countermeasure(
      "Add/Widen Paved Shoulder",
      "Pavement Treatments",
      15,
      calculatedCMF.calculate([startingWidth, finalWidth], 3750, 750)
    ),
  ];

  const benefitCalculator = new BenefitCalculator(
    crashProfile,
    countermeasures
  );
  const totalBenefit = benefitCalculator.totalBenefit;

  expect(totalBenefit).toEqual(325900);
});

// Issue 81 part 2

test("Issue 81 test case 3 #2", () => {
  const startingWidth = 2;
  const finalWidth = 6;
  const countermeasureCode = "4.1.3.S1.1";

  const crashProfile: CrashProfile = {
    locality: Locality.Local,
    roadwayType: RoadwayType.Segment,
    peerGroup: 5,
    crashes: new Map<ConditionType, Map<CrashType, Map<Severity, number>>>([
      [
        ConditionType.plain,
        new Map<CrashType, Map<Severity, number>>([
          [CrashType.AG, new Map([[Severity.N, 1]])],
          [CrashType.FO, new Map([[Severity.N, 1]])],
          [CrashType.OVT, new Map([[Severity.B, 1]])],
          [CrashType.T, new Map([[Severity.C, 1]])],
        ]),
      ],
    ]),
    periodStart: periodStart,
    periodEnd: periodEnd,
  };

  const calculatedCMF: CalculatedCounterMeasure =
    calculatedCounterMeasures.filter(
      (calcCMF) => calcCMF.code === countermeasureCode
    )[0];

  expect(calculatedCMF).toBeDefined();
  expect(() =>
    calculatedCMF.validate([startingWidth, finalWidth], 3750, 750)
  ).not.toThrowError();

  const countermeasures: Countermeasure[] = [
    new Countermeasure(
      "Add/Widen Paved Shoulder",
      "Pavement Treatments",
      15,
      calculatedCMF.calculate([startingWidth, finalWidth], 3750, 750)
    ),
    new Countermeasure(
      "Install Rumble Strips (Shoulder)",
      "Pavement Treatments",
      8,
      new Map([
        [CrashType.FO, 0.67],
        [CrashType.OVT, 0.67],
        [CrashType.ROR, 0.67],
      ])
    ),
  ];

  const benefitCalculator = new BenefitCalculator(
    crashProfile,
    countermeasures
  );
  const totalBenefit = benefitCalculator.totalBenefit;

  expect(totalBenefit).toEqual(12000);
});

test("Annual Fatalities Prevented", () => {
  expect(benefitCalculator.annualFatalitiesPrevented).toBe(
    (1 / 5) * (1 - 0.97 * 0.82) + (1 / 5) * (1 - 0.6)
  );
});

test("Total Fatalities Prevented", () => {
  expect(benefitCalculator.totalFatalitiesPrevented).toBe(
    ((1 / 5) * (1 - 0.97 * 0.82) + (1 / 5) * (1 - 0.6)) * 5
  );
});

test("Wet Pavement Non Aggregated", () => {
  const wetProfile: CrashProfile = {
    locality: crashProfile.locality,
    roadwayType: crashProfile.roadwayType,
    peerGroup: crashProfile.peerGroup,
    crashes: new Map<ConditionType, Map<CrashType, Map<Severity, number>>>([
      [
        ConditionType.plain,
        new Map([[CrashType.WP, new Map([[Severity.B, 1]])]]),
      ],
    ]),
    periodStart: crashProfile.periodStart,
    periodEnd: crashProfile.periodEnd,
  };

  const wetCalculator: BenefitCalculator = new BenefitCalculator(wetProfile, [
    new Countermeasure(
      "Increase Pavement Friction",
      "Pavement Treatments",
      5,
      new Map([
        [CrashType.AG, 0.76],
        [CrashType.AN, 0.75],
        [CrashType.FO, 0.75],
        [CrashType.FTF, 0.75],
        [CrashType.LT, 0.76],
        [CrashType.OtherNC, 0.76],
        [CrashType.OtherO, 0.76],
        [CrashType.OVT, 0.75],
        [CrashType.PD, 0.76],
        [CrashType.PDC, 0.76],
        [CrashType.PKV, 0.76],
        [CrashType.FTR, 0.83],
        [CrashType.RT, 0.76],
        [CrashType.SSD, 0.76],
        [CrashType.SOD, 0.75],
        [CrashType.T, 0.76],
        [CrashType.TR, 0.76],
        [CrashType.NGT, 0.0],
        [CrashType.ROR, 0.0],
        [CrashType.WP, 0.43],
      ])
    ),
  ]);

  const wetBenefit = wetCalculator.totalBenefit;

  // Number obtained from spreadsheet
  expect(wetBenefit).toEqual(Math.floor(17149 / 50) * 50);
});

test("Wet Pavement Aggregated From Crashes", () => {
  const crashes: Crash[] = [
    new Crash(
      0, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.A, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      4, // Total Injuries
      0, // Fatalities
      3, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      1, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.F, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      4, // Total Injuries
      1, // Fatalities
      2, // A Injuries
      1, // B Injuries
      0, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      2, // ID
      CrashType.AG, // Crash Type
      CrashInjuryType.N, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      5, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      0, // B Injuries
      0, // C Injuries
      5, // Non Injuries
      1 // Number of Vehicles
    ),
  ];

  const wetPavementCrashes: Crash[] = [
    new Crash(
      6, // ID
      CrashType.FO, // Crash Type
      CrashInjuryType.B, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      5, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      1, // B Injuries
      2, // C Injuries
      3, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      7, // ID
      CrashType.AN, // Crash Type
      CrashInjuryType.C, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      2, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      0, // B Injuries
      2, // C Injuries
      0, // Non Injuries
      1 // Number of Vehicles
    ),
    new Crash(
      8, // ID
      CrashType.TR, // Crash Type
      CrashInjuryType.B, // Crash Severity
      "Dry", // Surface Condition
      "Clear", // Weather Condition
      "Daylight", // Lighting Condition
      2020, // Crash Year
      5, // Crash Month
      9, // Crash Day
      13, // Crash Hour
      5, // Crash Weekday
      0, // Roadway ID
      RoadwayType.Segment, // Roadway Type
      4, // Total Injuries
      0, // Fatalities
      0, // A Injuries
      1, // B Injuries
      0, // C Injuries
      3, // Non Injuries
      1 // Number of Vehicles
    ),
  ];

  for (const crash of wetPavementCrashes) {
    crash.countAsWetPavement = true;
  }

  const wetProfile: CrashProfile = {
    locality: crashProfile.locality,
    roadwayType: crashProfile.roadwayType,
    peerGroup: crashProfile.peerGroup,
    crashes: createCrashTable([...crashes, ...wetPavementCrashes], true),
    periodStart: crashProfile.periodStart,
    periodEnd: crashProfile.periodEnd,
  };

  const wetCalculator: BenefitCalculator = new BenefitCalculator(wetProfile, [
    new Countermeasure(
      "Increase Pavement Friction",
      "Pavement Treatments",
      5,
      new Map([
        [CrashType.AG, 0.76],
        [CrashType.AN, 0.75],
        [CrashType.FO, 0.75],
        [CrashType.FTF, 0.75],
        [CrashType.LT, 0.76],
        [CrashType.OtherNC, 0.76],
        [CrashType.OtherO, 0.76],
        [CrashType.OVT, 0.75],
        [CrashType.PD, 0.76],
        [CrashType.PDC, 0.76],
        [CrashType.PKV, 0.76],
        [CrashType.FTR, 0.83],
        [CrashType.RT, 0.76],
        [CrashType.SSD, 0.76],
        [CrashType.SOD, 0.75],
        [CrashType.T, 0.76],
        [CrashType.TR, 0.76],
        [CrashType.NGT, 0.0],
        [CrashType.ROR, 0.0],
        [CrashType.WP, 0.43],
      ]),
      "",
      "",
      "Miles",
      "4.17.AL.1",
      ["All"],
      "Segment",
      "Local"
    ),
  ]);

  const wetBenefit = wetCalculator.totalBenefit;

  // I am putting the above into the spreadsheet and getting the result to compare to this from there.
  expect(wetBenefit).toEqual(Math.floor(664769 / 50) * 50);
  // expect(wetBenefit).toEqual(Math.floor(338963 / 50) * 50);
  // expect(wetBenefit).toEqual(Math.floor(325832 / 50) * 50);
});
