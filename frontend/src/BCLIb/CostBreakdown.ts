/**
 * Here we will have a class with functions designed to calculate the cost of a project via the EUAC costs of each counter measure.
 *
 * Important Functions:
 * Starting Cost = Unit Cost * Units
 * # Rebuilds = FLOOR(Longest service life / service life of this counter measure)
 * Present Worth = Starting Cost
 * For n = 1 to # Rebuilds - 1
 *   Present Worth += (Starting Cost) / 1.04^(Service life of this counter measure * n)
 * EAUC = Present Worth * (interest * (1 + interest) ^(longest service life) ) / ((1 + interest)^(longest service life) - 1))
 *
 * Note: For us interest will always be 0.04
 * The EAUC equation can be simplified as
 * Present Worth * 0.04 * (1 + 1 / ((1 + 0.04)^(longest service life) - 1))
 */

import { RoadwayType } from "./models/enums";
import { IAppliedCountermeasure } from "./models/interfaces";
import { CmfVector, ProjectElements } from "./models/types";

/**
 * A small utility class to calculate the EUAC Cost of a counter measure or whole project
 */
export class CostCalculator {
  /** The longest service life of the countermeasures for this project */
  max_service_life: number;

  /**
   * Just copy over the applied counter measures and get the max service life
   * @param appliedCountermeasures The applied countermeasures of the project.
   */
  constructor(appliedCountermeasures: IAppliedCountermeasure[]) {
    this.max_service_life = this.get_max_service_life(appliedCountermeasures);
  }

  /**
   * Goes through the currently given countermeasures's service lives and returns the longest one.
   * @returns The longest service life of known counter measures
   */
  get_max_service_life(
    appliedCountermeasures: IAppliedCountermeasure[]
  ): number {
    return Math.max(
      ...appliedCountermeasures.map(
        (appliedCountermeasure) =>
          appliedCountermeasure.countermeasure.serviceLife
      )
    );
  }

  /**
   * Given an IAppliedCountermeasure, returns that IAppliedCountermeasure with the presentWorth and EUAC cost calculated.
   *
   * @param appliedCountermeasure The counter measure for which to calculate costs.
   */
  calculateAppliedCountermeasure(
    appliedCountermeasure: IAppliedCountermeasure
  ): IAppliedCountermeasure {
    const counterMeasureCopy: IAppliedCountermeasure = {
      ...appliedCountermeasure,
    };
    counterMeasureCopy.presentWorth = CostCalculator.calculate_present_worth(
      this.max_service_life,
      appliedCountermeasure
    );
    counterMeasureCopy.EAUCCost = CostCalculator.calculate_EUAC(
      this.max_service_life,
      counterMeasureCopy.presentWorth
    );

    return counterMeasureCopy;
  }

  /**
   * Calculates and returns the total cost of the applied counter measures.
   * @param appliedCountermeasures
   * @returns
   */
  static calculateTotalCost(
    appliedCountermeasures: IAppliedCountermeasure[]
  ): number {
    const costCalculator = new CostCalculator(appliedCountermeasures);
    return appliedCountermeasures
      .map((appliedCountermeasure) =>
        CostCalculator.round50(
          costCalculator.calculateAppliedCountermeasure(appliedCountermeasure)
            .EAUCCost
        )
      )
      .reduce(
        (previousValue: number, currentValue: number) =>
          previousValue + currentValue,
        0
      );
  }

  /**
   * Populates the countermeasures of a given type with their EUAC and Present Worth values
   * @param proj
   * @param type
   */
  static populateCounterMeasures(
    proj: ProjectElements,
    type: RoadwayType
  ): void {
    const typedCounterMeasures: IAppliedCountermeasure[] = proj
      .get(type)
      .map((roadwayElement) => roadwayElement.appliedCountermeasures)
      .flat(1);

    const typedCostCalculator: CostCalculator = new CostCalculator(
      typedCounterMeasures
    );

    proj.get(type).map((roadwayElement, featureIdx) => {
      roadwayElement.appliedCountermeasures.map(
        (appliedCountermeasure, countermeasureIdx) => {
          proj.get(type)[featureIdx].appliedCountermeasures[countermeasureIdx] =
            typedCostCalculator.calculateAppliedCountermeasure(
              appliedCountermeasure
            );
        }
      );
    });
  }

  /**
   * Calculates the project cost by summing the cost of the intersections and the cost of the segments
   * @param studyAreaFeatures
   * @returns
   */
  static calculateTotalProjectCost(studyAreaFeatures: ProjectElements): number {
    CostCalculator.populateCounterMeasures(
      studyAreaFeatures,
      RoadwayType.Segment
    );
    CostCalculator.populateCounterMeasures(
      studyAreaFeatures,
      RoadwayType.Intersection
    );

    const segmentCounterMeasures = studyAreaFeatures
      .get(RoadwayType.Segment)
      .map((roadwayElement) => roadwayElement.appliedCountermeasures)
      .flat(1);

    const intersectionCounterMeasures = studyAreaFeatures
      .get(RoadwayType.Intersection)
      .map((roadwayElement) => roadwayElement.appliedCountermeasures)
      .flat(1);

    return (
      CostCalculator.calculateTotalCost(segmentCounterMeasures) +
      CostCalculator.calculateTotalCost(intersectionCounterMeasures)
    );
  }

  /**
   * Given the longest service live and a counter measure, creates and calculates the present_worth array in the same way the Benefit/Cost spreadsheet tool does it.
   *
   * The equation is taken directly from the Macro in the spreadsheet.
   *
   * 'Calculating Present worth values based on service life
   *
   * For Row = 1 To 4
   * For all the rows for ActiveCell.Offset
   * ContainerPort: 8000
   * Macro in the spreadsheet
   * And CostCounter CostCM(Row, 1)
   * apiVersion: v1
   * kind: Service
   * metadata:
   *    name: django-store-services
   * specs:
   *    port:
   *      port:9000
   *      targetPort: 9090
   * yaml configuration files port number 9090
   *
   *
   *
   *   If ActiveCell.Offset(Row, 2) = ActiveCell.Offset(1, 5) And ActiveCell.Offset(Row, 2) <> 0 Then
   *
   *       ActiveCell.Offset(Row + 10, 1) = CostCM(Row, 1)
   *
   *   Else
   *
   *       If ActiveCell.Offset(Row, 2) <> 0 Then
   *
   *           ActiveCell.Offset(Row + 10, 1) = CostCM(Row, 1)
   *
   *           n = Round(ActiveCell.Offset(1, 5) / ServLife(Row, 1), 0)
   *
   *           'On Error Resume Next
   *
   *           For Pworth = 1 To n - 1
   *
   *               ActiveCell.Offset(Row + 10, Pworth + 1) = Round(CostCM(Row, 1) / ((1 + ActiveCell.Offset(1, 3)) ^ (ServLife(Row, 1) * Pworth)), 0)
   *
   *           Next Pworth
   *
   *       Else
   *
   *           On Error Resume Next
   *
   *       End If
   *
   *       'Application.DisplayAlerts = False
   *
   *   End If
   *
   * Next Row
   *
   * @param max_service_life The longest service life of applied counter measures.
   * @param appliedCountermeasure The counter measure for which to calculate the present worth array.
   * @returns {number[]} The present worth array in the form [1st cost, 2nd cost, 3rd cost, ... etc]. The total present worth cost of the countermeasure is the sum of this array
   */
  static calculate_present_worth(
    max_service_life: number,
    appliedCountermeasure: IAppliedCountermeasure
  ): number[] {
    const startCost =
      appliedCountermeasure.quantity * appliedCountermeasure.unitCost;
    const presentWorth = [Math.round(startCost)];
    if (max_service_life !== appliedCountermeasure.countermeasure.serviceLife) {
      const rebuilds = evenRound(
        max_service_life / appliedCountermeasure.countermeasure.serviceLife
      );
      for (let i = 1; i < rebuilds; i++) {
        presentWorth.push(
          evenRound(
            startCost /
              Math.pow(
                1.04,
                appliedCountermeasure.countermeasure.serviceLife * i
              )
          )
        );
      }
    }
    return presentWorth;
  }

  /**
   * Calculates the EUAC cost of a project given the maximum service life and the present worth array of the counter measure
   * @param max_service_life The longest service life of applied counter measures.
   * @param presentWorth The array of present worths.
   * @returns {number} The EUAC cost of this counter measure
   */
  static calculate_EUAC(
    max_service_life: number,
    presentWorth: number[]
  ): number {
    // Present Worth * 0.04 * (1 + 1 / ((1 + 0.04)^(longest service life) - 1))
    if (!presentWorth) {
      return 0;
    }
    const presentWorthTotal = presentWorth.reduce(
      (accumulator, currentValue) => accumulator + currentValue,
      0
    );
    return evenRound(
      presentWorthTotal *
        0.04 *
        (1 + 1 / (Math.pow(1 + 0.04, max_service_life) - 1))
    );
  }

  /** Returns the cost of the countermeasure rounded up to the number of units of 50 it will use.
   * The spreadsheet uses this rounding.
   */
  static round50(EUACCost: number): number {
    return Math.ceil(EUACCost / 50) * 50;
  }
}

function evenRound(input: number) {
  const floor = Math.floor(input);
  const remainder = input - Math.floor(input);
  const result =
    remainder === 0.5
      ? floor % 2 === 0
        ? floor
        : floor + 1
      : Math.round(input);
  return result;
}
