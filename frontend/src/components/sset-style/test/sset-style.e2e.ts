import { newE2EPage } from '@stencil/core/testing';

describe('sset-style', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<sset-style></sset-style>');

    const element = await page.find('sset-style');
    expect(element).toHaveClass('hydrated');
  });
});
