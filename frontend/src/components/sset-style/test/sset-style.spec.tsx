import { newSpecPage } from '@stencil/core/testing';
import { SsetStyle } from '../sset-style';

describe('sset-style', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SsetStyle],
      html: `<sset-style></sset-style>`,
    });
    expect(page.root).toEqualHtml(`
      <sset-style>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </sset-style>
    `);
  });
});
