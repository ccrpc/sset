export default [
  {
    id: `theme-custom-segments`,
    type: 'line',
    source: 'segments',
    'source-layer': 'segments',
    minzoom: 6,
    maxzoom: 16,        
    paint:  {
      'line-color': '#00c',
      'line-width': 3
    },
  },
  {
    id: `theme-custom-intersections`,
    type: 'circle',
    source: 'intersections',
    'source-layer': 'intersections',
    minzoom: 6,
    maxzoom: 16,        
    paint:  {
      'circle-color': '#0c0',
      'circle-radius': 4,
      'circle-stroke-width': 2,
      'circle-stroke-color': '#fff',
    },
  },
  {
    id: `theme-custom-crashes`,
    type: 'circle',
    source: 'crashes',
    'source-layer': 'crashes',
    minzoom: 6,
    maxzoom: 16,        
    paint:  {
      'circle-color': '#c00',
      'circle-radius': 2,
      'circle-stroke-width': 2,
      'circle-stroke-color': '#fff',
    }
  }
]