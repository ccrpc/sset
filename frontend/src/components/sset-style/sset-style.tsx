import { Component, Prop, h } from "@stencil/core";
import baseTheme from "./assets/layers-1.json";
@Component({
  tag: "sset-style",
  styleUrl: "sset-style.css",
})
export class SsetStyle {
  // Pass-through arguments
  @Prop() insertIntoStyle: string;
  @Prop() insertBeneathLayer: string;

  // Tile definitions
  @Prop() crashTiles: string;
  @Prop() intersectionTiles: string;
  @Prop() segmentTiles: string;

  // Layer Presentation options
  @Prop() theme = baseTheme;

  @Prop() selectedFeatureId;

  turbo5 = ["#23171b", "#26bce1", "#95fb51", "#ff821d", "#900c00"];
  turbo7 = [
    "#23171b",
    "#3987f9",
    "#2ee5ae",
    "#95fb51",
    "#feb927",
    "#e54813",
    "#900c00",
  ];

  componentWillRender() {
    // Set Segment conditional styling
    this.theme.find(
      (thm) =>
        thm["source-layer"] === "segments" &&
        thm["id"] === "theme-custom-segments"
    ).paint["line-color"] = [
      "case",
      ["==", this.selectedFeatureId, ["number", ["get", "id"]]],
      "#ffff00",
      [
        "to-color",
        [
          "at",
          ["get", "Total Number of Risk Factors Present"],
          ["literal", this.turbo7],
        ],
      ],
    ];

    // Creating the outline
    this.theme.find(
      (thm) =>
        thm["source-layer"] === "segments" && thm["id"] === "segmentOutlineLeft"
    ).filter = ["==", this.selectedFeatureId, ["get", "id"]];

    this.theme.find(
      (thm) =>
        thm["source-layer"] === "segments" &&
        thm["id"] === "segmentOutlineRight"
    ).filter = ["==", this.selectedFeatureId, ["get", "id"]];

    // Set Intersection conditional styling
    this.theme.find((thm) => thm["source-layer"] === "intersections").paint[
      "circle-color"
    ] = [
      "case",
      ["==", this.selectedFeatureId, ["number", ["get", "id"]]],
      "#ffff00",
      [
        "to-color",
        [
          "at",
          ["get", "Total Number of Risk Factors Present"],
          ["literal", this.turbo5],
        ],
      ],
    ];
  }
  render() {
    const appJson = {
      version: 8,
      sources: {
        crashes: {
          type: "vector",
          url: this.crashTiles,
        },
        segments: {
          type: "vector",
          url: this.segmentTiles,
        },
        intersections: {
          type: "vector",
          url: this.intersectionTiles,
        },
      },
      layers: this.theme,
      glyphs: "https://maps.ccrpc.org/fonts/{fontstack}/{range}.pbf",
    };

    return (
      <gl-style
        id="app"
        insertIntoStyle={this.insertIntoStyle}
        insertBeneathLayer={this.insertBeneathLayer}
        json={appJson}
        clickableLayers={[
          "theme-custom-segments",
          "theme-custom-intersections",
        ]}
      ></gl-style>
    );
  }
}
