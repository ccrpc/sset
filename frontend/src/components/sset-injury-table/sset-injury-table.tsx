import { Component, h, Prop } from '@stencil/core';
import { Crash } from '../../BCLIb/models/classes/Crash';
import { Severity } from '../../BCLIb/models/enums';

@Component({
  tag: 'sset-injury-table',
  styleUrl: 'sset-injury-table.css',
  })
export class SsetInjuryTable {

  @Prop() selectedCrashes: Crash[] = [];
  @Prop() endStudyYear!: number;

  crashYears: number[] = [];
  crashTypeLabels: string[] = [];

  componentWillLoad() {
    this.crashTypeLabels = [... new Set(
      this.selectedCrashes
        .map(c => c.crashType)
    )].sort()

    const years = [];

    for (let i = this.endStudyYear-4; i <= this.endStudyYear; i++) 
      years.push(i)

    this.crashYears = years;
  }

  render() {
    if (!this.selectedCrashes.length) return null;

    const totalInjProf = Crash.mergeInjuryProfiles(this.selectedCrashes)

    return (
      <div class="info-tab" id="crash-year-injury">
        <slot/>
        <div class="table-wrapper">
          <table>
            <tr>
              <th></th>
              <th>Total Fatalities</th>
              <th>Total Injuries</th>
              <th>A-Injuries</th>
              <th>B-Injuries</th>
              <th>C-Injuries</th>
              <th>Non-Injuries</th>
            </tr>
            {
              this.crashYears.map(cy => { 
                const injProf = Crash.mergeInjuryProfiles(this.selectedCrashes.filter(c => c.crashYear === cy))
                return (
                  <tr>
                    <th>{cy}</th>
                    <td>{injProf.get(Severity.F)}</td>
                    <td>{injProf.get(Severity.A) + injProf.get(Severity.B) + injProf.get(Severity.C)}</td>
                    <td>{injProf.get(Severity.A)}</td>
                    <td>{injProf.get(Severity.B)}</td>
                    <td>{injProf.get(Severity.C)}</td>
                    <td>{injProf.get(Severity.N)}</td>
                  </tr>
                )})
            }
            <tr class="new-section">
              <th>Total</th>
              <td>{totalInjProf.get(Severity.F)}</td>
              <td>{totalInjProf.get(Severity.A) + totalInjProf.get(Severity.B) + totalInjProf.get(Severity.C)}</td>
              <td>{totalInjProf.get(Severity.A)}</td>
              <td>{totalInjProf.get(Severity.B)}</td>
              <td>{totalInjProf.get(Severity.C)}</td>
              <td>{totalInjProf.get(Severity.N)}</td>
            </tr>
          </table>
        </div>
      </div>
    );
  }

}
