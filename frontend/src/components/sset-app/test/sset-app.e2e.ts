import { newE2EPage } from '@stencil/core/testing';

describe('sset-app', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<sset-app></sset-app>');

    const element = await page.find('sset-app');
    expect(element).toHaveClass('hydrated');
  });
});
