import { newSpecPage } from '@stencil/core/testing';
import { SsetApp } from '../sset-app';

describe('sset-app', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SsetApp],
      html: `<sset-app></sset-app>`,
    });
    expect(page.root).toEqualHtml(`
      <sset-app>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </sset-app>
    `);
  });
});
