import {
  Component,
  Prop,
  State,
  EventEmitter,
  Event,
  getAssetPath,
  Listen,
  h,
} from "@stencil/core";
import { loadCountermeasures } from "../../BCLIb/utils";
import countermeasures from "../../BCLIb/assets/cmf_data/countermeasure_data.json";

import { downloadProjectFile } from "../../docx-export/export";
import { Countermeasure } from "../../BCLIb/models/classes/Countermeasure";
import { Crash } from "../../BCLIb/models/classes/Crash";
import { RoadwayType } from "../../BCLIb/models/enums";
import { ProjectElements } from "../../BCLIb/models/types";
import { IRoadwayElement } from "../../BCLIb/models/interfaces";
import { Intersection } from "../../BCLIb/models/classes/Intersection";
import { Segment } from "../../BCLIb/models/classes/Segment";
import { CRASH_PROP_LABELS, SEG_PROP_LABELS } from "../../BCLIb/models/consts";
import { LngLatBounds, LngLatLike, Map as MapboxGLMap } from "mapbox-gl";
import { LineString } from "geojson";
import { waitForIdle } from "../../docx-export/segments/locationMap";
import {
  HTMLStencilElement,
  OverlayEventDetail,
  modalController,
} from "@ionic/core";

@Component({
  tag: "sset-app",
  styleUrl: "sset-app.sass",
  assetsDirs: ["assets"],
  })
export class SsetApp {
  /** A reference to the webmapgl Map object */
  map?: HTMLGlMapElement;
  /** A reference to the webmapgl app object */
  app?: HTMLGlAppElement;
  tabs?: HTMLIonTabsElement;
  /** A reference to a webmapgl drawer */
  drawer?: HTMLGlDrawerElement;
  /** An array of all of the crashes */
  allCrashes: Crash[];
  /** An array of all of the segments */
  allSegments: Segment[];
  /** An array of all of the intersections */
  allIntersections: Intersection[];
  /** The year the study ends, the last year this tool uses data for. */
  endStudyYear = Number(process.env.STUDY_END_YEAR);

  /** The currently selected feature, the feature whose information is displayed in the drawer */
  @State() selectedFeature: IRoadwayElement;
  /** A map with all of the user-added features
   *
   * The segments would be accessed via studyAreaFeatures.get(RoadwayType.Segment)
   * And that is a IRoadwayElement[].
   */
  @State() studyAreaFeatures: ProjectElements;
  /** A list of all possible countermeasures, loaded from a json file. */
  @State() countermeasures: Countermeasure[] = [];

  /** The url of the crash tiles */
  @Prop() crashTiles = "/tiles/crashes.json";
  /** The url of the segment tiles */
  @Prop() segmentTiles = "/tiles/segments.json";
  /** The url of the intersection tiles */
  @Prop() intersectionTiles = "/tiles/intersections.json";
  /** The latitude of the center of the map when the page is first loaded.*/
  @Prop() lat = 40.1378;
  /** The longitude of the center of the map when the page is first loaded.*/
  @Prop() long = -88.1189;
  /** The zoom of the map when the page is first loaded.*/
  @Prop() zoom = 9.0;

  @Event() addClicked: EventEmitter;

  /** This event fires when a user has clicked on one or more clickable features on the map
   * What counts as "clickable" is defined by sset-style below.
   *
   * @param {CustomEvent} e An event whose detail object contains an array of the features clicked, aka {features: any[]}. The contents of the features object can be found by looking at Mapbox GL.
   */
  @Listen("glClickedFeaturesGathered", { target: "body" })
  async handleClick(e: CustomEvent) {
    let roadwayElem: IRoadwayElement;
    let infraIdLabel: string;

    switch (e.detail.features[0].sourceLayer) {
      case "intersections":
        infraIdLabel = CRASH_PROP_LABELS.INTERSECTION_ID;
        roadwayElem = Intersection.fromFeatureProps(
          e.detail.features[0].properties
        );
        break;
      case "segments":
        infraIdLabel = CRASH_PROP_LABELS.SSET_ID;
        roadwayElem = Segment.fromFeatureProps(e.detail.features[0].properties);
        break;
      default:
        throw new Error(
          `Cannot read feature type "${e.detail.features[0].sourceLayer}"`
        );
    }

    // disable user map controls
    const mapboxGLMap = this.map.map as MapboxGLMap;
    mapboxGLMap.dragPan.disable();
    mapboxGLMap.scrollZoom.disable();
    mapboxGLMap.doubleClickZoom.disable();
    mapboxGLMap.keyboard.disable();

    if (roadwayElem.roadwayType === RoadwayType.Segment) {
      /*
       * Ok, so this part addresses a problem we have with rendered crashes on segments.
       * You see if the crashes are not on-screen when you select a feature then they won't be seen by querySourceFeatures,
       * simply due to a limitation with mapbox GL.
       *
       * So, when a user selects a segment the view is zoomed so that the whole segment is visible.
       * Due to tile boundaries the geometry returned from glClickedFeaturesGathered may only be a sub-segment
       * of the whole segment. Once one is zoomed out a little bit, querySourceFeatures will return more sub-segments
       * of the segment. Then we can just add them together into one LngLatBound and zoom based on that.
       *
       * To make sure we get all of the sub-segments, and thereby a boundary that represents the whole segment,
       * we continually zoom out until we see the whole segment.
       * Specifically, we run a do-while loop where
       * 1. It queries for multiple sub-segments and builds the largest geometry,
       * 2. It checks whether the current map bounds contain that geometry (aka the whole segment is visible)
       * 3. If not, it moves the map so that its created geometry is completely visible
       * As a note this loop may zoom out if necessary but will never zoom in, this is to minimize the user having to zoom.
       *
       * It then repeats so that if more of the segment has been revealed it takes those into account as it builds
       * the now-largest geometry. If its most recent largest geometry is completely in the viewport it knows it is done
       * and the loop exits.
       *
       * There is an additional if statement such that if the the largest geometry is visible when this whole things starts,
       * the map does not move.
       */

      let featureBounds: LngLatBounds;
      let currentBounds: LngLatBounds;

      do {
        const subSegments = mapboxGLMap.querySourceFeatures("app:segments", {
          sourceLayer: "segments",
          filter: [
            "==",
            ["get", SEG_PROP_LABELS.ID],
            ["literal", roadwayElem.id],
          ],
        });

        featureBounds = new LngLatBounds();
        for (const feature of subSegments) {
          for (const coord of (feature.geometry as LineString).coordinates) {
            featureBounds.extend(coord as LngLatLike);
          }
        }

        currentBounds = mapboxGLMap.getBounds();

        if (
          !currentBounds.contains(featureBounds.getNorthEast()) ||
          !currentBounds.contains(featureBounds.getNorthWest()) ||
          !currentBounds.contains(featureBounds.getSouthEast()) ||
          !currentBounds.contains(featureBounds.getSouthWest())
        ) {
          mapboxGLMap.fitBounds(
            [featureBounds.getSouthWest(), featureBounds.getNorthEast()],
            {
              maxZoom: mapboxGLMap.getZoom() - 0.02,
              linear: true,
            }
          );
          await waitForIdle(mapboxGLMap);
        }
      } while (
        !currentBounds.contains(featureBounds.getNorthEast()) ||
        !currentBounds.contains(featureBounds.getNorthWest()) ||
        !currentBounds.contains(featureBounds.getSouthEast()) ||
        !currentBounds.contains(featureBounds.getSouthWest())
      );
    }

    roadwayElem.crashes = [
      ...new Map<number, Crash>(
        this.map.map
          .querySourceFeatures("app:crashes", { sourceLayer: "crashes" })
          .filter(
            (crash: any) =>
              crash.properties[CRASH_PROP_LABELS.SSET_INFRA] ===
                roadwayElem.roadwayType &&
              roadwayElem.id === crash.properties[infraIdLabel]
          )
          .map((c: any) => [
            c.properties[CRASH_PROP_LABELS.ID],
            Crash.fromFeatureProps(c.properties),
          ])
      ).values(),
    ];

    this.selectFeature(roadwayElem);
    // Re-enable user controls
    mapboxGLMap.dragPan.enable();
    mapboxGLMap.scrollZoom.enable();
    mapboxGLMap.keyboard.enable();
    mapboxGLMap.doubleClickZoom.enable();
  }

  /**
   * This function handles when the user clicks on a feature in the sset-study-group-list
   *
   * @param {CustomEvent<IRoadwayElement>} e An event whose event.detail is the feature to select.
   */
  @Listen("groupFeatureClicked", { target: "body" })
  handleListSelect(e: CustomEvent<IRoadwayElement>) {
    this.selectFeature(e.detail);
  }

  /**
   * Handles selecting a feature, including populating its crash data from the map.
   * @param roadwayElem The feature to select
   */
  selectFeature(roadwayElem: IRoadwayElement) {
    this.selectedFeature = roadwayElem;
    this.drawer.open = true;
    this.tabs.select("roadway-info");
  }

  /** Triggered when the user clicks the "add this feature" button
   * Adds the currently selected feature to either the list of included intersections or segments (depending on whether the currently selected feature is a segment or intersection)
   *
   * Does nothing if the currently selected feature is already present.
   */
  @Listen("addClicked", { target: "body" })
  handleAddClick() {
    if (
      this.studyAreaFeatures
        .get(this.selectedFeature.roadwayType)
        .map((ft) => ft.id)
        .includes(this.selectedFeature.id)
    )
      return;

    this.studyAreaFeatures = new Map([
      [
        RoadwayType.Intersection,
        this.selectedFeature.roadwayType === RoadwayType.Intersection
          ? [
              ...this.studyAreaFeatures.get(RoadwayType.Intersection),
              this.selectedFeature,
            ]
          : this.studyAreaFeatures.get(RoadwayType.Intersection),
      ],
      [
        RoadwayType.Segment,
        this.selectedFeature.roadwayType === RoadwayType.Segment
          ? [
              ...this.studyAreaFeatures.get(RoadwayType.Segment),
              this.selectedFeature,
            ]
          : this.studyAreaFeatures.get(RoadwayType.Segment),
      ],
    ]);
  }

  async showCrashDetails() {
    const modal: HTMLIonModalElement = await modalController.create({
      component: "sset-crash-details-modal",
      backdropDismiss: false,
      cssClass: "my-custom-class",
      componentProps: {
        studyAreaFeatures: this.studyAreaFeatures,
      },
    });

    modal
      .onDidDismiss()
      .then(async (modalDetails: OverlayEventDetail<boolean>) => {
        if (modalDetails.data) {
          this.studyAreaFeatures = new Map([
            [
              RoadwayType.Intersection,
              this.studyAreaFeatures.get(RoadwayType.Intersection),
            ],
            [
              RoadwayType.Segment,
              this.studyAreaFeatures.get(RoadwayType.Segment),
            ],
          ]);
        }
      });

    await modal.present();
  }

  componentWillLoad() {
    this.countermeasures = loadCountermeasures(countermeasures);
    this.studyAreaFeatures = new Map([
      [RoadwayType.Intersection, []],
      [RoadwayType.Segment, []],
    ]);
  }

  componentDidLoad() {
    if (this.map.map) {
      this.map.map.setMaxBounds([
        [-88.4909897, 39.8724889],
        [-87.9176502, 40.4140038],
      ]);
    }
  }

  render() {
    return (
      <gl-app
        label="Systemic Safety Evaluation Tool"
        splitPane={true}
        menuLabel="Study Area Elements"
      >
        <img
          class="logo"
          src={getAssetPath("./assets/rpc_logo.jpg")}
          slot="start-buttons"
          style={{
            width: "auto",
          }}
        />

        <ion-buttons slot="end-buttons">
          <ion-button
            onClick={async () => this.showCrashDetails()}
            title="Nighttime / Wet Pavement"
          >
            <ion-icon name="moon-outline"></ion-icon>/
            <ion-icon name="water"></ion-icon>
          </ion-button>
          <ion-button
            onClick={async () => {
              if (this.drawer.open) {
                this.drawer.open = false;
                await waitForIdle(this.map.map);
              }
              downloadProjectFile(
                this.studyAreaFeatures,
                this.endStudyYear,
                this.map
              );
            }}
            title="Download Form"
          >
            <ion-icon name="download-outline"></ion-icon>
          </ion-button>
          <gl-drawer-toggle />
          <gl-fullscreen />
        </ion-buttons>

        <sset-study-group-list
          slot="menu"
          endStudyYear={this.endStudyYear}
          countermeasures={this.countermeasures}
          studyAreaFeatures={this.studyAreaFeatures}
        />
        <gl-map
          ref={(r: HTMLGlMapElement) => (this.map = r)}
          longitude={this.long}
          latitude={this.lat}
          zoom={this.zoom}
          maxzoom={21}
          minzoom={6}
          click-mode="top-feature"
          hash="true"
        >
          <gl-style
            id="basemap"
            url="https://maps.ccrpc.org/basemaps/hybrid-2020/style.json"
            basemap={true}
            name="Basic"
            enabled={true}
          ></gl-style>
          <sset-style
            insertIntoStyle="basemap"
            insertBeneathLayer="admin_sub"
            crash-tiles={this.crashTiles}
            segment-tiles={this.segmentTiles}
            intersection-tiles={this.intersectionTiles}
            selectedFeatureId={this.selectedFeature?.id ?? 0}
          ></sset-style>
        </gl-map>

        <gl-drawer
          slot="after-content"
          drawerTitle="Selected Roadway Info"
          ref={(r: HTMLGlDrawerElement) => (this.drawer = r)}
        >
          <ion-button
            slot="toolbar-end-buttons"
            onClick={() => this.addClicked.emit()}
          >
            <ion-label color="primary">Add Feature</ion-label>
          </ion-button>
          <ion-tabs id="tabs" ref={(r: HTMLIonTabsElement) => (this.tabs = r)}>
            <ion-tab tab="roadway-info">
              <sset-table selectedFeature={this.selectedFeature} />
            </ion-tab>

            <ion-tab tab="risk-factors">
              <sset-risk-factor-table selectedFeature={this.selectedFeature} />
            </ion-tab>

            <ion-tab tab="injury-info">
              {this.selectedFeature?.crashes.length ? (
                <sset-injury-table
                  endStudyYear={this.endStudyYear}
                  selectedCrashes={this.selectedFeature.crashes}
                />
              ) : null}
            </ion-tab>

            <ion-tab tab="crash-info">
              {this.selectedFeature?.crashes.length ? (
                <sset-crash-table
                  endStudyYear={this.endStudyYear}
                  selectedCrashes={this.selectedFeature.crashes}
                />
              ) : null}
            </ion-tab>

            <ion-tab-bar slot="top">
              <ion-tab-button tab="roadway-info">
                <ion-icon name="information-circle-outline"></ion-icon>
                <ion-label>Roadway Info</ion-label>
              </ion-tab-button>

              <ion-tab-button tab="risk-factors">
                <ion-icon name="megaphone-outline"></ion-icon>
                <ion-label>Risk Factors</ion-label>
              </ion-tab-button>

              <ion-tab-button
                tab="injury-info"
                disabled={!this.selectedFeature?.crashes.length}
              >
                <ion-icon name="stats-chart-outline"></ion-icon>
                <ion-label>Injuries</ion-label>
              </ion-tab-button>

              <ion-tab-button
                tab="crash-info"
                disabled={!this.selectedFeature?.crashes.length}
              >
                <ion-icon name="stats-chart-outline"></ion-icon>
                <ion-label>Crashes</ion-label>
              </ion-tab-button>
            </ion-tab-bar>
          </ion-tabs>
        </gl-drawer>
      </gl-app>
    );
  }

  componentDidRender() {
    // The following two lines disable rotation of the map
    if (this.map.map) {
      this.map.map.dragRotate.disable();
      this.map.map.touchZoomRotate.disableRotation();
    }
  }
}
