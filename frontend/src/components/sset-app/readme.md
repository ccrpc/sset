# sset-app



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute            | Description                                                           | Type     | Default                       |
| ------------------- | -------------------- | --------------------------------------------------------------------- | -------- | ----------------------------- |
| `crashTiles`        | `crash-tiles`        | The url of the crash tiles                                            | `string` | `"/tiles/crashes.json"`       |
| `intersectionTiles` | `intersection-tiles` | The url of the intersection tiles                                     | `string` | `"/tiles/intersections.json"` |
| `lat`               | `lat`                | The latitude of the center of the map when the page is first loaded.  | `number` | `40.1378`                     |
| `long`              | `long`               | The longitude of the center of the map when the page is first loaded. | `number` | `-88.1189`                    |
| `segmentTiles`      | `segment-tiles`      | The url of the segment tiles                                          | `string` | `"/tiles/segments.json"`      |
| `zoom`              | `zoom`               | The zoom of the map when the page is first loaded.                    | `number` | `9.0`                         |


## Events

| Event        | Description | Type               |
| ------------ | ----------- | ------------------ |
| `addClicked` |             | `CustomEvent<any>` |


## Dependencies

### Depends on

- gl-app
- ion-buttons
- ion-button
- ion-icon
- gl-drawer-toggle
- gl-fullscreen
- [sset-study-group-list](../sset-study-group-list)
- gl-map
- gl-style
- [sset-style](../sset-style)
- gl-drawer
- ion-label
- ion-tabs
- ion-tab
- [sset-table](../sset-attribute-table)
- [sset-risk-factor-table](../sset-risk-factor-table)
- [sset-injury-table](../sset-injury-table)
- [sset-crash-table](../sset-crash-table)
- ion-tab-bar
- ion-tab-button

### Graph
```mermaid
graph TD;
  sset-app --> gl-app
  sset-app --> ion-buttons
  sset-app --> ion-button
  sset-app --> ion-icon
  sset-app --> gl-drawer-toggle
  sset-app --> gl-fullscreen
  sset-app --> sset-study-group-list
  sset-app --> gl-map
  sset-app --> gl-style
  sset-app --> sset-style
  sset-app --> gl-drawer
  sset-app --> ion-label
  sset-app --> ion-tabs
  sset-app --> ion-tab
  sset-app --> sset-table
  sset-app --> sset-risk-factor-table
  sset-app --> sset-injury-table
  sset-app --> sset-crash-table
  sset-app --> ion-tab-bar
  sset-app --> ion-tab-button
  gl-app --> ion-menu
  gl-app --> ion-header
  gl-app --> ion-toolbar
  gl-app --> ion-title
  gl-app --> ion-content
  gl-app --> ion-menu-toggle
  gl-app --> ion-button
  gl-app --> ion-icon
  gl-app --> ion-buttons
  gl-app --> ion-footer
  gl-app --> ion-split-pane
  gl-app --> ion-app
  ion-menu --> ion-backdrop
  ion-button --> ion-ripple-effect
  gl-drawer-toggle --> ion-button
  gl-drawer-toggle --> ion-icon
  gl-fullscreen --> ion-button
  gl-fullscreen --> ion-icon
  sset-study-group-list --> ion-content
  sset-study-group-list --> ion-list
  sset-study-group-list --> ion-accordion-group
  sset-study-group-list --> ion-list-header
  sset-study-group-list --> ion-label
  sset-study-group-list --> ion-item-divider
  sset-study-group-list --> ion-item
  sset-study-group-list --> ion-icon
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  sset-style --> gl-style
  gl-drawer --> ion-header
  gl-drawer --> ion-toolbar
  gl-drawer --> ion-buttons
  gl-drawer --> ion-title
  gl-drawer --> ion-button
  gl-drawer --> ion-icon
  gl-drawer --> ion-content
  sset-risk-factor-table --> ion-icon
  ion-tab-button --> ion-ripple-effect
  style sset-app fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
