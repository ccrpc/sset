import { Component, Prop, h, Event, EventEmitter, State } from "@stencil/core";
import {
  InputCustomEvent,
  OverlayEventDetail,
  SelectCustomEvent,
  modalController,
} from "@ionic/core";
import { Countermeasure } from "../../BCLIb/models/classes/Countermeasure";
import { Crash } from "../../BCLIb/models/classes/Crash";
import { IRoadwayElement } from "../../BCLIb/models/interfaces";
import {
  CalculatedCounterMeasure,
  calculatedCounterMeasures,
} from "../../BCLIb/calculatedCounterMeasures";
import { Locality, RoadwayType } from "../../BCLIb/models/enums";
import { ProjectElements } from "../../BCLIb/models/types";
import { INTX_PROP_LABELS, SEG_PROP_LABELS } from "../../BCLIb/models/consts";

const formatter = Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});

/**
 * The following counter measures allow the user to change the service life when they are calculated
 */
const variableServiceLifeCountermeasures = [
  "4.8.3.S1.1",
  "4.8.3.S8.1",
  "4.8.3.S12.1",
  "4.8.8.S6.1",
  "4.8.8.S12.1",
  "4.8.9.S6.1",
  "4.8.9.S12.1",
];

const userDefinedCountermeasure = [
  "4.8.101.UD.1",
  "3.5.101.UD.1",
  "2.8.11.UD.1",
  "1.5.101.UD.1",
];

/**
 * This component is the form the user sees when they click on the "add a counter measure button"
 * It is created / rendered by the sset-study-group-list in a modal, several properties are passed into it at that point.
 */

@Component({
  tag: "sset-countermeasure-form",
  styleUrl: "sset-countermeasure-form.css",
  })
export class SsetCountermeasureForm {
  /** A list of all possible counter measures */
  @Prop() countermeasures!: Countermeasure[];
  /** The countermeasure selected from the overview menu on the left. If this is not given (aka we are adding a new counter measure) we create an object for it. */
  @Prop({ mutable: true }) countermeasure: Countermeasure =
    new Countermeasure();
  /** The feature this countermeasure is for (so a segment or intersection) */
  @Prop() feature: IRoadwayElement;
  /** The index of said feature in the list of visible features in the sset-study-group-list */
  @Prop() featureIdx!: number;
  /** The index of this countermeasure in the attached feature's list of counter measures */
  @Prop() countermeasureIdx!: number;

  /** A list of all of the selected features so that we can get the AADT's for calculated countermeasures. */
  @Prop() studyAreaFeatures: ProjectElements;

  @State() calculatedCounterMeasure: CalculatedCounterMeasure;

  /** The number of units (in whichever type of unit fits) */
  @Prop({ mutable: true }) quantity = 0;
  /** The price for each unit. */
  @Prop({ mutable: true }) pricePerUnit = 0;

  @State() tick = {};

  /** The crashes on the feature this counter measure is attached to */
  crashes: Crash[] = [];

  /** Whether the current countermeasure is user defined */
  userDefined: boolean;

  @Event() countermeasureUpdated: EventEmitter;

  /**
   * Opens a model to prompt the user for input for a calculated countermeasure
   */
  async calculateCountermeasure() {
    let aadt: number = undefined;
    const possibleAADT = new Set(
      this.studyAreaFeatures
        .get(RoadwayType.Segment)
        .map(
          (roadwayElement) =>
            roadwayElement.infoProps.get(SEG_PROP_LABELS.AADT) as number
        )
    );

    if (possibleAADT.size === 1) {
      aadt = possibleAADT.values().next().value;
    }

    const modal: HTMLIonModalElement = await modalController.create({
      component: "sset-calculated-countermeasure-modal",
      backdropDismiss: false,
      cssClass: "calculated-modal",
      componentProps: {
        aadt: aadt,
        countermeasure: this.countermeasure,
        userInputs: this.countermeasure.userInputs,
        calculatedCounterMeasure: this.calculatedCounterMeasure,
        possibleAADT: possibleAADT,
      },
    });

    modal.onDidDismiss().then(
      async (
        input: OverlayEventDetail<{
          userInputs: (string | number)[];
          aadtMajor: number;
        }>
      ) => {
        if (input.data) {
          this.countermeasure.aadtMajor = input.data.aadtMajor;
          this.countermeasure.userInputs = input.data.userInputs;
          this.calculateCMF();
        }
      }
    );

    await modal.present();
  }

  /**
   * Opens a model to allow the user to define a countermeasure
   */
  async queryUserForCountermeasure(useExisting: boolean) {
    const modelProps = {
      projectType: this.feature.roadwayType,
      projectSystem:
        this.feature.locality === Locality.Local
          ? "Local"
          : this.feature.locality === Locality.State
            ? "State"
            : "",
    };

    if (useExisting) {
      Object.assign(modelProps, {
        name: this.countermeasure.name,
        code: this.countermeasure.code,
        category: this.countermeasure.category,
        serviceLife: this.countermeasure.serviceLife,
        cmfVector: this.countermeasure.cmfVector,
        settingType: this.countermeasure.settingType,
        facilityType: this.countermeasure.facilityType,
        units: this.countermeasure.units,
        affectedCrashTypes: this.countermeasure.affectedCrashTypes,
      });
    }

    const modal: HTMLIonModalElement = await modalController.create({
      component: "sset-user-defined-countermeasure-modal",
      backdropDismiss: true,
      cssClass: "user-defined-modal",
      componentProps: modelProps,
    });

    modal
      .onDidDismiss()
      .then(async (input: OverlayEventDetail<Countermeasure>) => {
        if (input.data) {
          this.countermeasure = input.data;
          this.userDefined = true;
          this.checkWhetherCalculated(true); // A user defined countermeasure will never be calculated; this call is to make sure things update
        }
      });

    await modal.present();
  }

  /**
   * Checks whether the current countermeasures is calculated and adjusts the popup
   * as necessary.
   * @param newCountermeasure Whether this form previously had a countermeasure and now has a different one
   * Most likely caused by selecting a new calculated countermeasure from the drop-down.
   */
  checkWhetherCalculated(newCountermeasure: boolean) {
    if (
      calculatedCounterMeasures
        .map((calcCM: CalculatedCounterMeasure) => calcCM.code)
        .includes(this.countermeasure.code)
    ) {
      this.calculatedCounterMeasure = calculatedCounterMeasures.filter(
        (calcCM: CalculatedCounterMeasure) =>
          calcCM.code === this.countermeasure.code
      )[0];

      if (
        !newCountermeasure &&
        this.countermeasure.userInputs &&
        this.countermeasure.userInputs.length > 0
      ) {
        // The countermeasure already has user inputs, likely due to being pulled up from the
        // already-added countermeasures
        return;
      }

      this.countermeasure.userInputs = [];

      let needAADT = false;
      let possibleAADT = undefined;
      let aadt: number = undefined;
      if (this.calculatedCounterMeasure.aadtMajorRequired) {
        this.countermeasure.aadtMajor = undefined;
        if (this.countermeasure.code.charAt(0) === "3") {
          // This means the countermeasure is for an intersection.
          // For intersection Min said we use the highest AADT of selected intersection.
          this.countermeasure.aadtMajor = Math.max(
            ...this.studyAreaFeatures
              .get(RoadwayType.Intersection)
              .map(
                (roadwayElement) =>
                  roadwayElement.infoProps.get(
                    INTX_PROP_LABELS.AADT_MAJOR
                  ) as number
              )
          );
        } else {
          // This calculated countermeasure is for a segment.
          // If all of the segments have the same AADT we will use that, otherwise allow the user to select the AADT to use
          possibleAADT = new Set(
            this.studyAreaFeatures
              .get(RoadwayType.Segment)
              .map(
                (roadwayElement) =>
                  roadwayElement.infoProps.get(SEG_PROP_LABELS.AADT) as number
              )
          );

          if (possibleAADT.size === 1) {
            aadt = possibleAADT.values().next().value;
          }
        }
        needAADT = this.countermeasure.aadtMajor === undefined;
      }

      if (this.calculatedCounterMeasure.aadtMinorRequired) {
        this.countermeasure.aadtMinor = Math.max(
          ...this.studyAreaFeatures
            .get(RoadwayType.Intersection)
            .map(
              (roadwayElement) =>
                roadwayElement.infoProps.get(
                  INTX_PROP_LABELS.AADT_MINOR
                ) as number
            )
        );
      }

      if (this.calculatedCounterMeasure.numberOfInputs === 0 && !needAADT) {
        // Calculated the CmfVector immediately as we do not need to wait for user input
        this.countermeasure.aadtMajor = aadt;
        this.calculateCMF();
      } else {
        this.calculateCountermeasure();
      }
    } else {
      this.calculatedCounterMeasure = undefined;
    }
  }

  componentWillLoad() {
    this.crashes = this.feature.crashes;
    this.userDefined = this.countermeasure?.code
      ? userDefinedCountermeasure.includes(this.countermeasure.code)
      : false;
    this.checkWhetherCalculated(false);
  }

  render() {
    return [
      <ion-content
        style={{
          position: "absolute",
          height: "100%",
          width: "100%",
        }}
      >
        <ion-grid>
          <ion-row>
            <ion-col size={9}>
              <ion-item>
                <ion-select
                  name="cm-select"
                  interface="popover"
                  label="Countermeasure"
                  label-placement="stacked"
                  value={this.countermeasure}
                  onIonChange={async (
                    event: SelectCustomEvent<Countermeasure>
                  ) => {
                    let oldCode: string = undefined;
                    if (this.countermeasure) {
                      oldCode = this.countermeasure.code;
                    }
                    const newCountermeasure: Countermeasure = event.detail
                      .value as Countermeasure;
                    if (
                      newCountermeasure.name === "User Defined Countermeasure"
                    ) {
                      await this.queryUserForCountermeasure(false);
                    } else {
                      this.countermeasure = newCountermeasure;
                      this.userDefined = false;
                      this.checkWhetherCalculated(
                        !(oldCode === this.countermeasure.code)
                      );
                    }
                  }}
                >
                  {this.getSelectOptions()}
                </ion-select>
              </ion-item>
            </ion-col>

            <ion-col size={3}>
              <ion-item>
                <ion-input
                  label="Service Life"
                  label-placement="stacked"
                  value={this.countermeasure.serviceLife}
                  readonly
                />
              </ion-item>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col size={3}>
              <ion-item>
                <ion-input
                  type="number"
                  inputmode="decimal"
                  min={0}
                  value={this.pricePerUnit}
                  onInput={(e: InputCustomEvent) => {
                    this.pricePerUnit = e.target.value as number;
                  }}
                  label="Unit Cost $"
                  label-placement="stacked"
                ></ion-input>
              </ion-item>
            </ion-col>

            <ion-col size={3}>
              <ion-item>
                <ion-input
                  type="number"
                  inputmode="decimal"
                  min={0}
                  value={this.quantity}
                  onInput={(e: InputCustomEvent) => {
                    this.quantity = e.target.value as number;
                  }}
                  label="Quantity"
                  label-placement="stacked"
                />
              </ion-item>
            </ion-col>

            <ion-col size={3}>
              <ion-item>
                <ion-input
                  value={this.countermeasure.units}
                  readonly
                  label="Units"
                  label-placement="stacked"
                />
              </ion-item>
            </ion-col>

            <ion-col size={3}>
              <ion-item>
                <ion-input
                  value={formatter.format(this.pricePerUnit * this.quantity)}
                  readonly
                  label-placement="stacked"
                  label="Total Cost"
                />
              </ion-item>
            </ion-col>
          </ion-row>
          {this.calculatedCounterMeasure !== undefined ? (
            [
              <ion-row>
                {this.calculatedCounterMeasure.aadtMajorRequired ? (
                  <ion-col size={3}>
                    <ion-item>
                      <ion-label position="stacked">AADT Used</ion-label>
                      <ion-label position="stacked">
                        {this.countermeasure.aadtMajor}
                      </ion-label>
                    </ion-item>
                  </ion-col>
                ) : (
                  ""
                )}
                {Array.from(
                  Array(this.calculatedCounterMeasure.numberOfInputs).keys()
                ).map((index: number) => {
                  return (
                    <ion-col size={3}>
                      <ion-item>
                        <ion-label position="stacked">{`User input ${
                          index + 1
                        }`}</ion-label>
                        <ion-label position="stacked">
                          {this.countermeasure.userInputs[index]
                            ? this.countermeasure.userInputs[index]
                            : undefined}
                        </ion-label>
                      </ion-item>
                    </ion-col>
                  );
                })}
                {(this.calculatedCounterMeasure.aadtMajorRequired ? 3 : 0) +
                  this.calculatedCounterMeasure.numberOfInputs * 3 <=
                9 ? (
                      <ion-col
                        size={3}
                        offset={Math.max(
                          9 -
                        (this.calculatedCounterMeasure.aadtMajorRequired
                          ? 3
                          : 0) -
                        this.calculatedCounterMeasure.numberOfInputs * 3,
                          0
                        )}
                      >
                        <ion-button
                          id="recalculate"
                          onClick={() => {
                            this.calculateCountermeasure();
                          }}
                        >
                      Recalculate
                        </ion-button>
                      </ion-col>
                    ) : null}
              </ion-row>,
              (this.calculatedCounterMeasure.aadtMajorRequired ? 3 : 0) +
                this.calculatedCounterMeasure.numberOfInputs * 3 >
              9 ? (
                    <ion-row class="ion-justify-content-end">
                      <ion-col size={3}>
                        <ion-button
                          id="recalculate"
                          onClick={() => {
                            this.calculateCountermeasure();
                          }}
                        >
                      Recalculate
                        </ion-button>
                      </ion-col>
                    </ion-row>
                  ) : null,
            ]
          ) : this.userDefined ? (
            <ion-row class="ion-justify-content-between">
              <ion-col style={{ "align-items": "center", display: "flex" }}>
                {this.countermeasure.code} - {this.countermeasure.name}
              </ion-col>

              <ion-col size={2}>
                <ion-button
                  id="editUserDefined"
                  onClick={() => {
                    this.queryUserForCountermeasure(true);
                  }}
                >
                  Edit
                </ion-button>
              </ion-col>
            </ion-row>
          ) : (
            ""
          )}

          {[...(this.countermeasure.cmfVector?.entries() ?? [])]
            .filter((e) => e[1] !== 1)
            .map((entry) => {
              return (
                <ion-row>
                  <ion-col size={6}>
                    <ion-item>
                      <ion-input
                        value={entry[0]}
                        readonly
                        label-placement="stacked"
                        label="Crash Type Affected"
                      />
                    </ion-item>
                  </ion-col>

                  <ion-col size={3}>
                    <ion-item>
                      <ion-input
                        value={entry[1]}
                        readonly
                        label-placement="stacked"
                        label="CMF"
                      />
                    </ion-item>
                  </ion-col>
                </ion-row>
              );
            })}
        </ion-grid>
        <ion-button
          expand="full"
          style={{
            position: "sticky",
            bottom: 0,
            width: "100%",
            zIndex: 10000,
          }}
          onClick={() => {
            if (this.countermeasure) {
              this.countermeasureUpdated.emit({
                featureIdx: this.featureIdx,
                roadwayType: this.feature.roadwayType,
                countermeasureIdx: this.countermeasureIdx,
                countermeasure: {
                  countermeasure: this.countermeasure,
                  unitCost: this.pricePerUnit,
                  quantity: this.quantity,
                },
              });
            }
          }}
        >
          Update
        </ion-button>
      </ion-content>,
    ];
  }

  calculateCMF(): void {
    if (typeof this.calculatedCounterMeasure === "undefined") {
      console.log("Calculated Countermeasure not defined, returning");
      return;
    }
    if (this.calculatedCounterMeasure.numberOfInputs > 0) {
      for (
        let i = 0;
        i < this.calculatedCounterMeasure.inputTypes.length;
        i++
      ) {
        if (
          this.calculatedCounterMeasure.inputTypes[i] === "number" &&
          this.countermeasure.userInputs[i]
        ) {
          this.countermeasure.userInputs[i] = Number(
            this.countermeasure.userInputs[i]
          );
        }
      }
    }
    try {
      this.calculatedCounterMeasure.validate(
        this.countermeasure.userInputs,
        this.countermeasure.aadtMajor,
        this.countermeasure.aadtMinor
      );
      this.countermeasure.cmfVector = this.calculatedCounterMeasure.calculate(
        this.countermeasure.userInputs,
        this.countermeasure.aadtMajor,
        this.countermeasure.aadtMinor
      );
    } catch (error) {
      alert(`${error.name} - ${error.message}`);
      return;
    }

    if (variableServiceLifeCountermeasures.includes(this.countermeasure.code)) {
      const serviceLifeIndex =
        this.calculatedCounterMeasure.inputPrompts.findIndex((element) =>
          typeof element === "string"
            ? element.startsWith("Effective Service Life")
            : false
        );
      if (serviceLifeIndex !== -1) {
        this.countermeasure.serviceLife = this.countermeasure.userInputs[
          serviceLifeIndex
        ] as number;
      } else {
        alert(
          "Countermeasure is marked as having a variable service life, but service life could not be found\nPlease contact CCRPC"
        );
      }
    }

    this.tick = {};
  }

  /**
   * Selecting the countermeasures the user can choose from, filtering by countermeasures either for intersections or segments,
   * sorting by category based on the starting part of the categories code, then sorting by code of the countermeasure
   * @returns
   */
  getSelectOptions(): HTMLIonSelectOptionElement[] {
    const typedCountermeasures = this.countermeasures.filter(
      (cms: Countermeasure) =>
        this.feature.roadwayType === cms.projectType &&
        cms.projectSystem === "Local"
    );
    const letUserDefine: Countermeasure = new Countermeasure(
      "User Defined Countermeasure"
    );
    const userDefinedOption: HTMLIonSelectOptionElement = (
      <ion-select-option value={letUserDefine}>
        {letUserDefine.name}
      </ion-select-option>
    );
    const listOfOptions: HTMLIonSelectOptionElement[] = [
      ...new Set(
        typedCountermeasures.map((cm: Countermeasure): string => cm.category)
      ),
    ]
      .map((category: string): [string, string] => {
        const codeArray = typedCountermeasures
          .filter((cm) => cm.category === category)[0]
          .code.split(".");
        return [`${codeArray[0]}.${codeArray[1]}`, category];
      })
      .sort(
        (
          categoryTupleA: [string, string],
          categoryTupleB: [string, string]
        ): number => sortViaCode(categoryTupleA[0], categoryTupleB[0])
      )
      .map((categoryTuple: [string, string]): HTMLIonSelectOptionElement[] => {
        return [
          <ion-select-option
            disabled
          >{`${categoryTuple[0]} - ${categoryTuple[1]}`}</ion-select-option>,
          typedCountermeasures
            .filter((cm) => cm.category === categoryTuple[1])
            .sort((a, b) => sortViaCode(a.code, b.code))
            .map((cm) => (
              <ion-select-option value={cm}>
                {cm.code} - {cm.name}
              </ion-select-option>
            )),
        ];
      })
      .flat();

    listOfOptions.push(userDefinedOption);
    return listOfOptions;
  }
}

/**
 * A helper function to be passed into Array.prototype.sort.
 *
 * It sorts via a countermeasure code, getting the number aspect of each part and comparing them until it finds one that is less or more than the other.
 * @param a The code of the first object
 * @param b The code of the second object
 * @returns
 */
function sortViaCode(codeA: string, codeB: string): number {
  const codePartA: (number | string)[] = codeA.split(".");
  const codeATypes: string[] = Array<string>(codePartA.length);
  const codePartB: (number | string)[] = codeB.split(".");
  const codeBTypes: string[] = Array<string>(codePartB.length);

  for (let index = 0; index < codePartA.length; index++) {
    const numericAttempt = Number(codePartA[index]);
    if (!Number.isNaN(numericAttempt)) {
      codePartA[index] = numericAttempt;
      codeATypes[index] = "number";
    }
    codeATypes[index] = "string";
  }

  for (let index = 0; index < codePartB.length; index++) {
    const numericAttempt = Number(codePartB[index]);
    if (!Number.isNaN(numericAttempt)) {
      codePartB[index] = numericAttempt;
      codeBTypes[index] = "number";
    }
    codeBTypes[index] = "string";
  }

  for (
    let index = 0;
    index < codePartA.length && index < codePartB.length;
    index++
  ) {
    if (codeATypes[index] === "number") {
      if (codeBTypes[index] === "number") {
        // At this index both are numbers, we can compare them directly
        if ((codePartA[index] as number) > (codePartB[index] as number)) {
          return 1;
        } else if (
          (codePartA[index] as number) < (codePartB[index] as number)
        ) {
          return -1;
        }
      } else {
        // CodeB at this index is a string so we will need to convert CodeA at this index to a string and compare the two strings
        if (String(codePartA[index] as number) > (codePartB[index] as string)) {
          return 1;
        } else if (
          String(codePartA[index] as number) < (codePartB[index] as string)
        ) {
          return -1;
        }
      }
    } else {
      if (codeBTypes[index] === "number") {
        // a is string, be is number, convert b
        if ((codePartA[index] as string) > String(codePartB[index] as number)) {
          return 1;
        } else if (
          (codePartA[index] as string) < String(codePartB[index] as number)
        ) {
          return -1;
        }
      } else {
        // Both are a string at this index, just compare them directly
        if ((codePartA[index] as string) > (codePartB[index] as string)) {
          return 1;
        } else if (
          (codePartA[index] as string) < (codePartB[index] as string)
        ) {
          return -1;
        }
      }
    }
  }

  return 0;
}
