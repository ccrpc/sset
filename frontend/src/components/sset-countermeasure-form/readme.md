# sset-page-countermeasure-selection



<!-- Auto Generated Below -->


## Overview

This component is the form the user sees when they click on the "add a counter measure button"
It is created / rendered by the sset-study-group-list in a modal, several properties are passed into it at that point.

## Properties

| Property                         | Attribute            | Description                                                                                                                                                | Type                                  | Default                |
| -------------------------------- | -------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------- | ---------------------- |
| `countermeasure`                 | --                   | The countermeasure selected from the overview menu on the left. If this is not given (aka we are adding a new counter measure) we create an object for it. | `Countermeasure`                      | `new Countermeasure()` |
| `countermeasureIdx` _(required)_ | `countermeasure-idx` | The index of this countermeasure in the attached feature's list of counter measures                                                                        | `number`                              | `undefined`            |
| `countermeasures` _(required)_   | --                   | A list of all possible counter measures                                                                                                                    | `Countermeasure[]`                    | `undefined`            |
| `feature`                        | --                   | The feature this countermeasure is for (so a segment or intersection)                                                                                      | `IRoadwayElement`                     | `undefined`            |
| `featureIdx` _(required)_        | `feature-idx`        | The index of said feature in the list of visible features in the sset-study-group-list                                                                     | `number`                              | `undefined`            |
| `pricePerUnit`                   | `price-per-unit`     | The price for each unit.                                                                                                                                   | `number`                              | `0`                    |
| `quantity`                       | `quantity`           | The number of units (in whichever type of unit fits)                                                                                                       | `number`                              | `0`                    |
| `studyAreaFeatures`              | --                   | A list of all of the selected features so that we can get the AADT's for calculated countermeasures.                                                       | `Map<RoadwayType, IRoadwayElement[]>` | `undefined`            |


## Events

| Event                   | Description | Type               |
| ----------------------- | ----------- | ------------------ |
| `countermeasureUpdated` |             | `CustomEvent<any>` |


## Dependencies

### Depends on

- ion-content
- ion-grid
- ion-row
- ion-col
- ion-item
- ion-select
- ion-input
- ion-label
- ion-button
- ion-select-option

### Graph
```mermaid
graph TD;
  sset-countermeasure-form --> ion-content
  sset-countermeasure-form --> ion-grid
  sset-countermeasure-form --> ion-row
  sset-countermeasure-form --> ion-col
  sset-countermeasure-form --> ion-item
  sset-countermeasure-form --> ion-select
  sset-countermeasure-form --> ion-input
  sset-countermeasure-form --> ion-label
  sset-countermeasure-form --> ion-button
  sset-countermeasure-form --> ion-select-option
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-select --> ion-select-popover
  ion-select --> ion-popover
  ion-select --> ion-action-sheet
  ion-select --> ion-alert
  ion-select --> ion-icon
  ion-select-popover --> ion-item
  ion-select-popover --> ion-checkbox
  ion-select-popover --> ion-radio-group
  ion-select-popover --> ion-radio
  ion-select-popover --> ion-list
  ion-select-popover --> ion-list-header
  ion-select-popover --> ion-label
  ion-popover --> ion-backdrop
  ion-action-sheet --> ion-backdrop
  ion-action-sheet --> ion-icon
  ion-action-sheet --> ion-ripple-effect
  ion-alert --> ion-ripple-effect
  ion-alert --> ion-backdrop
  ion-input --> ion-icon
  ion-button --> ion-ripple-effect
  style sset-countermeasure-form fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
