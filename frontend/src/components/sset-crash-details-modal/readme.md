# sset-crash-details-modal



<!-- Auto Generated Below -->


## Properties

| Property            | Attribute | Description | Type                                  | Default     |
| ------------------- | --------- | ----------- | ------------------------------------- | ----------- |
| `studyAreaFeatures` | --        |             | `Map<RoadwayType, IRoadwayElement[]>` | `undefined` |


## Dependencies

### Depends on

- ion-toggle
- ion-header
- ion-toolbar
- ion-title
- ion-label
- ion-buttons
- ion-button
- ion-content
- ion-footer

### Graph
```mermaid
graph TD;
  sset-crash-details-modal --> ion-toggle
  sset-crash-details-modal --> ion-header
  sset-crash-details-modal --> ion-toolbar
  sset-crash-details-modal --> ion-title
  sset-crash-details-modal --> ion-label
  sset-crash-details-modal --> ion-buttons
  sset-crash-details-modal --> ion-button
  sset-crash-details-modal --> ion-content
  sset-crash-details-modal --> ion-footer
  ion-toggle --> ion-icon
  ion-button --> ion-ripple-effect
  style sset-crash-details-modal fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
