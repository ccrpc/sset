import { Component, Prop, h, Host, Element } from "@stencil/core";
import { ProjectElements } from "../../BCLIb/models/types";
import { RoadwayType } from "../../BCLIb/models/enums";
import { ToggleChangeEventDetail } from "@ionic/core";

@Component({
  tag: "sset-crash-details-modal",
  styleUrl: "sset-crash-details-modal.sass",
  shadow: true,
})
export class SsetCrashDetailsModal {
  @Element() el: HTMLElement;
  @Prop() studyAreaFeatures: ProjectElements;

  // Toggles if of the form RoadwayType, Feature Index, CrashIndex -> toggle reference
  toggles: Map<
    RoadwayType,
    Map<number, Map<number, Map<string, HTMLIonToggleElement>>>
  >;

  /** Closes the modal this component assumes it is in */
  async closeModal(update: boolean): Promise<void> {
    await (this.el.closest("ion-modal") as HTMLIonModalElement).dismiss(update);
  }

  setToggle(
    roadwayType: RoadwayType,
    featureIndex: number,
    crashIndex: number,
    type: string,
    el: HTMLIonToggleElement
  ) {
    if (this.toggles.get(roadwayType)) {
      if (this.toggles.get(roadwayType).get(featureIndex)) {
        if (this.toggles.get(roadwayType).get(featureIndex).get(crashIndex)) {
          this.toggles
            .get(roadwayType)
            .get(featureIndex)
            .get(crashIndex)
            .set(type, el);
        } else {
          this.toggles
            .get(roadwayType)
            .get(featureIndex)
            .set(crashIndex, new Map([[type, el]]));
        }
      } else {
        this.toggles
          .get(roadwayType)
          .set(featureIndex, new Map([[crashIndex, new Map([[type, el]])]]));
      }
    } else {
      this.toggles.set(
        roadwayType,
        new Map([
          [featureIndex, new Map([[crashIndex, new Map([[type, el]])]])],
        ])
      );
    }
  }

  toggleOtherType(
    roadwayType: RoadwayType,
    featureIndex: number,
    crashIndex: number,
    type: string
  ) {
    if (this.toggles.get(roadwayType)) {
      if (this.toggles.get(roadwayType).get(featureIndex)) {
        if (this.toggles.get(roadwayType).get(featureIndex).get(crashIndex)) {
          const otherToggle = this.toggles
            .get(roadwayType)
            .get(featureIndex)
            .get(crashIndex)
            .get(type === "WP" ? "NGT" : "WP");
          if (otherToggle) {
            otherToggle.checked = false;
          }
        }
      }
    }
  }

  toggleAll(
    conditionType: string,
    toggles: Map<
      RoadwayType,
      Map<number, Map<number, Map<string, HTMLIonToggleElement>>>
    >
  ) {
    for (const [roadwayType, innerMap] of toggles) {
      for (const [featureIndex, innerInnerMap] of innerMap) {
        for (const [crashIndex, innerInnerInnerMap] of innerInnerMap) {
          for (const [type, toggleReference] of innerInnerInnerMap) {
            if (!toggleReference.disabled) {
              if (conditionType === "off") {
                toggleReference.checked = false;
              } else if (type === conditionType) {
                toggleReference.checked = true;
                this.toggleOtherType(
                  roadwayType,
                  featureIndex,
                  crashIndex,
                  type
                );
              }
            }
          }
        }
      }
    }
  }

  updateCrashes(
    proj: ProjectElements,
    toggles: Map<
      RoadwayType,
      Map<number, Map<number, Map<string, HTMLIonToggleElement>>>
    >
  ) {
    for (const [roadwayType, innerMap] of toggles) {
      for (const [featureIndex, innerInnerMap] of innerMap) {
        for (const [crashIndex, innerInnerInnerMap] of innerInnerMap) {
          for (const [type, toggleReference] of innerInnerInnerMap) {
            if (type === "NGT") {
              proj.get(roadwayType)[featureIndex].crashes[
                crashIndex
              ].countAsNightTime = toggleReference.checked;
            } else if (type === "WP") {
              proj.get(roadwayType)[featureIndex].crashes[
                crashIndex
              ].countAsWetPavement = toggleReference.checked;
            }
          }
        }
      }
    }
  }

  crashDetailTable(type: RoadwayType, proj: ProjectElements) {
    return proj.get(type).flatMap((roadwayElem, featureIndex) => {
      return roadwayElem.crashes.map((crash, crashIndex) => {
        return (
          <tr>
            <td>{roadwayElem.roadwayType}</td>
            <td>{roadwayElem.id}</td>
            <td>{crash.id}</td>
            <td>{crash.crashYear}</td>
            <td>{crash.crashMonth}</td>
            <td>{crash.crashDay}</td>
            <td>{crash.crashHour}</td>
            <td>{crash.crashWeekday}</td>
            <td>{crash.numberOfVehicles}</td>
            <td>{crash.crashSeverity}</td>
            <td>{crash.crashType}</td>
            <td>{crash.weatherCondition}</td>
            <td>{crash.lightCondition}</td>
            <td>{crash.surfaceCondition}</td>
            <td>
              <ion-toggle
                checked={crash.countAsNightTime}
                ref={(el: HTMLIonToggleElement) => {
                  this.setToggle(type, featureIndex, crashIndex, "NGT", el);
                }}
                disabled={crash.lightCondition !== "Darkness"}
                onIonChange={(event: CustomEvent<ToggleChangeEventDetail>) => {
                  if (event.detail.checked) {
                    this.toggleOtherType(type, featureIndex, crashIndex, "NGT");
                  }
                }}
              ></ion-toggle>
            </td>
            <td>
              <ion-toggle
                checked={crash.countAsWetPavement}
                ref={(el: HTMLIonToggleElement) => {
                  this.setToggle(type, featureIndex, crashIndex, "WP", el);
                }}
                disabled={crash.surfaceCondition === "Dry"}
                onIonChange={(event: CustomEvent<ToggleChangeEventDetail>) => {
                  if (event.detail.checked) {
                    this.toggleOtherType(type, featureIndex, crashIndex, "WP");
                  }
                }}
              ></ion-toggle>
            </td>
          </tr>
        );
      });
    });
  }

  componentWillLoad() {
    this.toggles = new Map([
      [RoadwayType.Intersection, new Map()],
      [RoadwayType.Segment, new Map()],
    ]);
  }

  render() {
    return (
      <Host>
        <ion-header>
          <ion-toolbar color="secondary">
            <ion-title>Crash Details</ion-title>
            <ion-label slot="end">Toggle All: </ion-label>
            <ion-buttons slot="end">
              <ion-button
                fill="outline"
                onClick={() => this.toggleAll("NGT", this.toggles)}
              >
                NGT
              </ion-button>
              <ion-button
                fill="outline"
                onClick={() => this.toggleAll("WP", this.toggles)}
              >
                WP
              </ion-button>
              <ion-button
                fill="outline"
                onClick={() => this.toggleAll("off", this.toggles)}
              >
                Off
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-header>
        <ion-content>
          <div class="table-wrapper">
            <table>
              <tr>
                <th>Feature Type</th>
                <th>Feature ID</th>
                <th>Crash ID</th>
                <th>Year</th>
                <th>Month</th>
                <th>Day</th>
                <th>Hour</th>
                <th>Day of Week</th>
                <th>Number of Vehicles</th>
                <th>Crash Severity</th>
                <th>Collision Type</th>
                <th>Weather</th>
                <th>Lighting</th>
                <th>Surface Condition</th>
                <th>NGT</th>
                <th>WP</th>
              </tr>
              {this.crashDetailTable(
                RoadwayType.Intersection,
                this.studyAreaFeatures
              )}
              {this.crashDetailTable(
                RoadwayType.Segment,
                this.studyAreaFeatures
              )}
            </table>
          </div>
        </ion-content>
        <ion-footer>
          <ion-toolbar>
            <ion-buttons slot="end">
              <ion-button onClick={async () => await this.closeModal(false)}>
                Cancel
              </ion-button>
              <ion-button
                onClick={async () => {
                  this.updateCrashes(this.studyAreaFeatures, this.toggles);
                  await this.closeModal(true);
                }}
              >
                Save
              </ion-button>
            </ion-buttons>
          </ion-toolbar>
        </ion-footer>
      </Host>
    );
  }
}
