import { Component, h, Prop } from '@stencil/core';
import { Segment } from '../../BCLIb/models/classes/Segment';
import { SEG_PROP_LABELS } from '../../BCLIb/models/consts';
import { RoadwayType } from '../../BCLIb/models/enums';
import { IRoadwayElement } from '../../BCLIb/models/interfaces';

@Component({
  tag: 'sset-risk-factor-table',
  styleUrl: 'sset-risk-factor-table.css',
})
export class SsetRiskFactorTable {

  @Prop() selectedFeature: IRoadwayElement;

  segmentRiskTable = () => {
    (this.selectedFeature as Segment).riskFactors
    return (
      <div class="info-tab">
        <slot/>
        <div class="table-wrapper">
          <table>
            <tr class="header-row">
              <th></th>
              <th>Value</th>
              <th>Critical Range</th>
              <th>Risk Factor Present</th>
            </tr>
            <tr>
              <th>Functional Classification</th>
              <td>{this.selectedFeature.infoProps.get(SEG_PROP_LABELS.FUNCTIONAL_CLASSIFICATION)}</td>
              <td>Major Collector</td>
              <td class="risk-factor-present">
                {
                  this.selectedFeature.riskFactors.get(SEG_PROP_LABELS.RISK_FUNCTIONAL_CLASSIFICATION)
                    ? <ion-icon class="yellow" name="alert-circle"/> : ""
                }
              </td>
            </tr>
            <tr>
              <th>AADT</th>
              <td>{this.selectedFeature.infoProps.get("AADT")}</td>
              <td>{'>5,000'}</td>
              <td class="risk-factor-present">
                {
                  this.selectedFeature.riskFactors.get("Risk Factor Present - AADT")
                    ? <ion-icon class="yellow" name="alert-circle"/> : ""
                }
              </td>
            </tr>
            <tr>
              <th>Paved and unpaved shoulder width</th>
              <td>
                {
                  this.selectedFeature.infoProps.get("Paved Shoulder Width (ft)")
                  + ', ' +
                  this.selectedFeature.infoProps.get("Unpaved Shoulder Width (ft)")
                }
              </td>
              <td>0, 3</td>
              <td class="risk-factor-present">
                {
                  this.selectedFeature.riskFactors.get("Risk Factor Present - Shoulder Width")
                    ? <ion-icon class="yellow" name="alert-circle"/> : ""
                }
              </td>
            </tr>
            <tr>
              <th>Roadway access point density</th>
              <td>{this.selectedFeature.infoProps.get("Access Point Density")}</td>
              <td>1</td>
              <td class="risk-factor-present">
                {
                  this.selectedFeature.riskFactors.get("Risk Factor Present - Access Point Density")
                    ? <ion-icon class="yellow" name="alert-circle"/> : ""
                }
              </td>
            </tr>
            <tr>
              <th>Overtaking demand</th>
              <td>{this.selectedFeature.infoProps.get("Overtaking Demand")}</td>
              <td>3</td>
              <td class="risk-factor-present">
                {
                  this.selectedFeature.riskFactors.get("Risk Factor Present - Overtaking Demand")
                    ? <ion-icon class="yellow" name="alert-circle"/> : ""
                }
              </td>
            </tr>
            <tr>
              <th>Speed limit (mph)</th>
              <td>{this.selectedFeature.infoProps.get("Speed Limit (mph)")}</td>
              <td>{'>45'}</td>
              <td class="risk-factor-present">
                {
                  this.selectedFeature.riskFactors.get("Risk Factor Present - Speed Limit")
                    ? <ion-icon class="yellow" name="alert-circle"/> : ""
                }
              </td>
            </tr>
            <tr>
              <th>Total number of risk factors present</th>
              <td></td>
              <td></td>
              <td class="risk-factor-present">
                {
                  [...Array(this.selectedFeature.riskFactors.get("Total Number of Risk Factors Present")).keys()]
                    .map(_ => <ion-icon class="yellow" name="alert-circle"/>)
                }
              </td>
            </tr>
          </table>
        </div>
      </div>
    )
  }

  intersectionRiskTable = () => {
    return (
      <div class="info-tab" id="risk-table">
        <slot/>
        <div class="table-wrapper">
          <table id="risk-table">
            <tr class="header-row">
              <th></th>
              <th>Value</th>
              <th>Critical Range</th>
              <th>Risk Factor Present</th>
            </tr>
            <tr>
              <th>Intersection Type</th>
              <td>{this.selectedFeature.riskFactors.get("Intersection Type")}</td>
              <td>42ST</td>
              <td class="risk-factor-present">
                {
                  this.selectedFeature.riskFactors.get("Risk Factor Present - Intersection Type")
                    ? <ion-icon class="yellow" name="alert-circle"/> : ""
                }
              </td>
            </tr>
            <tr>
              <th>Skew angle category</th>
              <td>{this.selectedFeature.riskFactors.get("Skew Angle")}</td>
              <td>2</td>
              <td class="risk-factor-present">
                {
                  this.selectedFeature.riskFactors.get("Risk Factor Present - Skew Angle")
                    ? <ion-icon class="yellow" name="alert-circle"/> : ""
                }
              </td>
            </tr>
            <tr>
              <th>AADT major</th>
              <td>{this.selectedFeature.riskFactors.get("AADT on Major Approaches")}</td>
              <td>{'>5,000'}</td>
              <td class="risk-factor-present">
                {
                  this.selectedFeature.riskFactors.get("Risk Factor Present - AADT on Major Approaches")
                    ? <ion-icon class="yellow" name="alert-circle"/> : ""
                }
              </td>
            </tr>
            <tr>
              <th>AADT minor</th>
              <td>{this.selectedFeature.riskFactors.get("AADT on Minor Approaches")}</td>
              <td>{'>1,000'}</td>
              <td class="risk-factor-present">
                {
                  this.selectedFeature.riskFactors.get("Risk Factor Present - AADT on Minor Approaches")
                    ? <ion-icon class="yellow" name="alert-circle"/> : ""
                }
              </td>
            </tr>
            <tr>
              <th>Total number of risk factors present</th>
              <td></td>
              <td></td>
              <td class="risk-factor-present">
                {
                  [...Array(this.selectedFeature.riskFactors.get("Total Number of Risk Factors Present")).keys()]
                    .map(_ => <ion-icon class="yellow" name="alert-circle"/>)
                }
              </td>
            </tr>
          </table>
        </div>
      </div>
    )
  }

  render() {
    if (!this.selectedFeature) return;

    return this.selectedFeature.roadwayType === RoadwayType.Segment
      ? this.segmentRiskTable()
      : this.intersectionRiskTable()
  }

}
