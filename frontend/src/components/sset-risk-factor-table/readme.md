# sset-risk-factor-table



<!-- Auto Generated Below -->


## Properties

| Property          | Attribute | Description | Type              | Default     |
| ----------------- | --------- | ----------- | ----------------- | ----------- |
| `selectedFeature` | --        |             | `IRoadwayElement` | `undefined` |


## Dependencies

### Used by

 - [sset-app](../sset-app)

### Depends on

- ion-icon

### Graph
```mermaid
graph TD;
  sset-risk-factor-table --> ion-icon
  sset-app --> sset-risk-factor-table
  style sset-risk-factor-table fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
