# sset-crash-table



<!-- Auto Generated Below -->


## Properties

| Property                    | Attribute        | Description | Type      | Default     |
| --------------------------- | ---------------- | ----------- | --------- | ----------- |
| `endStudyYear` _(required)_ | `end-study-year` |             | `number`  | `undefined` |
| `selectedCrashes`           | --               |             | `Crash[]` | `[]`        |


## Dependencies

### Used by

 - [sset-app](../sset-app)

### Graph
```mermaid
graph TD;
  sset-app --> sset-crash-table
  style sset-crash-table fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
