import { Component, h, Prop } from "@stencil/core";
import { Crash } from "../../BCLIb/models/classes/Crash";
@Component({
  tag: "sset-crash-table",
  styleUrl: "sset-crash-table.css",
})
export class SsetCrashTable {
  @Prop() selectedCrashes: Crash[] = [];
  @Prop() endStudyYear!: number;

  CRASH_SEVERITIES = [
    "A Injury Crash",
    "B Injury Crash",
    "C Injury Crash",
    "Fatal Crash",
    "No Injuries",
  ];
  crashTypeLabels: string[] = [];
  crashYears: number[] = [];

  componentWillLoad() {
    this.crashTypeLabels = [
      ...new Set(this.selectedCrashes.map((c) => c.crashType)),
    ].sort();
    const years = [];

    for (let i = this.endStudyYear - 4; i <= this.endStudyYear; i++)
      years.push(i);

    this.crashYears = years;
  }

  render() {
    if (!this.selectedCrashes.length) return null;

    return [
      <div class="info-tab" id="crash-type-injury">
        <slot />
        <div class="table-wrapper">
          <table>
            <tr>
              <th></th>
              <th>Total Crashes</th>
              {this.CRASH_SEVERITIES.map((sev) => (
                <th>{`${sev} Crashes`}</th>
              ))}
            </tr>
            {[...new Set(this.selectedCrashes.map((crash) => crash.crashType))]
              .sort()
              .map((ct) => {
                return (
                  <tr>
                    <th>{ct}</th>
                    <td>
                      {
                        this.selectedCrashes.filter(
                          (crash) => crash.crashType === ct
                        ).length
                      }
                    </td>
                    {this.CRASH_SEVERITIES.map((cs) => {
                      return (
                        <td>
                          {
                            this.selectedCrashes
                              .filter((crash) => crash.crashType === ct)
                              .filter((crash) => crash.crashSeverity === cs)
                              .length
                          }
                        </td>
                      );
                    })}
                  </tr>
                );
              })}
            <tr class="new-section">
              <th>Wet Pavement</th>
              <td>
                {
                  this.selectedCrashes.filter(
                    (crash) => crash.surfaceCondition !== "Dry"
                  ).length
                }
              </td>
              {this.CRASH_SEVERITIES.map((cs) => {
                return (
                  <td>
                    {
                      this.selectedCrashes
                        .filter((crash) => crash.surfaceCondition !== "Dry")
                        .filter((crash) => crash.crashSeverity === cs).length
                    }
                  </td>
                );
              })}
            </tr>
            <tr>
              <th>Darkness</th>
              <td>
                {
                  this.selectedCrashes.filter(
                    (crash) => crash.lightCondition === "Darkness"
                  ).length
                }
              </td>
              {this.CRASH_SEVERITIES.map((cs) => {
                return (
                  <td>
                    {
                      this.selectedCrashes
                        .filter((crash) => crash.lightCondition === "Darkness")
                        .filter((crash) => crash.crashSeverity === cs).length
                    }
                  </td>
                );
              })}
            </tr>
            <tr class="new-section">
              <th>Total</th>
              <td>{this.selectedCrashes.length}</td>
              {this.CRASH_SEVERITIES.map((sev) => {
                return (
                  <td>
                    {
                      this.selectedCrashes.filter(
                        (crash) => crash.crashSeverity === sev
                      ).length
                    }
                  </td>
                );
              })}
            </tr>
          </table>
        </div>
      </div>,
    ];
  }
}
