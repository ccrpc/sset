import { newSpecPage } from '@stencil/core/testing';
import { SsetAttributeTable } from '../sset-attribute-table';

describe('sset-attribute-table', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SsetAttributeTable],
      html: `<sset-attribute-table></sset-attribute-table>`,
    });
    expect(page.root).toEqualHtml(`
      <sset-attribute-table>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </sset-attribute-table>
    `);
  });
});
