import { newE2EPage } from '@stencil/core/testing';

describe('sset-attribute-table', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<sset-attribute-table></sset-attribute-table>');

    const element = await page.find('sset-attribute-table');
    expect(element).toHaveClass('hydrated');
  });
});
