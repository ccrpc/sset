# sset-table



<!-- Auto Generated Below -->


## Properties

| Property          | Attribute | Description | Type              | Default     |
| ----------------- | --------- | ----------- | ----------------- | ----------- |
| `selectedFeature` | --        |             | `IRoadwayElement` | `undefined` |


## Dependencies

### Used by

 - [sset-app](../sset-app)

### Graph
```mermaid
graph TD;
  sset-app --> sset-table
  style sset-table fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
