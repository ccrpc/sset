import { Component, h, Prop } from '@stencil/core';
import { IRoadwayElement } from '../../BCLIb/models/interfaces';
@Component({
  tag: 'sset-table',
  styleUrl: 'sset-attribute-table.css',
})
export class SsetAttributeTable {

  @Prop() selectedFeature: IRoadwayElement 

  render() {
    if (!this.selectedFeature) return;

    return (
      <div class="info-tab" id="roadway-table">
        <div class="table-wrapper">
          <slot/>
          <table>
            {[...this.selectedFeature.infoPropsTable.entries()].map(([, [label, val]]) => {
              return (
                <tr>
                  <th>{label}</th>
                  <td>{val}</td>
                </tr>
              )
            })}
          </table>
        </div>
      </div>
    );
  }

}
