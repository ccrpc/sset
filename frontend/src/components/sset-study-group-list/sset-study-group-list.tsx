import {
  Component,
  h,
  Prop,
  Event,
  EventEmitter,
  Listen,
  State,
  Watch,
} from "@stencil/core";
import {
  BenefitCalculator,
  getTotalProjectBenefit,
  whetherToAggregate,
} from "../../BCLIb/BenefitBreakdown";
import { modalController } from "@ionic/core";
import { createCrashTable } from "../../BCLIb/utils";
import {
  CrashType,
  Locality,
  RoadwayType,
  Severity,
} from "../../BCLIb/models/enums";
import { Countermeasure } from "../../BCLIb/models/classes/Countermeasure";
import { ProjectElements } from "../../BCLIb/models/types";
import {
  IAppliedCountermeasure,
  IRoadwayElement,
} from "../../BCLIb/models/interfaces";
import { SEG_PROP_LABELS } from "../../BCLIb/models/consts";
import { CostCalculator } from "../../BCLIb/CostBreakdown";
import { Crash } from "../../BCLIb/models/classes/Crash";

const formatter = Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});

@Component({
  tag: "sset-study-group-list",
  styleUrl: "sset-study-group-list.css",
  })
export class SsetStudyGroupList {
  /**
   * The possible countermeasures the user can choose from when they add a counter measure
   */
  @Prop() countermeasures: Countermeasure[];
  @Prop({ mutable: true }) studyAreaFeatures: ProjectElements;
  @Prop() endStudyYear!: number;
  @State() modal: any;
  @State() issegmentbuttoncreated = false;
  @State() isintersectionbuttoncreated = false;

  @State() totalProjectBenefit = 0;
  @State() totalProjectCost = 0;
  benefitCostRatio = () => this.totalProjectBenefit / this.totalProjectCost;

  @Event() removeButtonClicked: EventEmitter;
  @Event() groupFeatureClicked: EventEmitter<IRoadwayElement>;

  /**
   * This listens for a counter measure being added via the sset-counter-measure form.
   * It is, currently, called in the onClick function around line 208 of sset-counter-measure-form.tsx
   * @param {CustomEvent} e The event passed into the function. e.detail will hold what is passed into the emit function.
   */
  @Listen("countermeasureUpdated", { target: "body" })
  handleCmUpdate(
    e: CustomEvent<{
      featureIdx: number;
      roadwayType: RoadwayType;
      countermeasureIdx: number;
      countermeasure: {
        countermeasure: Countermeasure;
        unitCost: number;
        quantity: number;
      };
    }>
  ) {
    this.studyAreaFeatures.get(e.detail.roadwayType)[
      e.detail.featureIdx
    ].appliedCountermeasures[e.detail.countermeasureIdx] = e.detail
      .countermeasure as IAppliedCountermeasure;

    this.updateOverview();

    // If the project cost and project benefit did not change, updateOverview will not trigger a re-render (stencil is smart like that)
    // That said in the scenario of a new countermeasure being added with no unit price / quantity the total price will not change.
    // If there are no crashes prevented by this countermeasure the total benefit will not change.
    // Or if a countermeasure is edited but it does not change the total price or total benefit, no re-render will be triggered
    // So just to play things safe we will manually trigger a re-render whenever a counter measure is updated
    this.studyAreaFeatures = new Map([
      [RoadwayType.Segment, this.studyAreaFeatures.get(RoadwayType.Segment)],
      [
        RoadwayType.Intersection,
        this.studyAreaFeatures.get(RoadwayType.Intersection),
      ],
    ]);

    this.modal.dismiss();
  }

  /**
   * This presents the sset-study-group-form component as a modal
   * @param componentProps The properties to pass along to the sset-study-group-form component.
   */
  async presentModal(componentProps) {
    this.modal = await modalController.create({
      component: "sset-countermeasure-form",
      componentProps: {
        ...componentProps,
        countermeasures: this.countermeasures,
        studyAreaFeatures: this.studyAreaFeatures,
      },
    });

    await this.modal.present();
  }

  /**
   * When the studyArea is updated, update our overview.
   *
   * Without this one could click "Add Feature" to add a segment (or intersection) and the totalCostBenefit would not update to include the crashes
   * which occur in that segment (or intersection) until someone updated a countermeasure.
   */
  @Watch("studyAreaFeatures")
  studyAreaUpdated() {
    this.updateOverview();
  }

  /** Updates the total project benefit, cost, and ratio. */
  updateOverview() {
    // Gets the benefit of the counter measures for this project.
    this.totalProjectBenefit = getTotalProjectBenefit(
      this.studyAreaFeatures,
      this.endStudyYear
    );

    CostCalculator.populateCounterMeasures(
      this.studyAreaFeatures,
      RoadwayType.Segment
    );
    CostCalculator.populateCounterMeasures(
      this.studyAreaFeatures,
      RoadwayType.Intersection
    );

    this.totalProjectCost = CostCalculator.calculateTotalProjectCost(
      this.studyAreaFeatures
    );
  }

  render() {
    return [
      <ion-content class="info-tab" id="study-group-list">
        <ion-list>
          <ion-accordion-group>
            <ion-list-header color="primary">
              <ion-label>
                <h2>Intersections</h2>
              </ion-label>
            </ion-list-header>
            {this.studyAreaFeatures
              .get(RoadwayType.Intersection)
              ?.map((ft, idx) =>
                idx === 0
                  ? this.getAccordion_for_button_intersection(ft, idx)
                  : null
              )}
            {this.studyAreaFeatures
              .get(RoadwayType.Intersection)
              ?.map((ft, idx) =>
                this.getAccordion_for_intersection_and_segments(ft, idx)
              )}
          </ion-accordion-group>

          <ion-item-divider />

          <ion-accordion-group>
            <ion-list-header color="primary">
              <ion-label>
                <h2>Segments</h2>
              </ion-label>
            </ion-list-header>
            {this.studyAreaFeatures
              .get(RoadwayType.Segment)
              ?.map((ft, idx) =>
                idx === 0 ? this.getAccordion_for_button_segment(ft, idx) : null
              )}
            {this.studyAreaFeatures
              .get(RoadwayType.Segment)
              ?.map((ft, idx) =>
                this.getAccordion_for_intersection_and_segments(ft, idx)
              )}
          </ion-accordion-group>

          <ion-item-divider />
          <ion-accordion-group>
            <ion-list-header color="primary">
              <ion-label>
                <h2>Selected Countermeasures</h2>
              </ion-label>
            </ion-list-header>
            {/* Currently all countermeasures are attached to the first segment or intersection rather than on a per basis */}

            {this.studyAreaFeatures.get(RoadwayType.Intersection)[0]
              ? this.getAccordion(
                this.studyAreaFeatures.get(RoadwayType.Intersection)[0],
                0
              )
              : null}
            {this.studyAreaFeatures.get(RoadwayType.Segment)[0]
              ? this.getAccordion(
                this.studyAreaFeatures.get(RoadwayType.Segment)[0],
                0
              )
              : null}
          </ion-accordion-group>
        </ion-list>
      </ion-content>,

      // for the lower portion of the study area features

      <div class="info-tab" id="study-group-stats">
        <h1>Study Area Overview</h1>
        <table>
          <tr>
            <th>Total Project Benefit</th>
            <td>{formatter.format(this.totalProjectBenefit)}</td>
          </tr>
          <tr>
            <th>Total Project Cost</th>
            <td>{formatter.format(this.totalProjectCost)}</td>
          </tr>
          <tr>
            <th>Benefit-Cost Ratio</th>
            <td>{this.benefitCostRatio().toFixed(2)}</td>
          </tr>
          <tr>
            <th>Roadway Length (miles)</th>
            <td>
              {(
                this.studyAreaFeatures
                  .get(RoadwayType.Segment)
                  ?.map(
                    (seg) =>
                      seg.infoProps.get(SEG_PROP_LABELS.LENGTH_FT) as number
                  )
                  ?.reduce((a, b) => a + b, 0) / 5280.0
              ).toFixed(2)}
            </td>
          </tr>
          <tr>
            <th>Number of Intersections</th>
            <td>
              {this.studyAreaFeatures.get(RoadwayType.Intersection).length}
            </td>
          </tr>
          <tr>
            <th>Risk Factors Present</th>
            <td>
              {[...this.studyAreaFeatures.values()]
                .flat()
                .map(
                  (re) => re.riskFactors.get(SEG_PROP_LABELS.RISK_SUM) as number
                )
                .reduce((a, b) => a + b, 0)}
            </td>
          </tr>
          <tr>
            <th>Five-year crashes</th>
            <td>
              {
                [...this.studyAreaFeatures.values()]
                  .flat()
                  .flatMap((re) => re.crashes).length
              }
            </td>
          </tr>
          <tr>
            <th>Animal Crashes</th>
            <td>
              {
                [...this.studyAreaFeatures.values()]
                  .flat()
                  .flatMap((re) => re.crashes)
                  .filter((c) => c.crashType === CrashType.AN).length
              }
            </td>
          </tr>
          <tr>
            <th>Wet Pavement Crashes</th>
            <td>
              {
                [...this.studyAreaFeatures.values()]
                  .flat()
                  .flatMap((re) => re.crashes)
                  .filter((c) => c.surfaceCondition !== "Dry").length
              }
            </td>
          </tr>
          <tr>
            <th>Darkness Crashes</th>
            <td>
              {
                [...this.studyAreaFeatures.values()]
                  .flat()
                  .flatMap((re) => re.crashes)
                  .filter((crash) => crash.lightCondition === "Darkness").length
              }
            </td>
          </tr>
          <tr>
            <th>Five-year Fatalities</th>
            <td>
              {[...this.studyAreaFeatures.values()]
                .flat()
                .flatMap((re) => re.crashes)
                .map((crash) => crash.injuryProfile.get(Severity.F))
                .reduce((a, b) => a + b, 0)}
            </td>
          </tr>
          <tr>
            <th>Five-year A-injuries</th>
            <td>
              {[...this.studyAreaFeatures.values()]
                .flat()
                .flatMap((re) => re.crashes)
                .map((crash) => crash.injuryProfile.get(Severity.A))
                .reduce((a, b) => a + b, 0)}
            </td>
          </tr>
          <tr>
            <th>Five-year B-injuries</th>
            <td>
              {[...this.studyAreaFeatures.values()]
                .flat()
                .flatMap((re) => re.crashes)
                .map((crash) => crash.injuryProfile.get(Severity.B))
                .reduce((a, b) => a + b, 0)}
            </td>
          </tr>
          <tr>
            <th>Five-year C-injuries</th>
            <td>
              {[...this.studyAreaFeatures.values()]
                .flat()
                .flatMap((re) => re.crashes)
                .map((crash) => crash.injuryProfile.get(Severity.C))
                .reduce((a, b) => a + b, 0)}
            </td>
          </tr>
        </table>
      </div>,
    ];
  }

  getAccordion(ft: IRoadwayElement, ftIdx: number) {
    return (
      <ion-label>
        <ion-list slot="content">
          {[...ft.appliedCountermeasures.values()].map((cmd, idx) => {
            return (
              <ion-item>
                <ion-icon
                  name="close"
                  onClick={() =>
                    this.removeCountermeasure(ft.roadwayType, cmd, idx)
                  }
                />
                <ion-label
                  onClick={() => {
                    this.presentModal({
                      countermeasure:
                        ft.appliedCountermeasures[idx].countermeasure,
                      countermeasureIdx: idx,
                      feature: ft,
                      featureIdx: ftIdx,
                      quantity: ft.appliedCountermeasures[idx].quantity,
                      pricePerUnit: ft.appliedCountermeasures[idx].unitCost,
                    });
                  }}
                >
                  {cmd.countermeasure.name}
                </ion-label>
                {validCountermeasure(ft.appliedCountermeasures[idx]) ? (
                  <ion-label slot="end">
                    {formatter.format(this.getCountermeasureBenefit(ft, cmd))}
                    {" / "}
                    {formatter.format(CostCalculator.round50(cmd.EAUCCost))}
                    {" = "}
                    {(
                      this.getCountermeasureBenefit(ft, cmd) /
                      CostCalculator.round50(cmd.EAUCCost)
                    ).toFixed(2)}
                  </ion-label>
                ) : (
                  <ion-icon
                    name="warning-outline"
                    title="Countermeasure entry is incomplete."
                    slot="end"
                  />
                )}
              </ion-item>
            );
          })}
        </ion-list>
      </ion-label>
    );
  }

  // this portion id for making the Intersection and segment in case we decide to make the h2 tags expandable. Call them on the h2 tags with click function and pass the string as arguments.
  // arguments will be the h2 tags - 'Intersection' and 'Segments'
  expandedAccordions: string[] = [];

  isAccordionExpanded(name: string) {
    return this.expandedAccordions.includes(name);
  }

  toggleAccordion(name: string) {
    if (this.expandedAccordions.includes(name)) {
      this.expandedAccordions = this.expandedAccordions.filter(
        (n) => n !== name
      );
    } else {
      this.expandedAccordions.push(name);
    }
  }

  removeFeature(feature: IRoadwayElement, index: number) {
    const remove = confirm(
      `Would you like to remove the ${feature.roadwayType} ${feature.name}-${feature.id}? `
    );
    if (!remove) {
      return;
    }

    this.studyAreaFeatures.get(feature.roadwayType).splice(index, 1);

    if (index !== 0) {
      this.studyAreaFeatures.get(
        feature.roadwayType
      )[0].appliedCountermeasures = [];
    }

    this.studyAreaFeatures = new Map<RoadwayType, IRoadwayElement[]>([
      [RoadwayType.Segment, this.studyAreaFeatures.get(RoadwayType.Segment)],
      [
        RoadwayType.Intersection,
        this.studyAreaFeatures.get(RoadwayType.Intersection),
      ],
    ]);
  }

  removeCountermeasure(
    type: RoadwayType,
    countermeasure: IAppliedCountermeasure,
    index: number
  ) {
    const remove = confirm(
      `Would you like to remove the ${type} Countermeasure ${countermeasure.countermeasure.name}?`
    );
    if (!remove) {
      return;
    }

    this.studyAreaFeatures.get(type)[0].appliedCountermeasures.splice(index, 1);

    this.studyAreaFeatures = new Map<RoadwayType, IRoadwayElement[]>([
      [RoadwayType.Segment, this.studyAreaFeatures.get(RoadwayType.Segment)],
      [
        RoadwayType.Intersection,
        this.studyAreaFeatures.get(RoadwayType.Intersection),
      ],
    ]);
  }

  getAccordion_for_intersection_and_segments(
    ft: IRoadwayElement,
    ftIdx: number
  ) {
    const featureName = ft.name;
    return (
      <ion-label>
        <ion-item slot="header" color="medium">
          <ion-icon
            name="close"
            onClick={() => this.removeFeature(ft, ftIdx)}
          />
          <ion-label
            onClick={() => this.groupFeatureClicked.emit(ft)}
          >{`${featureName}-${ft.id}`}</ion-label>
        </ion-item>
      </ion-label>
    );
  }

  getAccordion_for_button_segment(ft: IRoadwayElement, ftIdx: number) {
    const featureName = ft.name;

    return (
      <ion-label>
        {[...ft.appliedCountermeasures.values()].map((cmd, idx) => null)}
        <ion-item
          onClick={() => {
            this.presentModal({
              countermeasure: new Countermeasure(),
              countermeasureIdx: ft.appliedCountermeasures.length,
              feature: ft,
              featureIdx: ftIdx,
            });
          }}
        >
          <ion-icon name="add" />
          <ion-label>Add Countermeasure for Segments</ion-label>
        </ion-item>
      </ion-label>
    );
  }

  getAccordion_for_button_intersection(ft: IRoadwayElement, ftIdx: number) {
    const featureName = ft.name;
    return (
      <ion-label>
        {[...ft.appliedCountermeasures.values()].map((cmd, idx) => null)}
        <ion-item
          onClick={() => {
            this.presentModal({
              countermeasure: new Countermeasure(),
              countermeasureIdx: ft.appliedCountermeasures.length,
              feature: ft,
              featureIdx: ftIdx,
            });
          }}
        >
          <ion-icon name="add" />
          <ion-label>Add Countermeasure for Intersections</ion-label>
        </ion-item>
      </ion-label>
    );
  }

  /**
   * Get the benefit given by the selected counter measure when using the "counter measures apply to whole project" approach.
   * @param roadwayElem The element whose counter measure to get
   * @param cmIdx The index of the countermeasure in that element's array
   * @returns {number} The benefit given by this counter measure
   */
  getCountermeasureBenefit(
    roadwayElem: IRoadwayElement,
    appliedCountermeasure: IAppliedCountermeasure
  ): number {
    const totalCrashes: Map<number, Crash[]> = new Map<number, Crash[]>();

    this.studyAreaFeatures
      .get(roadwayElem.roadwayType)
      .forEach((iterativeRoadwayElement) => {
        if (!totalCrashes.get(iterativeRoadwayElement.peerGroup)) {
          totalCrashes.set(iterativeRoadwayElement.peerGroup, []);
        }
        totalCrashes
          .get(iterativeRoadwayElement.peerGroup)
          .push(...iterativeRoadwayElement.crashes);
      });

    let benefit = 0;
    for (const [group, crashes] of totalCrashes) {
      const crashTable = createCrashTable(
        crashes,
        whetherToAggregate([appliedCountermeasure])
      );

      const crashProfile = {
        locality: Locality.Local,
        roadwayType: roadwayElem.roadwayType,
        peerGroup: group,
        crashes: crashTable,
        periodStart: new Date(`${this.endStudyYear - 4}-01-01`),
        periodEnd: new Date(`${this.endStudyYear}-12-31`),
      };

      const bc = new BenefitCalculator(crashProfile, [
        appliedCountermeasure.countermeasure,
      ]);
      benefit = benefit + bc.totalBenefit;
    }
    return benefit;
  }

  /**
   * Get the benefit given by the selected counter measure when using the Partial Application approach
   * @param roadwayElem The element whose counter measure to get
   * @param cmIdx The index of the countermeasure in that element's array
   * @returns {number} The benefit given by this counter measure
   */
  getCountermeasureBenefitPartialApplication(
    roadwayElem: IRoadwayElement,
    cmIdx: number
  ): number {
    const crashTables = createCrashTable(
      roadwayElem.crashes,
      whetherToAggregate(roadwayElem.appliedCountermeasures)
    );
    const crashProfile = {
      locality: Locality.Local,
      roadwayType: roadwayElem.roadwayType,
      peerGroup: roadwayElem.peerGroup,
      crashes: crashTables,
      periodStart: new Date(`${this.endStudyYear - 4}-01-01`),
      periodEnd: new Date(`${this.endStudyYear}-12-31`),
    };
    const countermeasure =
      roadwayElem.appliedCountermeasures[cmIdx].countermeasure;
    const bc = new BenefitCalculator(crashProfile, [countermeasure]);
    return bc.totalBenefit;
  }
}

/**
 * Whether the price of the countermeasure can be calculated.
 * @param {IAppliedCountermeasure} cm The countermeasure to check
 * @returns
 */
function validCountermeasure(cm: IAppliedCountermeasure): boolean {
  if (cm.quantity) {
    if (cm.unitCost) {
      return true;
    }
  }
  return false;
}
