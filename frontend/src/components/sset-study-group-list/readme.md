# sset-study-group-list

### this portion id for making the Intersection and segment expandable.

// Call them on the h2 tags with click function and pass the string as arguments.
// arguments will be the h2 tags - 'Intersection' and 'Segments'
expandedAccordions: string[] = [];

isAccordionExpanded(name: string) {
return this.expandedAccordions.includes(name);
}

toggleAccordion(name: string) {
if (this.expandedAccordions.includes(name)) {
this.expandedAccordions = this.expandedAccordions.filter(
(n) => n !== name
);
} else {
this.expandedAccordions.push(name);
}
}

### Make use of these seperated ways to go with the segmented approach

getAccordion_for_button_segment(ft: IRoadwayElement, ftIdx: number) {
const featureName = ft.name;

    return (
      <ion-label>
        {[...ft.appliedCountermeasures.values()].map((cmd, idx) => {})}
        <ion-item
          onClick={() => {
            this.presentModal({
              countermeasure: new Countermeasure(),
              countermeasureIdx: ft.appliedCountermeasures.length,
              feature: ft,
              featureIdx: ftIdx,
            });
          }}
        >
          <ion-icon name="add" />
          <ion-label>Add Countermeasure for Segments</ion-label>
        </ion-item>
      </ion-label>
    );

}

getAccordion_for_button_intersection(ft: IRoadwayElement, ftIdx: number) {
const featureName = ft.name;

    return (
      <ion-label>
        {[...ft.appliedCountermeasures.values()].map((cmd, idx) => {})}
      <ion-item
          onClick={() => {
          this.presentModal({
          countermeasure: new Countermeasure(),
          countermeasureIdx: ft.appliedCountermeasures.length,
          feature: ft,
          featureIdx: ftIdx,
          });
          }} >
      <ion-icon name="add" />
      <ion-label>Add Countermeasure for Intersections</ion-label>
      </ion-item>
      </ion-label>
      );
      }

<!-- Auto Generated Below -->


## Properties

| Property                    | Attribute        | Description                                                                           | Type                                  | Default     |
| --------------------------- | ---------------- | ------------------------------------------------------------------------------------- | ------------------------------------- | ----------- |
| `countermeasures`           | --               | The possible countermeasures the user can choose from when they add a counter measure | `Countermeasure[]`                    | `undefined` |
| `endStudyYear` _(required)_ | `end-study-year` |                                                                                       | `number`                              | `undefined` |
| `studyAreaFeatures`         | --               |                                                                                       | `Map<RoadwayType, IRoadwayElement[]>` | `undefined` |


## Events

| Event                 | Description | Type                           |
| --------------------- | ----------- | ------------------------------ |
| `groupFeatureClicked` |             | `CustomEvent<IRoadwayElement>` |
| `removeButtonClicked` |             | `CustomEvent<any>`             |


## Dependencies

### Used by

 - [sset-app](../sset-app)

### Depends on

- ion-content
- ion-list
- ion-accordion-group
- ion-list-header
- ion-label
- ion-item-divider
- ion-item
- ion-icon

### Graph
```mermaid
graph TD;
  sset-study-group-list --> ion-content
  sset-study-group-list --> ion-list
  sset-study-group-list --> ion-accordion-group
  sset-study-group-list --> ion-list-header
  sset-study-group-list --> ion-label
  sset-study-group-list --> ion-item-divider
  sset-study-group-list --> ion-item
  sset-study-group-list --> ion-icon
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  sset-app --> sset-study-group-list
  style sset-study-group-list fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
