# sset-calculated-countermeasure-modal



<!-- Auto Generated Below -->


## Properties

| Property                   | Attribute | Description | Type                       | Default     |
| -------------------------- | --------- | ----------- | -------------------------- | ----------- |
| `aadt`                     | `aadt`    |             | `number`                   | `undefined` |
| `calculatedCounterMeasure` | --        |             | `CalculatedCounterMeasure` | `undefined` |
| `countermeasure`           | --        |             | `Countermeasure`           | `undefined` |
| `possibleAADT`             | --        |             | `Set<number>`              | `undefined` |
| `userInputs`               | --        |             | `(string \| number)[]`     | `[]`        |


## Dependencies

### Depends on

- ion-header
- ion-title
- ion-content
- ion-grid
- ion-row
- ion-col
- ion-label
- ion-select
- ion-select-option
- ion-item-divider
- ion-input
- ion-button

### Graph
```mermaid
graph TD;
  sset-calculated-countermeasure-modal --> ion-header
  sset-calculated-countermeasure-modal --> ion-title
  sset-calculated-countermeasure-modal --> ion-content
  sset-calculated-countermeasure-modal --> ion-grid
  sset-calculated-countermeasure-modal --> ion-row
  sset-calculated-countermeasure-modal --> ion-col
  sset-calculated-countermeasure-modal --> ion-label
  sset-calculated-countermeasure-modal --> ion-select
  sset-calculated-countermeasure-modal --> ion-select-option
  sset-calculated-countermeasure-modal --> ion-item-divider
  sset-calculated-countermeasure-modal --> ion-input
  sset-calculated-countermeasure-modal --> ion-button
  ion-select --> ion-select-popover
  ion-select --> ion-popover
  ion-select --> ion-action-sheet
  ion-select --> ion-alert
  ion-select --> ion-icon
  ion-select-popover --> ion-item
  ion-select-popover --> ion-checkbox
  ion-select-popover --> ion-radio-group
  ion-select-popover --> ion-radio
  ion-select-popover --> ion-list
  ion-select-popover --> ion-list-header
  ion-select-popover --> ion-label
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-popover --> ion-backdrop
  ion-action-sheet --> ion-backdrop
  ion-action-sheet --> ion-icon
  ion-action-sheet --> ion-ripple-effect
  ion-alert --> ion-ripple-effect
  ion-alert --> ion-backdrop
  ion-input --> ion-icon
  ion-button --> ion-ripple-effect
  style sset-calculated-countermeasure-modal fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
