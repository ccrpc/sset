import { InputCustomEvent, SelectCustomEvent } from "@ionic/core";
import { Component, Host, h, Prop, Element } from "@stencil/core";
import { CalculatedCounterMeasure } from "../../BCLIb/calculatedCounterMeasures";
import { Countermeasure } from "../../BCLIb/models/classes/Countermeasure";

@Component({
  tag: "sset-calculated-countermeasure-modal",
  styleUrl: "sset-calculated-countermeasure-modal.css",
  shadow: true,
  })
export class SsetCalculatedCountermeasureModal {
  @Element() el: HTMLElement;

  @Prop() aadt: number;
  @Prop() userInputs: (string | number)[] = [];
  @Prop() countermeasure: Countermeasure;
  @Prop() calculatedCounterMeasure: CalculatedCounterMeasure;
  @Prop() possibleAADT: Set<number>;

  /** Closes the modal this component assumes it is in */
  async closeModal(
    userInput: (string | number)[],
    aadt: number
  ): Promise<void> {
    await (this.el.closest("ion-modal") as HTMLIonModalElement).dismiss(
      aadt || userInput
        ? {
            userInputs: userInput,
            aadtMajor: aadt,
          }
        : undefined
    );
  }

  render() {
    return (
      <Host>
        <ion-header>
          <ion-title id="user-input-title">
            {this.countermeasure.code} - {this.countermeasure.name}
          </ion-title>
        </ion-header>
        <ion-content>
          <ion-grid>
            {this.possibleAADT !== undefined
              ? [
                  <ion-row>
                    <ion-col>
                      <ion-label>
                        Select AADT for this counter measure
                      </ion-label>
                    </ion-col>
                    <ion-col>
                      <ion-select
                        name="aadt-select"
                        interface="popover"
                        value={this.aadt}
                        onIonChange={(event: SelectCustomEvent<number>) => {
                          this.aadt = event.target.value;
                        }}
                      >
                        {Array.from(this.possibleAADT).map((aadt) => (
                          <ion-select-option value={aadt}>
                            {aadt}
                          </ion-select-option>
                        ))}
                      </ion-select>
                    </ion-col>
                  </ion-row>,
                  <ion-row>
                    <ion-item-divider />
                  </ion-row>,
                ]
              : ""}
            {Array.from(
              Array(
                this.calculatedCounterMeasure
                  ? this.calculatedCounterMeasure.numberOfInputs
                  : 0
              ).keys()
            ).map((index: number) => {
              return [
                <ion-row>
                  <ion-col>
                    <ion-label text-wrap>
                      {this.calculatedCounterMeasure?.inputPrompts[index]
                        ? this.calculatedCounterMeasure?.inputPrompts[index]
                        : `User Input ${index}`}
                    </ion-label>
                  </ion-col>
                  <ion-col>
                    <ion-input
                      id={`input_${index}`}
                      value={
                        this.userInputs[index] ? this.userInputs[index] : ""
                      }
                      onIonInput={(event: InputCustomEvent) => {
                        this.userInputs[index] = event.target.value;
                      }}
                      placeholder={`User Input ${index + 1}`}
                    ></ion-input>
                  </ion-col>
                </ion-row>,
                index === this.calculatedCounterMeasure.numberOfInputs - 1 ? (
                  ""
                ) : (
                  <ion-row>
                    <ion-item-divider />
                  </ion-row>
                ),
              ];
            })}
            <ion-row>
              <ion-col>
                <ion-button
                  id="save-and-close"
                  expand="full"
                  color="success"
                  onClick={() => {
                    this.closeModal(this.userInputs, this.aadt);
                  }}
                >
                  SAVE AND CLOSE
                </ion-button>
              </ion-col>
              <ion-col>
                <ion-button
                  id="cancel-calculate"
                  expand="full"
                  color="warning"
                  onClick={() => {
                    this.closeModal(undefined, undefined);
                  }}
                >
                  CANCEL
                </ion-button>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-content>
      </Host>
    );
  }
}
