# sset-user-defined-countermeasure-modal



<!-- Auto Generated Below -->


## Properties

| Property             | Attribute        | Description | Type                                              | Default     |
| -------------------- | ---------------- | ----------- | ------------------------------------------------- | ----------- |
| `affectedCrashTypes` | --               |             | `string[]`                                        | `[]`        |
| `category`           | `category`       |             | `string`                                          | `undefined` |
| `cmfVector`          | --               |             | `Map<CrashType, number>`                          | `undefined` |
| `code`               | `code`           |             | `string`                                          | `undefined` |
| `facilityType`       | `facility-type`  |             | `string`                                          | `"All"`     |
| `name`               | `name`           |             | `string`                                          | `undefined` |
| `projectSystem`      | `project-system` |             | `string`                                          | `undefined` |
| `projectType`        | `project-type`   |             | `RoadwayType.Intersection \| RoadwayType.Segment` | `undefined` |
| `serviceLife`        | `service-life`   |             | `number`                                          | `undefined` |
| `settingType`        | `setting-type`   |             | `string`                                          | `"All"`     |
| `units`              | `units`          |             | `string`                                          | `"Miles"`   |


## Dependencies

### Depends on

- ion-col
- ion-checkbox
- ion-header
- ion-title
- ion-content
- ion-grid
- ion-row
- ion-label
- ion-input
- ion-select
- ion-select-option
- ion-radio-group
- ion-radio
- ion-button

### Graph
```mermaid
graph TD;
  sset-user-defined-countermeasure-modal --> ion-col
  sset-user-defined-countermeasure-modal --> ion-checkbox
  sset-user-defined-countermeasure-modal --> ion-header
  sset-user-defined-countermeasure-modal --> ion-title
  sset-user-defined-countermeasure-modal --> ion-content
  sset-user-defined-countermeasure-modal --> ion-grid
  sset-user-defined-countermeasure-modal --> ion-row
  sset-user-defined-countermeasure-modal --> ion-label
  sset-user-defined-countermeasure-modal --> ion-input
  sset-user-defined-countermeasure-modal --> ion-select
  sset-user-defined-countermeasure-modal --> ion-select-option
  sset-user-defined-countermeasure-modal --> ion-radio-group
  sset-user-defined-countermeasure-modal --> ion-radio
  sset-user-defined-countermeasure-modal --> ion-button
  ion-input --> ion-icon
  ion-select --> ion-select-popover
  ion-select --> ion-popover
  ion-select --> ion-action-sheet
  ion-select --> ion-alert
  ion-select --> ion-icon
  ion-select-popover --> ion-item
  ion-select-popover --> ion-checkbox
  ion-select-popover --> ion-radio-group
  ion-select-popover --> ion-radio
  ion-select-popover --> ion-list
  ion-select-popover --> ion-list-header
  ion-select-popover --> ion-label
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  ion-popover --> ion-backdrop
  ion-action-sheet --> ion-backdrop
  ion-action-sheet --> ion-icon
  ion-action-sheet --> ion-ripple-effect
  ion-alert --> ion-ripple-effect
  ion-alert --> ion-backdrop
  ion-button --> ion-ripple-effect
  style sset-user-defined-countermeasure-modal fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
