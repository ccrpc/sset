import {
  InputCustomEvent,
  SelectCustomEvent,
  CheckboxCustomEvent,
  RadioGroupCustomEvent,
} from "@ionic/core";
import { Component, Host, h, Element, State, Prop } from "@stencil/core";
import { Countermeasure } from "../../BCLIb/models/classes/Countermeasure";
import { CmfVector } from "../../BCLIb/models/types";
import { CrashType, RoadwayType } from "../../BCLIb/models/enums";
import { labelToCode } from "../../BCLIb/utils";

const excludedTypes: CrashType[] = [
  CrashType.ROR,
  CrashType.RTF,
  CrashType.RTR,
  CrashType.RTS,
];

const crashTypes = Array.from(
  Countermeasure.defaultVector,
  ([crashType, _]) => crashType
).filter((type: CrashType) => !excludedTypes.includes(type));

@Component({
  tag: "sset-user-defined-countermeasure-modal",
  styleUrl: "sset-user-defined-countermeasure-modal.css",
  shadow: true,
})
export class SsetCalculatedCountermeasureModal {
  @Element() el: HTMLElement;

  @Prop() projectType: RoadwayType;
  @Prop() projectSystem: string;

  @Prop() name: string;
  @Prop() code: string;
  @Prop() category: string;
  @Prop() serviceLife: number;
  @Prop() cmfVector: CmfVector;
  @Prop() settingType = "All";
  @Prop() facilityType = "All";
  @Prop() units = "Miles";
  @Prop() affectedCrashTypes: string[] = [];

  @State() customUnits: string;
  @State() customCMF = 1;
  @State() selectCMF: boolean;

  /** Closes the modal this component assumes it is in */
  async closeModal(countermeasure: Countermeasure): Promise<void> {
    await (this.el.closest("ion-modal") as HTMLIonModalElement).dismiss(
      countermeasure
    );
  }

  updateAffectedCrashes(type: CrashType | "All", checked: boolean) {
    const code = type !== "All" ? labelToCode(type) : "All";
    if (checked) {
      if (!this.affectedCrashTypes.includes(code)) {
        this.affectedCrashTypes.push(code);
      }
    } else {
      const index = this.affectedCrashTypes.indexOf(code);
      if (index !== -1) {
        this.affectedCrashTypes.splice(index, 1);
      }
    }
    this.affectedCrashTypes = [...this.affectedCrashTypes];
  }

  createCMFVector(): CmfVector {
    const resultVector = new Map(Countermeasure.defaultVector);

    for (const type of crashTypes) {
      const code = labelToCode(type);
      if (
        (!this.selectCMF && type !== CrashType.NGT && type !== CrashType.WP) ||
        (this.selectCMF && this.affectedCrashTypes.includes(code))
      ) {
        resultVector.set(type, this.customCMF);
      }
    }
    return resultVector;
  }

  componentWillLoad() {
    if (this.projectSystem === "Local") {
      if (this.projectType === "Segment") {
        this.code = "4.8.101.UD.1";
      } else if (this.projectType === "Intersection") {
        this.code = "3.5.101.UD.1";
      }
    } else if (this.projectSystem === "State") {
      if (this.projectType === "Segment") {
        this.code = "2.8.11.UD.1";
      } else if (this.projectType === "Intersection") {
        this.code = "1.5.101.UD.1";
      }
    }

    this.selectCMF = !(
      !this.cmfVector ||
      !this.affectedCrashTypes ||
      this.affectedCrashTypes.includes("All")
    );

    if (this.cmfVector) {
      for (const type of crashTypes) {
        const cmf = this.cmfVector.get(type);
        if (cmf !== 1) {
          this.customCMF = cmf;
          break;
        }
      }
    }
  }

  render() {
    const crashTypeCheckboxes: HTMLIonCheckboxElement[] = crashTypes.map(
      (crashType: CrashType) => (
        <ion-col>
          <ion-checkbox
            onIonChange={(event: CheckboxCustomEvent) => {
              this.updateAffectedCrashes(crashType, event.detail.checked);
            }}
            checked={this.affectedCrashTypes.includes(labelToCode(crashType))}
          >
            {crashType}
          </ion-checkbox>
        </ion-col>
      )
    );

    return (
      <Host>
        <ion-header>
          <ion-title id="user-input-title">
            User Defined Countermeasure
          </ion-title>
        </ion-header>
        <ion-content>
          <ion-grid>
            <ion-row>
              <ion-col>
                <ion-label>Name:</ion-label>
              </ion-col>
              <ion-col>
                <ion-input
                  id="userName"
                  value={this.name ? this.name : ""}
                  onIonInput={(event: InputCustomEvent) => {
                    this.name = event.target.value as string;
                  }}
                  placeholder="The Name of the countermeasure"
                ></ion-input>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col>
                <ion-label>Code:</ion-label>
              </ion-col>
              <ion-col>
                <ion-input
                  id="userCode"
                  value={this.code ? this.code : ""}
                  disabled={true}
                  onIonInput={(event: InputCustomEvent) => {
                    this.code = event.target.value as string;
                  }}
                  placeholder="The Code of the countermeasure"
                ></ion-input>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col>
                <ion-label>Category:</ion-label>
              </ion-col>
              <ion-col>
                <ion-input
                  id="userCategory"
                  value={this.category ? this.category : ""}
                  onIonInput={(event: InputCustomEvent) => {
                    this.category = event.target.value as string;
                  }}
                  placeholder="The Category of the countermeasure"
                ></ion-input>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col>
                <ion-label>Service Life:</ion-label>
              </ion-col>
              <ion-col>
                <ion-input
                  id="userServiceLife"
                  value={this.serviceLife ? this.serviceLife : ""}
                  onIonInput={(event: InputCustomEvent) => {
                    this.serviceLife = Number(event.target.value);
                  }}
                  placeholder="The Service Life of the countermeasure"
                ></ion-input>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col>
                <ion-label>Unit Type:</ion-label>
              </ion-col>
              <ion-col>
                <ion-select
                  name="userUnitType"
                  interface="popover"
                  label="Unit Type"
                  label-placement="stacked"
                  value={this.units ? this.units : "Choose Unit Type"}
                  onIonChange={(event: SelectCustomEvent<string>) => {
                    this.units = event.detail.value;
                  }}
                >
                  <ion-select-option value="Miles">Miles</ion-select-option>
                  <ion-select-option value="Unit Qnty">
                    Unit Quantity
                  </ion-select-option>
                  <ion-select-option value="Other">Other</ion-select-option>
                </ion-select>
              </ion-col>
            </ion-row>
            {this.units === "Other" ? (
              <ion-row>
                <ion-col>
                  <ion-label>Custom Unit Type:</ion-label>
                </ion-col>
                <ion-col>
                  <ion-input
                    id="userUnitCustom"
                    value={this.customUnits ? this.customUnits : ""}
                    onIonInput={(event: InputCustomEvent) => {
                      this.customUnits = String(event.target.value);
                    }}
                    placeholder="Enter A Custom Unit Type"
                  ></ion-input>
                </ion-col>
              </ion-row>
            ) : (
              ""
            )}
            <ion-row>
              <ion-col>
                <ion-label>Crash Mitigation Factor:</ion-label>
              </ion-col>
              <ion-col>
                <ion-input
                  id="userCMF"
                  value={this.customCMF ? this.customCMF : ""}
                  onIonInput={(event: InputCustomEvent) => {
                    this.customCMF = Number(event.target.value);
                  }}
                  type="number"
                  step="0.01"
                ></ion-input>
              </ion-col>
            </ion-row>

            <ion-row>
              <ion-col>
                <ion-label>Affected Crash Types</ion-label>
              </ion-col>
            </ion-row>
            <ion-radio-group
              onIonChange={(event: RadioGroupCustomEvent<boolean>) => {
                this.selectCMF = event.detail.value;
              }}
              value={this.selectCMF}
            >
              <ion-row>
                <ion-col push="1">
                  <ion-radio value={false} labelPlacement="end">
                    All
                  </ion-radio>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col push="1">
                  <ion-radio value={true} labelPlacement="end">
                    Manually Select Crash Type
                  </ion-radio>
                </ion-col>
              </ion-row>
            </ion-radio-group>
            {this.selectCMF
              ? [
                  <ion-row>{crashTypeCheckboxes.slice(0, 4)}</ion-row>,
                  <ion-row>{crashTypeCheckboxes.slice(4, 8)}</ion-row>,
                  <ion-row>{crashTypeCheckboxes.slice(8, 12)}</ion-row>,
                  <ion-row>{crashTypeCheckboxes.slice(12, 16)}</ion-row>,
                  <ion-row>{crashTypeCheckboxes.slice(16, 20)}</ion-row>,
                ]
              : ""}

            <ion-row>
              <ion-col>
                <ion-button
                  id="save-and-close"
                  color="success"
                  onClick={() => {
                    this.closeModal(
                      new Countermeasure(
                        this.name,
                        this.category,
                        this.serviceLife,
                        this.createCMFVector(),
                        this.settingType,
                        this.facilityType,
                        this.units === "Other" ? this.customUnits : this.units,
                        this.code,
                        this.selectCMF ? this.affectedCrashTypes : ["All"],
                        this.projectType,
                        this.projectSystem
                      )
                    );
                  }}
                >
                  SAVE AND CLOSE
                </ion-button>
              </ion-col>
            </ion-row>
          </ion-grid>
        </ion-content>
      </Host>
    );
  }
}
