import * as docx from "docx";
import { ProjectElements } from "../../BCLIb/models/types";
import { RoadwayType } from "../../BCLIb/models/enums";
import { daytimeChart } from "../../chart/crashes-by-time-of-day-bar";
import { barChart } from "../../chart/crash-by-month-bar";
import { lineChart } from "../../chart/crashes-by-year-line";
import { pieChart } from "../../chart/crashes-severity-type-pie";
import { lightChart } from "../../chart/lightCondition-pie";
import { perChart } from "../../chart/Percent-crashes-road-surface-pie";
import { weatherChart } from "../../chart/Percent-crashes-weather-condition-pie";

const chartInfos: {
  name: string;
  chartFunc: (proj: ProjectElements, type: RoadwayType) => string;
}[] = [
  { name: "Crashes by month", chartFunc: barChart },
  {
    name: "Crashes by time of day",
    chartFunc: daytimeChart,
  },
  { name: "Crashes by year", chartFunc: lineChart },
  { name: "Crashes by severity", chartFunc: pieChart },
  {
    name: "Percent of crashes by road light conditions",
    chartFunc: lightChart,
  },
  {
    name: "Percent of crashes by road surface condition",
    chartFunc: perChart,
  },
  {
    name: "Percent of crashes by weather condition",
    chartFunc: weatherChart,
  },
];

export async function charts(proj: ProjectElements): Promise<docx.Paragraph[]> {
  const charts: docx.Paragraph[] = [];
  for (const [index, chartInfo] of chartInfos.entries()) {
    charts.push(
      ...(await getCharts(
        proj,
        chartInfo.name,
        chartInfo.chartFunc,
        index !== 0,
        RoadwayType.Intersection
      ))
    );
    charts.push(
      ...(await getCharts(
        proj,
        chartInfo.name,
        chartInfo.chartFunc,
        index !== 0,
        RoadwayType.Segment
      ))
    );
  }
  return [
    new docx.Paragraph({
      heading: docx.HeadingLevel.HEADING_2,
      alignment: docx.AlignmentType.CENTER,
      children: [
        new docx.TextRun({
          text: "Charts",
          color: docx.hexColorValue("#000000"),
          underline: {
            type: docx.UnderlineType.THICK,
          },
        }),
      ],
    }),
    ...charts,
  ];
}

async function getCharts(
  proj: ProjectElements,
  name: string,
  chartFunc: (proj: ProjectElements, type: RoadwayType) => string,
  pageBreak: boolean,
  type: RoadwayType
): Promise<docx.Paragraph[]> {
  return [
    new docx.Paragraph({
      alignment: docx.AlignmentType.CENTER,
      pageBreakBefore: pageBreak,
      children: [
        new docx.TextRun({
          text: `${name} (${
            type === RoadwayType.Segment ? "Segment" : "Intersection"
          } Crashes)`,
          color: docx.hexColorValue("#000000"),
          underline: {
            type: docx.UnderlineType.SINGLE,
          },
        }),
        new docx.TextRun({
          text: "",
          break: 2,
        }),
        await displayImage(chartFunc(proj, type)),
      ],
    }),
  ];
}

async function displayImage(url: string): Promise<docx.ImageRun> {
  const [natWidth, natHeight] = await getImageDimensions(url);
  return new docx.ImageRun({
    data: await (await fetch(url)).arrayBuffer(),
    transformation: {
      width: natWidth,
      height: natHeight,
    },
  });
}

function getImageDimensions(dataURL: string): Promise<[number, number]> {
  return new Promise((resolve) => {
    const img = new Image();
    img.onload = () => {
      resolve([img.naturalWidth, img.naturalHeight]);
    };
    img.src = dataURL;
  });
}
