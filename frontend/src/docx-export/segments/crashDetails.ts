import * as docx from "docx";
import { Crash } from "../../BCLIb/models/classes/Crash";
import {
  CrashInjuryType,
  RoadwayType,
  Severity,
} from "../../BCLIb/models/enums";
import { ProjectElements } from "../../BCLIb/models/types";

export async function crashDetailsByYear(
  proj: ProjectElements,
  predominantCrashTypes: string,
  proposedImprovements: string,
  estimatedProjectCost: string,
  benefitCostRatio: string,
  annualFatalCrashRate: string,
  annualSevereCrashRate: string,
  localRoadsRuralFunctionalClass: string | number,
  endStudyYear: number
): Promise<(docx.Table | docx.Paragraph)[]> {
  const showIntersectionCrashDetails =
    proj.get(RoadwayType.Intersection).length > 0;
  const showSegmentCrashDetails = proj.get(RoadwayType.Segment).length > 0;
  return [
    new docx.Paragraph({
      heading: docx.HeadingLevel.HEADING_2,
      children: [
        new docx.TextRun({
          text: "Crash Details",
          color: docx.hexColorValue("#000000"),
          underline: {
            type: docx.UnderlineType.THICK,
          },
        }),
      ],
    }),
    ...(showIntersectionCrashDetails
      ? [
          new docx.Paragraph({
            children: [
              new docx.TextRun({
                text: "Intersection Crashes",
                color: docx.hexColorValue("#000000"),
                underline: {
                  type: docx.UnderlineType.SINGLE,
                },
              }),
              new docx.TextRun({
                text: "",
                break: 1,
              }),
            ],
          }),
          await crashTable(proj, endStudyYear, RoadwayType.Intersection),
        ]
      : []),
    ...(showSegmentCrashDetails
      ? [
          new docx.Paragraph({
            children: [
              new docx.TextRun({
                text: "",
                break: 1,
              }),
              new docx.TextRun({
                text: "Segment Crashes",
                color: docx.hexColorValue("#000000"),
                underline: {
                  type: docx.UnderlineType.SINGLE,
                },
              }),
              new docx.TextRun({
                text: "",
                break: 1,
              }),
            ],
          }),
          await crashTable(proj, endStudyYear, RoadwayType.Segment),
        ]
      : []),
    new docx.Paragraph(""),
    crashDetails(
      predominantCrashTypes,
      proposedImprovements,
      estimatedProjectCost,
      benefitCostRatio,
      annualFatalCrashRate,
      annualSevereCrashRate,
      localRoadsRuralFunctionalClass
    ),
  ];
}

async function crashTable(
  proj: ProjectElements,
  endStudyYear: number,
  roadwayType: RoadwayType
): Promise<docx.Table> {
  return new docx.Table({
    width: {
      size: 100,
      type: docx.WidthType.PERCENTAGE,
    },
    rows: [
      new docx.TableRow({
        tableHeader: true,
        children: [
          "Year",
          "Total Crashes",
          "Fatal Crashes",
          "Fatalities",
          "A Injury Crashes",
          "A Injuries",
          "B Injury Crashes",
          "B Injuries",
          "C Injury Crashes",
          "C Injuries",
          "No Injury Crashes",
          "Wet-Pavement Crashes",
          "Darkness Crashes",
        ].map((label) => {
          return new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            margins: {
              right: 60,
              left: 60,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    text: label,
                    size: 12,
                    bold: true,
                  }),
                ],
              }),
            ],
          });
        }),
      }),
      ...(await getCrashDetailRows(
        proj.get(roadwayType).flatMap((roadwayElem) => roadwayElem.crashes),
        endStudyYear
      )),
    ],
  });
}

function crashDetails(
  predominantCrashTypes: string,
  proposedImprovements: string,
  estimatedProjectCost: string,
  benefitCostRatio: string,
  annualFatalCrashRate: string,
  annualSevereCrashRate: string,
  localRoadsRuralFunctionalClass: string | number
): docx.Table {
  return new docx.Table({
    width: {
      type: docx.WidthType.PERCENTAGE,
      size: 100,
    },
    columnWidths: [50, 50],
    rows: [
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Local Description: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Problem Description: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Previous Safety Improvements: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Collision Diagram: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Images (Y/N): ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Predominant Crash Types: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: predominantCrashTypes,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        height: {
          value: 80,
          rule: docx.HeightRule.EXACT,
        },
        children: [
          new docx.TableCell({
            columnSpan: 4,
            shading: {
              color: "#000000",
              fill: "#616161",
            },
            children: [],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Proposed Improvement(s): ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: proposedImprovements,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        height: {
          value: 80,
          rule: docx.HeightRule.EXACT,
        },
        children: [
          new docx.TableCell({
            columnSpan: 4,
            shading: {
              color: "#000000",
              fill: "#616161",
            },
            children: [],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Estimated Project Cost ($000's): ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: estimatedProjectCost,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Benefit-Cost Ratio: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: benefitCostRatio,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 4,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Local Projects: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Annual Fatal Crash Rate (Fatal Crashes/100 Miles): ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: annualFatalCrashRate,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Annual A Injury Crash Rate (A Injury Crashes/100 Miles): ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: annualSevereCrashRate,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Local Roads Rural Functional Class: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: localRoadsRuralFunctionalClass.toString(),
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Approved: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Central HSIP Approval Date: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Signed: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: "State Safety Engineer ",
                    break: 1,
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Funding (HSIP/HRRR/RAIL): ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Comment: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Distribution (OOP/District/BSPE/LRS/BDE): ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
    ],
  });
}

async function getCrashDetailRows(
  crashes: Crash[],
  endYear: number
): Promise<docx.TableRow[]> {
  const years = [];
  for (let i = endYear - 4; i <= endYear; i++) years.push(i);

  return Promise.all(
    years.map(async (year) => {
      return new docx.TableRow({
        children: [
          new docx.TableCell({
            children: [new docx.Paragraph(`${year}`)],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${crashes.filter((crash) => crash.crashYear === year).length}`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${
                  crashes
                    .filter((crash) => crash.crashYear === year)
                    .filter(
                      (crash) => crash.crashSeverity === CrashInjuryType.F
                    ).length
                }`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${crashes
                  .filter((crash) => crash.crashYear === year)
                  .map((crash) => crash.injuryProfile.get(Severity.F))
                  .reduce((a, b) => a + b, 0)}`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${
                  crashes
                    .filter((crash) => crash.crashYear === year)
                    .filter(
                      (crash) => crash.crashSeverity === CrashInjuryType.A
                    ).length
                }`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${crashes
                  .filter((crash) => crash.crashYear === year)
                  .map((crash) => crash.injuryProfile.get(Severity.A))
                  .reduce((a, b) => a + b, 0)}`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${
                  crashes
                    .filter((crash) => crash.crashYear === year)
                    .filter(
                      (crash) => crash.crashSeverity === CrashInjuryType.B
                    ).length
                }`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${crashes
                  .filter((crash) => crash.crashYear === year)
                  .map((crash) => crash.injuryProfile.get(Severity.B))
                  .reduce((a, b) => a + b, 0)}`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${
                  crashes
                    .filter((crash) => crash.crashYear === year)
                    .filter(
                      (crash) => crash.crashSeverity === CrashInjuryType.C
                    ).length
                }`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${crashes
                  .filter((crash) => crash.crashYear === year)
                  .map((crash) => crash.injuryProfile.get(Severity.C))
                  .reduce((a, b) => a + b, 0)}`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${
                  crashes
                    .filter((crash) => crash.crashYear === year)
                    .filter(
                      (crash) => crash.crashSeverity === CrashInjuryType.N
                    ).length
                }`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${
                  crashes
                    .filter((crash) => crash.crashYear === year)
                    .filter((crash) => crash.surfaceCondition !== "Dry").length
                }`
              ),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph(
                `${
                  crashes
                    .filter((crash) => crash.crashYear === year)
                    .filter((crash) => crash.lightCondition === "Darkness")
                    .length
                }`
              ),
            ],
          }),
        ],
      });
    })
  );
}
