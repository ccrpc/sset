import * as docx from "docx";

export function timelineTemplate(): (docx.Table | docx.Paragraph)[] {
  const thisYear = new Date().getFullYear();
  return [
    new docx.Paragraph({
      alignment: docx.AlignmentType.CENTER,
      heading: docx.HeadingLevel.HEADING_2,
      children: [
        new docx.TextRun({
          text: "Project Timeline",
          color: docx.hexColorValue("#000000"),
          underline: {
            type: docx.UnderlineType.SINGLE,
          },
        }),
        new docx.TextRun({
          text: "",
          break: 1,
        }),
      ],
    }),
    new docx.Table({
      width: {
        size: 100,
        type: docx.WidthType.PERCENTAGE,
      },
      columnWidths: [2, 2, ...new Array(24).fill(1)],
      rows: [
        new docx.TableRow({
          height: {
            rule: docx.HeightRule.ATLEAST,
            value: 300,
          },
          children: [
            new docx.TableCell({
              columnSpan: 2,
              borders: {
                left: { style: docx.BorderStyle.NONE },
                top: { style: docx.BorderStyle.NONE },
                right: { style: docx.BorderStyle.THICK },
                bottom: { style: docx.BorderStyle.THICK },
              },
              children: [
                new docx.Paragraph({
                  children: [
                    new docx.TextRun({
                      text: "",
                    }),
                  ],
                }),
              ],
            }),
            new docx.TableCell({
              columnSpan: 12,
              verticalAlign: docx.VerticalAlign.CENTER,
              borders: {
                left: { style: docx.BorderStyle.THICK },
                top: { style: docx.BorderStyle.THICK },
                right: { style: docx.BorderStyle.THICK },
                bottom: { style: docx.BorderStyle.SINGLE },
              },
              children: [
                new docx.Paragraph({
                  alignment: docx.AlignmentType.CENTER,
                  children: [
                    new docx.TextRun({
                      text: String(thisYear + 1),
                    }),
                  ],
                }),
              ],
            }),
            new docx.TableCell({
              columnSpan: 12,
              borders: {
                left: { style: docx.BorderStyle.THICK },
                top: { style: docx.BorderStyle.THICK },
                right: { style: docx.BorderStyle.THICK },
                bottom: { style: docx.BorderStyle.SINGLE },
              },
              verticalAlign: docx.VerticalAlign.CENTER,
              children: [
                new docx.Paragraph({
                  alignment: docx.AlignmentType.CENTER,
                  children: [
                    new docx.TextRun({
                      text: String(thisYear + 2),
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
        new docx.TableRow({
          height: {
            rule: docx.HeightRule.ATLEAST,
            value: 500,
          },
          children: [
            new docx.TableCell({
              verticalAlign: docx.VerticalAlign.CENTER,
              borders: {
                left: { style: docx.BorderStyle.THICK },
              },
              children: [
                new docx.Paragraph({
                  alignment: docx.AlignmentType.CENTER,
                  children: [
                    new docx.TextRun({
                      text: "Tasks",
                    }),
                  ],
                }),
              ],
            }),
            new docx.TableCell({
              verticalAlign: docx.VerticalAlign.CENTER,
              children: [
                new docx.Paragraph({
                  alignment: docx.AlignmentType.CENTER,
                  children: [
                    new docx.TextRun({
                      text: "Period",
                    }),
                  ],
                }),
              ],
            }),
            ...monthLabels(),
            ...monthLabels(),
          ],
        }),
        ...contentRows(3, 3),
      ],
    }),
  ];
}

function monthLabels(): docx.TableCell[] {
  return [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sept",
    "Oct",
    "Nov",
    "Dec",
  ].map((label, index) => {
    return new docx.TableCell({
      verticalAlign: docx.VerticalAlign.CENTER,
      textDirection: docx.TextDirection.TOP_TO_BOTTOM_RIGHT_TO_LEFT,
      shading: {
        color: "#000000",
        type:
          index % 2 === 0
            ? docx.ShadingType.PERCENT_15
            : docx.ShadingType.CLEAR,
      },
      borders: {
        left: {
          style: index === 0 ? docx.BorderStyle.THICK : docx.BorderStyle.SINGLE,
        },

        right: {
          style:
            index === 11 ? docx.BorderStyle.THICK : docx.BorderStyle.SINGLE,
        },
      },
      children: [
        new docx.Paragraph({
          alignment: docx.AlignmentType.CENTER,
          children: [
            new docx.TextRun({
              text: label,
            }),
          ],
        }),
      ],
    });
  });
}

function contentRows(
  numberOfTasks: number,
  numberOfSubTasks: number
): docx.TableRow[] {
  return [...new Array(numberOfTasks).keys()].flatMap((task, taskIndex) => {
    return [
      contentRow(`Task ${task + 1}`, true, false),
      ...new Array(numberOfSubTasks)
        .fill(1)
        .map((_, subTaskIndex) =>
          contentRow(
            "Subtask",
            false,
            taskIndex === numberOfSubTasks - 1 &&
              subTaskIndex === numberOfSubTasks - 1
          )
        ),
    ];
  });
}

function contentRow(
  taskText: string,
  shading: boolean,
  bottomBorder: boolean
): docx.TableRow {
  return new docx.TableRow({
    children: [
      new docx.TableCell({
        verticalAlign: docx.VerticalAlign.CENTER,
        shading: {
          color: "#000000",
          type: shading ? docx.ShadingType.PERCENT_10 : docx.ShadingType.CLEAR,
        },
        borders: {
          left: { style: docx.BorderStyle.THICK },
          bottom: {
            style: bottomBorder
              ? docx.BorderStyle.THICK
              : docx.BorderStyle.SINGLE,
          },
        },
        children: [
          new docx.Paragraph({
            alignment: docx.AlignmentType.CENTER,
            children: [new docx.TextRun({ text: taskText })],
          }),
        ],
      }),
      new docx.TableCell({
        verticalAlign: docx.VerticalAlign.CENTER,
        shading: {
          color: "#000000",
          type: shading ? docx.ShadingType.PERCENT_10 : docx.ShadingType.CLEAR,
        },
        borders: {
          right: { style: docx.BorderStyle.THICK },
          bottom: {
            style: bottomBorder
              ? docx.BorderStyle.THICK
              : docx.BorderStyle.SINGLE,
          },
        },
        children: [
          new docx.Paragraph({
            alignment: docx.AlignmentType.CENTER,
            children: [new docx.TextRun({ text: "X Days" })],
          }),
        ],
      }),
      ...new Array(24).fill(1).map((_, index) => {
        return new docx.TableCell({
          borders: {
            right: {
              style:
                index === 11 || index === 23
                  ? docx.BorderStyle.THICK
                  : docx.BorderStyle.SINGLE,
            },
            bottom: {
              style: bottomBorder
                ? docx.BorderStyle.THICK
                : docx.BorderStyle.SINGLE,
            },
          },
          shading: {
            color: "#000000",
            type: shading
              ? docx.ShadingType.PERCENT_10
              : docx.ShadingType.CLEAR,
          },
          children: [
            new docx.Paragraph({
              children: [new docx.TextRun({ text: "" })],
            }),
          ],
        });
      }),
    ],
  });
}
