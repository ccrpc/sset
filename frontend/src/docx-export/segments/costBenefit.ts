import * as docx from "docx";
import { CostCalculator } from "../../BCLIb/CostBreakdown";
import { getTypedBenefit } from "../../BCLIb/BenefitBreakdown";
import { CrashType, RoadwayType } from "../../BCLIb/models/enums";
import {
  IAppliedCountermeasure,
  IRoadwayElement,
} from "../../BCLIb/models/interfaces";
import { CmfVector, ProjectElements } from "../../BCLIb/models/types";
import { labelToCode } from "../../BCLIb/utils";
import { Style } from "docx";

const formatter = Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});

export async function costBenefit(
  proj: ProjectElements,
  endStudyYear: number
): Promise<(docx.Table | docx.Paragraph)[]> {
  const result: (docx.Paragraph | docx.Table)[] = [];

  if (proj.get(RoadwayType.Intersection).length > 0) {
    result.push(
      new docx.Paragraph({
        heading: docx.HeadingLevel.HEADING_2,
        alignment: docx.AlignmentType.CENTER,
        children: [
          new docx.TextRun({
            text: "LOCAL INTERSECTIONS BENEFIT COST ANALYSIS",
            color: docx.hexColorValue("#000000"),
            underline: {
              type: docx.UnderlineType.THICK,
            },
          }),
        ],
      })
    );
    result.push(
      await countermeasureTable(proj, RoadwayType.Intersection, endStudyYear)
    );
  }

  if (proj.get(RoadwayType.Segment).length > 0) {
    result.push(
      new docx.Paragraph({
        heading: docx.HeadingLevel.HEADING_2,
        alignment: docx.AlignmentType.CENTER,
        children: [
          new docx.TextRun({
            text: "LOCAL SEGMENTS BENEFIT COST ANALYSIS",
            color: docx.hexColorValue("#000000"),
            underline: {
              type: docx.UnderlineType.THICK,
            },
          }),
        ],
      })
    );
    result.push(
      await countermeasureTable(proj, RoadwayType.Segment, endStudyYear)
    );
  }

  return result;
}

async function countermeasureTable(
  proj: ProjectElements,
  roadwayType: RoadwayType,
  endStudyYear: number
): Promise<docx.Table> {
  const typedCountermeasures: IAppliedCountermeasure[] = proj
    .get(roadwayType)
    .map(
      (roadwayElement: IRoadwayElement) => roadwayElement.appliedCountermeasures
    )
    .flat(1);

  const totalBenefit = getTypedBenefit(proj, endStudyYear, roadwayType);
  const totalCost = CostCalculator.calculateTotalCost(typedCountermeasures);
  const benefitCostRatio = (totalBenefit.benefit / totalCost).toFixed(2);

  return new docx.Table({
    columnWidths: [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    width: {
      size: 100,
      type: docx.WidthType.PERCENTAGE,
    },
    rows: [
      // Overall Header Row
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 9,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "BENEFIT CALCULATIONS",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 7,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "COUNTERMEASURE COST CALCULATIONS",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      // Blank Row
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 9,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 7,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      // Countermeasure Header Row
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 5,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
            },
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    italics: true,
                    text: "COUNTERMEASURE",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    italics: true,
                    text: "CMF",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 3,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              right: { style: docx.BorderStyle.THICK },
            },
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    italics: true,
                    text: "Crash Type affected by this improvement",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    italics: true,
                    text: "Unit Cost",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    italics: true,
                    text: "Quantity",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    italics: true,
                    text: "Units",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    italics: true,
                    text: "Total Cost",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    italics: true,
                    text: "Service Life",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    italics: true,
                    text: "Present worth",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              right: { style: docx.BorderStyle.THICK },
            },
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    italics: true,
                    text: "EUAC",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      // This creates a row for each countermeasure with the information about that countermeasure
      ...[...(await coalesceCountermeasures(proj, roadwayType)).values()].map(
        (obj) => {
          return new docx.TableRow({
            children: [
              new docx.TableCell({
                columnSpan: 5,
                verticalAlign: docx.VerticalAlign.CENTER,
                borders: {
                  left: { style: docx.BorderStyle.THICK },
                },
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: obj.name,
                      }),
                    ],
                  }),
                ],
              }),
              new docx.TableCell({
                columnSpan: 1,
                verticalAlign: docx.VerticalAlign.CENTER,
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: obj.cmf,
                      }),
                    ],
                  }),
                ],
              }),
              new docx.TableCell({
                columnSpan: 3,
                verticalAlign: docx.VerticalAlign.CENTER,
                borders: {
                  right: { style: docx.BorderStyle.THICK },
                },
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: String(obj.crashTypesAffected),
                      }),
                    ],
                  }),
                ],
              }),
              new docx.TableCell({
                columnSpan: 1,
                verticalAlign: docx.VerticalAlign.CENTER,
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: formatter.format(obj.unitCost),
                      }),
                    ],
                  }),
                ],
              }),
              new docx.TableCell({
                columnSpan: 1,
                verticalAlign: docx.VerticalAlign.CENTER,
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: String(obj.quantity),
                      }),
                    ],
                  }),
                ],
              }),
              new docx.TableCell({
                columnSpan: 1,
                verticalAlign: docx.VerticalAlign.CENTER,
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: obj.units,
                      }),
                    ],
                  }),
                ],
              }),
              new docx.TableCell({
                columnSpan: 1,
                verticalAlign: docx.VerticalAlign.CENTER,
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: formatter.format(obj.totalCost),
                      }),
                    ],
                  }),
                ],
              }),
              new docx.TableCell({
                columnSpan: 1,
                verticalAlign: docx.VerticalAlign.CENTER,
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: String(obj.serviceLife),
                      }),
                    ],
                  }),
                ],
              }),
              new docx.TableCell({
                columnSpan: 1,
                verticalAlign: docx.VerticalAlign.CENTER,
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: formatter.format(obj.presentWorth),
                      }),
                    ],
                  }),
                ],
              }),
              new docx.TableCell({
                columnSpan: 1,
                verticalAlign: docx.VerticalAlign.CENTER,
                borders: {
                  right: { style: docx.BorderStyle.THICK },
                },
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: formatter.format(obj.euac),
                      }),
                    ],
                  }),
                ],
              }),
            ],
          });
        }
      ),
      // A blank countermeasure row as if there were a countermeasure with no information
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 5,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 3,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              right: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              right: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      // Total Benefit and cost
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 3,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    italics: true,
                    bold: true,
                    text: "TOTAL BENEFIT",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.RIGHT,
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: formatter.format(totalBenefit.benefit),
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.NONE },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 3,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.NONE },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 5,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    bold: true,
                    italics: true,
                    text: "TOTAL COST",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.RIGHT,
                children: [
                  new docx.TextRun({
                    bold: true,
                    italics: true,
                    text: formatter.format(totalCost),
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      // A blank row with no borders to offset the next information
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 16,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              start: { style: docx.BorderStyle.NONE },
              end: { style: docx.BorderStyle.NONE },
              bottom: { style: docx.BorderStyle.NONE },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    italics: true,
                    bold: true,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      // Benefit / Cost row
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 3,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    italics: true,
                    bold: true,
                    text: "BENEFIT / COST",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.RIGHT,
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: String(benefitCostRatio),
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.NONE },
              right: { style: docx.BorderStyle.NONE },
              top: { style: docx.BorderStyle.NONE },
              bottom: { style: docx.BorderStyle.NONE },
            },
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 4,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "ANNUAL FATALITIES PREVENTED",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    bold: true,
                    italics: true,
                    text: String(totalBenefit.annualPrevented.toFixed(3)),
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.NONE },
              right: { style: docx.BorderStyle.NONE },
              top: { style: docx.BorderStyle.NONE },
              bottom: { style: docx.BorderStyle.NONE },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.RIGHT,
                children: [
                  new docx.TextRun({
                    bold: true,
                    italics: true,
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 3,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    bold: true,
                    italics: true,
                    text: "TOTAL FATALITIES PREVENTED",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.RIGHT,
                children: [
                  new docx.TextRun({
                    bold: true,
                    italics: true,
                    text: String(totalBenefit.totalPrevented.toFixed(3)),
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
    ],
  });
}

/**
 *
 * @param {ProjectElements} proj An array of all of the features in the project
 * @param {RoadwayType} roadwayType What type of countermeasures to look at, either Intersections or Semgents
 * @returns A map of form countermeasure.code: {information about that countermeasure}
 */
async function coalesceCountermeasures(
  proj: ProjectElements,
  roadwayType: RoadwayType
): Promise<
  Map<
    string,
    {
      name: string;
      cmf: any;
      crashTypesAffected: string[];
      unitCost: number;
      quantity: number;
      units: string;
      totalCost: number;
      serviceLife: number;
      presentWorth: number;
      euac: number;
    }
  >
> {
  const map = new Map<
    string,
    {
      name: string;
      cmf: any;
      crashTypesAffected: string[];
      unitCost: number;
      quantity: number;
      units: string;
      totalCost: number;
      serviceLife: number;
      presentWorth: number;
      euac: number;
    }
  >();

  const typedCountermeasures: IAppliedCountermeasure[] = proj
    .get(roadwayType)
    .map(
      (roadwayElement: IRoadwayElement) => roadwayElement.appliedCountermeasures
    )
    .flat(1);

  const costCalculator: CostCalculator = new CostCalculator(
    typedCountermeasures
  );

  proj
    .get(roadwayType)
    .flatMap((re: IRoadwayElement) => re.appliedCountermeasures)
    .forEach((acm: IAppliedCountermeasure) => {
      const acmWithCost = costCalculator.calculateAppliedCountermeasure(acm);
      if (!map.get(acm.countermeasure.code)) {
        map.set(acm.countermeasure.code, {
          name: [
            acm.countermeasure.code,
            acm.countermeasure.category,
            acm.countermeasure.name,
          ].join(" - "),
          cmf: getCMFFromVector(acm.countermeasure.cmfVector),
          crashTypesAffected: acm.countermeasure.affectedCrashTypes,
          unitCost: acm.unitCost,
          quantity: acm.quantity,
          units: acm.countermeasure.units,
          totalCost: acm.unitCost * acm.quantity,
          serviceLife: acm.countermeasure.serviceLife,
          presentWorth: acmWithCost.presentWorth.reduce(
            (previousValue, currentValue) => previousValue + currentValue,
            0
          ),
          euac: CostCalculator.round50(acmWithCost.EAUCCost),
        });
      } else {
        // The following occurs if more than one instance of the same counter measure is added.
        // Whether that is something we wish to allow is questionable.
        const oldObj = map.get(acm.countermeasure.code);

        map.set(acm.countermeasure.code, {
          ...oldObj,
          quantity: oldObj.quantity + acm.quantity,
          totalCost: oldObj.totalCost + acm.unitCost * acm.quantity,
          presentWorth:
            oldObj.presentWorth +
            acmWithCost.presentWorth.reduce(
              (previousValue, currentValue) => previousValue + currentValue,
              0
            ),
          euac: oldObj.euac + CostCalculator.round50(acmWithCost.EAUCCost),
        });
      }
    });
  return map;
}

function createCmfValue(vector: CmfVector): Map<number, CrashType[]> {
  const cmfValue: Map<number, CrashType[]> = new Map();
  vector.forEach((mitigationFactor: number, crashType: CrashType) => {
    if (mitigationFactor === 1) return;
    if (cmfValue.has(mitigationFactor)) {
      cmfValue.get(mitigationFactor).push(crashType);
    } else {
      cmfValue.set(mitigationFactor, [crashType]);
    }
  });

  return cmfValue;
}

function getCMFFromVector(vector: CmfVector): string {
  const cmfValue = createCmfValue(vector);
  if (cmfValue.size === 1) {
    return String([...cmfValue.keys()][0]);
  } else {
    let result = "";
    cmfValue.forEach((crashTypes: CrashType[], mitigationFactor: number) => {
      result = result.concat(
        `${crashTypes
          .map((ct: CrashType) => labelToCode(ct))
          .join(",")}=${mitigationFactor};`
      );
    });
    return result;
  }
}
