import * as docx from "docx";
import { Paragraph } from "docx";
import {
  CrashInjuryType,
  RoadwayType,
  Severity,
} from "../../BCLIb/models/enums";
import { ProjectElements } from "../../BCLIb/models/types";

export function rawCrashData(
  proj: ProjectElements
): (docx.Table | docx.Paragraph)[] {
  return [
    new docx.Paragraph({
      heading: docx.HeadingLevel.HEADING_2,
      alignment: docx.AlignmentType.CENTER,
      children: [
        new docx.TextRun({
          text: "Raw Crash Data",
          color: docx.hexColorValue("#000000"),
          underline: {
            type: docx.UnderlineType.THICK,
          },
        }),
      ],
    }),
    new docx.Paragraph({
      children: [
        new docx.TextRun({
          text: "Intersection Crashes",
          color: docx.hexColorValue("#000000"),
          underline: {
            type: docx.UnderlineType.SINGLE,
          },
        }),
        new docx.TextRun({
          text: "",
          break: 1,
        }),
      ],
    }),
    crashTable(proj, RoadwayType.Intersection),
    new docx.Paragraph({
      children: [
        new docx.TextRun({
          text: "",
          break: 1,
        }),
        new docx.TextRun({
          text: "Segment Crashes",
          color: docx.hexColorValue("#000000"),
          underline: {
            type: docx.UnderlineType.SINGLE,
          },
        }),
        new docx.TextRun({
          text: "",
          break: 1,
        }),
      ],
    }),
    crashTable(proj, RoadwayType.Segment),
  ];
}

function crashTable(proj: ProjectElements, type: RoadwayType): docx.Table {
  return new docx.Table({
    width: {
      type: docx.WidthType.PERCENTAGE,
      size: 100,
    },
    rows: [
      new docx.TableRow({
        children: [
          "CRASH ID",
          "YEAR",
          "MONTH",
          "DAY",
          "HOUR",
          "DAY OF WEEK",
          "NUMBER OF VEHICLES",
          "INJURIES",
          "REC_TYPE",
          "FATALITIES",
          "COLLISION TYPE",
          "WEATHER",
          "LIGHTNING",
          "SURFACE COND",
          "VEH1 DIRECTION",
          "VEH1 MANEUVER",
          "VEH2 DIRECTION",
          "VEH2 MANEUVER",
        ].map((header) => {
          return new docx.TableCell({
            width: {
              size: 1,
              type: docx.WidthType.AUTO,
            },
            verticalAlign: docx.VerticalAlign.CENTER,
            children: [
              new Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 14,
                    text: header,
                  }),
                ],
              }),
            ],
          });
        }),
      }),
      ...proj
        .get(type)
        .flatMap((re) => re.crashes)
        .map((c) => {
          let color: string;
          switch (c.crashSeverity) {
            case CrashInjuryType.F:
              color = "#ffa8a8";
              break;
            case CrashInjuryType.A:
              color = "#fad778";
              break;
            default:
              color = "#ffffff";
          }
          return new docx.TableRow({
            children: [
              c.id,
              c.crashYear,
              c.crashMonth,
              c.crashDay,
              c.crashHour,
              c.crashWeekday,
              c.numberOfVehicles,
              c.totalInjuries,
              c.crashSeverity,
              c.injuryProfile.get(Severity.F),
              c.crashType,
              c.weatherCondition,
              c.lightCondition,
              c.surfaceCondition,
              "",
              "",
              "",
              "",
            ].map((field) => {
              return new docx.TableCell({
                shading: {
                  color: color,
                  type: docx.ShadingType.PERCENT_75,
                },
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 14,
                        text: field.toString(),
                      }),
                    ],
                  }),
                ],
              });
            }),
          });
        }),
    ],
  });
}
