import * as docx from "docx";
import { RoadwayType } from "../../BCLIb/models/enums";
import { ProjectElements } from "../../BCLIb/models/types";
import {
  Map as MapboxGLMap,
  MapboxGeoJSONFeature,
  LngLatBounds,
  LngLatLike,
  LineLayer,
  CircleLayer,
  LngLat,
} from "mapbox-gl";
import { IRoadwayElement } from "../../BCLIb/models/interfaces";
import {
  INTX_PROP_LABELS,
  SEG_PROP_LABELS,
  CRASH_PROP_LABELS,
} from "../../BCLIb/models/consts";
import { LineString, Point } from "geojson";

const turbo5 = ["#23171b", "#26bce1", "#95fb51", "#ff821d", "#900c00"];
const turbo7 = [
  "#23171b",
  "#3987f9",
  "#2ee5ae",
  "#95fb51",
  "#feb927",
  "#e54813",
  "#900c00",
];
const years: (string | number)[] = [
  2019,
  "#64ff00",
  2020,
  "#e63913",
  2021,
  "#00dcff",
  2022,
  "#8801cb",
  2023,
  "#f5cd04",
  "#b04e1d",
];

const screenshotMaxWidth = 900;
const screenshotMaxHeight = 600;

interface imageData {
  height: number;
  width: number;
  dataUrl: string;
  name: string;
  id: number;
  type: RoadwayType;
}

export async function locationMap(
  proj: ProjectElements,
  map: MapboxGLMap
): Promise<(docx.Paragraph | docx.Table)[]> {
  const segmentIDList: number[] = proj
    .get(RoadwayType.Segment)
    .map((segment: IRoadwayElement) => segment.id);

  const intersectionIDList: number[] = proj
    .get(RoadwayType.Intersection)
    .map((intersection: IRoadwayElement) => intersection.id);

  // Disable user controls while we get the information we need
  map.dragPan.disable();
  map.scrollZoom.disable();
  map.doubleClickZoom.disable();
  map.keyboard.disable();

  const previousBounds = map.getBounds();
  const previousStyle = map.getStyle();

  map.setMaxBounds(null);

  // Zoom out to show the while map so that the query includes all of the features
  map.fitBounds(
    [
      [-88.489976, 39.851991],
      [-87.915597, 40.414454],
    ],
    { linear: false }
  );
  await waitForIdle(map);

  // Look for the features the user has selected.
  // Note that if a feature spans a tile boundary two of it will be found by this query and the user will just need to use whichever picture they prefer.
  let segments: MapboxGeoJSONFeature[] = [];
  if (segmentIDList) {
    segments = map.querySourceFeatures("app:segments", {
      sourceLayer: "segments",
      filter: ["in", ["get", SEG_PROP_LABELS.ID], ["literal", segmentIDList]],
    });
  }

  let intersections: MapboxGeoJSONFeature[] = [];
  if (intersectionIDList) {
    intersections = map.querySourceFeatures("app:intersections", {
      sourceLayer: "intersections",
      filter: [
        "in",
        ["get", INTX_PROP_LABELS.ID],
        ["literal", intersectionIDList],
      ],
    });
  }

  const segmentBounds: Map<number, { name: string; bounds: LngLatBounds }> =
    createBounds(RoadwayType.Segment, segments);

  const intersectionBounds: Map<
    number,
    { name: string; bounds: LngLatBounds }
  > = createBounds(RoadwayType.Intersection, intersections);

  colorCrashesByYear(map);
  // pan to each feature (via fitBounds) and take a screenshot
  const screenshots: imageData[] = [
    ...(await panAndScreenshot(
      RoadwayType.Intersection,
      map,
      intersectionBounds
    )),
    ...(await panAndScreenshot(RoadwayType.Segment, map, segmentBounds)),
  ];

  map.setStyle(previousStyle, { diff: true });
  map.fitBounds([previousBounds.getSouthWest(), previousBounds.getNorthEast()]);

  // Re-enable user controls
  map.setMaxBounds([
    [-88.4909897, 39.8724889],
    [-87.9176502, 40.4140038],
  ]);

  map.dragPan.enable();
  map.scrollZoom.enable();
  map.keyboard.enable();
  map.doubleClickZoom.enable();

  const docxImages: docx.Paragraph[] = [];
  for (const screenshot of screenshots) {
    docxImages.push(await featureImageDocx(screenshot));
  }

  return [
    new docx.Paragraph({
      heading: docx.HeadingLevel.HEADING_2,
      alignment: docx.AlignmentType.CENTER,
      children: [
        new docx.TextRun({
          text: "Location Map",
          color: docx.hexColorValue("#000000"),
          underline: {
            type: docx.UnderlineType.THICK,
          },
        }),
      ],
    }),
    createLegend(),
    new docx.Paragraph({
      children: [new docx.PageBreak()],
    }),

    ...docxImages,
  ];
}

/**
 * Returns a docx.Paragraph with the formatting and information of the image.
 * @param screenshot The image to create the docx object for.
 * @returns
 */
async function featureImageDocx(
  screenshot: imageData
): Promise<docx.Paragraph> {
  const widthHeightRatio = screenshot.width / screenshot.height;

  let width = screenshot.width;
  let height = screenshot.height;
  if (screenshot.width > screenshotMaxWidth) {
    width = screenshotMaxWidth;
    height = screenshotMaxWidth / widthHeightRatio;
  } else if (screenshot.height > screenshotMaxHeight) {
    width = screenshotMaxHeight * widthHeightRatio;
    height = screenshotMaxHeight;
  }
  return new docx.Paragraph({
    alignment: docx.AlignmentType.CENTER,
    children: [
      new docx.TextRun({
        text:
          screenshot.type === RoadwayType.Segment
            ? `${screenshot.name}, section: ${screenshot.id}`
            : screenshot.name,
        color: docx.hexColorValue("#000000"),
        underline: {
          type: docx.UnderlineType.SINGLE,
        },
      }),
      new docx.TextRun({
        text: "",
        break: 1,
      }),
      new docx.ImageRun({
        data: await (await fetch(screenshot.dataUrl)).arrayBuffer(),
        transformation: {
          width: width,
          height: height,
        },
      }),
    ],
  });
}

/**
 * Creates and returns a screenshot of the map, the entire rendered area.
 * @param map The mapbox-gl map of which to take the screenshot
 * @returns the base65Url of the image, as a png
 */
function takeScreenshot(map: MapboxGLMap): Promise<string> {
  return new Promise((resolve) => {
    // We trigger on a render event as otherwise the image will be empty due to mapbox-gl using a drawing buffer.
    map.once("render", () => {
      resolve(map.getCanvas().toDataURL());
    });
    (map as any)._render(); // Causes the map to render. We are using (map as any) as the _render() function is not publicly exposed. If there is a publicly exposed equivalent, change this.
  });
}

/**
 * Returns a promise that will fire the next time the map is idle.
 *
 * See also https://docs.mapbox.com/mapbox-gl-js/api/map/#map.event:idle
 * @param map The mapboxgl map to wait on
 * @returns
 */
export function waitForIdle(map: MapboxGLMap): Promise<void> {
  return new Promise((resolve) => {
    map.once("idle", () => {
      resolve();
    });
  });
}

/**
 * Creates bounds for the features passed in
 * @param type Whether the features passed in are Segments or Intersections
 * @param features The features from which to create the bounds
 * @returns The bounds from those features
 */
function createBounds(
  type: RoadwayType,
  features: MapboxGeoJSONFeature[]
): Map<number, { name: string; bounds: LngLatBounds }> {
  const featureBounds: Map<number, { name: string; bounds: LngLatBounds }> =
    new Map();

  for (const feature of features) {
    const id = Number(
      feature.properties[
      type === RoadwayType.Segment ? SEG_PROP_LABELS.ID : INTX_PROP_LABELS.ID
      ]
    );
    let bounds: LngLatBounds;
    if (!featureBounds.has(id)) {
      bounds = new LngLatBounds();
    } else {
      bounds = featureBounds.get(id).bounds;
    }
    if (type === RoadwayType.Segment) {
      for (const coord of (feature.geometry as LineString).coordinates) {
        bounds.extend(coord as LngLatLike);
      }
    } else {
      bounds.extend((feature.geometry as Point).coordinates as LngLatLike);
    }
    featureBounds.set(id, {
      name:
        type === RoadwayType.Segment
          ? feature.properties[SEG_PROP_LABELS.NAME]
          : feature.properties[INTX_PROP_LABELS.NAME],
      bounds: bounds,
    });
  }

  return featureBounds;
}

/**
 * Pans\* to each bound supplied in featureBounds and takes a screenshot.
 *
 * \* Using map.fitBounds
 * @param type The type of bounds we are looking at, either Segment or Intersection
 * @param map The mapboxgl map the features reside on
 * @param featureBounds The bounds of the features
 * @returns
 */
async function panAndScreenshot(
  type: RoadwayType,
  map: MapboxGLMap,
  featureBounds: Map<number, { name: string; bounds: LngLatBounds }>
): Promise<imageData[]> {
  const screenshots: imageData[] = [];

  for (const [id, { name, bounds }] of featureBounds) {
    const extendedBounds: LngLatBounds = extendBounds(
      bounds,
      type === RoadwayType.Segment ? 23 : 47
    );
    map.fitBounds(
      [extendedBounds.getSouthWest(), extendedBounds.getNorthEast()],
      {
        linear: false,
      }
    );
    await waitForIdle(map);
    await highlightFeature(type, id, map);
    const data = await takeScreenshot(map);
    const canvas = map.getCanvas();
    const width = canvas.width / window.devicePixelRatio;
    const height = canvas.height / window.devicePixelRatio;
    screenshots.push({
      width: width,
      height: height,
      dataUrl: data,
      name: name,
      id: id,
      type: type,
    });
  }
  return screenshots;
}

function extendBounds(bounds: LngLatBounds, meters: number): LngLatBounds {
  const extendedBounds = new LngLatBounds([
    bounds.getSouthWest(),
    bounds.getNorthEast(),
  ]);
  if (meters === 0) {
    return extendedBounds;
  }
  extendedBounds.extend(extendedBounds.getSouthWest().toBounds(meters));
  extendedBounds.extend(extendedBounds.getNorthEast().toBounds(meters));
  return extendedBounds;
}

/**
 * Changes the style of the supplied mapboxgl map to highlight the specified feature.
 *
 * For segments highlighting means coloring them #ffff00 and giving them an outline,
 * for intersections highlighting means coloring them #ffff00
 * @param type The RoadwayType of the feature (either Segment or Intersection)
 * @param id The id of the feature
 * @param map A reference to the mapboxgl map the features reside in
 */
async function highlightFeature(
  type: RoadwayType,
  id: number,
  map: MapboxGLMap
) {
  const style = map.getStyle();

  // Set Segment conditional styling
  const segmentLayer: LineLayer = style.layers.find(
    (layer) =>
      layer["source-layer"] === "segments" &&
      layer["id"] === "app:theme-custom-segments"
  ) as LineLayer;

  segmentLayer.paint["line-color"] = [
    "case",
    ["==", type === RoadwayType.Segment ? id : 0, ["number", ["get", "id"]]],
    "#ffff00",
    [
      "to-color",
      [
        "at",
        ["get", "Total Number of Risk Factors Present"],
        ["literal", turbo7],
      ],
    ],
  ];
  segmentLayer.paint["line-opacity"] = [
    "case",
    ["==", type === RoadwayType.Segment ? id : 0, ["number", ["get", "id"]]],
    0,
    type === RoadwayType.Segment ? 1 : 0,
  ];

  // Creating the outline

  const outlineLeft: LineLayer = style.layers.find(
    (layer) =>
      layer["source-layer"] === "segments" &&
      layer["id"] === "app:segmentOutlineLeft"
  ) as LineLayer;

  outlineLeft.filter = [
    "==",
    type === RoadwayType.Segment ? id : 0,
    ["get", "id"],
  ];

  const outlineRight: LineLayer = style.layers.find(
    (layer) =>
      layer["source-layer"] === "segments" &&
      layer["id"] === "app:segmentOutlineRight"
  ) as LineLayer;

  outlineRight.filter = [
    "==",
    type === RoadwayType.Segment ? id : 0,
    ["get", "id"],
  ];

  // Working with intersections

  const intersectionLayer: CircleLayer = style.layers.find(
    (layer) =>
      layer["source-layer"] === "intersections" &&
      layer["id"] === "app:theme-custom-intersections"
  ) as CircleLayer;

  intersectionLayer.paint["circle-color"] = [
    "case",
    [
      "==",
      type === RoadwayType.Intersection ? id : 0,
      ["number", ["get", "id"]],
    ],
    "#ffff00",
    [
      "to-color",
      [
        "at",
        ["get", "Total Number of Risk Factors Present"],
        ["literal", turbo5],
      ],
    ],
  ];

  intersectionLayer.paint["circle-opacity"] = [
    "case",
    [
      "==",
      type === RoadwayType.Intersection ? id : 0,
      ["number", ["get", "id"]],
    ],
    0,
    1,
  ];

  intersectionLayer.paint["circle-stroke-opacity"] = [
    "case",
    [
      "==",
      type === RoadwayType.Intersection ? id : 0,
      ["number", ["get", "id"]],
    ],
    1,
    0,
  ];

  intersectionLayer.paint["circle-radius"] = 8;

  intersectionLayer.paint["circle-stroke-color"] = "#352b26";
  intersectionLayer.paint["circle-stroke-width"] = 5;

  // Filtering crashes

  const crashLayer: CircleLayer = style.layers.find(
    (layer) =>
      layer["source-layer"] === "crashes" &&
      layer["id"] === "app:theme-custom-crashes"
  ) as CircleLayer;

  crashLayer.paint["circle-opacity"] = [
    "case",
    [
      "all",
      ["==", type, ["string", ["get", CRASH_PROP_LABELS.SSET_INFRA]]],
      [
        "==",
        id,
        [
          "number",
          [
            "get",
            type === RoadwayType.Segment
              ? CRASH_PROP_LABELS.SSET_ID
              : CRASH_PROP_LABELS.INTERSECTION_ID,
          ],
        ],
      ],
    ],
    1,
    0,
  ];

  // Setting style

  map.setStyle(style, { diff: true });
  await waitForIdle(map);
}

/**
 * Changes the style of the provided mapboxgl map to color the crashes by crash year.
 * Assumes the crash layer is named 'theme-custom-crashes'.
 * @param map The mapbogxl map instance whose style to change
 */
async function colorCrashesByYear(map: MapboxGLMap): Promise<void> {
  const style = map.getStyle();
  const crashLayer = style.layers.find(
    (layer) =>
      layer["source-layer"] === "crashes" &&
      layer["id"] === "app:theme-custom-crashes"
  ) as CircleLayer;

  crashLayer.paint["circle-color"] = [
    "match",
    ["get", CRASH_PROP_LABELS.CRASHYEAR],
    ...years,
  ];
  crashLayer.paint["circle-stroke-opacity"] = 0;
  crashLayer.paint["circle-radius"] = 10;

  map.setStyle(style, { diff: true });
  await waitForIdle(map);
}

function createLegend(): docx.Table {
  const yearColors: docx.TableRow[] = [];
  for (let index = 0; index < years.length; index += 2) {
    yearColors.push(
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text:
                      index === years.length - 1
                        ? "Other"
                        : String(years[index]),
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            shading: {
              type: docx.ShadingType.SOLID,
              color: String(
                years[index === years.length - 1 ? index : index + 1]
              ),
            },
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "",
                  }),
                ],
              }),
            ],
          }),
        ],
      })
    );
  }

  return new docx.Table({
    columnWidths: [1, 1],
    width: {
      size: 25,
      type: docx.WidthType.PERCENTAGE,
    },
    rows: [
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Legend",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Year",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Color",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      ...yearColors,
    ],
  });
}
