import * as docx from "docx";

export function logoAndTitle(logo: ArrayBuffer): docx.Table {
  return new docx.Table({
    width: {
      type: docx.WidthType.PERCENTAGE,
      size: 100,
    },
    rows: [
      new docx.TableRow({
        children: [
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.TOP,
            margins: {
              top: 0,
              left: 0,
            },
            borders: {
              top: {
                style: docx.BorderStyle.NONE,
                size: 0,
                color: docx.hexColorValue("#ffffff"),
              },
              bottom: {
                style: docx.BorderStyle.NONE,
                size: 0,
                color: docx.hexColorValue("#ffffff"),
              },
              left: {
                style: docx.BorderStyle.NONE,
                size: 0,
                color: docx.hexColorValue("#ffffff"),
              },
              right: {
                style: docx.BorderStyle.NONE,
                size: 0,
                color: docx.hexColorValue("#ffffff"),
              },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.LEFT,
                children: [
                  new docx.ImageRun({
                    data: logo,
                    transformation: {
                      width: 289,
                      height: 79,
                    },
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.TOP,
            margins: {
              top: 0,
              right: 0,
            },
            borders: {
              top: {
                style: docx.BorderStyle.NONE,
                size: 0,
                color: docx.hexColorValue("#ffffff"),
              },
              bottom: {
                style: docx.BorderStyle.NONE,
                size: 0,
                color: docx.hexColorValue("#ffffff"),
              },
              left: {
                style: docx.BorderStyle.NONE,
                size: 0,
                color: docx.hexColorValue("#ffffff"),
              },
              right: {
                style: docx.BorderStyle.NONE,
                size: 0,
                color: docx.hexColorValue("#ffffff"),
              },
            },
            children: [
              new docx.Paragraph({
                heading: docx.HeadingLevel.HEADING_1,
                alignment: docx.AlignmentType.RIGHT,
                children: [
                  new docx.TextRun({
                    color: docx.hexColorValue("#000000"),
                    text: "HSIP Candidate Form",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
    ],
  });
}

export function projectSummary(
  roadName: string,
  intersectingRoadway: null,
  totalLengthMiles: string,
  ruralOrUrban: string,
  lanes: string,
  segAADT: string | number,
  intxAADT: number | "N/A",
  speedLimit: string,
  lighting: any,
  peerGroup: string
): docx.Table {
  return new docx.Table({
    layout: docx.TableLayoutType.FIXED,
    columnWidths: [25, 25, 25, 25],
    width: {
      size: 100,
      type: docx.WidthType.PERCENTAGE,
    },
    rows: [
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 3,
            children: [],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "FY",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "ID: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Contract: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Award Date: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Completion Date: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "District: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: "5",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "County: Champaign County",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "City: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Key Route: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 3,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Marked Route: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Road Name: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: roadName,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Intersecting Roadway: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: intersectingRoadway,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Length: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: `${totalLengthMiles} miles`,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Mile station: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        height: {
          value: 80,
          rule: docx.HeightRule.EXACT,
        },
        children: [
          new docx.TableCell({
            columnSpan: 4,
            shading: {
              color: "#000000",
              fill: "#616161",
            },
            children: [],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 4,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Location Description: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "(Rural/Urban): ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: ruralOrUrban,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Lanes: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: lanes,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "AADT (Segment): ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: `${segAADT}`,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Total Entering AADT (Intersection): ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: `${intxAADT}`,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Speed Limit: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: speedLimit ? `${speedLimit} mph` : "",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Friction Test Results: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Lighting Present: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: String(lighting),
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "CHSP Emphasis Area(s): ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "(District Documentation/Systemic Improvements): ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 4,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Peer Group: ",
                    bold: true,
                  }),
                  new docx.TextRun({
                    text: peerGroup,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 4,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    text: "Other: ",
                    bold: true,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        height: {
          value: 80,
          rule: docx.HeightRule.EXACT,
        },
        children: [
          new docx.TableCell({
            columnSpan: 4,
            shading: {
              color: "#000000",
              fill: "#616161",
            },
            children: [],
          }),
        ],
      }),
    ],
  });
}
