import * as docx from "docx";

export function projectDescription(
  segAADT: string | number,
  totalLengthMiles: string,
  startStudyYear: number,
  endStudyYear: number
): (docx.Paragraph | docx.Table)[] {
  return [
    new docx.Paragraph({
      heading: docx.HeadingLevel.HEADING_2,
      alignment: docx.AlignmentType.CENTER,
      children: [
        new docx.TextRun({
          text: "PROJECT DESCRIPTION - PROJECT DATA INPUT (LOCAL SEGMENTS)",
          color: docx.hexColorValue("#000000"),
          underline: {
            type: docx.UnderlineType.THICK,
          },
        }),
      ],
    }),
    projectDescriptionTable(
      segAADT,
      totalLengthMiles,
      startStudyYear,
      endStudyYear
    ),
  ];
}

function projectDescriptionTable(
  segAADT: string | number,
  totalLengthMiles: string,
  startStudyYear: number,
  endStudyYear: number
): docx.Table {
  return new docx.Table({
    width: {
      type: docx.WidthType.PERCENTAGE,
      size: 100,
    },
    columnWidths: [25, 25, 25, 25],
    rows: [
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 3,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Project: ",
                  }),
                  new docx.TextRun({
                    text: " ",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Prepared by: ",
                  }),
                  new docx.TextRun({
                    text: " ",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "District: ",
                  }),
                  new docx.TextRun({
                    text: "5",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "County: ",
                  }),
                  new docx.TextRun({
                    text: "Champaign",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "City: ",
                  }),
                  new docx.TextRun({
                    text: " ",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Date: ",
                  }),
                  new docx.TextRun({
                    text: " ",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Key Route: ",
                  }),
                  new docx.TextRun({
                    text: " ",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Marked Route: ",
                  }),
                  new docx.TextRun({
                    text: "Champaign",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Mile Post: ",
                  }),
                  new docx.TextRun({
                    text: " ",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Current AADT: ",
                  }),
                  new docx.TextRun({
                    text: String(segAADT),
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 4,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Location: ",
                  }),
                  new docx.TextRun({
                    text: " ",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 3,
            children: [],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Length (miles): ",
                  }),
                  new docx.TextRun({
                    text: `${totalLengthMiles} miles`,
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Crash data: ",
                  }),
                  new docx.TextRun({
                    text: "5 Years",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Begin Station: ",
                  }),
                  new docx.TextRun({
                    text: "",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "From: ",
                  }),
                  new docx.TextRun({
                    text: String(startStudyYear),
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "End Station: ",
                  }),
                  new docx.TextRun({
                    text: "",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "To: ",
                  }),
                  new docx.TextRun({
                    text: String(endStudyYear),
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Traffic Growth Factor: ",
                  }),
                  new docx.TextRun({
                    text: "3.0%",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 3,
            children: [],
          }),
          new docx.TableCell({
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Interest Rate: ",
                  }),
                  new docx.TextRun({
                    text: "4.0%",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                children: [
                  new docx.TextRun({
                    bold: true,
                    text: "Peer Group: ",
                  }),
                  new docx.TextRun({
                    text: "",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            columnSpan: 2,
            children: [],
          }),
        ],
      }),
    ],
  });
}
