import * as docx from "docx";
import { convertInchesToTwip } from "docx";
import { Countermeasure } from "../../BCLIb/models/classes/Countermeasure";
import { Crash } from "../../BCLIb/models/classes/Crash";
import { CrashType, Severity, RoadwayType } from "../../BCLIb/models/enums";
import { ProjectElements } from "../../BCLIb/models/types";
import { labelToCode } from "../../BCLIb/utils";

/**
 * Creates and returns the crash distribution by type and severity portion of the HSIP form.
 * @param {ProjectElements} proj
 * @returns {(docx.Paragraph | docx.Table)[]}
 */
export function crashSeverityDistribution(
  proj: ProjectElements
): (docx.Paragraph | docx.Table)[] {
  const result: (docx.Paragraph | docx.Table)[] = [];

  if (proj.get(RoadwayType.Intersection).length > 0) {
    result.push(
      new docx.Paragraph({
        heading: docx.HeadingLevel.HEADING_2,
        alignment: docx.AlignmentType.CENTER,
        children: [
          new docx.TextRun({
            text: "LOCAL INTERSECTION CRASH SEVERITY DISTRIBUTION BY CRASH TYPE FOR ANALYSIS PERIOD",
            color: docx.hexColorValue("#000000"),
            underline: {
              type: docx.UnderlineType.THICK,
            },
          }),
        ],
      })
    );
    result.push(crashSeverityTable(proj, RoadwayType.Intersection));
  }

  if (proj.get(RoadwayType.Segment).length > 0) {
    result.push(
      new docx.Paragraph({
        heading: docx.HeadingLevel.HEADING_2,
        alignment: docx.AlignmentType.CENTER,
        children: [
          new docx.TextRun({
            text: "LOCAL SEGMENTS CRASH SEVERITY DISTRIBUTION BY CRASH TYPE FOR ANALYSIS PERIOD",
            color: docx.hexColorValue("#000000"),
            underline: {
              type: docx.UnderlineType.THICK,
            },
          }),
        ],
      })
    );
    result.push(crashSeverityTable(proj, RoadwayType.Segment));
  }

  return result;
}

/**
 * Gets the table of crash severity distribution.
 * Is made to look the same as the user would have when entering into the spreadsheet tool / the result of example HSIP forms in the past.
 * @param {ProjectElements} proj The features added to the project
 * @param {RoadwayType} roadwayType Whether we are looking at intersection or segment crashes.
 * @returns {docx.Table}
 */
function crashSeverityTable(
  proj: ProjectElements,
  roadwayType: RoadwayType
): docx.Table {
  const numCols = Countermeasure.defaultVector.size + 4;
  return new docx.Table({
    columnWidths: new Array(numCols).fill(1),
    width: {
      type: docx.WidthType.PERCENTAGE,
      size: 100,
    },
    rows: [
      new docx.TableRow({
        children: [
          new docx.TableCell({
            columnSpan: 2,
            rowSpan: 2,
            verticalAlign: docx.VerticalAlign.CENTER,
            textDirection: docx.TextDirection.BOTTOM_TO_TOP_LEFT_TO_RIGHT,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    bold: true,
                    underline: {
                      color: "#000000",
                      type: docx.UnderlineType.SINGLE,
                    },
                    text: "Crash Type",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            textDirection: docx.TextDirection.BOTTOM_TO_TOP_LEFT_TO_RIGHT,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            columnSpan: 1,
            rowSpan: 2,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    bold: true,
                    text: "All Crashes (Aggregated crash input only)",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            columnSpan: Countermeasure.defaultVector.size - 2,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            rowSpan: 1,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    bold: true,
                    text: "CRASH TYPE",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            columnSpan: 2,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            rowSpan: 1,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    bold: true,
                    text: "SPECIAL CASE",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            textDirection: docx.TextDirection.BOTTOM_TO_TOP_LEFT_TO_RIGHT,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            rowSpan: 2,
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    bold: true,
                    text: "Total",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        height: {
          value: convertInchesToTwip(1),
          rule: docx.HeightRule.EXACT,
        },
        children: [
          ...[...Countermeasure.defaultVector.keys()]
            .filter((ct) => ![CrashType.NGT, CrashType.WP].includes(ct))
            .map((ct) => {
              return new docx.TableCell({
                verticalAlign: docx.VerticalAlign.CENTER,
                textDirection: docx.TextDirection.BOTTOM_TO_TOP_LEFT_TO_RIGHT,
                columnSpan: 1,
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 12,
                        bold: true,
                        text: ct.toString(),
                      }),
                    ],
                  }),
                ],
              });
            }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            textDirection: docx.TextDirection.BOTTOM_TO_TOP_LEFT_TO_RIGHT,
            borders: {
              left: { style: docx.BorderStyle.THICK },
            },
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    bold: true,
                    text: CrashType.NGT.toString(),
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            textDirection: docx.TextDirection.BOTTOM_TO_TOP_LEFT_TO_RIGHT,
            columnSpan: 1,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    bold: true,
                    text: CrashType.WP.toString(),
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      new docx.TableRow({
        children: [
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            shading: {
              color: "#000000",
              type: docx.ShadingType.PERCENT_15,
            },
            columnSpan: 2,
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    underline: {
                      color: "#000000",
                      type: docx.UnderlineType.SINGLE,
                    },
                    text: "Crash Severity",
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    italics: true,
                    text: "ALL",
                  }),
                ],
              }),
            ],
          }),
          ...[...Countermeasure.defaultVector.keys()]
            .filter((ct) => ![CrashType.NGT, CrashType.WP].includes(ct))
            .map((ct) => {
              return new docx.TableCell({
                verticalAlign: docx.VerticalAlign.CENTER,
                borders: {
                  top: { style: docx.BorderStyle.THICK },
                  bottom: { style: docx.BorderStyle.THICK },
                },
                children: [
                  new docx.Paragraph({
                    alignment: docx.AlignmentType.CENTER,
                    children: [
                      new docx.TextRun({
                        size: 12,
                        italics: true,
                        text: labelToCode(ct),
                      }),
                    ],
                  }),
                ],
              });
            }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
              left: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    italics: true,
                    text: labelToCode(CrashType.NGT),
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    italics: true,
                    text: labelToCode(CrashType.WP),
                  }),
                ],
              }),
            ],
          }),
          new docx.TableCell({
            verticalAlign: docx.VerticalAlign.CENTER,
            borders: {
              left: { style: docx.BorderStyle.THICK },
              right: { style: docx.BorderStyle.THICK },
              top: { style: docx.BorderStyle.THICK },
              bottom: { style: docx.BorderStyle.THICK },
            },
            children: [
              new docx.Paragraph({
                alignment: docx.AlignmentType.CENTER,
                children: [
                  new docx.TextRun({
                    size: 12,
                    italics: true,
                    text: "TOT",
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
      ...[
        ["Fatal Crashes", Severity.F],
        ["A Injury Crashes", Severity.A],
        ["B Injury Crashes", Severity.B],
        ["C Injury Crashes", Severity.C],
        ["PDO Crashes", Severity.N],
      ].map(([label, sev]) => {
        return new docx.TableRow({
          children: [
            new docx.TableCell({
              verticalAlign: docx.VerticalAlign.CENTER,
              borders: {
                left: { style: docx.BorderStyle.THICK },
                right: { style: docx.BorderStyle.THICK },
                bottom: {
                  style:
                    label === "PDO Crashes"
                      ? docx.BorderStyle.THICK
                      : docx.BorderStyle.SINGLE,
                },
              },
              columnSpan: 2,
              children: [
                new docx.Paragraph({
                  alignment: docx.AlignmentType.CENTER,
                  children: [
                    new docx.TextRun({
                      size: 12,
                      text: label,
                    }),
                  ],
                }),
              ],
            }),
            new docx.TableCell({
              borders: {
                left: { style: docx.BorderStyle.THICK },
                right: { style: docx.BorderStyle.THICK },
                bottom: {
                  style:
                    label === "PDO Crashes"
                      ? docx.BorderStyle.THICK
                      : docx.BorderStyle.SINGLE,
                },
              },
              children: [],
            }),
            ...[...Countermeasure.defaultVector.keys()]
              .filter((ct) => ![CrashType.NGT, CrashType.WP].includes(ct))
              .map((ct) => {
                return new docx.TableCell({
                  verticalAlign: docx.VerticalAlign.CENTER,
                  borders: {
                    bottom: {
                      style:
                        label === "PDO Crashes"
                          ? docx.BorderStyle.THICK
                          : docx.BorderStyle.SINGLE,
                    },
                  },
                  children: [
                    new docx.Paragraph({
                      alignment: docx.AlignmentType.CENTER,
                      children: [
                        new docx.TextRun({
                          size: 12,
                          text: String(
                            proj
                              .get(roadwayType)
                              .flatMap((re) => re.crashes)
                              .filter((c) => getCrashSeverity(c) === sev)
                              .filter((c) => c.crashType === ct).length || ""
                          ),
                        }),
                      ],
                    }),
                  ],
                });
              }),
            ...[...Countermeasure.defaultVector.keys()]
              .filter((ct) => [CrashType.NGT, CrashType.WP].includes(ct))
              .map((ct) => {
                return new docx.TableCell({
                  verticalAlign: docx.VerticalAlign.CENTER,
                  borders: {
                    bottom: {
                      style:
                        label === "PDO Crashes"
                          ? docx.BorderStyle.THICK
                          : docx.BorderStyle.SINGLE,
                    },
                    left: {
                      style:
                        ct === CrashType.NGT
                          ? docx.BorderStyle.THICK
                          : docx.BorderStyle.SINGLE,
                    },
                  },
                  children: [
                    new docx.Paragraph({
                      alignment: docx.AlignmentType.CENTER,
                      children: [
                        new docx.TextRun({
                          size: 12,
                          text: String(
                            proj
                              .get(roadwayType)
                              .flatMap((re) => re.crashes)
                              .filter((c) => getCrashSeverity(c) === sev)
                              .filter((c) =>
                                ct === CrashType.WP
                                  ? c.surfaceCondition !== "Dry"
                                  : c.lightCondition === "Darkness"
                              ).length || ""
                          ),
                        }),
                      ],
                    }),
                  ],
                });
              }),
            new docx.TableCell({
              verticalAlign: docx.VerticalAlign.CENTER,
              borders: {
                left: { style: docx.BorderStyle.THICK },
                right: { style: docx.BorderStyle.THICK },
                bottom: {
                  style:
                    label === "PDO Crashes"
                      ? docx.BorderStyle.THICK
                      : docx.BorderStyle.SINGLE,
                },
              },
              shading: {
                color: "#000000",
                type: docx.ShadingType.PERCENT_15,
              },
              children: [
                new docx.Paragraph({
                  alignment: docx.AlignmentType.CENTER,
                  children: [
                    new docx.TextRun({
                      size: 12,
                      text: proj
                        .get(roadwayType)
                        .flatMap((re) => re.crashes)
                        .filter((c) => getCrashSeverity(c) === sev)
                        .filter((c) =>
                          Object.values(CrashType).includes(c.crashType)
                        )
                        .length.toString(),
                    }),
                  ],
                }),
              ],
            }),
          ],
        });
      }),
    ],
  });
}

/**
 * Returns the severity of a crash depended on what kinds of injuries it has.
 *
 * For example a cash with a Fatal injury will be classified as a Fatal Injury crash.
 *
 * A crash with both a C Injury and an A Injury will be classified as an A Injury crash.
 *
 * TODO: Crashes have a field for severity called "crashinjuriesseverity". Do we wish to use this rather than looking at the injuryProfile?
 * @param {Crash} crash The crash whose severity to determine.
 * @returns {Severity} The severity of the crash
 */
function getCrashSeverity(crash: Crash): Severity {
  if (crash.injuryProfile.get(Severity.F)) return Severity.F;
  else if (crash.injuryProfile.get(Severity.A)) return Severity.A;
  else if (crash.injuryProfile.get(Severity.B)) return Severity.B;
  else if (crash.injuryProfile.get(Severity.C)) return Severity.C;
  else if (crash.injuryProfile.get(Severity.N)) return Severity.N;
}
