## Document Export

The purpose of this module is to define a document template in an editable
`.docx` format. The code here is quite verbose and repetative out of necessity
since the objective is to simulate the original HSIP application as closely as
possible.


An example HSIP Full Application example can be found [here](https://champaigncountyillinois.sharepoint.com/sites/SystemicSafetyEvaluationTool/Shared%20Documents/General/Grant%20files/HSIP%20CH%2018%20Full%20Application_06_09_20.pdf)


The pages of interest are:
- HSIP Candidate Form (Page 2,3)
- Project Description - Project Data Input (Page 4)
- Raw Crash Data (Page 5)


Documentation for the `.docx` module can be found in these places:
- [docx JS User Guide](https://docx.js.org/#/)
- [docx JS API Reference](https://docx.js.org/api/)