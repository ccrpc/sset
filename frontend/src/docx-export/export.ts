import * as docx from "docx";
import { findMode } from "../BCLIb/utils";
import { convertInchesToTwip, Paragraph } from "docx";
import { getTotalProjectBenefit } from "../BCLIb/BenefitBreakdown";
import { ProjectElements } from "../BCLIb/models/types";
import { PEER_GROUP_LABEL_LOOKUP } from "../BCLIb/models/consts";
import { RoadwayType, Severity } from "../BCLIb/models/enums";
import { logoAndTitle, projectSummary } from "./segments/intro";
import { crashDetailsByYear } from "./segments/crashDetails";
import { projectDescription } from "./segments/projectDescription";
import { crashSeverityDistribution } from "./segments/crashSeverityDistribution";
import { costBenefit } from "./segments/costBenefit";
import { rawCrashData } from "./segments/rawCrashData";
import { locationMap } from "./segments/locationMap";
import { CostCalculator } from "../BCLIb/CostBreakdown";
import { charts } from "./segments/charts";
import { timelineTemplate } from "./segments/timeline";

const formatter = Intl.NumberFormat("en-US", {
  style: "currency",
  currency: "USD",
});

export async function downloadProjectFile(
  proj: ProjectElements,
  endStudyYear: number,
  map: HTMLGlMapElement
) {
  const logo = await (
    await fetch("./build/assets/idot_logo.png")
  ).arrayBuffer();

  // Obtaining
  const [
    roadName,
    intersectingRoadway,
    totalLengthMiles,
    ruralOrUrban,
    lanes,
    segAADT,
    intxAADT,
    speedLimit,
    lighting,
    peerGroup,
    predominantCrashTypes,
    proposedImprovements,
    estimatedProjectCost,
    benefitCostRatio,
    annualFatalCrashRate,
    annualSevereCrashRate,
    localRoadsRuralFunctionalClass,
  ] = await Promise.all([
    // roadName
    (async () => {
      const names = await Promise.all(
        proj.get(RoadwayType.Segment).map(async (re) => re.name)
      );
      return [...new Set(names)].join(", ") ?? "N/A";
    })(),

    // intersectingRoadway
    (async () => {
      return null;
    })(),

    // length
    (async () => {
      const lengths = await Promise.all(
        proj
          .get(RoadwayType.Segment)
          .map(async (re) => re.infoProps.get("Length (ft)") as number)
      );
      const totalLength = lengths.reduce((a, b) => a + b, 0) / 5280; // Dividing by 5280 to convert from feet to miles.
      return totalLength ? `${totalLength.toFixed(2)}` : "N/A";
    })(),

    // ruralOrUrban
    (async () => {
      const settings = [
        ...new Set(
          [...proj.values()].flat().map((re) => re.infoProps.get("Setting"))
        ),
      ].sort();
      return settings.join(" & ");
    })(),

    // lanes
    (async () => {
      const laneCounts = [
        ...new Set(
          proj
            .get(RoadwayType.Segment)
            .map((re) => re.infoProps.get("Total Number of Lanes"))
        ),
      ].sort();
      return laneCounts.join("/");
    })(),

    // segAADT
    (async () => {
      const segAADTs = [
        ...new Set(
          proj.get(RoadwayType.Segment).map((re) => re.infoProps.get("AADT"))
        ),
      ].sort();
      return segAADTs.reduce((a, b) => (a > b ? a : b), 0) || "N/A";
    })(),

    // intxAADT
    (async () => {
      const intxAADTs = [
        ...new Set(
          proj
            .get(RoadwayType.Intersection)
            .map(
              (re) =>
                (re.infoProps.get("AADT on Major Approaches") as number) +
                (re.infoProps.get("AADT on Minor Approaches") as number)
            )
        ),
      ].sort();
      return intxAADTs.reduce((a, b) => (a > b ? a : b), 0) || "N/A";
    })(),

    // speedLimit
    (async () => {
      const speeds = [
        ...new Set(
          proj
            .get(RoadwayType.Segment)
            .map((re) => re.infoProps.get("Speed Limit (mph)") as number)
        ),
      ].sort();
      return speeds.join("/");
    })(),

    // lighting
    (async () => {
      const lights = [...proj.values()]
        .flat()
        .map((re) => re.infoProps.get("Lighting"));

      return findMode(lights);
    })(),

    // peerGroup
    (async () => {
      let peerGroup = "";

      if (!proj.get(RoadwayType.Segment).length) {
        peerGroup =
          PEER_GROUP_LABEL_LOOKUP.local.intersection[
            findMode(
              proj.get(RoadwayType.Intersection).map((re) => re.peerGroup)
            )
          ];
      } else {
        const peerGroups = [
          ...new Set(proj.get(RoadwayType.Segment).map((re) => re.peerGroup)),
        ];

        peerGroup =
          PEER_GROUP_LABEL_LOOKUP.local.segment[
            peerGroups
              .map((pg) => {
                return {
                  peerGroup: pg,
                  length: proj
                    .get(RoadwayType.Segment)
                    .filter((re) => re.peerGroup === pg)
                    .map((re) => re.infoProps.get("Length (ft)") as number)
                    .reduce((a, b) => a + b, 0),
                };
              })
              .reduce((a, b) => (a.length > b.length ? a : b), {
                peerGroup: 0,
                length: 0,
              }).peerGroup
          ];
      }

      return peerGroup;
    })(),

    // predominantCrashTypes
    (async () => {
      const crashes = [...proj.values()].flat().flatMap((re) => re.crashes);

      // Find unique crash types
      const cTypes = [...new Set(crashes.map((c) => c.crashType))];

      // Filter Crash types that compose less than ten percent of all crashes
      return cTypes
        .map((ct) => {
          return {
            crashType: ct,
            percentage:
              crashes.filter((c) => c.crashType === ct).length / crashes.length,
          };
        })
        .filter((cp) => cp.percentage > 0.1)
        .map((cp) => cp.crashType)
        .join(", ");
    })(),

    // proposedImprovements
    (async () => {
      const countermeasures = [...proj.values()]
        .flat()
        .flatMap((re) =>
          re.appliedCountermeasures.map((acm) => acm.countermeasure.name)
        );
      return [...new Set(countermeasures)].join(", ");
    })(),

    // estimatedProjectCost
    (async () => {
      return formatter.format(CostCalculator.calculateTotalProjectCost(proj));
    })(),

    // benefitCostRatio
    (async () => {
      const benefit = getTotalProjectBenefit(proj, endStudyYear);

      const cost = CostCalculator.calculateTotalProjectCost(proj);

      return (benefit / cost).toFixed(2);
    })(),

    // annualFatalCrashRate
    (async () => {
      // Intersection length is
      // https://idot.illinois.gov/Assets/uploads/files/Transportation-System/Manuals-Guides-&-Handbooks/Safety/SAFETY%201.06%20-%20Safety%20Engineering%20Policy%20Memorandum.pdf
      const INTX_LENGTH = 0.1; // miles

      const numFatalCrashes = [...proj.values()]
        .flat()
        .flatMap((re) => re.crashes)
        .filter((c) => c.injuryProfile.get(Severity.F) > 0).length;

      const totalRoadwayLength =
        proj
          .get(RoadwayType.Segment)
          .map((re) => (re.infoProps.get("Length (ft)") as number) / 5280)
          .reduce((a, b) => a + b, 0) +
        proj.get(RoadwayType.Intersection).length * INTX_LENGTH;

      return (numFatalCrashes / 5 / (totalRoadwayLength / 100)).toFixed(2);
    })(),

    // annualSevereCrashRate
    (async () => {
      // Intersection length is
      // https://idot.illinois.gov/Assets/uploads/files/Transportation-System/Manuals-Guides-&-Handbooks/Safety/SAFETY%201.06%20-%20Safety%20Engineering%20Policy%20Memorandum.pdf
      const INTX_LENGTH = 0.1; // miles

      const numSevereCrashes = [...proj.values()]
        .flat()
        .flatMap((re) => re.crashes)
        .filter((c) => c.injuryProfile.get(Severity.A) > 0).length;

      const totalRoadwayLength =
        proj
          .get(RoadwayType.Segment)
          .map((re) => (re.infoProps.get("Length (ft)") as number) / 5280)
          .reduce((a, b) => a + b, 0) +
        proj.get(RoadwayType.Intersection).length * INTX_LENGTH;

      return (numSevereCrashes / 5 / (totalRoadwayLength / 100)).toFixed(2);
    })(),

    // localRoadsRuralFunctionalClass
    (async () => {
      if (!proj.get(RoadwayType.Segment).length) return "";

      const funcClasses = [
        ...new Set(
          proj
            .get(RoadwayType.Segment)
            .map((re) => re.infoProps.get("Functional Classification"))
        ),
      ];

      return funcClasses
        .map((fc) => {
          return {
            funcClass: fc,
            length: proj
              .get(RoadwayType.Segment)
              .filter(
                (re) => re.infoProps.get("Functional Classification") === fc
              )
              .map((re) => re.infoProps.get("Length (ft)") as number)
              .reduce((a, b) => a + b, 0),
          };
        })
        .reduce((a, b) => (a.length > b.length ? a : b), {
          funcClass: "",
          length: 0,
        }).funcClass;
    })(),
  ]);

  // Starting docx contents
  const doc = new docx.Document({
    sections: [
      {
        properties: {
          page: {
            margin: {
              top: convertInchesToTwip(0.5),
              bottom: convertInchesToTwip(0.5),
              left: convertInchesToTwip(0.5),
              right: convertInchesToTwip(0.5),
            },
          },
        },
        children: [
          // Log and Title
          logoAndTitle(logo),
          new docx.Paragraph(""),
          // Project Summary
          projectSummary(
            roadName,
            intersectingRoadway,
            totalLengthMiles,
            ruralOrUrban,
            lanes,
            segAADT,
            intxAADT,
            speedLimit,
            lighting,
            peerGroup
          ),
          // Crash Details
          ...(await crashDetailsByYear(
            proj,
            predominantCrashTypes,
            proposedImprovements,
            estimatedProjectCost,
            benefitCostRatio,
            annualFatalCrashRate,
            annualSevereCrashRate,
            localRoadsRuralFunctionalClass,
            endStudyYear
          )),
        ],
      },
      {
        properties: {
          type: docx.SectionType.NEXT_PAGE,
          page: {
            size: {
              orientation: docx.PageOrientation.LANDSCAPE,
            },
            margin: {
              top: convertInchesToTwip(0.5),
              bottom: convertInchesToTwip(0.5),
              left: convertInchesToTwip(0.5),
              right: convertInchesToTwip(0.5),
            },
          },
        },
        children: [
          // Project Description
          ...projectDescription(
            segAADT,
            totalLengthMiles,
            endStudyYear - 4,
            endStudyYear
          ),
          // Severity by Crash Type Table
          ...crashSeverityDistribution(proj),
          // Countermeasure Cost Benefit
          ...(await costBenefit(proj, endStudyYear)),
        ],
      },
      {
        properties: {
          type: docx.SectionType.NEXT_PAGE,
          page: {
            size: {
              orientation: docx.PageOrientation.LANDSCAPE,
            },
            margin: {
              top: convertInchesToTwip(0.5),
              bottom: convertInchesToTwip(0.5),
              left: convertInchesToTwip(0.5),
              right: convertInchesToTwip(0.5),
            },
          },
        },
        // Raw Crash Table
        children: [...rawCrashData(proj)],
      },
      {
        properties: {
          type: docx.SectionType.NEXT_PAGE,
          page: {
            size: {
              orientation: docx.PageOrientation.LANDSCAPE,
            },
            margin: {
              top: convertInchesToTwip(0.5),
              bottom: convertInchesToTwip(0.5),
              left: convertInchesToTwip(0.5),
              right: convertInchesToTwip(0.5),
            },
          },
        },
        children: [
          // Location Map
          ...(await locationMap(proj, map.map)),
        ],
      },
      {
        properties: {
          type: docx.SectionType.NEXT_PAGE,
          page: {
            size: {
              orientation: docx.PageOrientation.LANDSCAPE,
            },
            margin: {
              top: convertInchesToTwip(0.5),
              bottom: convertInchesToTwip(0.5),
              left: convertInchesToTwip(0.5),
              right: convertInchesToTwip(0.5),
            },
          },
        },
        children: [
          // Graphs
          ...(await charts(proj)),
        ],
      },
      {
        properties: {
          type: docx.SectionType.NEXT_PAGE,
          page: {
            size: {
              orientation: docx.PageOrientation.LANDSCAPE,
            },
            margin: {
              top: convertInchesToTwip(0.5),
              bottom: convertInchesToTwip(0.5),
              left: convertInchesToTwip(0.5),
              right: convertInchesToTwip(0.5),
            },
          },
        },
        children: [
          // Project Timeline Template
          ...timelineTemplate(),
        ],
      },
    ],
  });

  docx.Packer.toBlob(doc).then((blob) => {
    const a = document.createElement("a");

    const currentDate = new Date();

    const month = currentDate.toLocaleString("default", { month: "long" });

    const day = currentDate.getDate();

    const year = currentDate.getFullYear();

    const formattedDate = `${month}-${day}-${year}`;

    a.download = `SSET ${formattedDate}-${Math.floor(Math.random() * 100000)}.docx`;

    a.href = URL.createObjectURL(blob);

    a.addEventListener("click", (e) => {
      setTimeout(() => URL.revokeObjectURL(a.href), 30 * 1000);
    });

    a.click();
  });
}
