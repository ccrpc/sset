## User Guide

The typical user workflow for this application is as follows:

- **Select a roadway feature on the map**
- **Browse the information tabs in the bottom drawer:**
  - Roadway Info: Displays several properties about the selected roadway feature.
  - Risk Factors: Describes which risk factors are present at the selected roadway feature.
  - Injuries: If any, will display injury counts by year and severity
  - Crashes: If any, will display crash counts by type and severity
- **Add selected feature to study group:**
  - By clicking the "ADD FEATURE" button in the top-right corner of the info drawer, the user will add the selected roadway to the study area elements. Aggregate information about this study group will be shown in the bottom-left panel
- **Add Countermeasures:**
  - By clicking the drop-down icon on an element in the study group, the user may add or modify applied countermeasures. Benefit/Cost metrics will be updated in the bottom-left panel when countermeasures are applied or modified.
- **Export Documents:**
  - By clicking on the download button in the top-right corner, the application will export and download pre-filled HSIP Application documents to the user's default download folder.

## Developer Guide

### Kubernetes Local Development

Create the sset namespace and set up the persistent volume claim

`k apply -k infra/setup`

Apply all CRD's - (Assuming project is cloned into the `/opt/code/` direcotry)

`k apply -k /opt/code/sset/infra/dev/`

To delete the CRD's

`k delete -k /opt/code/sset/infra/dev/`

One can also build the images to a local registry and deploy them for development purposes with:

`skaffold dev`

Pressing CTR+C to stop skaffold will also cause it to clean up and delete the objects it created other than the namespace and persistent volume claim.
This is done so that the tile data in the persistent volume claim will persist and not need to be exported every time you restart the dev loop.
To delete the namespace and the persistent volume claim run

`k delete -k infra/setup`

### Exporting the tiles locally

To run the application locally you will want / need to have the mbtiles in the persistent volume for the subwaytile-server instance to serve them.

First add the postgis secret as described in [Secret Documentation](./infra/k8s/export/README.md).

Next run `skaffold dev` in on terminal to create all of the resources the application uses to run in your local kubernetes. Keep it running (do not ctrl+C yet).

Go to another terminal and run `k -n sset create job --from=cronjob/sset-export-map-tiles sset-export-once` to create the mbtiles in the persistent volume.
The output of this command will show up in the terminal running `skaffold dev`.

For cleanup feel free to CTRL + C in the `skaffold dev` terminal. The deletion of the cronjob will also delete the job created from it.

<!-- Note: If kubernetes does not have access to your machine's DNS the export job will fail as it goes to the wrong ip for gis.ccrpc.org. Add your machine's DNS to your kubernetes by following: https://wiki.ccrpc.org/pdc/sysadmin-cookbook#set_up_microk8s_dns -->

### Exporting the tiles on the remote

First use `k2 -n sset get jobs` to check whether a job with the name `sset-export-once` already exists.

If it exists use `k2 -n sset delete job/sset-export-once` to delete it.

Once you have deleted the job, or if it did not exist, run `k2 -n sset create job --from=cronjob/sset-export-map-tiles sset-export-once`.

Use `k2 -n sset get jobs` to make sure the job was created and `k2 -n sset logs job/sset-export-map-tiles sset-export-once -f` to see the logs from the job.

Once the job has reported 'Tile creation process compleated', feel free to delete it with `k2 -n sset delete job/sset-export-once`.

### Frontend

Run the application with

`skaffold dev`

The `dev` profile under skaffold watches your filesystem for changes so should reflect any changes you make in the code to the running website.

You should be able to access the application at `sset.ccrpc.org.localhost`

### Pushing to gitlab

Note that we have jobs in the Gitlab CI/CD (defined in the `.gitlab-ci.yml` file) that push the images automatically for a tagged release.

To push the images you have to the gitlab repository, so that cloud1 may be able to pull them down, run `skaffold build -p publish --default-repo registry.gitlab.com`

### Python Graph API

### Tile Exporter

See [Module Documentation](./tiles/README.md)

#### Note on kubernetes shortcuts:

Any mention of `k` above refers to kubectl running on your local machine.

Any mention of `k1` or `k2` refer to `kremote cloud1` and `kremote cloud2` respectively.

`kremote` should be defined in your `~/.bashrc.d` folder, as made avaialble in the [bashrc.d repository](https://gitlab.com/ccrpc/bashrc.d/).

Note that for kremote to work, you must have been given access to the cloud1 and cloud2 servers.

# Yearly Crash Data Updates

We are given each year updates on the most recent crash data by the planning and community department. To update the application to use this data, please do the following:
- Check that there are no type mismatches between the crash database table for the previous year (usually of the form `street.crash_2022`, `street.crash_2023`, et cetera) in our PostgreSQL database and the table for the updated year
- Pull down the tiles from the correct table in the [/tiles/](/tiles/) folder, usually by adjusting the `FROM` claues in [/tiles/export_crash_tiles.sh](/tiles/export_crash_tiles.sh) file.
- The application looks at a 5 year range, and this range must be updated in a few different places. These can usually also be located by using Ctrl+Shift+F to search for the year you are replacing in all files.
  - STUDY_START_YEAR and STUDY_END_YEAR in [/frontend/.env](/frontend/.env).
  - The range for the [Number of Crashes By Year](/frontend/src/chart/crashes-by-year-line.ts)
  - The coloring used in the docx export [here](/frontend/src/docx-export/segments/locationMap.ts)
- There are a few things the application reads from the crash data itself which may change from year-to-year. To check whether these have changed you can use a SQL on the previous crash data table and the new crash data table to compare the DISTINCT values.

For Example:
```sql
SELECT DISTINCT(roadsurfacecond)
FROM street.crash_2022
```
```sql
SELECT DISTINCT(roadsurfacecond)
FROM street.crash_2023
```
Some of the things that may have changed, and what to do in case they do:
  - weathercondition
    - This is probably the simplest one and is simply referenced in [one of our charts](/frontend/src/chart/Percent-crashes-weather-condition-pie.ts). Update the information there, checking first with the planners whether the IDOT meta data has changed.
  - typeoffirstcrash
    - This would necessitate changes in BCLib and the custom countermeasure selections, as well as a few other integral places.
  - crashinjuriesseverity
    - If this changes large portions of the tool will have to be rethought, as this affects expected amount of money saved due to crash prevention calculations.
    That said if this value has changed IDOT will need to have released completely new countermeasure mitigation factor information and this will send out ripples to several other applications.
  - lightcondition
    - An addition or removal here might change the "countAsNightTime" logic, though as long as night / day can still be differentiated hopefully to other BCLib changes will need to be made.
    The logic on whether a crash can count as night time can be found in the [sset-crash-details-modal](/frontend/src/components/sset-crash-details-modal/).
  - roadsurfacecond
    - Similarly to lightcondition, additions or removals here might change the "countAsWetPavement" logic. If whether a crash counts as a wet pavement crash can still be determined, no BCLib changes would need to take place.
    The logic on whether a crash can count as night time can be found in the [sset-crash-details-modal](/frontend/src/components/sset-crash-details-modal/).