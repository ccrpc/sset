#!/bin/sh

# Data sources: PostGIS and GeoPackage
GPKG="../var/tiles/crashes.gpkg"
MBTILES="../var/tiles/crashes.mbtiles"

# Include helpers
source /tiles/tile_helpers.sh

read -r -d '' crashes_query <<EOM
SELECT
	fid								as "$CRASH_ID",
	geom							as "$CRASH_GEOM",
	icn								as "$CRASH_ICN",
	crashyear						as "$CRASH_CRASHYEAR",
	crashmonth						as "$CRASH_CRASHMONTH",
	crashday						as "$CRASH_CRASHDAY",
	crashhour						as "$CRASH_CRASHHOUR",
	dayofweek						as "$CRASH_DAYOFWEEK",
	typeoffirstcrash				as "$CRASH_TYPEOFFIRSTCRASH",
	totalfatals						as "$CRASH_TOTALFATALS",
	totalinjured					as "$CRASH_TOTALINJURED",
	noinjuries						as "$CRASH_NOINJURIES",
	ainjuries						as "$CRASH_AINJURIES",
	binjuries						as "$CRASH_BINJURIES",
	cinjuries						as "$CRASH_CINJURIES",
	crashinjuriesseverity			as "$CRASH_CRASHINJURIESSEVERITY",
	numberofvehicles				as "$CRASH_NUMBEROFVEHICLES",
	cause1							as "$CRASH_CAUSE1",
	cause2							as "$CRASH_CAUSE2",
	roadwayfunctionalclass			as "$CRASH_ROADWAYFUNCTIONALCLASS",
	nhs								as "$CRASH_NHS",
	roadsurfacecond					as "$CRASH_ROADSURFACECOND",
	roaddefects						as "$CRASH_ROADDEFECTS",
	lightcondition					as "$CRASH_LIGHTCONDITION",
	weathercondition				as "$CRASH_WEATHERCONDITION",
	numberoflanes					as "$CRASH_NUMBEROFLANES",
	roadalignment					as "$CRASH_ROADALIGNMENT",
	trafficwaydescrip				as "$CRASH_TRAFFICWAYDESCRIP",
	trafficcontroldevice			as "$CRASH_TRAFFICCONTROLDEVICE",
	trafficcontroldevicecondition	as "$CRASH_TRAFFICCONTROLDEVICECONDITION",
	vehicle_1_type					as "$CRASH_VEHICLE_1_TYPE",
	vehicle_2_type					as "$CRASH_VEHICLE_2_TYPE",
	heavy							as "$CRASH_HEAVY",
	workzonerelated					as "$CRASH_WORKZONERELATED",
	citytownshipflag				as "$CRASH_CITYTOWNSHIPFLAG",
	cityname						as "$CRASH_CITYNAME",
	hitandrun						as "$CRASH_HITANDRUN",
	railroadcrossingnumber			as "$CRASH_RAILROADCROSSINGNUMBER",
	tscrashlat						as "$CRASH_TSCRASHLAT",
	tscrashlong						as "$CRASH_TSCRASHLONG",
	mpa								as "$CRASH_MPA",
	sset_id							as "$CRASH_SSET_ID",
	intersection_id					as "$CRASH_INTERSECTION_ID",
	sset_infra						as "$CRASH_SSET_INFRA"
FROM street.crash_2023 AS sc
WHERE crashyear >= 2019
AND crashyear <= 2023
AND sset_infra IS NOT NULL
EOM

remove_file "$GPKG"
remove_file "$MBTILES"
add_layer "crashes" "6" "16" "$crashes_query"

# Convert GeoPackage to MBTiles
export_tiles \
	"Champaign County Crashes" \
	"County Crash Data for the SSET B/C Tool" \
	"6" "16"
