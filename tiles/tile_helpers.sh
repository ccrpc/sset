#!/bin/sh

SRC=PG:"dbname='$POSTGRES_DB' host='$POSTGRES_HOST' port='5432' user='$POSTGRES_USER' password='$POSTGRES_PASSWORD'"

# Delete a file if it exists
# Arguments: file_path
function remove_file() {
  if [ -f "$1" ] ; then
    rm "$1"
  fi
}

# Add layer to MBTiles config
# Arguments: gpkg_layer_name, tile_layer_name, min_zoom, max_zoom
layer_config=''
function add_layer_config() {
  if [ "$layer_config" != "" ]; then layer_config="$layer_config, "; fi
  layer_config="$layer_config\"$1\": {\"target_name\": \"$2\", \"minzoom\": $3, \"maxzoom\": $4}"
}

# Add layer to GeoPackage and MBTiles config
# Arguments: tile_layer_name, min_zoom, max_zoom, sql
function add_layer() {
  if [ -f "$GPKG" ]; then
    ogr2ogr -f GPKG "$GPKG" "$SRC" \
      -update \
      -mapFieldType Date=String \
      -sql "$4" \
      -nln "$1_$2_$3"
  else
    ogr2ogr -f GPKG "$GPKG" "$SRC" \
      -mapFieldType Date=String \
      -sql "$4" \
      -nln "$1_$2_$3"
  fi 

  if [[ $(ogrinfo -ro -so -noextent -nomd "$GPKG" "$1_$2_$3" | sed -n 's/^Feature Count: //p') -eq 0 ]]; then
    ogrinfo -q -sql "drop table $1_$2_$3" "$GPKG"
  else
    add_layer_config "$1_$2_$3" "$1" "$2" "$3"
  fi
}

# Export GeoPackage layers as MBTiles
# Arguments: name, description, min_zoom, max_zoom
function export_tiles() {
  ogr2ogr -f MBTILES "$MBTILES" "$GPKG" \
    -progress \
    -dsco NAME="$1" \
    -dsco DESCRIPTION="$2" \
    -dsco MINZOOM="$3" \
    -dsco MAXZOOM="$4" \
    -dsco CONF="{$layer_config}"
}
