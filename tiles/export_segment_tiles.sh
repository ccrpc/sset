#!/bin/sh

# Data sources: PostGIS and GeoPackage
GPKG=/var/tiles/segments.gpkg
MBTILES=/var/tiles/segments.mbtiles

# Include helpers
source /tiles/tile_helpers.sh

# TODO: Add Where clause
read -r -d '' segments_query <<EOM
SELECT
	sset_id							as "$SEG_SSET_ID",
	geom							as "$SEG_GEOM",
	name							as "$SEG_NAME",
	setting_type					as "$SEG_SETTING",
	peer_group						as "$SEG_PEER_GROUP",
	county_highway					as "$SEG_COUNTY_HIGHWAY",
	total_lanes						as "$SEG_TOTAL_LANES",
	lane_width_ft					as "$SEG_LANE_WIDTH_FT",
	speed_limit						as "$SEG_SPEED_LIMIT",
	median_type						as "$SEG_MEDIAN_TYPE",
	paved_shoulder_width_ft			as "$SEG_PAVED_SHOULDER_WIDTH_FT",
	unpaved_shoulder_width_ft		as "$SEG_UNPAVED_SHOULDER_WIDTH_FT",
	outside_shoulder_1_type			as "$SEG_OUTSIDE_SHOULDER_1_TYPE",
	outside_shoulder_1_width_ft		as "$SEG_OUTSIDE_SHOULDER_1_WIDTH_FT",
	outside_shoulder_2_type			as "$SEG_OUTSIDE_SHOULDER_2_TYPE",
	outside_shoulder_2_width_ft		as "$SEG_OUTSIDE_SHOULDER_2_WIDTH_FT",
	shoulder_rumble_strips			as "$SEG_SHOULDER_RUMBLE_STRIPS",
	curvature						as "$SEG_CURVATURE",
	vertical_alignment_variation	as "$SEG_VERTICAL_ALIGNMENT_VARIATION",
	access_point_density			as "$SEG_ACCESS_POINT_DENSITY",
	overtaking_demand				as "$SEG_OVERTAKING_DEMAND",
	aadt							as "$SEG_AADT",
	aadt_year						as "$SEG_AADT_YEAR",
	surface_type					as "$SEG_SURFACE_TYPE",
	surface_width_ft				as "$SEG_SURFACE_WIDTH_FT",
	two_way_left_turn_lane			as "$SEG_TWO_WAY_LEFT_TURN_LANE",
	segment_lighting				as "$SEG_SEGMENT_LIGHTING",
	auto_speed_enforcement			as "$SEG_AUTO_SPEED_ENFORCEMENT",
	bridge_deck_width_ft			as "$SEG_BRIDGE_DECK_WIDTH_FT",
	bridge_roadway_width_ft			as "$SEG_BRIDGE_ROADWAY_WIDTH_FT",
	bridge_length_ft				as "$SEG_BRIDGE_LENGTH_FT",
	cross_rail						as "$SEG_CROSS_RAIL",
	functional_classification		as "$SEG_FUNCTIONAL_CLASSIFICATION",
	length_ft						as "$SEG_LENGTH_FT",
	segment_id						as "$SEG_SEGMENT_ID",
	risk_functional_classification	as "$SEG_RISK_FUNCTIONAL_CLASSIFICATION",
	risk_aadt						as "$SEG_RISK_AADT",
	risk_shoulder_width				as "$SEG_RISK_SHOULDER_WIDTH",
	risk_access_point_density		as "$SEG_RISK_ACCESS_POINT_DENSITY",
	risk_overtaking_demand			as "$SEG_RISK_OVERTAKING_DEMAND",
	risk_speed_limit				as "$SEG_RISK_SPEED_LIMIT",
	risk_sum						as "$SEG_RISK_SUM",
	animal_crash_counts				as "$SEG_ANIMAL_CRASH_COUNTS",
	total_crash_counts				as "$SEG_TOTAL_CRASH_COUNTS",
	fatal_or_severe_counts			as "$SEG_FATAL_OR_SEVERE_COUNTS"
FROM street.sset_road_segments AS srs
EOM

remove_file "$GPKG"
remove_file "$MBTILES"
add_layer "segments" "6" "16" "$segments_query"

# Convert GeoPackage to MBTiles
export_tiles \
    "Champaign County Road Segments" \
    "County Road Segments Data for the SSET B/C Tool" \
    "4" "14"
