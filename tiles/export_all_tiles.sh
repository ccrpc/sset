#!/bin/sh

set -o allexport
source /tiles/labels.env
set +o allexport

echo "MBTILE Creation process started"

echo "Creating Crash Tiles"
/tiles/export_crash_tiles.sh
echo "Creating Intersection Tiles"
/tiles/export_intersection_tiles.sh
echo "Creating Segment Tiles"
/tiles/export_segment_tiles.sh

echo "Tile creation process compleated"