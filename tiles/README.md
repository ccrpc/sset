## Tile Export Scripts

These scripts query the PostGIS database to retrieve an up-to-date list of
features from the relevant tables and exporting them as vector tiles that can
be served in our application. The tables queried are as follows:
- street.crashes
- street.sset_intersections
- street.sset_segments


For convenience, there is are two scripts which will query the database and put the resulting files in the `$PWD/data` folder.
These scripts are called `run_docker.sh` and `run_podman.sh` and run via docker or podman respectively.
Whether the script uses docker or podman is the only difference, they have the exact same functionality.

To run it locally, you **must** have an environment file named `.env` in the `/tiles/` directory with the following database credentials:
- POSTGRES_DB=          # Name of database
- POSTGRES_HOST=        # Name of host
- POSTGRES_USER=        # Name of database user
- POSTGRES_PASSWORD=    # Password for database user

Under no circumstances should this information be checked into our GitLab repository.
Always ensure that any `.env` files are listed in your `.gitignore`

Example usage: `./run_docker.sh`

This will export mbtiles and their associated geopackages to the `$PWD/data` folder.