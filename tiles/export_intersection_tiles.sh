#!/bin/sh

# Data sources: PostGIS and GeoPackage
GPKG=/var/tiles/intersections.gpkg
MBTILES=/var/tiles/intersections.mbtiles

# Include helpers
source /tiles/tile_helpers.sh

# TODO: Add Where clause
read -r -d '' intersections_query <<EOM
SELECT
	intersection_id					as "$INTX_INTERSECTION_ID",
	name							as "$INTX_NAME",
	setting_type					as "$INTX_SETTING",
	peer_group						as "$INTX_PEER_GROUP",
	geom 							as "$INTX_GEOM",
	aadt_major 						as "$INTX_AADT_MAJOR",
	aadt_minor 						as "$INTX_AADT_MINOR",
	skew_angle 						as "$INTX_SKEW_ANGLE",
	skew_angle_category 			as "$INTX_SKEW_ANGLE_CATEGORY",
	left_turn_lane_approach_count 	as "$INTX_LEFT_TURN_LANE_APPROACH_COUNT",
	lighting 						as "$INTX_LIGHTING",
	intersection_type 				as "$INTX_INTERSECTION_TYPE",
	risk_intersection_type 			as "$INTX_RISK_INTERSECTION_TYPE",
	risk_skew_angle_category 		as "$INTX_RISK_SKEW_ANGLE_CATEGORY",
	risk_aadt_major 				as "$INTX_RISK_AADT_MAJOR",
	risk_aadt_minor 				as "$INTX_RISK_AADT_MINOR",
	risk_sum 						as "$INTX_RISK_SUM",
	animal_crash_counts 			as "$INTX_ANIMAL_CRASH_COUNTS",
	total_crash_counts 				as "$INTX_TOTAL_CRASH_COUNTS"
FROM street.sset_intersections AS si
EOM

remove_file "$GPKG"
remove_file "$MBTILES"
add_layer "intersections" "6" "16" "$intersections_query"

# Convert GeoPackage to MBTiles
export_tiles \
    "Champaign County Intersections" \
    "County Intersection Data for the SSET B/C Tool" \
    "4" "14"
