#!/bin/bash

podman build -t sset.tiles:export .
podman run -v $PWD/data:/var/tiles:Z --env-file .env --name export_tiles sset.tiles:export
podman container rm export_tiles
podman image rm sset.tiles:export
