#!/bin/bash

docker build -t sset.tiles:export .
docker run -v $PWD/data:/var/tiles --env-file .env --name export_tiles sset.tiles:export
docker container rm export_tiles
docker image rm sset.tiles:export
