# Import libraries
import os
import io
import numpy as np
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import seaborn as sns
import sqlalchemy

end_year = 2019
# Create a folder to save crash analysis figures and tables
# directory ='crash_analysis_results'
# try:
#     os.makedirs('./'+directory, exist_ok = False)
#     print("Analysis figures and tables would be saved in '%s' folder" % directory)
# except OSError as error:
#     print("Warning: '%s' folder already exists" % directory)
#     print("Please create a new folder to avoid conflicts")

# Filter crashes in target years
# crashes_in_targetyrs = ssetcrashes_SQL[
#     (ssetcrashes_SQL["crashyear"] >= start_year)
#     & (ssetcrashes_SQL["crashyear"] <= end_year)
# ]

# Filter crashes at target locations
# crash locations are determined by 'sset_infra'
# For crashes at intersections, they may also have sset_id(s) (segment id) since the intersections are on the segments.
# However, sset_infra determines their infrastructure types
# Therefore, a crash that has a sset_id, an intersection_id, and sset_infra=Intersection is considered as an intersection crash

# Prep.for plotting
time_sorter_dict = {
    "crashyear": np.arange(end_year - 4, end_year + 1),
    "crashmonth": np.arange(1, 13),
    "dayofweek": [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    ],
    "crashhour": np.arange(0, 25),
}
time_label_dict = {
    "crashyear": "Year",
    "crashmonth": "Month",
    "dayofweek": "Day of week",
    "crashhour": "Hour",
}
time_xticklabel_dict = {
    "crashyear": np.arange(end_year - 4, end_year + 1),
    "crashmonth": [
        "Jan.",
        "Feb.",
        "Mar.",
        "Apr.",
        "May",
        "Jun.",
        "Jul.",
        "Aug.",
        "Sep.",
        "Oct.",
        "Nov.",
        "Dec.",
    ],
    "dayofweek": [
        "Monday",
        "Tuesday",
        "Wednesday",
        "Thursday",
        "Friday",
        "Saturday",
        "Sunday",
    ],
    "crashhour": np.arange(0, 25),
}
severity_color_dict = {
    "Fatal Crash": "black",
    "A Injury Crash": "red",
    "B Injury Crash": "orange",
    "C Injury Crash": "green",
    "No Injuries": "#4f94d4",
}
crashtype_color_dict = {
    "Rear End": "grey",
    "Turning": "cornflowerblue",
    "Angle": "purple",
    "Front to Front": "red",
    "Pedestrian": "pink",
    "Sideswipe Same Direction": "yellowgreen",
    "Sideswipe Opposite Direction": "brown",
    "Parked Motor Vehicle": "mediumpurple",
    "Pedalcyclist": "darkgoldenrod",
    "Fixed Object": "orange",
    "Other Non-Collision": "olivedrab",
    "Other Object": "olivedrab",
    "Overturned": "crimson",
}


# Wrapper that exports a resulting figure as bytes
def export_as_bytes(func):
    def inner(localcrashes):
        fig = func(localcrashes)
        bytes_image = io.BytesIO()
        fig.savefig(bytes_image, format="png")
        bytes_image.seek(0)
        return bytes_image

    return inner


def counts_by_index(col, sorter, loccrashes):
    """
    This function count crashes by index
    """
    col_counts = pd.DataFrame(index=sorter)
    for i in sorter:  # padding for 0
        if i in pd.unique(loccrashes[col].value_counts().index):
            col_counts.loc[i, col] = loccrashes[col].value_counts()[i]
        else:
            col_counts.loc[i, col] = 0
    return col_counts


# Plotting functions


@export_as_bytes
def plot_counts_by_time_statistics(loccrashes):
    """
    This function plots the crash counts by 4 time dimensions (cols): ['crashyear', 'crashmonth', 'dayofweek', 'crashhour']
    """
    fig, axs = plt.subplots(2, 2, figsize=(16, 10))
    for axes, col in zip(
        [(0, 0), (0, 1), (1, 0), (1, 1)],
        ["crashyear", "crashmonth", "dayofweek", "crashhour"],
    ):
        sorter = time_sorter_dict[col]
        col_counts = counts_by_index(col, sorter, loccrashes)
        axs[axes].bar(col_counts.index, col_counts[col].values)
        axs[axes].set_ylabel("Number of Crashes", labelpad=10)
        axs[axes].yaxis.get_major_locator().set_params(integer=True)
        axs[axes].set_xticks(sorter)
        axs[axes].set_xticklabels(time_xticklabel_dict[col])
        axs[axes].set_title("Crash Counts by " + time_label_dict[col], y=1.02)

    return fig


@export_as_bytes
def counts_by_type_severity(loccrashes):
    """
    This function visualizes the crash counts by collision type and severity level
    """
    severitylevel = [
        "Fatal Crash",
        "A Injury Crash",
        "B Injury Crash",
        "C Injury Crash",
        "No Injuries",
    ]
    severity_count_type = pd.crosstab(loccrashes["crashinjuriesseverity"], loccrashes["typeoffirstcrash"])
    for i in severitylevel:
        if i not in severity_count_type.index:
            severity_count_type.loc[i, :] = 0
    ## sort by severity level
    severity_count_type = pd.DataFrame.transpose(severity_count_type.reindex(severitylevel))
    ## sort by crash counts
    severity_count_type = severity_count_type.reindex(
        severity_count_type.sum(axis=1).sort_values(ascending=False).index
    )
    ## Plot
    severity_count_type.plot.bar(
        stacked=True,
        color=list({key: severity_color_dict[key] for key in severitylevel}.values()),
    )
    plt.xlabel(" ")
    plt.ylabel("Number of Crashes", labelpad=20)
    plt.xticks(rotation=90)
    plt.legend(title="Severity Level")
    plt.title(
        "Crash Counts by Crash Type and Severity Level \n" + " (" + str(end_year - 4) + "-" + str(end_year) + ")",
        y=1.05,
    )
    severity_count_type["Sum"] = severity_count_type.sum(axis=1)
    severity_count_type["Percent"] = round(severity_count_type["Sum"] / sum(severity_count_type["Sum"]), 2)
    severity_count_type.loc["Total", :] = severity_count_type.sum(axis=0)

    return plt


@export_as_bytes
def counts_by_severity_type(loccrashes):
    """
    This function visualizes the crash counts by severity level and collision type, and save the results in a csv file
    """
    severitylevel = [
        "Fatal Crash",
        "A Injury Crash",
        "B Injury Crash",
        "C Injury Crash",
        "No Injuries",
    ]
    severity_count_type = pd.crosstab(loccrashes["crashinjuriesseverity"], loccrashes["typeoffirstcrash"])
    ## Padding 0 counts
    if len(list(set(severitylevel) - set(severity_count_type.index))) > 0:
        for s in list(set(severitylevel) - set(severity_count_type.index)):
            severity_count_type.loc[s, :] = 0
    ## Sort columns by crash counts
    severity_count_type = severity_count_type[list(severity_count_type.sum(axis=0).sort_values(ascending=False).index)]
    ## Sort indices by severity level
    severity_count_type = severity_count_type.reindex(severitylevel).astype(int)
    severity_count_type = severity_count_type.rename(
        index={
            "A Injury Crash": "A Injury",
            "B Injury Crash": "B Injury",
            "C Injury Crash": "C Injury",
        }
    )
    ## Plot
    severity_count_type.plot.bar(stacked=True)
    plt.ylabel("Number of Crashes", labelpad=20)
    plt.title(
        "Crash Counts by Crash Type and Severity Level \n" + " (" + str(end_year - 4) + "-" + str(end_year) + ")",
        y=1.05,
    )
    plt.xticks(rotation=0)
    plt.legend(bbox_to_anchor=(1.05, 1))

    severity_count_type["Sum"] = severity_count_type.sum(axis=1)
    severity_count_type["Percent"] = round(severity_count_type["Sum"] / sum(severity_count_type["Sum"]), 2)
    severity_count_type.loc["Total", :] = severity_count_type.sum(axis=0)

    return plt


@export_as_bytes
def pieplot_of_single_attrib(loccrashes, col):
    """
    This function plots pie chart of a single attribute
    col:'roadsurfacecond','lightcondition','weathercondition'
    """
    fig, axs = plt.subplots(figsize=(8, 5))
    axs.pie(
        loccrashes[col].value_counts(),
        labels=loccrashes[col].value_counts().index,
        autopct="%1.1f%%",
        startangle=90,
    )
    # loccrashes[col].value_counts().to_csv(
    #     "./" + directory + "/Percentage_" + col + ".csv"
    # )

    bytes_image = io.BytesIO()
    plt.savefig(bytes_image, format="png")
    bytes_image.seek(0)
    return bytes_image

    # print('Percentage_'+col+'.csv saved')
    # plt.savefig('./'+directory+'/Pie plot of '+col+'.png', bbox_inches='tight', dpi=1000)
    # print('Pie plot of '+col+'.png saved')


if __name__ == "__main__":
    pass
    # # Plot crash counts by time
    # plot_counts_by_time_statistics(loccrashes)

    # # Plot crash type by severity
    # counts_by_type_severity(loccrashes)

    # # Plot pie chart of road surface conditions
    # col = "roadsurfacecond"
    # pieplot_of_single_attrib(loccrashes, col)

    # # Plot pie chart of light conditions
    # col = "lightcondition"
    # pieplot_of_single_attrib(loccrashes, col)

    # # Plot pie chart of weather conditions
    # col = "weathercondition"
    # pieplot_of_single_attrib(loccrashes, col)
