from ast import arg

from numpy import bytes_
from flask import Flask, request, send_file
from graphs import counts_by_type_severity, plot_counts_by_time_statistics 
import geopandas as gpd
import sqlalchemy
import os

app = Flask(__name__)

end_year = 2019

conn_string = f'postgresql://{os.getenv("POSTGRES_USER")}:{os.getenv("POSTGRES_PASSWORD")}@{os.getenv("POSTGRES_HOST")}:5432/{os.getenv("POSTGRES_DB")}'
engine = sqlalchemy.create_engine(conn_string)
conn = engine.connect()

def get_filtered_crashes(intx_ids, seg_ids):
    # Read from the database
    ssetcrashes_SQL = f"""
        SELECT * FROM street.crash
        WHERE crashyear BETWEEN {end_year-4} AND {end_year}
    """
    ssetcrashes_SQL = gpd.GeoDataFrame.from_postgis(ssetcrashes_SQL, conn)
    segcrashes = ssetcrashes_SQL[ssetcrashes_SQL["sset_id"].isin(seg_ids)]
    intxcrashes = ssetcrashes_SQL[ssetcrashes_SQL["intersection_id"].isin(intx_ids)]
    return segcrashes.append(intxcrashes)

def parse_ids(args):
    segment_ids = list(map(int, args.get('segmentIds').split(','))) if args.get('segmentIds') else []
    intx_ids = list(map(int, args.get('intxIds').split(','))) if args.get('intxIds') else [] 
    return get_filtered_crashes(intx_ids, segment_ids)

@app.route('/')
def hello():
    return 'Hello World!'

@app.route('/time_statistics', endpoint='func1')
def time_statistics():
    localcrashes = parse_ids(request.args)
    bytes_obj = plot_counts_by_time_statistics(localcrashes)

    return send_file(bytes_obj,
                     attachment_filename='plot.png',
                     mimetype='image/png')


@app.route('/counts_by_type_severity', endpoint='func2')
def time_statistics():
    localcrashes = parse_ids(request.args)
    bytes_obj = counts_by_type_severity(localcrashes)

    return send_file(bytes_obj,
                     attachment_filename='plot.png',
                     mimetype='image/png')

                